package com.urbamart.ecommerce;

import android.app.Application;

import java.io.File;

import ren.yale.android.cachewebviewlib.ResourceInterceptor;
import ren.yale.android.cachewebviewlib.WebViewCacheInterceptor;
import ren.yale.android.cachewebviewlib.WebViewCacheInterceptorInst;
import ren.yale.android.cachewebviewlib.config.CacheExtensionConfig;

public class App extends Application {

    private static final String CACHE_NAME = "cache_path";
    private static final String CACHE_PATH_NAME = "Test1";
    private static final String DYNAMIC_CACHE_PATH_NAME = "Test2";
    @Override
    public void onCreate() {
        super.onCreate();
//        WebViewCacheInterceptor.Builder builder =  new WebViewCacheInterceptor.Builder(this);
//
//        builder.setCachePath(new File(this.getCacheDir(),CACHE_PATH_NAME))
//            .setDynamicCachePath(new File(this.getCacheDir(),DYNAMIC_CACHE_PATH_NAME))
//            .setCacheSize(1024*1024*100)
//            .setConnectTimeoutSecond(20)
//            .setReadTimeoutSecond(20);
//
//
//        CacheExtensionConfig extension = new CacheExtensionConfig();
//        extension.addExtension("json").removeExtension("html")
//        .removeExtension("htm").removeExtension("swf").removeExtension("png")
//        .removeExtension("jpg").removeExtension("jpeg").removeExtension("gif")
//        .removeExtension("bmp");
//
//        builder.setCacheExtensionConfig(extension);
//        builder.setDebug(true);
//
//        builder.setResourceInterceptor(new ResourceInterceptor() {
//            @Override
//            public boolean interceptor(String url) {
//                System.out.println("ResourceInterceptor :-" + url);
//                return true;
//            }
//        });
//
//        WebViewCacheInterceptorInst.getInstance().init(builder);
    }
}
