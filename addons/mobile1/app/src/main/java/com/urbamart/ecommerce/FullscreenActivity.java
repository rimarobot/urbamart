package com.urbamart.ecommerce;

import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.MailTo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.PermissionRequest;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.File;
import java.io.FileNotFoundException;

import ren.yale.android.cachewebviewlib.WebViewCacheInterceptorInst;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class FullscreenActivity extends AppCompatActivity {

    private ProgressBar pgsBar;
    private WebView webView;
    private ImageView logo;
    private Long mOnGoingDownload;
    private DownloadManager mDownloadManger;
    private  String mimetype;
    private ValueCallback<Uri> mUploadMessage;
    public ValueCallback<Uri[]> uploadMessage;
    private IntentIntegrator qrScan;

    public static final int REQUEST_SELECT_FILE = 100;
    private final static int FILECHOOSER_RESULTCODE = 1;
    private final static int ORDER_SCANNER_RESULTCODE = 49374;

    private BroadcastReceiver mDownloadCompleteListener  = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(getApplicationContext(), "Download Finished",  Toast.LENGTH_LONG).show();
            try {
                clearDownloadingState();
                long downloadId = intent.getLongExtra("extra_download_id", 0L);
                DownloadManager downloadManger = mDownloadManger;
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(downloadId);
                Cursor cursor = downloadManger.query(query);
                if (cursor.moveToFirst()) {
                    int downloadStatus = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
                    String downloadLocalUri = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                    String downloadMimeType = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_MEDIA_TYPE));
                    if ((downloadStatus == DownloadManager.STATUS_SUCCESSFUL) && downloadLocalUri != null) {
                        Uri attachmentUri = Uri.parse(downloadLocalUri);
                        if (ContentResolver.SCHEME_FILE.equals(attachmentUri.getScheme())) {
                            // FileUri - Convert it to contentUri.
                            File file = new File(attachmentUri.getPath());
                            attachmentUri = FileProvider.getUriForFile(FullscreenActivity.this, "com.urbamart.urbamart.com", file);;
                        }

                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setDataAndType(attachmentUri, downloadMimeType);
                        i.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        startActivity(i);
                    }
                }
                cursor.close();
            }catch (Exception ex){

            }

        }
    };

    private void enableEvent(){
        webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });
    }
    private void hideProgressBar(){
        if(logo.getVisibility() == View.VISIBLE) {
            logo.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
        } else{
            enableEvent();
        }
        pgsBar.setVisibility(View.GONE);
        webView.setAlpha(1.0f);
    }

    private void showProgressBar(){
        if(logo.getVisibility() == View.VISIBLE){
            webView.setAlpha(0.5f);
            pgsBar.setVisibility(View.VISIBLE);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        WebViewCacheInterceptorInst.getInstance().enableForce(true);
        setContentView(R.layout.activity_fullscreen);
        qrScan = new IntentIntegrator(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        logo = (ImageView) findViewById(R.id.logo);
        pgsBar = (ProgressBar) findViewById(R.id.pBar);
        pgsBar.setVisibility(View.GONE);
        webView  = (WebView) findViewById(R.id.webview);
        //webView.clearHistory();
        mDownloadManger = (DownloadManager)getSystemService(DOWNLOAD_SERVICE);
        webView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String _mimetype, long contentLength) {

                if(checkDownloadPermission()) {
                    Toast.makeText(getApplicationContext(), "Downloading File",  Toast.LENGTH_LONG).show();
                    hideProgressBar();
                    enableEvent();
                    mimetype = _mimetype;
                    Uri fileUri = Uri.parse(url);
                    String fileName = URLUtil.guessFileName(url,contentDisposition,mimetype);
                    String cookies = CookieManager.getInstance().getCookie(url);
                    try {
                        DownloadManager.Request request = new DownloadManager.Request(fileUri);
                        request.allowScanningByMediaScanner();
                        request.setMimeType(mimetype);
                        request.addRequestHeader("cookie", cookies);
                        request.addRequestHeader("User-Agent", userAgent);
                        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                        mDownloadManger = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                        mDownloadManger.enqueue(request);
                        registerReceiver(mDownloadCompleteListener, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE) );

                    } catch (Exception ex) {

                    }

                }
            }
        });
//        webView.setInitialScale(1);
//        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
//        webView.setScrollbarFadingEnabled(false);
        WebSettings viewSettings = webView.getSettings();
        viewSettings.setJavaScriptEnabled(true);
        viewSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        viewSettings.setAppCacheEnabled(true);
        //viewSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        viewSettings.setDomStorageEnabled(true);
        viewSettings.setLoadWithOverviewMode(true);
        viewSettings.setUseWideViewPort(true);
        viewSettings.setBuiltInZoomControls(false);
        viewSettings.setDisplayZoomControls(false);
        viewSettings.setSupportZoom(false);
        viewSettings.setLoadsImagesAutomatically(true);
        viewSettings.setDefaultTextEncodingName("utf-8");
        viewSettings.setBlockNetworkImage(false);
        viewSettings.setBlockNetworkLoads(false);
        viewSettings.setSupportMultipleWindows(true);
        viewSettings.setUserAgentString(Configs.UserAgent);
        viewSettings.setAllowFileAccess(true);
        viewSettings.setAllowContentAccess(true);
        webView.setWebViewClient(new MyWebViewClient());
        webView.setWebChromeClient(new MyWebChromeClient());
        webView.addJavascriptInterface(new JavaScriptInterface(FullscreenActivity.this), "Android");
//        webView.loadUrl(Configs.Url);
//        webView.loadUrl("http://10.0.2.2:90/urbamartm");
        webView.loadUrl("http://localhost:9090/urbamartm");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webView.setWebContentsDebuggingEnabled(true);
        }
        // accept cookies
        CookieManager.getInstance().setAcceptCookie(true);

        // PWA settings
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            viewSettings.setDatabasePath(this.getApplicationContext().getFilesDir().getAbsolutePath());
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            viewSettings.setAppCacheMaxSize(Long.MAX_VALUE);
        }
        viewSettings.setAppCachePath(this.getApplicationContext().getCacheDir().getAbsolutePath());
        viewSettings.setDatabaseEnabled(true);

        // enable mixed content mode conditionally
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            viewSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            viewSettings.setAllowFileAccessFromFileURLs(true);
            viewSettings.setAllowUniversalAccessFromFileURLs(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {

            if (requestCode == REQUEST_SELECT_FILE)
            {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                {
                    if (uploadMessage == null)
                        return;
                    uploadMessage.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, intent));
                    uploadMessage = null;

                }
            }
            else if (requestCode == FILECHOOSER_RESULTCODE)
            {
                if (null == mUploadMessage)
                    return;
                Uri result1 = intent == null || resultCode != RESULT_OK ? null : intent.getData();
                mUploadMessage.onReceiveValue(result1);
                mUploadMessage = null;
            }
            else if(requestCode == ORDER_SCANNER_RESULTCODE){
                IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
                if (result != null) {
                    if (result.getContents() != null) {
                        Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                        webView.loadUrl("javascript:BarCodeScannerCallBack('"+ result.getContents() +"')");
                    }
                }
            }
        super.onActivityResult(requestCode, resultCode, intent);
    }

    public class JavaScriptInterface {
        Context mContext;

        JavaScriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void BarCodeScanner(){
            qrScan.setOrientationLocked(false);
            qrScan.setPrompt("Scan Invoice");
            qrScan.initiateScan();
        }
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    private boolean checkDownloadPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(this, "Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
            return false;
        }
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            unregisterReceiver(mDownloadCompleteListener);
        }catch (Exception ex){

        }

    }

    private class MyWebViewClient extends WebViewClient {

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            final Uri uri = Uri.parse(url);
//            WebViewCacheInterceptorInst.getInstance().loadUrl(view,url);
            return handleUri(uri, view);
        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            final Uri uri = request.getUrl();
//            WebViewCacheInterceptorInst.getInstance().loadUrl(view,request.getUrl().toString());
            return handleUri(uri, view);
        }

        private boolean handleUri(final Uri uri, WebView view) {

            System.out.println("shouldOverrideUrlLoading :-" + uri.toString());
            final String host = uri.getHost();
//            if (Configs.Url.contains(host)) {
            if (logo.getVisibility() == View.GONE) {
                pgsBar.setVisibility(View.VISIBLE);
                webView.setAlpha(0.5f);
                view.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return true;
                    }
                });

            }
            Intent intent;
            String url = uri.toString();
            if (url.startsWith("tel:")) {
                hideProgressBar();
                enableEvent();
                intent = new Intent("android.intent.action.DIAL");
                intent.setData(Uri.parse(url));
                try{
                    startActivity(intent);
                }catch (ActivityNotFoundException ex){
                }

                return true;

            } else if (url.startsWith("sms:")) {
                hideProgressBar();
                enableEvent();
                intent = new Intent("android.intent.action.SENDTO");
                intent.setData(Uri.parse(url));

                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException ex) {
                }

                return true;

            }else if (url.startsWith("mailto:")) {
                hideProgressBar();
                enableEvent();
                intent = new Intent("android.intent.action.SEND");
                intent.setType("message/rfc822");
                MailTo var28 = MailTo.parse(url);
                MailTo mailTo = var28;
                intent.putExtra("android.intent.extra.EMAIL", new String[]{mailTo.getTo()});
                intent.putExtra("android.intent.extra.CC", mailTo.getCc());
                intent.putExtra("android.intent.extra.SUBJECT", mailTo.getSubject());

                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException var12) {
                }

                return true;

            }

            return false;
//            } else {
//                final Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                startActivity(intent);
//                return true;
//            }

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            System.out.println("onPageStarted :-" + url);
            showProgressBar();
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
//            System.out.println("onPageFinished :-" + url);
            hideProgressBar();
            super.onPageFinished(view, url);
        }

//        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//        @Nullable
//        @Override
//        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
//            return  WebViewCacheInterceptorInst.getInstance().interceptRequest( request);
//        }
//
//        @Nullable
//        @Override
//        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
//            return  WebViewCacheInterceptorInst.getInstance().interceptRequest(url);
//        }


        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
//            System.out.println("onReceivedError :-" + error.getDescription());

            if ((error.getErrorCode() == WebViewClient.ERROR_HOST_LOOKUP
                || error.getErrorCode() == WebViewClient.ERROR_TIMEOUT
            ) && (error.getErrorCode() != WebViewClient.ERROR_CONNECT)) {
                view.loadData("", "", null);
                showError();
            }
            super.onReceivedError(view, request, error);
        }

        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//            System.out.println("onReceivedError :-" + description);
            if ((errorCode == WebViewClient.ERROR_HOST_LOOKUP || errorCode == WebViewClient.ERROR_TIMEOUT
            ) && (errorCode != WebViewClient.ERROR_CONNECT)) {
                view.loadData("", "", null);
                showError();
            }
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        @Override
        public void onLoadResource(WebView view, String url) {
//            System.out.println("onLoadResource :-" + url);
            super.onLoadResource(view, url);
        }
    }

    private class MyWebChromeClient extends WebChromeClient{
        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
            super.onShowCustomView(view, callback);
        }


        // For 3.0+ Devices (Start)
        // onActivityResult attached before constructor
        protected void openFileChooser(ValueCallback uploadMsg, String acceptType)
        {
            mUploadMessage = uploadMsg;
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("image/*");
            startActivityForResult(Intent.createChooser(i, "File Browser"), FILECHOOSER_RESULTCODE);
        }


        // For Lollipop 5.0+ Devices
        public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams)
        {
            if (uploadMessage != null) {
                uploadMessage.onReceiveValue(null);
                uploadMessage = null;
            }

            uploadMessage = filePathCallback;

            Intent intent = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                intent = fileChooserParams.createIntent();
            }
            try
            {
                startActivityForResult(intent, REQUEST_SELECT_FILE);
            } catch (ActivityNotFoundException e)
            {
                uploadMessage = null;
                Toast.makeText(getApplicationContext(), "Cannot Open File Chooser", Toast.LENGTH_LONG).show();
                return false;
            }
            return true;
        }



        //For Android 4.1 only
        protected void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture)
        {
            mUploadMessage = uploadMsg;
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, "File Browser"), FILECHOOSER_RESULTCODE);
        }

        protected void openFileChooser(ValueCallback<Uri> uploadMsg)
        {
            mUploadMessage = uploadMsg;
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("image/*");
            startActivityForResult(Intent.createChooser(i, "File Chooser"), FILECHOOSER_RESULTCODE);
        }

    }
    private void showError(){
        AlertDialog.Builder builder = new AlertDialog.Builder(FullscreenActivity.this);
        builder.setCancelable(false);
        builder.setTitle(Html.fromHtml("<font color='#F79F1F'><b>Urbamart</b></font>"));
        builder.setMessage(Html.fromHtml("<font color='#120049'>Your data services are not working.Please check your data services.</font>"));
        builder.setPositiveButton(Html.fromHtml("<font color='#F79F1F'><b>OK</b></font>"), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getApplicationContext(), FullscreenActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void hideError(){

    }


    protected final void cancelDownload() {
        if (this.mOnGoingDownload != null) {
            DownloadManager var10000 = this.mDownloadManger;
            long[] var10001 = new long[1];
            Long var10004 = this.mOnGoingDownload;
            var10001[0] = var10004;
            var10000.remove(var10001);
            this.clearDownloadingState();
        }

    }

    protected final void clearDownloadingState() {
        this.unregisterReceiver(this.mDownloadCompleteListener);
        this.mOnGoingDownload = (Long)null;
    }


}
