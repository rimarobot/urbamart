// use a cacheName for cache versioning
var cacheName = 'v1:urbamart';

// during the install phase you usually want to cache static assets
self.addEventListener('install', function(e) {
    // once the SW is installed, go ahead and fetch the resources to make this work offline
    e.waitUntil(
        caches.open(cacheName).then(function(cache) {
            return cache.addAll([
                '/urbamartm/public/assets/css/vendors.css',
                '/urbamartm/public/assets/css/aiz-core.css',
                '/urbamartm/public/assets/css/custom-style.css',
                '/urbamartm/public/assets/js/vendors.js',
                '/urbamartm/public/assets/js/aiz-core.js',
                '/urbamartm/public/assets/img/placeholder.jpg',
                '/urbamartm/public/assets/img/placeholder-rect.jpg'
            ]).then(function() {
                self.skipWaiting();
            });
        })
    );
});

// when the browser fetches a url
self.addEventListener('fetch', function(event) {
    // either respond with the cached object or go ahead and fetch the actual url
    event.respondWith(
        caches.match(event.request).then(function(response) {
            if (response) {
                // retrieve from cache
                return response;
            }
            // fetch as normal
            return fetch(event.request);
        })
    );
});