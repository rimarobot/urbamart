if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('./public/assets/js/sw.js', { scope: '/urbamartm/public/assets/js/' }).then(function(reg) {
        console.log('Successfully registered service worker', reg);
    }).catch(function(err) {
        console.warn('Error whilst registering service worker', err);
    });
}

window.addEventListener('online', function(e) {
    // re-sync data with server
    console.log("You are online");
    AIZ.plugins.notify('warning', 'You are online');
}, false);

window.addEventListener('offline', function(e) {
    // queue up events for server
    console.log("You are offline");
    AIZ.plugins.notify('warning', 'You are offline');
}, false);