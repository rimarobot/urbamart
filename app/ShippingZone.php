<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingZone extends Model
{
    protected $table = 'shipping_zones';

    public function locations(){
        return $this->hasMany('App\ShippingZoneLocation','zone_id','id' );
    }


    public function methods(){
        return $this->hasMany('App\ShippingZoneMethod','zone_id','id' );
    }
}
