<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingZoneLocation extends Model
{
    protected $table = 'shipping_zone_locations';

    public function zone()
    {
        return $this->belongsTo('App\ShippingZone','zone_id','id' );
    }
}
