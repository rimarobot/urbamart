<?php

namespace App\Http\Controllers;

use App\DriverPayment;
use Illuminate\Http\Request;

class DriverPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DriverPayment  $driverPayment
     * @return \Illuminate\Http\Response
     */
    public function show(DriverPayment $driverPayment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DriverPayment  $driverPayment
     * @return \Illuminate\Http\Response
     */
    public function edit(DriverPayment $driverPayment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DriverPayment  $driverPayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DriverPayment $driverPayment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DriverPayment  $driverPayment
     * @return \Illuminate\Http\Response
     */
    public function destroy(DriverPayment $driverPayment)
    {
        //
    }
}
