<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShippingCompany;

class CompanyController extends Controller
{
    public function index(Request $request)
    {  
        $sort_search =null;
        $companies = ShippingCompany::orderBy('created_at', 'desc'); 
        if ($request->has('search')){
            $sort_search = $request->search;
            $companies = $companies->where('company_name', 'like', '%'.$sort_search.'%');
        }
        $companies = $companies->paginate(10); 
        return view('shipping.companies.index', compact('companies'));
    }

    public function create()
    {
         return view('shipping.companies.create');
    }

    public function store(Request $request)
    {
        $company = new ShippingCompany;
        $company->country_id = $request->country;
        $company->company_name = $request->name;
        $company->company_url = $request->url;
        $company->company_address = $request->address;
        $company->company_phone = $request->phone;
        $company->company_email = $request->email;
        if($company->save()){
            flash(__('Shipping Company has been inserted successfully'))->success();
            return redirect()->route('companies.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    public function edit($id)
    {
        $company = ShippingCompany::findOrFail(decrypt($id));
        return view('shipping.companies.edit', compact('company'));
    }


    public function update(Request $request, $id)
    {
        $company = ShippingCompany::findOrFail($id);
        $company->country_id = $request->country;
        $company->company_name = $request->name;
        $company->company_url = $request->url;
        $company->company_address = $request->address;
        $company->company_phone = $request->phone;
        $company->company_email = $request->email;
        if($company->save()){
            flash(__('Shipping Company has been updated successfully'))->success();

            return redirect()->route('companies.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    public function destroy($id)
    {
        if(ShippingCompany::destroy($id)){
            flash(__('Shipping Company has been deleted successfully'))->success();
            return redirect()->route('companies.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    public function update_status(Request $request)
    {
        $company = ShippingCompany::findOrFail($request->id);
        $company->status = $request->status;
        if($company->save()){
            return 1;
        }
        return 0;
    }
}