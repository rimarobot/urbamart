<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShippingZoneMethod;
use App\ShippingZone;
use Illuminate\Support\Facades\Log; 
use Carbon\Carbon;
use Cookie;


class ShippingController extends Controller
{
    public function create(Request $request)
    { 
        $zone_id = $request->id; 
         return view('shipping.methods.create', compact('zone_id'));
    }  
    
    public function shipping_methods(Request $request){
        $location   =  $request->location;
        $type       = $request->type;
        $zone = decrypt(Cookie::get('zone')); 
        $shipping = array();     
        if((explode(':', $zone))[0] == 0)               
            $methods = \App\ShippingZoneMethod::where('zone_id', (explode(':', $zone))[1] )->orderBy('order', 'DESC')->get(); 
        else {
            $methods = \App\ShippingZoneMethod::where('zone_id', \App\ShippingZoneMethod::find((explode(':', $zone))[1] )->zone_id)->orderBy('order', 'DESC')->get(); 
        }
        $city = (explode(':', $zone))[2];
        foreach ($methods as $key => $method) 
        {
                $method_data = json_decode($method->data, true);
                if($method_data['method_id'] =='flat-rate')                
                    $formula = $method_data[$type];
                else  
                    $formula = $method_data['cost'];
                
                $formula = str_replace('[qty]', 1, $formula);                
                $shipping_cost =  eval("return $formula;");

                
                if($shipping_cost != 0)
                    $cost = format_price(convert_price($shipping_cost));
                else  
                    $cost = 'Free';                 

                    array_push($shipping, array( 
                                                'id' => $method->id,
                                                'delivery'=> $method_data['delivery_time_min'].' - '. $method_data['delivery_time_max'].' Days',
                                                'cost' => $cost,
                                                'carrier' => $method->name
                    ));                
                
        }
            
        
        return view('frontend.shipping_method', compact('location','type','shipping', 'city'));
    }

    public function shipping_methods_mannual(Request $request){
        
        $shop       =  $request->shop;
        $type       = $request->type;
        $location   = $request->location;
        $city       = $request->city;
        $shipping = array();  

        $zone = \App\ShippingZoneLocation::where('location_code', 'like', '%'. $city .'%' )->where('type', 'state')->first(); 
        if(!empty($zone)){
            $methods = \App\ShippingZoneMethod::where('zone_id', $zone->zone_id )->where('method_id','=','flat-rate')->orderBy('order', 'DESC')->get(); 
            foreach ($methods as $key => $method) 
            {
                    $method_data = json_decode($method->data, true);                     
                    if($method_data['method_id'] =='flat-rate')
                    $formula = $method_data[$type]; 
                    else  
                        $formula = $method_data['cost']; 
                        
                    $formula = str_replace('[qty]', 1, $formula);                
                    $shipping_cost =  eval("return $formula;");

                    
                    if($shipping_cost != 0)
                        $cost = format_price(convert_price($shipping_cost));
                    else  
                        $cost = 'Free'; 
                    

                        array_push($shipping, array( 
                                                    'id' => $method->id,
                                                    'delivery'=> $method_data['delivery_time_min'].' - '. $method_data['delivery_time_max'].' Days',
                                                    'cost' => $cost,
                                                    'carrier' => $method->name
                        ));                
                    
            }
        }  
        
            
        return response()->json( json_encode($shipping));
    }
    
    public function show($id)
    {
        //
    }

    public function store(Request $request)
    {
        $data = json_decode($request->getContent(),true);
        unset($data["_token"]); 
        $method = new ShippingZoneMethod;
        $method->zone_id = $data["zone_id"];
        $method->method_id = $data["method_id"];
        $method->company_id = $data["company_id"];
        $method->name = $data["name"];
        $method->description = $data["description"];  
        $method->status = 1;
        $method->order = $data["order"];
        $method->data =  json_encode($data);
 

        if($method->save()){  
            flash(__('Shipping Method has been added successfully'))->success();
            return response()->json([ 
                            'status' => true, 
                            'referer' => $request->headers->get('referer')  
                        ]);
        }
        else{
            flash(__('Something went wrong'))->error();
            return response()->json([ 
                            'status' => false, 
                            'referer' => $request->headers->get('referer')  
                        ]);
        }
    }

    public function edit(Request $request)
    { 
        $data = ShippingZoneMethod::findOrFail($request->id);
        return view('shipping.methods.edit', compact('data'));
    }


    public function update(Request $request, $id)
    {
        $data = json_decode($request->getContent(),true);
        unset($data["_token"]); 

        $method = ShippingZoneMethod::findOrFail($id);   
        $method->method_id = $data["method_id"];
        $method->company_id = $data["company_id"];
        $method->name = $data["name"];
        $method->description = $data["description"];  
        $method->status = 1;
        $method->order = $data["order"];
        $method->data =  json_encode($data);
        if($method->save()){
            flash(__('Shipping Method has been added successfully'))->success();
            return response()->json([ 
                            'status' => true, 
                            'referer' => $request->headers->get('referer')  
                        ]);
        }
        else{
             flash(__('Something went wrong'))->error();
            return response()->json([ 
                            'status' => false, 
                            'referer' => $request->headers->get('referer')  
                        ]);
        }
    }

    public function destroy(Request $request, $id)
    {
        if(ShippingZoneMethod::destroy($id)){
            flash(__('Shipping Method has been deleted successfully'))->success();  
        }
        else{
             flash(__('Something went wrong'))->error();              
        }
        return back();
    }
}