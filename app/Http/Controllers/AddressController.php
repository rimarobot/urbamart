<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'address' => 'required|string',
            'country' => 'required|string',
            'city' => 'required|string',
            'phone' => array(
                        'required',
                        'regex:/(^(\+256){1}[7]{1}[7|8]{1}[0-9]{6}[0-9])|(^(\+256){1}[7]{1}[0|3|5]{1}[0-9]{6}[0-9])|(^(\+254)[7]{1}(([0129]{1}[0-9]{1})|([4]{1}[0123568])|([5]{1}[789])|([6]{1}[89]))[0-9]{6})/u'
                    )
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $this->validator($request->all())->validate();
            $address = new Address;
            if($request->has('customer_id')){
                $address->user_id = $request->customer_id;
            }
            else{
                $address->user_id = Auth::user()->id;
            }
            $address->address = $request->address;
            $address->country = $request->country;
            $address->city = $request->city;
            // $address->postal_code = $request->postal_code;
            $address->phone = $request->phone;
            $address->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['address_data'] = Address::findOrFail($id);
        return view('frontend.user.address.edit_address_modal', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $address = Address::findOrFail($id);

        $address->address = $request->address;
        $address->country = $request->country;
        $address->city = $request->city;
        $address->postal_code = $request->postal_code;
        $address->phone = $request->phone;
        $address->save();

        flash(translate('Address info updated successfully'))->warning();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $address = Address::findOrFail($id);
        if(!$address->set_default){
            $address->delete();
            return back();
        }
        flash(translate('Default address can not be deleted'))->warning();
        return back();
    }

    public function set_default($id){
        foreach (Auth::user()->addresses as $key => $address) {
            $address->set_default = 0;
            $address->save();
        }
        $address = Address::findOrFail($id);
        $address->set_default = 1;
        $address->save();

        return back();
    }
}
