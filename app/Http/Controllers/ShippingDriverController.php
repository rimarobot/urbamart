<?php

namespace App\Http\Controllers;

use App\ShippingDriver;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;

class ShippingDriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::user()->user_type == 'admin')
            $drivers = ShippingDriver::paginate(15);
        else
            $drivers = ShippingDriver::where('company_id',  Auth::user()->staff->company_id)->paginate(15);
        return view('shipping.drivers.index', compact('drivers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('shipping.drivers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(User::where('email', $request->email)->first() != null){
            flash(translate('Email already used'))->error();
            return back();
        }

        if(isset($request->phone))
          if(User::where('phone', $request->phone)->first() != null){
                flash(translate('Phone already used'))->error();
                return back();
            }
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->mobile;
        $user->user_type = "driver";
        $user->password = Hash::make($request->password);
        if($user->save()){
            $driver = new ShippingDriver;
            $driver->user_id = $user->id;
            $driver->company_id = Auth::user()->staff->company_id;
            $driver->identity_document_number =  $request->id_number;
            $driver->identity_type =  $request->id_type;
            $driver->vehicle_identification_number =  $request->vin;
            $driver->vehicle_type =  $request->vehicle_type;

            if ($request->cod_status != null) {
                $driver->cash_on_delivery_status = 1;
            }
            else {
                $driver->cash_on_delivery_status = 0;
            }

            $driver->mobile_payment_number =  $request->mobile_payment_number;
            $driver->mobile_payment_name =  $request->mobile_payment_name;

            if ($request->mobile_payment_status != null) {
                $driver->mobile_payment_status = 1;
            }
            else {
                $driver->mobile_payment_status = 0;
            }

            if ($request->bank_payment_status != null) {
                $driver->bank_payment_status = 1;
            }
            else {
                $driver->bank_payment_status = 0;
            }
            $driver->bank_acc_number =  $request->bank_acc_number;
            $driver->bank_acc_name =  $request->bank_acc_name;
            $driver->bank_name =  $request->bank_name;
            if($driver->save()){
                flash(translate('Driver has been inserted successfully'))->success();
                return redirect()->route('drivers.index');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ShippingDrivers  $shippingDrivers
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ShippingDrivers  $shippingDrivers
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $driver = ShippingDriver::findOrFail(decrypt($id));
        return view('shipping.drivers.edit', compact('driver'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $driver = ShippingDriver::findOrFail($id);
        $user = $driver->user;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->mobile;
        if(strlen($request->password) > 0){
            $user->password = Hash::make($request->password);
        }
        if($user->save()){
            $driver->identity_document_number =  $request->id_number;
            $driver->identity_type =  $request->id_type;
            $driver->vehicle_identification_number =  $request->vin;
            $driver->vehicle_type =  $request->vehicle_type;

            if ($request->cod_status != null) {
                $driver->cash_on_delivery_status = 1;
            }
            else {
                $driver->cash_on_delivery_status = 0;
            }
            $driver->mobile_payment_number =  $request->mobile_payment_number;
            $driver->mobile_payment_name =  $request->mobile_payment_name;
            if ($request->mobile_payment_status != null) {
                $driver->mobile_payment_status = 1;
            }
            else {
                $driver->mobile_payment_status = 0;
            }

            if ($request->bank_payment_status != null) {
                $driver->bank_payment_status = 1;
            }
            else {
                $driver->bank_payment_status = 0;
            }
            $driver->bank_acc_number =  $request->bank_acc_number;
            $driver->bank_acc_name =  $request->bank_acc_name;
            $driver->bank_name =  $request->bank_name;
            if($driver->save()){
                flash(translate('Driver has been updated successfully'))->success();
                return redirect()->route('drivers.index');
            }
        }

        flash(translate('Something went wrong'))->error();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ShippingDrivers  $shippingDrivers
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy(ShippingDrivers::findOrFail($id)->user->id);
        if(ShippingDrivers::destroy($id)){
            flash(translate('Driver has been deleted successfully'))->success();
            return redirect()->route('drivers.index');
        }

        flash(translate('Something went wrong'))->error();
        return back();
    }
}
