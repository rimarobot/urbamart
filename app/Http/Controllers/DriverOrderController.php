<?php

namespace App\Http\Controllers;

use App\DriverOrder;
use App\ShippingDriver;
use Illuminate\Http\Request;
use App\Http\Controllers\OTPVerificationController;
use App\Http\Controllers\ClubPointController;
use App\Http\Controllers\AffiliateController;
use App\Order;
use App\Product;
use App\ProductStock;
use App\Color;
use App\OrderDetail;
use App\CouponUsage;
use App\OtpConfiguration;
use App\User;
use App\BusinessSetting;
use Auth;
use DB;
use Illuminate\Support\Facades\Log;
use App\CommissionHistory;

class DriverOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DriverOrder  $driverOrder
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DriverOrder  $driverOrder
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DriverOrder  $driverOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DriverOrder  $driverOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // All Orders
    public function all_deliveries(Request $request)
    {

         $date = $request->date;
         $sort_search = null;
         $orders = DriverOrder::orderBy('order_code', 'desc');
         if (Auth::user()->user_type != 'admin'){
            $orders->where('company_id', Auth::user()->staff->company_id);
         }

         if ($request->has('search')){
             $sort_search = $request->search;
             $orders = $orders->where('order_code', 'like', '%'.$sort_search.'%');
         }
         if ($date != null) {
             $orders = $orders->where('created_at', '>=', date('Y-m-d', strtotime(explode(" to ", $date)[0])))->where('created_at', '<=', date('Y-m-d', strtotime(explode(" to ", $date)[1])));
         }
         $orders = $orders->paginate(15);
         return view('backend.deliveries.index', compact('orders', 'sort_search', 'date'));
    }

    public function all_deliveries_show($id)
    {
         $order = DriverOrder::findOrFail(decrypt($id));
         return view('backend.deliveries.show', compact('order'));
    }

    public function pending_deliveries()
    {
        $delivery_status =  array('Pending', 'Delivering');

        if (Auth::user()->user_type != 'admin'){
            $company_id = Auth::user()->staff->company_id;
            $orders = DB::select("SELECT orders.id, orders.shipping_address, orders.payment_type, orders.payment_status, CONCAT(orders.code, '#', order_details.seller_id) code, orders.created_at, SUM(order_details.price) price, SUM(order_details.tax) tax, SUM(order_details.shipping_cost) shipping_cost, order_details.shipping_type, COUNT(order_details.seller_id) qty, (SELECT CONCAT(shops.name, ', ', shops.location_code, ', ', shops.state_code, ', ', shops.address,', ', shops.phone) FROM shops WHERE shops.user_id = order_details.seller_id) shop FROM orders INNER JOIN order_details ON orders.id = order_details.order_id WHERE order_details.delivery_status = 'on_delivery' AND order_details.shipping_type IN (SELECT shipping_zone_methods.name FROM shipping_zone_methods WHERE shipping_zone_methods.status = 1 AND shipping_zone_methods.company_id = :company_id) AND CONCAT(orders.code, '#',  order_details.seller_id) NOT IN (SELECT shipping_driver_orders.order_code FROM shipping_driver_orders) GROUP BY orders.id , order_details.seller_id , orders.shipping_address , orders.payment_type , orders.payment_status , orders.code , orders.created_at , order_details.shipping_type", ['company_id' => $company_id]);
            $drivers =  DB::select("SELECT shipping_company_drivers.id driver_id,  users.name, shipping_company_drivers.vehicle_type FROM shipping_company_drivers inner join users on  users.id = shipping_company_drivers.user_id where shipping_company_drivers.company_id=:company_id  and shipping_company_drivers.id not in (SELECT distinct driver_id FROM shipping_driver_orders  where shipping_driver_orders.status in (:status))",['company_id' =>$company_id, 'status'=> 'on_delivery' ]);
        }
        else{
            $orders = DB::select("SELECT orders.id, orders.shipping_address, orders.payment_type, orders.payment_status, CONCAT(orders.code, '#', order_details.seller_id) code, orders.created_at, SUM(order_details.price) price, SUM(order_details.tax) tax, SUM(order_details.shipping_cost) shipping_cost, order_details.shipping_type, COUNT(order_details.seller_id) qty, (SELECT CONCAT(shops.name, ', ', shops.location_code, ', ', shops.state_code, ', ', shops.address,', ', shops.phone) FROM shops WHERE shops.user_id = order_details.seller_id) shop FROM orders INNER JOIN order_details ON orders.id = order_details.order_id WHERE order_details.delivery_status = 'on_delivery' AND order_details.shipping_type IN (SELECT shipping_zone_methods.name FROM shipping_zone_methods WHERE shipping_zone_methods.status = 1 ) AND CONCAT(orders.code, '#',  order_details.seller_id) NOT IN (SELECT shipping_driver_orders.order_code FROM shipping_driver_orders) GROUP BY orders.id , order_details.seller_id , orders.shipping_address , orders.payment_type , orders.payment_status , orders.code , orders.created_at , order_details.shipping_type");
            $drivers =  DB::select("SELECT shipping_company_drivers.id driver_id,  users.name, shipping_company_drivers.vehicle_type FROM shipping_company_drivers inner join users on  users.id = shipping_company_drivers.user_id where shipping_company_drivers.id not in (SELECT distinct driver_id FROM shipping_driver_orders  where shipping_driver_orders.status in (:status))",['status'=> 'on_delivery' ]);
        }
        return view('backend.deliveries.pending', compact('orders', 'drivers'));
    }

    public function asign_deliveries(Request $request){
        $company_id = Auth::user()->staff->company_id;
        if(isset($request->selected_orders) && isset($request->driver_id)){
            $driver_id = $request->driver_id;
            $selected_orders = $request->selected_orders;
            DB::select("INSERT INTO shipping_driver_orders (order_id,driver_id,company_id, shop,qty, order_price,shipping_cost,shipping_address,payment_type,payment_status,order_code,shipping_type,status, created_at) SELECT orders.id, :driver_id, :company_id1, (SELECT shops.name FROM shops WHERE shops.user_id = order_details.seller_id), COUNT(order_details.seller_id), SUM(order_details.price) price, SUM(order_details.shipping_cost) shipping_cost, orders.shipping_address, orders.payment_type, orders.payment_status, CONCAT(orders.code,  '#', order_details.seller_id), order_details.shipping_type, 'on_delivery', CURRENT_TIMESTAMP() FROM orders RIGHT OUTER JOIN order_details ON orders.id = order_details.order_id WHERE order_details.delivery_status = 'on_delivery' AND order_details.shipping_type IN (SELECT shipping_zone_methods.name FROM shipping_zone_methods WHERE shipping_zone_methods.company_id = :company_id) AND CONCAT(orders.code,  '#', order_details.seller_id) IN ( " . $selected_orders . ") GROUP BY orders.id, order_details.seller_id , orders.shipping_address , orders.payment_type , orders.payment_status , orders.code , orders.created_at , order_details.shipping_type",['driver_id'=>$driver_id ,'company_id'=>$company_id ,'company_id1'=>$company_id]);
            flash(translate('Request successful'))->success();
            return redirect()->route('pending_deliveries');
        }
        else
        {
            flash(translate('Request failed'));
            return back();
        }

    }

    public function driver_pending_deliveries(Request $request){

        $date = $request->date;
        $sort_search = null;
        Log::debug($request);
        $orders = DriverOrder::where('driver_id', Auth::user()->driver->id)->Where('status', 'on_delivery')->orderBy('created_at', 'desc');

        if ($request->has('search')){
            $sort_search = $request->search;
            $orders = $orders->where('order_code', 'like', '%'.$sort_search.'%');
        }
        if ($date != null) {
            $orders = $orders->where('orders.created_at', '>=', date('Y-m-d', strtotime(explode(" to ", $date)[0])))->where('orders.created_at', '<=', date('Y-m-d', strtotime(explode(" to ", $date)[1])));
        }
        $orders = $orders->paginate(15);
        return view('frontend.user.driver.pending_orders', compact('orders', 'sort_search', 'date'));
    }

    public function driver_picked_deliveries(Request $request){

        $date = $request->date;
        $sort_search = null;
        $orders = DriverOrder::where('driver_id', Auth::user()->driver->id)->Where('status', 'picked')->orderBy('created_at', 'desc');

        if ($request->has('search')){
            $sort_search = $request->search;
            $orders = $orders->where('order_code', 'like', '%'.$sort_search.'%');
        }
        if ($date != null) {
            $orders = $orders->where('orders.created_at', '>=', date('Y-m-d', strtotime(explode(" to ", $date)[0])))->where('orders.created_at', '<=', date('Y-m-d', strtotime(explode(" to ", $date)[1])));
        }
        $orders = $orders->paginate(15);
        return view('frontend.user.driver.picked_orders', compact('orders', 'sort_search', 'date'));
    }

    public function driver_delivered_deliveries(Request $request){

        $date = $request->date;
        $sort_search = null;
        $orders = DriverOrder::where('driver_id', Auth::user()->driver->id)->Where('status', 'delivered')->orderBy('created_at', 'desc');

        if ($request->has('search')){
            $sort_search = $request->search;
            $orders = $orders->where('order_code', 'like', '%'.$sort_search.'%');
        }
        if ($date != null) {
            $orders = $orders->where('created_at', '>=', date('Y-m-d', strtotime(explode(" to ", $date)[0])))->where('created_at', '<=', date('Y-m-d', strtotime(explode(" to ", $date)[1])));
        }
        $orders = $orders->paginate(15);
        return view('frontend.user.driver.delivered_orders', compact('orders', 'sort_search', 'date'));
    }

    public function driver_transit_deliveries(Request $request){

        $date = $request->date;
        $sort_search = null;
        $orders = DriverOrder::where('driver_id', Auth::user()->driver->id )->where(function($query){
            $query->whereNotIn('status', ['picked','on_delivery','canceled', 'delivered'] )->orwhere(function($query2){
                $query2->where('status', 'delivered')->where('payment_status' , 'unpaid')->orwhere(function($query3){
                    $query3->where('status', 'delivered')->where('payment_status' , 'paid')->where('signature_name' , null);
                });
            });
          })->orderBy('created_at', 'desc');

        if ($request->has('search')){
            $sort_search = $request->search;
            $orders = $orders->where('order_code', 'like', '%'.$sort_search.'%');
        }
        if ($date != null) {
            $orders = $orders->where('orders.created_at', '>=', date('Y-m-d', strtotime(explode(" to ", $date)[0])))->where('orders.created_at', '<=', date('Y-m-d', strtotime(explode(" to ", $date)[1])));
        }
        $orders = $orders->paginate(15);
        //dd($orders);
        return view('frontend.user.driver.transit_orders', compact('orders', 'sort_search', 'date'));
    }

    public function driver_canceled_deliveries(Request $request){

        $date = $request->date;
        $sort_search = null;
        $orders = DriverOrder::where('driver_id', Auth::user()->driver->id)->Where('status', 'canceled')->orderBy('created_at', 'desc');

        if ($request->has('search')){
            $sort_search = $request->search;
            $orders = $orders->where('order_code', 'like', '%'.$sort_search.'%');
        }
        if ($date != null) {
            $orders = $orders->where('orders.created_at', '>=', date('Y-m-d', strtotime(explode(" to ", $date)[0])))->where('orders.created_at', '<=', date('Y-m-d', strtotime(explode(" to ", $date)[1])));
        }
        $orders = $orders->paginate(15);
        return view('frontend.user.driver.canceled_orders', compact('orders', 'sort_search', 'date'));
    }

    public function order_receiver_update(Request $request){
        $order = DriverOrder::findOrFail(decrypt($request->delivery_id));
        $order->signature_name = $request->name;
        $order->signature_identity = $request->identity;
        $order->signature_contact = $request->phone;
        $order->save();
        flash(translate('Receiver updated successfully'))->success();
        return back();
    }

    public function update_delivery_status(Request $request)
    {
        $delivery_status = decrypt($request->state);
        $driver_order = DriverOrder::findOrFail(decrypt($request->id));
        $driver_order->status = $delivery_status;
        $driver_order->save();

        $order = Order::findOrFail($driver_order->order_id);
        $order->delivery_viewed = '0';
        $order->save();
         foreach($order->orderDetails->where('seller_id', (explode('#', $driver_order->order_code))[1]) as $key => $orderDetail){
            $orderDetail->delivery_status = $delivery_status;
            $orderDetail->save();
        }


        //send delivery notification to all stakeholders
        $driver =  ShippingDriver::findOrFail($driver_order->driver_id);
        $user_ids = array($driver->user_id, $order->user_id, (explode('#', $driver_order->order_code))[1]);
        $notification = array();
        $notification['order_id']     = $order->id;
        $notification['order_code']   = (explode('#', $driver_order->order_code))[0];
        $notification['user_id']      = $order->user_id;
        $notification['seller_id']    = (explode('#', $driver_order->order_code))[1];
        $notification['company_id']   = $driver_order->company_id;
        $notification['driver_id']   = $driver_order->company_id;
        $notification['status']       = $delivery_status;
        delivery_notification($user_ids, $notification);


        if($delivery_status =='delivered'){
            if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated && \App\OtpConfiguration::where('type', 'otp_for_delivery_status')->first()->value){
                try {
                    $otpController = new OTPVerificationController;
                    $otpController->send_delivery_status($order);
                } catch (\Exception $e) {
                }
            }
        }

        return 1;
    }

    public function update_payment_status(Request $request)
    {
        $payment_status = 'paid';
        $driver_order = DriverOrder::findOrFail(decrypt($request->id));
        $driver_order->payment_status = $payment_status;
        $driver_order->save();

        $order = Order::findOrFail($driver_order->order_id);
        $order->payment_status_viewed = '0';
        $order->save();

        foreach($order->orderDetails->where('seller_id', (explode('#', $driver_order->order_code))[1]) as $key => $orderDetail){
            $orderDetail->payment_status = $payment_status;
            $orderDetail->save();
        }


        $status = 'paid';
        foreach($order->orderDetails as $key => $orderDetail){
            if($orderDetail->payment_status != 'paid'){
                $status = 'unpaid';
            }
        }


        $order->payment_status = $status;
        $order->save();


        if($order->payment_status == 'paid' && $order->commission_calculated == 0){
            if(\App\Addon::where('unique_identifier', 'seller_subscription')->first() == null || !\App\Addon::where('unique_identifier', 'seller_subscription')->first()->activated){

                foreach ($order->orderDetails as $key => $orderDetail) {
                    $orderDetail->payment_status = 'paid';
                    $orderDetail->save();
                    $commission_vendor = 0;
                    $commission_driver = 0;

                    if (get_setting('category_wise_commission') != 1) {
                        $commission_vendor = get_setting('vendor_commission');
                        $commission_driver = get_setting('driver_commission');
                    } else if ($orderDetail->product->user->user_type == 'seller') {
                        $commission_vendor = $orderDetail->product->category->commision_rate;
                    }

                    if ($orderDetail->product->user->user_type == 'seller') {
                        $seller = $orderDetail->product->user->seller;

                        if (get_setting('shipping_manage_by_admin') == 1) {
                            $admin_commission = (($orderDetail->price * $commission_vendor)/100) + (($orderDetail->shipping_cost * $commission_driver)/100);
                            $seller_earning = ($orderDetail->tax + $orderDetail->price) - (($orderDetail->price * $commission_vendor)/100);
                            $driver_earning = ($orderDetail->shipping_cost) - (($orderDetail->shipping_cost * $commission_driver)/100);
                            $seller->admin_to_pay = $seller->admin_to_pay + $seller_earning ;
                        } else {
                            $admin_commission = ((($orderDetail->tax + $orderDetail->shipping_cost + $orderDetail->price) * $commission_vendor)/100);
                            $seller_earning = $orderDetail->tax + $orderDetail->shipping_cost + $orderDetail->price - $admin_commission;
                            $driver_earning = 0;
                            $seller->admin_to_pay = $seller->admin_to_pay - $admin_commission;
                        }
                        $seller->save();

                        $commission_history = new CommissionHistory;
                        $commission_history->order_id = $order->id;
                        $commission_history->order_detail_id = $orderDetail->id;
                        $commission_history->seller_id = $orderDetail->seller_id;
                        $commission_history->admin_commission = $admin_commission;
                        $commission_history->driver_earning = $driver_earning;
                        $commission_history->seller_earning = $seller_earning;

                        $commission_history->save();
                    }

                }
            }


            if (\App\Addon::where('unique_identifier', 'affiliate_system')->first() != null && \App\Addon::where('unique_identifier', 'affiliate_system')->first()->activated) {
                $affiliateController = new AffiliateController;
                $affiliateController->processAffiliatePoints($order);
            }

            if (\App\Addon::where('unique_identifier', 'club_point')->first() != null && \App\Addon::where('unique_identifier', 'club_point')->first()->activated) {
                if ($order->user != null) {
                    $clubpointController = new ClubPointController;
                    $clubpointController->processClubPoints($order);
                }
            }

            $order->commission_calculated = 1;
            $order->save();

            //update driver earning
            $driver =  ShippingDriver::findOrFail($driver_order->driver_id);
            // $driver->admin_to_pay += $driver_order->shipping_cost;
            // $driver->save();

            //send delivery notification to all stakeholders
            $user_ids = array($driver->user_id, $order->user_id, (explode('#', $driver_order->order_code))[1]);
            $notification = array();
            $notification['order_id']     = $order->id;
            $notification['order_code']   = (explode('#', $driver_order->order_code))[0];
            $notification['user_id']      = $order->user_id;
            $notification['seller_id']    = (explode('#', $driver_order->order_code))[1];
            $notification['company_id']   = $driver_order->company_id;
            $notification['driver_id']   = $driver_order->company_id;
            $notification['status']       = $payment_status;
            delivery_notification($user_ids, $notification);

            if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated && \App\OtpConfiguration::where('type', 'otp_for_paid_status')->first()->value){
                try {
                    $otpController = new OTPVerificationController;
                    $otpController->send_payment_status($order);
                } catch (\Exception $e) {
                }
            }
        }




        return 1;
    }
}
