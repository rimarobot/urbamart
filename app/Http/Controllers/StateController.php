<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;  
use App\State; 

class StateController extends Controller
{
    public function filter(Request $request)
    { 
        return response()->json(State::where('country_id', Country::where('code', $request->code)->first()->id)->get()->toArray()); 
    }
}