<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ZapPay\XpressPay;
use App\ZapPay\MoMoPay;
use App\Order;
use App\BusinessSetting;
use App\Seller;
use Session;
use Auth;
use App\Wallet;
use App\CustomerPackage;
use App\SellerPackage;
use App\Http\Controllers\CustomerPackageController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\CommissionController;
use App\Http\Controllers\WalletController;
use Illuminate\Support\Facades\Log;

class ZapPayPaymentController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function CardPaymentCreate(){
        if(Session::has('payment_type')){
            $xpresspay =  new XpressPay(  env('XPRESSPAY_USERNAME'), env('XPRESSPAY_PASSWORD'), env('XPRESSPAY_VERSION'), env('XPRESSPAY_MERCHANT'),  env('XPRESSPAY_ENDPOINT'));

            if(Session::get('payment_type') == 'cart_payment')
            {
                 $order = Order::findOrFail(Session::get('order_id'));
                 $json_response =  [];
                try
                {

                    if(empty($order->payment_details)){
                            $data  = [     'apiOperation' => 'CREATE_CHECKOUT_SESSION',
                                    'order' => [
                                        'amount' => $order->grand_total,
                                        'currency' => 'UGX',
                                        'id' =>$order->code
                                    ],
                                    'interaction'=>[
                                        'merchant' =>[
                                            'address' =>[
                                                'line1' => env('APP_ADDRESS')
                                            ],
                                            'email' => env('MAIL_USERNAME'),
                                            'name'=> env('APP_NAME'),
                                            'phone'=>'+256751345334'
                                        ]
                                    ]
		                        ];

                            $xpresspay_response = $xpresspay->CreateSession(json_encode($data, JSON_PRETTY_PRINT));


                    }
                    else
                    {
                     $xpresspay_response =  $order->payment_details;
                    }

                   $order->payment_details = $xpresspay_response;
                   $order->save();
                   Log::info("XpressPay CreateSession order: ". $order->code ." resp: " . json_encode($xpresspay_response, JSON_PRETTY_PRINT));
                   $json_response =  is_array($xpresspay_response)?$xpresspay_response:json_decode($xpresspay_response);

                    if($json_response){
                        if($json_response->result == 'SUCCESS'){
                          return view('frontend.payment.order.card_payment', compact('order', 'json_response'));
                        }

                    }
                    flash($json_response->result.' '. $json_response->error->explanation)->error();
                    return redirect()->route('checkout.payment_info');

                } catch (\Exception $e) {
                    Log::info("XpressPay CreateSession order: ". $order->code . " error: " . $e->getMessage());
                    flash($e->getMessage())->error();
                    return redirect()->route('checkout.payment_info');
                }

            }
            else if(Session::get('payment_type') == 'wallet_payment')
            {

                if(Session::has('payment_data')){
                    $payment_data = Session::get('payment_data');
                    $json_response =  [];

                    try
                    {

                        $data  = [     'apiOperation' => 'CREATE_CHECKOUT_SESSION',
                                'order' => [
                                    'amount' => $payment_data['amount'],
                                    'currency' => 'UGX',
                                    'id' => $payment_data['code']
                                ],
                                'interaction'=>[
                                    'merchant' =>[
                                        'address' =>[
                                            'line1' => env('APP_ADDRESS')
                                        ],
                                        'email' => env('MAIL_USERNAME'),
                                        'name'=> env('APP_NAME'),
                                        'phone'=>'+256751345334'
                                    ]
                                ]
                            ];
                    Log::info("XpressPay data: " . json_encode($data, JSON_PRETTY_PRINT));
                    $xpresspay_response = $xpresspay->CreateSession(json_encode($data, JSON_PRETTY_PRINT));

                    $payment_data['payment_details'] = json_encode($xpresspay_response);


                    Session::put('payment_data',$payment_data );
                    $payment_data = Session::get('payment_data');
                    Log::info("XpressPay CreateSession wallet: ". $payment_data['code'] ." resp: " . json_encode($xpresspay_response, JSON_PRETTY_PRINT));
                    $json_response =  is_array($xpresspay_response)?$xpresspay_response:json_decode($xpresspay_response);

                        if($json_response){
                            if($json_response->result == 'SUCCESS'){
                            return view('frontend.payment.order.card_payment', compact('payment_data', 'json_response'));
                            }

                        }
                        flash($json_response->result.' '. $json_response->error->explanation)->error();
                        return redirect()->route('wallet.index');

                    } catch (\Exception $e) {
                        Log::info("XpressPay CreateSession wallet: ". $payment_data['code'] . " error: " . $e->getMessage());
                        flash($e->getMessage())->error();
                        return redirect()->route('wallet.index');
                    }
                }
            }
            else if(Session::get('payment_type') == 'seller_package_payment')
            {
                if(Session::has('payment_data')){
                    $payment_data = Session::get('payment_data');
                    $json_response =  [];

                    try
                    {

                        $data  = [     'apiOperation' => 'CREATE_CHECKOUT_SESSION',
                                'order' => [
                                    'amount' => $payment_data['amount'],
                                    'currency' => 'UGX',
                                    'id' => $payment_data['code']
                                ],
                                'interaction'=>[
                                    'merchant' =>[
                                        'address' =>[
                                            'line1' => env('APP_ADDRESS')
                                        ],
                                        'email' => env('MAIL_USERNAME'),
                                        'name'=> env('APP_NAME'),
                                        'phone'=>'+256751345334'
                                    ]
                                ]
                            ];

                    $xpresspay_response = $xpresspay->CreateSession(json_encode($data, JSON_PRETTY_PRINT));

                    $payment_data['payment_details'] = json_encode($xpresspay_response);
                    Session::put('payment_data',$payment_data );
                    $payment_data = Session::get('payment_data');
                    Log::info("XpressPay CreateSession seller packages: ". $payment_data['code'] ." resp: " . json_encode($xpresspay_response, JSON_PRETTY_PRINT));
                    $json_response =  is_array($xpresspay_response)?$xpresspay_response:json_decode($xpresspay_response);

                        if($json_response){
                            if($json_response->result == 'SUCCESS'){
                            return view('frontend.payment.order.card_payment', compact('payment_data', 'json_response'));
                            }

                        }
                        flash($json_response->result.' '. $json_response->error->explanation)->error();
                        return redirect()->route('seller_packages_list');

                    } catch (\Exception $e) {
                        Log::info("XpressPay CreateSession seller packages: ". $payment_data['code'] . " error: " . $e->getMessage());
                        flash($e->getMessage())->error();
                        return redirect()->route('seller_packages_list');
                    }
                }
            }
        }
    }

    public function CardPaymentRetrive(Request $request){
            $xpresspay =  new XpressPay(  env('XPRESSPAY_USERNAME'), env('XPRESSPAY_PASSWORD'),
            env('XPRESSPAY_VERSION'), env('XPRESSPAY_MERCHANT'),  env('XPRESSPAY_ENDPOINT'));
            $json_response =  [];
            $gatewayCode ='';
            // Log::info("XpressPay RetriveOrder: ". $request);
            if($request->type == 'cart_payment')
            {

                    try
                    {
                        $order = Order::findOrFail($request->id);
                        $xpresspay_response =  $xpresspay->RetriveOrder($order->code);
                        $json_response = json_decode($xpresspay_response, true);
                        Log::info("XpressPay RetriveOrder order: ". $order->code ." resp: " .$xpresspay_response);

                            foreach($json_response['transaction'] as $key=>$transaction){

                                $gatewayCode = $transaction['response']['gatewayCode'];
                                if ($gatewayCode == 'APPROVED')
                                    break;
                            }
                            Log::info("XpressPay RetriveOrder order: ". $order->code ." resp: " .$gatewayCode);

                            $order->gateway_details  = json_encode( $xpresspay_response);
                            $order->payment_status = $gatewayCode == 'APPROVED' ? 'paid' : 'unpaid';
                            $order->save();

                            if($gatewayCode == 'APPROVED')
                            {
                                if (\App\Addon::where('unique_identifier', 'affiliate_system')->first() != null && \App\Addon::where('unique_identifier', 'affiliate_system')->first()->activated) {
                                    $affiliateController = new AffiliateController;
                                    $affiliateController->processAffiliatePoints($order);
                                }

                                if (\App\Addon::where('unique_identifier', 'club_point')->first() != null && \App\Addon::where('unique_identifier', 'club_point')->first()->activated) {
                                    $clubpointController = new ClubPointController;
                                    $clubpointController->processClubPoints($order);
                                }
                                if (\App\Addon::where('unique_identifier', 'seller_subscription')->first() == null || !\App\Addon::where('unique_identifier', 'seller_subscription')->first()->activated) {
                                    if (BusinessSetting::where('type', 'category_wise_commission')->first()->value != 1) {
                                        $commission_percentage = BusinessSetting::where('type', 'vendor_commission')->first()->value;
                                        foreach ($order->orderDetails as $key => $orderDetail) {
                                            $orderDetail->payment_status = 'paid';
                                            $orderDetail->save();
                                            if ($orderDetail->product->user->user_type == 'seller') {
                                                $seller = $orderDetail->product->user->seller;
                                                $seller->admin_to_pay = $seller->admin_to_pay + ($orderDetail->price * (100 - $commission_percentage)) / 100 + $orderDetail->tax + $orderDetail->shipping_cost;
                                                $seller->save();
                                            }
                                        }
                                    } else {
                                        foreach ($order->orderDetails as $key => $orderDetail) {
                                            $orderDetail->payment_status = 'paid';
                                            $orderDetail->save();
                                            if ($orderDetail->product->user->user_type == 'seller') {
                                                $commission_percentage = $orderDetail->product->category->commision_rate;
                                                $seller = $orderDetail->product->user->seller;
                                                $seller->admin_to_pay = $seller->admin_to_pay + ($orderDetail->price * (100 - $commission_percentage)) / 100 + $orderDetail->tax + $orderDetail->shipping_cost;
                                                $seller->save();
                                            }
                                        }
                                    }
                                } else {
                                    foreach ($order->orderDetails as $key => $orderDetail) {
                                        $orderDetail->payment_status = 'paid';
                                        $orderDetail->save();
                                        if ($orderDetail->product->user->user_type == 'seller') {
                                            $seller = $orderDetail->product->user->seller;
                                            $seller->admin_to_pay = $seller->admin_to_pay + $orderDetail->price + $orderDetail->tax + $orderDetail->shipping_cost;
                                            $seller->save();
                                        }
                                    }
                                }

                                $order->commission_calculated = 1;
                                $order->save();


                                // Session::put('cart', Session::get('cart')->where('owner_id', '!=', Session::get('owner_id')));
                                Session::put('cart', collect([]));
                                Session::forget('payment_type');
                                Session::forget('delivery_info');
                                Session::forget('coupon_id');
                                Session::forget('coupon_discount');
                                // Session::forget('owner_id');

                                flash("Your order has been placed successfully")->success();

                                return response()->json([
                                        'next' =>  route('order_confirmed')
                                    ]);
                            }
                            else
                            {
                                flash("Payment failed with Reason: " . $gatewayCode )->error()->important();
                            return response()->json([
                                    'next' =>  route('checkout.payment_info')
                                ]);
                            }

                        } catch (\Exception $e) {
                            Log::info("XpressPay CardPaymentRetrive cart error: " . $e->getMessage());
                            return response()->json([
                                'next' =>  route('checkout.payment_info')
                            ]);
                        }

            }
            else if($request->type == 'wallet_payment')
            {

                    try
                    {
                            $payment_data = Session::get('payment_data');
                            $xpresspay_response =  $xpresspay->RetriveOrder($payment_data['code']);
                            $json_response = json_decode($xpresspay_response, true);
                            Log::info("XpressPay RetriveOrder wallet: ". $payment_data['code'] ." resp: " .$xpresspay_response);

                                foreach($json_response['transaction'] as $key=>$transaction){

                                    $gatewayCode = $transaction['response']['gatewayCode'];
                                    if ($gatewayCode == 'APPROVED')
                                        break;
                                }
                                Log::info("XpressPay RetriveOrder wallet: ". $payment_data['code'] ." resp: " .$gatewayCode);


                                if($gatewayCode == 'APPROVED')
                                {
                                    $user = Auth::user();
                                    $user->balance = $user->balance + $payment_data['amount'];
                                    $user->save();

                                    $wallet = new Wallet;
                                    $wallet->user_id = $user->id;
                                    $wallet->amount = $payment_data['amount'];
                                    $wallet->payment_method = $payment_data['payment_method'];
                                    $wallet->code = $payment_data['code'];
                                    $wallet->payment_details = $payment_data['payment_details'];
                                    $wallet->gateway_details  = json_encode( $xpresspay_response);
                                    $wallet->payment_status = $gatewayCode == 'APPROVED' ? 'paid' : 'unpaid';
                                    $wallet->save();

                                    Session::forget('payment_data');
                                    Session::forget('payment_type');

                                    flash("Your wallet has been recharged successfully")->success();

                                    return response()->json([
                                            'next' =>  route('wallet.index')
                                        ]);
                                }
                                else
                                {
                                    flash("Wallet recharge failed with Reason: " . $gatewayCode )->error()->important();
                                    return response()->json([
                                            'next' =>  route('wallet.index')
                                    ]);
                                }
                        } catch (\Exception $e) {
                             Log::info("XpressPay CardPaymentRetrive wallet error: " . $e->getMessage());
                                return response()->json([
                                    'next' =>  route('wallet.index')
                                ]);
                        }
            }
            else if($request->type == 'seller_package_payment')
            {
                    try
                    {
                            $payment_data = Session::get('payment_data');
                            $xpresspay_response =  $xpresspay->RetriveOrder($payment_data['code']);
                            $json_response = json_decode($xpresspay_response, true);
                            Log::info("XpressPay RetriveOrder seller packages: ". $payment_data['code'] ." resp: " .$xpresspay_response);

                                foreach($json_response['transaction'] as $key=>$transaction){

                                    $gatewayCode = $transaction['response']['gatewayCode'];
                                    if ($gatewayCode == 'APPROVED')
                                        break;
                                }
                                Log::info("XpressPay RetriveOrder seller packages: ". $payment_data['code'] ." resp: " .$gatewayCode);


                                if($gatewayCode == 'APPROVED')
                                {
                                    $payment_data = Session::get('payment_data');//;
                                    $seller = Auth::user()->seller;
                                    $seller->seller_package_id = $payment_data['seller_package_id'];
                                    $seller->seller_package_code = $payment_data['code'];
                                    $seller->seller_package_payment_type = $payment_data['payment_method'];
                                    $seller->seller_package_payment_status = $gatewayCode == 'APPROVED' ? 'paid' : 'unpaid';
                                    $seller->seller_package_payment_details = $payment_data['payment_details'];
                                    $seller->seller_package_gateway_details = json_encode( $xpresspay_response);

                                    $seller_package = SellerPackage::findOrFail($payment_data['seller_package_id']);
                                    $seller->remaining_uploads += $seller_package->product_upload;
                                    $seller->remaining_digital_uploads += $seller_package->digital_product_upload;
                                    $seller->invalid_at = date('Y-m-d', strtotime('+ '.$seller_package->duration.'days'));
                                    $seller->save();


                                    Session::forget('payment_data');
                                    Session::forget('payment_type');

                                    flash("Your Selected Package has been purchased successfully")->success();

                                    return response()->json([
                                            'next' =>  route('dashboard')
                                        ]);
                                }
                                else
                                {
                                    flash("Package purchase failed with Reason: " . $gatewayCode )->error()->important();
                                    return response()->json([
                                            'next' =>  route('seller_packages_list')
                                    ]);
                                }
                        } catch (\Exception $e) {
                                Log::info("XpressPay CardPaymentRetrive seller packages error: " . $e->getMessage());
                                flash("Package purchase failed Try again: " )->error()->important();
                                return response()->json([
                                    'next' =>  route('seller_packages_list')
                                ]);
                        }
            }

   }


    public function MobilePaymentCreate(Request $request){
                if(Session::has('payment_type'))
                {
                    $momopay =  new MoMoPay(  env('MOMOPAY_USERNAME'), env('MOMOPAY_PASSWORD'), env('MOMOPAY_APIKEY'), env('MOMOPAY_TESTMODE'), env('MOMOPAY_ENDPOINT'));
                    $json_response =  [];
                    if(Session::get('payment_type') == 'cart_payment')
                    {
                        $order = Order::findOrFail($request->order_id);
                        Log::info("MomoPay CreateSession order: ". $order->code);

                        if($order != null)
                        {

                            try
                            {
                                if($request->mobilemoney_option == 'mtn'){

                                     $data =   [ 'amount' => number_format( (float)round((float)$order->grand_total,0, PHP_ROUND_HALF_UP), 0, '.', ''),
                                                        'currency' => (env('MOMOPAY_TESTMODE') ? 'EUR' : 'UGX'),
                                                        'externalId' => $order->code,
                                                        'payer'=>[
                                                            'partyIdType' => 'MSISDN',
                                                            'partyId' => substr($request->phone,1)
                                                        ],
                                                        'payerMessage' => 'UrbaMart '. $order->code ,
                                                        'payeeNote' => 'UrbaMart '. $order->code
                                                    ];

                                    Log::info("MomoPay CreateSession order: ". $order->code .' request: ' . json_encode($data, JSON_PRETTY_PRINT));
                                    $reference_id =	$momopay->get_guid();
                                    $order->payment_details = $reference_id;
                                    $order->save();
                                    $momopay_response = $momopay->CreateSession(json_encode($data, JSON_PRETTY_PRINT), $reference_id);
                                    Log::info("MomoPay CreateSession order: ". $order->code .' response: ' . json_encode($momopay_response, JSON_PRETTY_PRINT));

                                    $json_response = json_decode($momopay_response, true);
                                    if($json_response['status'] == 'success'){
                                        return response()->json([
                                            'status' => true,
                                            'reference' => $reference_id,
                                            'id' => $order->id,
                                            'op' => $request->mobilemoney_option,
                                            'next' =>  route('mobile.status')
                                        ]);
                                    }
                                    else{
                                        flash("Payment failed")->error();
                                        return response()->json([
                                            'status' => false,
                                            'reference' => $reference_id,
                                            'id' => $order->id,
                                            'next' =>  route('checkout.payment_info')
                                        ]);
                                    }
                                }
                                else if($request->mobilemoney_option == 'airtel'){
                                    flash("Payment currently not supported")->error();
                                        return response()->json([
                                            'status' => false,
                                            'reference' => '',
                                            'id' => $order->id,
                                            'next' =>  route('checkout.payment_info')
                                        ]);
                                }else if($request->mobilemoney_option == 'mpesa'){
                                    flash("Payment currently not supported")->error();
                                        return response()->json([
                                            'status' => false,
                                            'reference' => '',
                                            'id' => $order->id,
                                            'next' =>  route('checkout.payment_info')
                                        ]);
                                }
                            }
                            catch (\Exception $e)
                            {
                                    Log::info("MomoPay CreateSession order: ". $order->code . " error: " . $e->getMessage());
                                    flash("Payment failed Try again")->error();
                                    return response()->json([
                                                'status' => false,
                                                'reference' => '',
                                                'id' =>  '',
                                                'next' =>  route('checkout.payment_info')
                                    ]);
                            }

                        }
                        else{
                            flash("Payment failed Try again")->error();
                            return response()->json([
                                        'status' => false,
                                        'reference' => '',
                                        'id' =>  '',
                                        'next' =>  route('checkout.payment_info')
                            ]);
                        }
                    }
                    else if(Session::get('payment_type') == 'wallet_payment')
                    {
                        if(Session::has('payment_data')){
                            $payment_data = Session::get('payment_data');
                            $json_response =  [];

                            try
                            {
                                if($request->mobilemoney_option == 'mtn'){

                                     $data =   [ 'amount' => number_format( (float)round((float)$payment_data['amount'],0, PHP_ROUND_HALF_UP), 0, '.', ''),
                                                        'currency' => (env('MOMOPAY_TESTMODE') ? 'EUR' : 'UGX'),
                                                        'externalId' => $payment_data['code'],
                                                        'payer'=>[
                                                            'partyIdType' => 'MSISDN',
                                                            'partyId' => substr($request->full_phone,1)
                                                        ],
                                                        'payerMessage' => 'UrbaMart '. $payment_data['code'] ,
                                                        'payeeNote' => 'UrbaMart '. $payment_data['code']
                                                    ];

                                    Log::info("MomoPay CreateSession wallet: ". $payment_data['code'] .' request: ' . json_encode($data, JSON_PRETTY_PRINT));

                                    $reference_id =	$momopay->get_guid();
                                    $payment_data['payment_details'] = $reference_id;
                                    Session::put('payment_data',$payment_data );
                                    $payment_data = Session::get('payment_data');

                                    $momopay_response = $momopay->CreateSession(json_encode($data, JSON_PRETTY_PRINT), $reference_id);
                                    Log::info("MomoPay CreateSession wallet: ". $payment_data['code'] .' response: ' . json_encode($momopay_response, JSON_PRETTY_PRINT));

                                    $json_response = json_decode($momopay_response, true);
                                    if($json_response['status'] == 'success'){
                                        return response()->json([
                                            'status' => true,
                                            'reference' => $reference_id,
                                            'id' => $payment_data['code'],
                                            'op' => $request->mobilemoney_option,
                                            'next' =>  route('mobile.status')
                                        ]);
                                    }
                                    else{
                                        flash("Payment failed")->error();
                                        return response()->json([
                                            'status' => false,
                                            'reference' => $reference_id,
                                            'id' => $payment_data['code'],
                                            'next' =>  route('wallet.index')
                                        ]);
                                    }
                                }
                                else if($request->mobilemoney_option == 'airtel'){
                                    flash("Payment currently not supported")->error();
                                        return response()->json([
                                            'status' => false,
                                            'reference' => '',
                                            'id' => $payment_data['code'],
                                            'next' =>  route('wallet.index')
                                        ]);
                                }else if($request->mobilemoney_option == 'mpesa'){
                                    flash("Payment currently not supported")->error();
                                        return response()->json([
                                            'status' => false,
                                            'reference' => '',
                                            'id' => $payment_data['code'],
                                            'next' =>  route('wallet.index')
                                        ]);
                                }
                            }
                            catch (\Exception $e)
                            {
                                    Log::info("MomoPay CreateSession wallet: ". $payment_data['code'] . " error: " . $e->getMessage());
                                    flash("Payment failed Try again")->error();
                                    return response()->json([
                                                'status' => false,
                                                'reference' => '',
                                                'id' =>  '',
                                                'next' =>  route('wallet.index')
                                    ]);
                            }
                        }
                    }
                    else if(Session::get('payment_type') == 'seller_package_payment')
                    {
                        if(Session::has('payment_data')){
                            $payment_data = Session::get('payment_data');
                            $json_response =  [];

                            try
                            {
                                if($request->mobilemoney_option == 'mtn'){

                                     $data =   [ 'amount' => number_format( (float)round((float)$payment_data['amount'],0, PHP_ROUND_HALF_UP), 0, '.', ''),
                                                        'currency' => (env('MOMOPAY_TESTMODE') ? 'EUR' : 'UGX'),
                                                        'externalId' => $payment_data['code'],
                                                        'payer'=>[
                                                            'partyIdType' => 'MSISDN',
                                                            'partyId' => substr($request->full_phone,1)
                                                        ],
                                                        'payerMessage' => 'UrbaMart '. $payment_data['code'] ,
                                                        'payeeNote' => 'UrbaMart '. $payment_data['code']
                                                    ];
                                    Log::info("MomoPay CreateSession seller package: ". $payment_data['code'] .' request: ' . json_encode($data, JSON_PRETTY_PRINT));

                                    $reference_id =	$momopay->get_guid();
                                    $payment_data['payment_details'] = $reference_id;
                                    Session::put('payment_data',$payment_data );
                                    $payment_data = Session::get('payment_data');

                                    $momopay_response = $momopay->CreateSession(json_encode($data, JSON_PRETTY_PRINT), $reference_id);
                                    Log::info("MomoPay CreateSession seller package: ". $payment_data['code'] .' response: ' . json_encode($momopay_response, JSON_PRETTY_PRINT));

                                    $json_response = json_decode($momopay_response, true);
                                    if($json_response['status'] == 'success'){
                                        return response()->json([
                                            'status' => true,
                                            'reference' => $reference_id,
                                            'id' => $payment_data['code'],
                                            'op' => $request->mobilemoney_option,
                                            'next' =>  route('mobile.status')
                                        ]);
                                    }
                                    else{
                                        flash("Payment failed")->error();
                                        return response()->json([
                                            'status' => false,
                                            'reference' => $reference_id,
                                            'id' => $payment_data['code'],
                                            'next' =>  route('seller_packages_list')
                                        ]);
                                    }
                                }
                                else if($request->mobilemoney_option == 'airtel'){
                                    flash("Payment currently not supported")->error();
                                        return response()->json([
                                            'status' => false,
                                            'reference' => '',
                                            'id' => $payment_data['code'],
                                            'next' =>  route('seller_packages_list')
                                        ]);
                                }else if($request->mobilemoney_option == 'mpesa'){
                                    flash("Payment currently not supported")->error();
                                        return response()->json([
                                            'status' => false,
                                            'reference' => '',
                                            'id' => $payment_data['code'],
                                            'next' =>  route('seller_packages_list')
                                        ]);
                                }
                            }
                            catch (\Exception $e)
                            {
                                    Log::info("MomoPay CreateSession seller package: ". $payment_data['code'] . " error: " . $e->getMessage());
                                    flash("Payment failed Try again")->error();
                                    return response()->json([
                                                'status' => false,
                                                'reference' => '',
                                                'id' =>  '',
                                                'next' =>  route('seller_packages_list')
                                    ]);
                            }
                        }
                    }
            }
    }

    public function MobilePaymentRetrive(Request $request){

        if($request->type == 'cart_payment'){
             try
             {
                $order = Order::findOrFail($request->id);
                if($order != null){
                    if($request->op == 'mtn'){
                        $momopay =  new MoMoPay(  env('MOMOPAY_USERNAME'), env('MOMOPAY_PASSWORD'), env('MOMOPAY_APIKEY'),
                        env('MOMOPAY_TESTMODE'), env('MOMOPAY_ENDPOINT'));

                        $json_response =  [];
                        $momopay_response = $momopay->RetriveOrder($order->payment_details);
                        Log::info("MomoPay Retrive order: ". $order->code .' response: ' . json_encode($momopay_response, JSON_PRETTY_PRINT));
                        if($momopay_response != null){
                            $json_response = json_decode($momopay_response, true);

                            $order->gateway_details  = $json_response;
                            $order->payment_status = $json_response['status'] == 'SUCCESSFUL' ? 'paid' : 'unpaid';
                            $order->save();

                            if($json_response['status'] == 'SUCCESSFUL'){
                                if (\App\Addon::where('unique_identifier', 'affiliate_system')->first() != null && \App\Addon::where('unique_identifier', 'affiliate_system')->first()->activated) {
                                    $affiliateController = new AffiliateController;
                                    $affiliateController->processAffiliatePoints($order);
                                }

                                if (\App\Addon::where('unique_identifier', 'club_point')->first() != null && \App\Addon::where('unique_identifier', 'club_point')->first()->activated) {
                                    $clubpointController = new ClubPointController;
                                    $clubpointController->processClubPoints($order);
                                }
                                if (\App\Addon::where('unique_identifier', 'seller_subscription')->first() == null || !\App\Addon::where('unique_identifier', 'seller_subscription')->first()->activated) {
                                    if (BusinessSetting::where('type', 'category_wise_commission')->first()->value != 1) {
                                        $commission_percentage = BusinessSetting::where('type', 'vendor_commission')->first()->value;
                                        foreach ($order->orderDetails as $key => $orderDetail) {
                                            $orderDetail->payment_status = 'paid';
                                            $orderDetail->save();
                                            if ($orderDetail->product->user->user_type == 'seller') {
                                                $seller = $orderDetail->product->user->seller;
                                                $seller->admin_to_pay = $seller->admin_to_pay + ($orderDetail->price * (100 - $commission_percentage)) / 100 + $orderDetail->tax + $orderDetail->shipping_cost;
                                                $seller->save();
                                            }
                                        }
                                    } else {
                                        foreach ($order->orderDetails as $key => $orderDetail) {
                                            $orderDetail->payment_status = 'paid';
                                            $orderDetail->save();
                                            if ($orderDetail->product->user->user_type == 'seller') {
                                                $commission_percentage = $orderDetail->product->category->commision_rate;
                                                $seller = $orderDetail->product->user->seller;
                                                $seller->admin_to_pay = $seller->admin_to_pay + ($orderDetail->price * (100 - $commission_percentage)) / 100 + $orderDetail->tax + $orderDetail->shipping_cost;
                                                $seller->save();
                                            }
                                        }
                                    }
                                } else {
                                    foreach ($order->orderDetails as $key => $orderDetail) {
                                        $orderDetail->payment_status = 'paid';
                                        $orderDetail->save();
                                        if ($orderDetail->product->user->user_type == 'seller') {
                                            $seller = $orderDetail->product->user->seller;
                                            $seller->admin_to_pay = $seller->admin_to_pay + $orderDetail->price + $orderDetail->tax + $orderDetail->shipping_cost;
                                            $seller->save();
                                        }
                                    }
                                }

                                $order->commission_calculated = 1;
                                $order->save();
                                // Session::put('cart', Session::get('cart')->where('owner_id', '!=', Session::get('owner_id')));
                                Session::put('cart', collect([]));
                                Session::forget('payment_type');
                                Session::forget('delivery_info');
                                Session::forget('coupon_id');
                                Session::forget('coupon_discount');
                                // Session::forget('owner_id');
                                flash("Your order has been placed successfully")->success();
                                return response()->json([ 'status' => 'success',  'next' =>  route('order_confirmed') ]);
                            }
                            else if($json_response['status'] == 'PENDING'){
                                return response()->json([  'status' => 'pending',  'next' =>  '' ]);
                            }else if($json_response['status'] == 'FAILED'){
                                flash("Payment failed with Error: " . $json_response['reason'])->error();
                                    return response()->json([ 'status' => 'failed', 'next' =>  route('checkout.payment_info') ]);
                            } else{
                                flash("Payment failed")->error();
                                return response()->json([ 'status' => 'failed', 'next' =>  route('checkout.payment_info') ]);
                            }
                        }
                        else
                        {
                            return response()->json([  'status' => 'pending',  'next' =>  '' ]);
                        }
                    }

                }
                else{
                    flash("Payment failed Try again")->error();
                    return response()->json([ 'status' => 'failed', 'next' =>  route('checkout.payment_info') ]);
                }



            } catch (\Exception $e) {
                        Log::info("MomoPay Retrive order: ". $order->code . " error: " . $e->getMessage());
                        flash("Payment failed Try again")->error();
                        return response()->json([ 'status' => 'failed', 'next' =>  route('checkout.payment_info') ]);
            }
        }
        elseif($request->type == 'wallet_payment')
        {
             try
             {
                $payment_data = Session::get('payment_data');
                if($payment_data != null){
                    if($request->op == 'mtn'){

                        $momopay =  new MoMoPay(  env('MOMOPAY_USERNAME'), env('MOMOPAY_PASSWORD'), env('MOMOPAY_APIKEY'), env('MOMOPAY_TESTMODE'), env('MOMOPAY_ENDPOINT'));

                        $json_response =  [];
                        $momopay_response = $momopay->RetriveOrder($payment_data['payment_details']);

                        Log::info("MomoPay Retrive wallet: ". $payment_data['code'] .' response: ' . json_encode($momopay_response, JSON_PRETTY_PRINT));
                        if($momopay_response != null)
                        {
                            $json_response = json_decode($momopay_response, true);

                            if($json_response['status'] == 'SUCCESSFUL')
                            {
                                $user = Auth::user();
                                $user->balance = $user->balance + $payment_data['amount'];
                                $user->save();

                                $wallet = new Wallet;
                                $wallet->user_id = $user->id;
                                $wallet->amount = $payment_data['amount'];
                                $wallet->payment_method = $payment_data['payment_method'];
                                $wallet->code = $payment_data['code'];
                                $wallet->payment_details = $payment_data['payment_details'];
                                $wallet->gateway_details  = json_encode( $momopay_response);
                                $wallet->payment_status = $json_response['status'] == 'SUCCESSFUL' ? 'paid' : 'unpaid';
                                $wallet->save();

                                Session::forget('payment_data');
                                Session::forget('payment_type');
                                flash("Your order has been placed successfully")->success();
                                return response()->json([ 'status' => 'success',  'next' =>  route('wallet.index') ]);
                            }
                            else if($json_response['status'] == 'PENDING'){
                                return response()->json([  'status' => 'pending',  'next' =>  '' ]);
                            }else if($json_response['status'] == 'FAILED'){
                                flash("Payment failed with Error: " . $json_response['reason'])->error();
                                    return response()->json([ 'status' => 'failed', 'next' =>  route('wallet.index') ]);
                            } else{
                                flash("Payment failed")->error();
                                return response()->json([ 'status' => 'failed', 'next' =>  route('wallet.index') ]);
                            }
                        }
                        else
                        {
                            return response()->json([  'status' => 'pending',  'next' =>  '' ]);
                        }
                    }

                }
                else{
                    flash("Payment failed Try again")->error();
                    return response()->json([ 'status' => 'failed', 'next' =>  route('wallet.index') ]);
                }



            } catch (\Exception $e) {
                        Log::info("MomoPay Retrive wallet: ". $order->code . " error: " . $e->getMessage());
                        flash("Payment failed Try again")->error();
                        return response()->json([ 'status' => 'failed', 'next' =>  route('wallet.index') ]);
            }
        }
        elseif($request->type == 'seller_package_payment'){
            try
             {
                $payment_data = Session::get('payment_data');
                if($payment_data != null){
                    if($request->op == 'mtn'){

                        $momopay =  new MoMoPay(  env('MOMOPAY_USERNAME'), env('MOMOPAY_PASSWORD'), env('MOMOPAY_APIKEY'), env('MOMOPAY_TESTMODE'), env('MOMOPAY_ENDPOINT'));

                        $json_response =  [];
                        $momopay_response = $momopay->RetriveOrder($payment_data['payment_details']);

                        Log::info("MomoPay Retrive wallet: ". $payment_data['code'] .' response: ' . json_encode($momopay_response, JSON_PRETTY_PRINT));
                        if($momopay_response != null){
                            $json_response = json_decode($momopay_response, true);

                            if($json_response['status'] == 'SUCCESSFUL'){

                                $payment_data = Session::get('payment_data');//;
                                $seller = Auth::user()->seller;
                                $seller->seller_package_id = $payment_data['seller_package_id'];
                                $seller->seller_package_code = $payment_data['code'];
                                $seller->seller_package_payment_type = $payment_data['payment_method'];
                                $seller->seller_package_payment_status = $json_response['status'] == 'SUCCESSFUL' ? 'paid' : 'unpaid';
                                $seller->seller_package_payment_details = $payment_data['payment_details'];
                                $seller->seller_package_gateway_details = json_encode( $momopay_response);

                                $seller_package = SellerPackage::findOrFail($payment_data['seller_package_id']);
                                $seller->remaining_uploads += $seller_package->product_upload;
                                $seller->remaining_digital_uploads += $seller_package->digital_product_upload;
                                $seller->invalid_at = date('Y-m-d', strtotime('+ '.$seller_package->duration.'days'));
                                $seller->save();


                                Session::forget('payment_data');
                                Session::forget('payment_type');
                                flash("Your Selected Package has been purchased successfully")->success();
                                return response()->json([ 'status' => 'success',  'next' =>  route('dashboard') ]);
                            }
                            else if($json_response['status'] == 'PENDING'){
                                return response()->json([  'status' => 'pending',  'next' =>  '' ]);
                            }else if($json_response['status'] == 'FAILED'){
                                flash("Payment failed with Error: " . $json_response['reason'])->error();
                                    return response()->json([ 'status' => 'failed', 'next' =>  route('seller_packages_list') ]);
                            } else{
                                flash("Package purchase failed Try again: " )->error()->important();
                                return response()->json([ 'status' => 'failed', 'next' =>  route('seller_packages_list') ]);
                            }
                        }
                        else
                        {
                            return response()->json([  'status' => 'pending',  'next' =>  '' ]);
                        }
                    }

                }
                else{
                    flash("Package purchase failed Try again: " )->error()->important();
                    return response()->json([ 'status' => 'failed', 'next' =>  route('seller_packages_list') ]);
                }



            } catch (\Exception $e) {
                        Log::info("MomoPay Retrive wallet: ". $order->code . " error: " . $e->getMessage());
                        flash("Payment failed Try again")->error();
                        return response()->json([ 'status' => 'failed', 'next' =>  route('seller_packages_list') ]);
            }
        }




    }

    public function MobilePaymentIndex()
    {
        if(Session::has('payment_type')){
            if(Session::get('payment_type') == 'cart_payment' || Session::get('payment_type') == 'wallet_payment'){
                return view('frontend.payment.order.mobile_payment');
            }
            elseif (Session::get('payment_type') == 'customer_package_payment') {
                $customer_package = CustomerPackage::findOrFail(Session::get('payment_data')['customer_package_id']);
                return view('frontend.payment.order.mobile_payment', compact('customer_package'));
            }
            elseif (Session::get('payment_type') == 'seller_package_payment') {
                $seller_package = SellerPackage::findOrFail(Session::get('payment_data')['seller_package_id']);
                return view('frontend.payment.order.mobile_payment', compact('seller_package'));
            }
        }
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
     function ZapPayPost(Request $request)
     {
        if($request->session()->has('payment_type')){
            if($request->session()->get('payment_type') == 'cart_payment'){
                $order = Order::findOrFail(Session::get('order_id'));

                $xpresspay =  new XpressPay(
                    env('XPRESSPAY_USERNAME'), env('XPRESSPAY_PASSWORD'), env('XPRESSPAY_VERSION'),
                    env('XPRESSPAY_MERCHANT'),  env('XPRESSPAY_ENDPOINT'));

                try {

                    $data  = [     'apiOperation' => 'CREATE_CHECKOUT_SESSION',
                                    'order' => [
                                        'amount' => number_format( (float)round((float)$order->grand_total,2, PHP_ROUND_HALF_UP), 2, '.', ''),
                                        'currency' => 'UGX',
                                        'id' =>$order->code
                                    ],
                                    'interaction'=>[
                                        'merchant' =>[
                                            'address' =>[
                                                'line1' => env('APP_ADDRESS')
                                            ],
                                            'email' => env('MAIL_USERNAME'),
                                            'name'=> env('APP_NAME'),
                                            'phone'=>'+256751345334'
                                        ]
                                    ]
		                        ];

                   $response = $xpresspay->CreateSession(json_encode($data, JSON_PRETTY_PRINT));
                   $order->payment_details = $response;
                   $order->save();

                } catch (\Exception $e) {
                    flash($e->getMessage())->error();
                    return redirect()->route('checkout.payment_info');
                }
                // return redirect()->route('checkout.payment_info');

               $checkoutController = new CheckoutController;
               return $checkoutController->checkout_done($request->session()->get('order_id'), $response);
            // }
            // elseif ($request->session()->get('payment_type') == 'wallet_payment') {
            //     Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

            //     try {
            //         $payment = json_encode(Stripe\Charge::create ([
            //                 "amount" => round(convert_to_usd($request->session()->get('payment_data')['amount']) * 100),
            //                 "currency" => "usd",
            //                 "source" => $request->stripeToken
            //         ]));
            //     } catch (\Exception $e) {
            //         flash($e->getMessage())->error();
            //         return redirect()->route('wallet.index');
            //     }


            //     $walletController = new WalletController;
            //     return $walletController->wallet_payment_done($request->session()->get('payment_data'), $payment);
            // }
            // elseif ($request->session()->get('payment_type') == 'customer_package_payment') {
            //     Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            //     $customer_package = CustomerPackage::findOrFail(Session::get('payment_data')['customer_package_id']);

            //     try {
            //         $payment = json_encode(Stripe\Charge::create ([
            //                 "amount" => round(convert_to_usd($customer_package->amount) * 100),
            //                 "currency" => "usd",
            //                 "source" => $request->stripeToken
            //         ]));
            //     } catch (\Exception $e) {
            //         flash($e->getMessage())->error();
            //         return redirect()->route('customer_packages_list_show');
            //     }


            //     $customer_package_controller = new CustomerPackageController;
            //     return $customer_package_controller->purchase_payment_done($request->session()->get('payment_data'), $payment);
            // }
            // elseif ($request->session()->get('payment_type') == 'seller_package_payment') {
            //     Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            //     $seller_package = SellerPackage::findOrFail(Session::get('payment_data')['seller_package_id']);

            //     try {
            //         $payment = json_encode(Stripe\Charge::create ([
            //                 "amount" => round(convert_to_usd($seller_package->amount) * 100),
            //                 "currency" => "usd",
            //                 "source" => $request->stripeToken
            //         ]));
            //     } catch (\Exception $e) {
            //         flash($e->getMessage())->error();
            //         return redirect()->route('seller_packages_list');
            //     }


            //     $seller_package_controller = new SellerPackageController;
            //     return $seller_package_controller->purchase_payment_done($request->session()->get('payment_data'), $payment);
            }
        }
    }
}
