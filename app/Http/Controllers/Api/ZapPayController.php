<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\ZapPay\XpressPay; 
use App\Order;
use Illuminate\Support\Facades\Log;

class ZapPayController extends Controller {
   
   public function CardPayment(Request $request){
  
            
            $xpresspay =  new XpressPay(  env('XPRESSPAY_USERNAME'), env('XPRESSPAY_PASSWORD'),
            env('XPRESSPAY_VERSION'), env('XPRESSPAY_MERCHANT'),  env('XPRESSPAY_ENDPOINT')); 
            $json_response =  [];
            $gatewayCode ='';
                       
           try{
              $order = Order::findOrFail($request->id);
              $xpresspay_response =  $xpresspay.RetriveOrder($order->code);
              $json_response = json_decode($xpresspay_response, true); 
                
                 
                foreach($json_response['transaction'] as $key=>$transaction){
                  
                    $gatewayCode = $transaction['response']['gatewayCode']; 
                    if ($gatewayCode == 'APPROVED')
                        break;
                }
                
                $order->gateway_details  = $json_response;
                $order->payment_status = $gatewayCode == 'APPROVED' ? 'paid' : 'unpaid'; 
                $order->save();  

                if($gatewayCode == 'APPROVED')
                {  


                   if (\App\Addon::where('unique_identifier', 'affiliate_system')->first() != null && \App\Addon::where('unique_identifier', 'affiliate_system')->first()->activated) {
                        $affiliateController = new AffiliateController;
                        $affiliateController->processAffiliatePoints($order);
                    }

                    if (\App\Addon::where('unique_identifier', 'club_point')->first() != null && \App\Addon::where('unique_identifier', 'club_point')->first()->activated) {
                        $clubpointController = new ClubPointController;
                        $clubpointController->processClubPoints($order);
                    }

                    if(\App\Addon::where('unique_identifier', 'seller_subscription')->first() == null || !\App\Addon::where('unique_identifier', 'seller_subscription')->first()->activated){
                        if (BusinessSetting::where('type', 'category_wise_commission')->first()->value != 1) {
                            $commission_percentage = BusinessSetting::where('type', 'vendor_commission')->first()->value;
                            foreach ($order->orderDetails as $key => $orderDetail) {
                                $orderDetail->payment_status = 'paid';
                                $orderDetail->save();
                                if($orderDetail->product->user->user_type == 'seller'){
                                    $seller = $orderDetail->product->user->seller;
                                    $seller->admin_to_pay = $seller->admin_to_pay + ($orderDetail->price*(100-$commission_percentage))/100 + $orderDetail->tax + $orderDetail->shipping_cost;
                                    $seller->save();
                                }
                            }
                        }
                        else{
                            foreach ($order->orderDetails as $key => $orderDetail) {
                                $orderDetail->payment_status = 'paid';
                                $orderDetail->save();
                                if($orderDetail->product->user->user_type == 'seller'){
                                    $commission_percentage = $orderDetail->product->category->commision_rate;
                                    $seller = $orderDetail->product->user->seller;
                                    $seller->admin_to_pay = $seller->admin_to_pay + ($orderDetail->price*(100-$commission_percentage))/100  + $orderDetail->tax + $orderDetail->shipping_cost;
                                    $seller->save();
                                }
                            }
                        }
                    }
                    else {
                        foreach ($order->orderDetails as $key => $orderDetail) {
                            $orderDetail->payment_status = 'paid';
                            $orderDetail->save();
                            if($orderDetail->product->user->user_type == 'seller'){
                                $seller = $orderDetail->product->user->seller;
                                $seller->admin_to_pay = $seller->admin_to_pay + $orderDetail->price + $orderDetail->tax + $orderDetail->shipping_cost;
                                $seller->save();
                            }
                        }
                    }

                    $order->commission_calculated = 1;
                    $order->save();

                   return response()->json([ 
                        'next' =>  route('order.done')
                    ]);
                }    
                else
                {
                   return response()->json([ 
                        'next' =>  route('checkout.payment_info')
                    ]); 
                }
                

            
            } catch (\Exception $e) { 
                    return response()->json([ 
                        'next' =>  route('checkout.payment_info')
                    ]);
            } 

   }

   public function MobilePayment(Request $request){
        
    }
}