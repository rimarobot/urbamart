<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShippingZone;
use App\ShippingZoneLocation;

use Illuminate\Support\Facades\Log;

class ZoneController extends Controller
{
    public function index(Request $request)
    {  
        $sort_search =null;
        $zones = ShippingZone::orderBy('created_at', 'desc'); 
        if ($request->has('search')){
            $sort_search = $request->search;
            $zones = $zones->where('name', 'like', '%'.$sort_search.'%');
        }
        $zones = $zones->paginate(10); 
        return view('shipping.zones.index', compact('zones'));
    }

    public function create()
    {
         return view('shipping.zones.create');
    }

    public function show($id)
    {
        //
    }

    public function store(Request $request)
    {
        $zone = new ShippingZone;
        $zone->name = $request->name;
        $zone->location_code = $request->country;
        $zone->order = $request->order; 
        if($zone->save()){
            foreach ($request->regions as $key => $region) {
                $loc = new ShippingZoneLocation;
                $loc->zone_id = $zone->id;
                $loc->location_code = $region ;
                $loc->type = (json_decode($region, true))['type'] ; 
                $loc->save(); 
            }

            flash(__('Shipping Zone has been inserted successfully'))->success();
            return redirect()->route('zones.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    public function edit($id)
    {
        $zone = ShippingZone::findOrFail(decrypt($id));
        return view('shipping.zones.edit', compact('zone'));
    }


    public function update(Request $request, $id)
    {
        $zone = ShippingZone::findOrFail($id);
        $zone->name = $request->name;
        $zone->location_code = $request->country;
        $zone->order = $request->order;
        if($zone->save()){
            ShippingZoneLocation::where('zone_id', $zone->id)->delete();
             foreach ($request->regions as $key => $region) {
                $loc = new ShippingZoneLocation;
                $loc->zone_id = $zone->id;
                $loc->location_code = $region ;
                $loc->type = (json_decode($region, true))['type'] ; 
                $loc->save(); 
            }
            flash(__('Shipping Zone has been updated successfully'))->success();
            return redirect()->route('zones.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    public function destroy($id)
    {
        if(ShippingZone::destroy($id)){
            flash(__('Shipping Zone has been deleted successfully'))->success();
            return redirect()->route('zones.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    public function update_status(Request $request)
    {
        $zone = ShippingZone::findOrFail($request->id);
        $zone->status = $request->status;
        if($zone->save()){
            return 1;
        }
        return 0;
    }
}