<?php 

namespace App\ZapPay;

class XpressPay {
 

    protected $endpoint = 'https://orientbank.test.gateway.mastercard.com/api/rest/';
    protected $api_username = null;
    protected $api_password = null;
    protected $version  = null;
    protected $merchant_id = null;

  
    public function __construct($api_username, $api_password, $version, $merchant_id, $endpoint=null) 
    {
        $this->api_username = (string) $api_username;
        $this->api_password = (string) $api_password;
        $this->version = (string) $version;
        $this->merchant_id = (string) $merchant_id;
        if(!is_null($endpoint)){
            $this->endpoint = (string) $endpoint;   
        }
    }

      
    public function CreateSession($data)
    { 
        $path =  $this->endpoint . 'version/' . $this->version . '/merchant/' . $this->merchant_id . '/session';
        $curl = $this->initCurlObj($path); 
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($data)));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: Application/json;charset=UTF-8"));

        $response = curl_exec($curl);        
        $headers = curl_getinfo($curl);
        $error_number = curl_errno($curl);
        $error_message = curl_error($curl);         
        curl_close($curl); 

        if($error_number != 0){              
                $response = json_encode([ 
                                'result' => 'ERROR',
                                'error' => [
                                    'cause' => $error_number,
                                    'explanation' => $error_message 
                                ] 
                            ]); 
        } 
        return $this->prettify($response);
    }

    public function RetriveOrder($orderId)
    {
        $path =  $this->endpoint . 'version/' . $this->version . '/merchant/' . $this->merchant_id . '/order/'.$orderId;
                
        $curl = $this->initCurlObj($path);
        curl_setopt($curl, CURLOPT_HTTPGET, 1); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);       

        $response = curl_exec($curl);        
        $headers = curl_getinfo($curl);

        $error_number = curl_errno($curl);
        $error_message = curl_error($curl);

        curl_close($curl);

        if($error_number != 0){              
                $response = json_encode( [ 
                                'result' => 'ERROR',
                                'error' => [
                                    'cause' => $error_number,
                                    'explanation' => $error_message 
                                ] 
                            ]); 
        } 
 
        return $this->prettify($response);
    }


    private function initCurlObj($url)
    {
        $curlObj = curl_init();

        curl_setopt($curlObj, CURLOPT_URL, $url);
        curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curlObj, CURLOPT_FAILONERROR, TRUE); 
        curl_setopt($curlObj, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, FALSE); 
        curl_setopt($curlObj, CURLOPT_USERPWD, $this->api_username . ":" . $this->api_password); 

        return $curlObj;
    }

    private function prettify($json)
    {
        return $json;//, JSON_PRETTY_PRINT);
    }

   
}
 