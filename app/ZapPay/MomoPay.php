<?php

namespace App\ZapPay;


use Illuminate\Support\Facades\Log;
class MoMoPay
{

    protected $endpoint = 'https://sandbox.momodeveloper.mtn.com';
    protected $api_username = null;
    protected $api_password = null;
    protected $api_key  = null;
    protected $testmode = false;

    public function __construct($api_username, $api_password, $api_key, $testmode,  $endpoint=null){
        
        $this->api_username = (string) $api_username;
        $this->api_password = (string) $api_password; 
        $this->api_key = (string) $api_key;
         if(!is_null($testmode) && $testmode =="1") 
            $this->testmode = true;  
        
        $this->testmode = (string) $testmode;
        if(!is_null($endpoint)){
            $this->endpoint = (string) $endpoint;   
        }
    } 

    private function prettify($json)
    {
        return json_encode(json_decode($json), JSON_PRETTY_PRINT);
    }

    private function initCurlObj($path,$headers)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $path);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_FAILONERROR, TRUE);

        if ($headers && !empty($headers)) 
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);

        return $curl;
    }

    public function get_guid() {
        $data = PHP_MAJOR_VERSION < 7 ? openssl_random_pseudo_bytes(16) : random_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);    // Set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);    // Set bits 6-7 to 10
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    private function post_token(){
			$url = $this->endpoint . '/collection/token/';  
			$headers =  [
				'Authorization:Basic ' . base64_encode( $this->api_username . ':'. $this->api_password ),
				'Ocp-Apim-Subscription-Key:' . $this->api_key 
            ];   
			$curl =  $this->initCurlObj($url, $headers);
			curl_setopt($curl, CURLOPT_POST, 1);
			$response = curl_exec($curl); 
			    
			if (curl_error($curl)){
				$jsonResponse = '{"access_token":"", "error":'.curl_error($curl).'}';  
			}   

			curl_close($curl);
			return $this->prettify($response);
	}


    public function CreateSession($data, $reference_id){
		 
        $response = '';
        $token_resp_encoded 	= $this->post_token();  
        $token_resp_decoded 	= json_decode($token_resp_encoded, true);
        
        if($token_resp_decoded && !empty($token_resp_decoded['access_token'])){
            
            $url = $this->endpoint . '/collection/v1_0/requesttopay'; 
            $headers =  [
                'Authorization:Bearer ' . $token_resp_decoded['access_token'], 
                'X-Reference-Id:'. $reference_id,
                'X-Target-Environment:'. ($this->testmode ? 'sandbox' : 'production'),
                'Content-Type: application/json',
                'Content-Length: '. strlen($data),
                'Ocp-Apim-Subscription-Key:' . $this->api_key					
            ]; 
            $curl =  $this->initCurlObj($url, $headers);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data); 
            
            $response = curl_exec($curl);
            $code = curl_getinfo($curl, CURLINFO_RESPONSE_CODE);
            $error_number = curl_errno($curl);
            $error_message = curl_error($curl);
            curl_close($curl);

            if( $error_number != 0)  { 
                 $response = json_encode([ 
                                'status' => 'failed',
                                'error' => [
                                    'cause' => $error_number,
                                    'explanation' => $error_message 
                                ] 
                            ]);
            }
            else if ($code == '202' ){
                $response = json_encode([ 
                                'status' => 'success',
                                'error' => [
                                    'cause' => $error_number,
                                    'explanation' => $error_message 
                                ] 
                            ]);
            }  
               
        }  
        return $response;			
    }

    public function RetriveOrder($reference_id){
            
        $token_resp_encoded 	= $this->post_token();
        $token_resp_decodeed 	= json_decode($token_resp_encoded, true);
        if($token_resp_decodeed && !empty($token_resp_decodeed['access_token'])){           
            
            $url = $this->endpoint . '/collection/v1_0/requesttopay/' . $reference_id; 
            $headers =  [
                'Authorization:Bearer ' . $token_resp_decodeed['access_token'],
                'X-Target-Environment:'. ($this->testmode ? 'sandbox' : 'production'), 
                'Ocp-Apim-Subscription-Key:' . $this->api_key					
            ];

            $curl =  $this->initCurlObj($url, $headers);
            curl_setopt($curl, CURLOPT_HTTPGET, 1);
            
            $response = curl_exec($curl); 
            $error_number = curl_errno($curl);
            $error_message = curl_error($curl);
            curl_close($curl);
            

            if($error_number != 0){
                 $response = '{"status": "ERROR", "error":' . $error_message . '}';
            }  
            
            //sample response
            /*
            '{ "amount": 100, "currency": "UGX", "financialTransactionId": 23503452,
                "externalId": 947354,
                "payer": { "partyIdType": "MSISDN", "partyId": 4656473839 },
                "status": "SUCCESSFUL"
             }'
             */            


            return $this->prettify($response);
        }

    }


}