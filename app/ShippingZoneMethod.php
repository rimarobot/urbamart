<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingZoneMethod extends Model
{
   protected $table = 'shipping_zone_methods';

   public function zone()
   {
        return $this->belongsTo('App\ShippingZone','zone_id','id' );
   }
   
   public function method()
   {
        return $this->belongsTo('App\ShippingMethod','method_id','id' );
   }

   public function company()
   {
        return $this->belongsTo('App\ShippingCompany','company_id','id' );
   }
   
}
