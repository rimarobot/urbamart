<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverPayment extends Model
{
    protected $table = 'shipping_driver_payments';
}
