<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingDriver extends Model
{
    protected $table = 'shipping_company_drivers';
    protected $key ='id';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function company()
    {
        return $this->belongsTo('App\ShippingCompany','company_id','id' );
    }

    public function orders()
    {
        return $this->hasMany('App\DriverOrder','driver_id','id' );
    }
}
