<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingCompany extends Model
{
   protected $table = 'shipping_company';

   public function zone_methods(){
        return $this->hasMany('App\ShippingZoneMethod','company_id','id' );
    }
}
