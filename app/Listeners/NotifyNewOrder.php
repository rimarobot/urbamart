<?php

namespace App\Listeners;

use PDF;
use Mail;
use App\Order;
use App\BusinessSetting;
use App\Shop;
use App\Events\NewOrder;
use App\Mail\InvoiceEmailManager;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\OTPVerificationController;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifyNewOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewOrder  $event
     * @return void
     */
    public function handle(NewOrder $event)
    {
            Log::info('$event->order: '.$event->order );
            $order = Order::findOrFail($event->order->id);
            //send customer order notication
            if(json_decode($order->shipping_address)->phone != null){
                $sms = "Hello ".substr(json_decode($order->shipping_address)->name, 0, 20).", thank you for placing your order #".$order->code." " .single_price($order->grand_total). " with ".env('APP_NAME').".";
                if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated && \App\OtpConfiguration::where('type', 'otp_for_order')->first()->value){

                    Log::info('$sms: '. sendSMS(str_replace('+', '', json_decode($order->shipping_address)->phone),'', $sms ));
                }
            }
            //send store notification
            foreach ($order->orderDetails()->distinct()->get('seller_id') as $seller) {
                $sub_order = $order->orderDetails()->where('seller_id',$seller->seller_id )->get();
                $shop = Shop::where('user_id', $seller->seller_id)->first();
                if(!empty($shop)){
                $sms = substr($shop->name, 0, 20).": You have a new order #".$order->code." for valued ".single_price($sub_order->sum('price')).". Login for details.";
                if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated && \App\OtpConfiguration::where('type', 'otp_for_order')->first()->value){

                    if(!empty($shop->phone))
                    {
                        try
                        {
                             Log::info('$sms: '.sendSMS(str_replace('+', '', $shop->phone),'',$sms) );
                        } catch (\Exception $e) {
                            Log::info($e);
                        }
                    }

                }
            }

            }



            // $array['view'] = 'emails.invoice';
            // $array['subject'] = 'Your order has been placed - '.$order->code;
            // $array['from'] = env('MAIL_USERNAME');
            // $array['order'] = $order;

            // foreach($seller_products as $key => $seller_product){
            //     try {
            //         Mail::to(\App\User::find($key)->email)->queue(new InvoiceEmailManager($array));
            //     } catch (\Exception $e) {

            //     }
            // }



            //sends email to customer with the invoice pdf attached
            // if(env('MAIL_USERNAME') != null){
            //     try {
            //         Mail::to($request->session()->get('shipping_info')['email'])->queue(new InvoiceEmailManager($array));
            //         Mail::to(User::where('user_type', 'admin')->first()->email)->queue(new InvoiceEmailManager($array));
            //     } catch (\Exception $e) {

            //     }
            // }

    }
}
