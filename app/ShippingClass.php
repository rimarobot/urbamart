<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingClass extends Model
{
   protected $table = 'shipping_classes';
}
