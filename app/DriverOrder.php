<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverOrder extends Model
{
    protected $table = 'shipping_driver_orders';
    protected $key ='id';

    public function driver()
    {
        return $this->belongsTo('App\ShippingDriver','driver_id','id' );
    }
}
