-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE TABLE IF NOT EXISTS `default_schema`.`affiliate_logs` (
  `id` INT(11) NOT NULL,
  `user_id` INT(11) NULL DEFAULT NULL,
  `guest_id` INT(11) NULL DEFAULT NULL,
  `referred_by_user` INT(11) NOT NULL,
  `amount` DOUBLE(20,2) NOT NULL,
  `order_id` BIGINT(20) NULL DEFAULT NULL,
  `order_detail_id` BIGINT(20) NULL DEFAULT NULL,
  `affiliate_type` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `status` TINYINT(4) NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  `updated_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `default_schema`.`affiliate_stats` (
  `id` INT(11) NOT NULL,
  `affiliate_user_id` INT(11) NOT NULL,
  `no_of_click` INT(11) NOT NULL DEFAULT 0,
  `no_of_order_item` INT(11) NOT NULL DEFAULT 0,
  `no_of_delivered` INT(11) NOT NULL DEFAULT 0,
  `no_of_cancel` INT(11) NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  `updated_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `default_schema`.`blogs` (
  `id` BIGINT(20) UNSIGNED NOT NULL,
  `category_id` INT(11) NOT NULL,
  `title` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `slug` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `short_description` TEXT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  `description` LONGTEXT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  `banner` INT(11) NULL DEFAULT NULL,
  `meta_title` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  `meta_img` INT(11) NULL DEFAULT NULL,
  `meta_description` TEXT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  `meta_keywords` TEXT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  `status` INT(1) NOT NULL DEFAULT 1,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NULL DEFAULT NULL,
  `deleted_at` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `default_schema`.`blog_categories` (
  `id` BIGINT(20) UNSIGNED NOT NULL,
  `category_name` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `slug` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NULL DEFAULT NULL,
  `deleted_at` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

ALTER TABLE `default_schema`.`carts` 
ADD COLUMN `owner_id` INT(11) NULL DEFAULT NULL AFTER `id`,
ADD COLUMN `address_id` INT(11) NOT NULL DEFAULT 0 AFTER `user_id`,
ADD COLUMN `discount` DOUBLE(10,2) NOT NULL DEFAULT 0.00 AFTER `shipping_cost`,
ADD COLUMN `coupon_code` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL AFTER `discount`,
ADD COLUMN `coupon_applied` TINYINT(4) NOT NULL DEFAULT 0 AFTER `coupon_code`;

ALTER TABLE `default_schema`.`categories` 
ADD COLUMN `order_level` INT(11) NOT NULL DEFAULT 0 AFTER `name`,
ADD INDEX `slug` (`slug` ASC);
;

CREATE TABLE IF NOT EXISTS `default_schema`.`commission_histories` (
  `id` INT(11) NOT NULL,
  `order_id` INT(11) NOT NULL,
  `order_detail_id` INT(11) NOT NULL,
  `seller_id` INT(11) NOT NULL,
  `admin_commission` DOUBLE(25,2) NOT NULL,
  `seller_earning` DOUBLE(25,2) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  `updated_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `default_schema`.`general_settings` 
ADD COLUMN `footer_logo` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL AFTER `logo`;

ALTER TABLE `default_schema`.`oauth_access_tokens` 
CHANGE COLUMN `user_id` `user_id` INT(11) NULL DEFAULT NULL ,
CHANGE COLUMN `name` `name` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL ;

ALTER TABLE `default_schema`.`oauth_auth_codes` 
CHANGE COLUMN `user_id` `user_id` INT(11) NOT NULL ;

ALTER TABLE `default_schema`.`oauth_clients` 
CHANGE COLUMN `user_id` `user_id` INT(11) NULL DEFAULT NULL ,
CHANGE COLUMN `name` `name` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ;

ALTER TABLE `default_schema`.`orders` 
ADD COLUMN `seller_id` INT(11) NULL DEFAULT NULL AFTER `guest_id`,
ADD COLUMN `delivery_status` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT 'pending' AFTER `shipping_address`;

ALTER TABLE `default_schema`.`page_translations` 
CHANGE COLUMN `content` `content` LONGTEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ;

ALTER TABLE `default_schema`.`products` 
ADD COLUMN `stock_visibility_state` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT 'quantity' AFTER `published`,
ADD COLUMN `cash_on_delivery` TINYINT(1) NOT NULL DEFAULT 1 AFTER `stock_visibility_state`,
ADD COLUMN `seller_featured` INT(11) NOT NULL DEFAULT 0 AFTER `featured`,
ADD COLUMN `low_stock_quantity` INT(11) NULL DEFAULT NULL AFTER `min_qty`,
CHANGE COLUMN `subcategory_id` `subcategory_id` INT(11) NULL DEFAULT NULL ,
CHANGE COLUMN `tags` `tags` VARCHAR(1000) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
ADD INDEX `name` (`name` ASC),
ADD INDEX `tags` (`tags`(255) ASC);
;

ALTER TABLE `default_schema`.`product_stocks` 
ADD COLUMN `image` INT(11) NULL DEFAULT NULL AFTER `qty`;

CREATE TABLE IF NOT EXISTS `default_schema`.`product_taxes` (
  `id` INT(11) NOT NULL,
  `product_id` INT(11) NOT NULL,
  `tax_id` INT(11) NOT NULL,
  `tax` DOUBLE(18,2) NOT NULL,
  `tax_type` VARCHAR(10) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  `updated_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `default_schema`.`refund_requests` 
ADD COLUMN `reject_reason` LONGTEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL AFTER `refund_status`;

ALTER TABLE `default_schema`.`sellers` 
ADD UNIQUE INDEX `user_id` (`user_id` ASC);
;

CREATE TABLE IF NOT EXISTS `default_schema`.`shipping_company_drivers` (
  `id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `company_id` INT(11) NULL DEFAULT 0,
  `admin_to_pay` DECIMAL(18,2) NOT NULL DEFAULT 0.00,
  `identity_document_number` VARCHAR(45) NOT NULL,
  `identity_type` VARCHAR(45) NOT NULL,
  `vehicle_identification_number` VARCHAR(45) NOT NULL,
  `vehicle_type` VARCHAR(45) NOT NULL,
  `verification_info` LONGTEXT NULL DEFAULT NULL,
  `cash_on_delivery_status` INT(11) NOT NULL DEFAULT 0,
  `mobile_payment_number` VARCHAR(45) NULL DEFAULT NULL,
  `mobile_payment_name` VARCHAR(100) NULL DEFAULT NULL,
  `mobile_payment_status` INT(11) NOT NULL DEFAULT 0,
  `bank_payment_status` INT(11) NOT NULL DEFAULT 0,
  `bank_acc_number` VARCHAR(50) NULL DEFAULT NULL,
  `bank_acc_name` VARCHAR(100) NULL DEFAULT NULL,
  `bank_name` VARCHAR(100) NULL DEFAULT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `default_schema`.`shipping_driver_orders` (
  `id` INT(11) NOT NULL,
  `order_id` INT(11) NOT NULL,
  `driver_id` INT(11) NULL DEFAULT NULL,
  `company_id` INT(11) NOT NULL DEFAULT 0,
  `order_price` DECIMAL(18,2) NOT NULL,
  `shop` VARCHAR(200) NULL DEFAULT NULL,
  `qty` INT(11) NULL DEFAULT NULL,
  `shipping_cost` DECIMAL(18,2) NOT NULL,
  `shipping_address` VARCHAR(255) NOT NULL,
  `payment_type` VARCHAR(255) NOT NULL,
  `payment_status` VARCHAR(50) NOT NULL,
  `order_code` VARCHAR(100) NOT NULL,
  `shipping_type` VARCHAR(255) NOT NULL,
  `status` VARCHAR(50) NOT NULL,
  `status_description` TEXT NULL DEFAULT NULL,
  `signature` LONGBLOB NULL DEFAULT NULL,
  `signature_name` VARCHAR(100) NULL DEFAULT NULL,
  `signature_identity` VARCHAR(100) NULL DEFAULT NULL,
  `signature_contact` VARCHAR(50) NULL DEFAULT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `driver_order_idx` (`driver_id` ASC),
  CONSTRAINT `driver_order`
    FOREIGN KEY (`driver_id`)
    REFERENCES `default_schema`.`shipping_company_drivers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `default_schema`.`shipping_driver_payments` (
  `id` INT(11) NOT NULL,
  `driver_id` INT(11) NOT NULL,
  `amount` DECIMAL(18,2) NOT NULL,
  `payment_details` LONGTEXT NOT NULL,
  `payment_method` VARCHAR(255) NOT NULL,
  `payment_reference` VARCHAR(255) NOT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  INDEX `driver_payment_idx` (`driver_id` ASC),
  CONSTRAINT `driver_payment`
    FOREIGN KEY (`driver_id`)
    REFERENCES `default_schema`.`shipping_company_drivers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

ALTER TABLE `default_schema`.`shops` 
CHANGE COLUMN `sliders` `sliders` LONGTEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL AFTER `logo`;

ALTER TABLE `default_schema`.`staff` 
ADD COLUMN `company_id` INT(11) NOT NULL DEFAULT 0 AFTER `role_id`;

CREATE TABLE IF NOT EXISTS `default_schema`.`taxes` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `tax_status` TINYINT(1) NOT NULL DEFAULT 1,
  `created_at` TIMESTAMP NULL DEFAULT current_timestamp(),
  `updated_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `default_schema`.`transaction_payments` (
  `id` INT(11) UNSIGNED NOT NULL,
  `transaction_id` INT(11) NOT NULL,
  `internal_reference` VARCHAR(100) NOT NULL,
  `external_reference` VARCHAR(100) NULL DEFAULT NULL,
  `payment_reciept` VARCHAR(45) NULL DEFAULT NULL,
  `status` VARCHAR(45) NULL DEFAULT NULL,
  `status_description` LONGTEXT NULL DEFAULT NULL,
  `amount` DECIMAL(18,2) NOT NULL,
  `currency` VARCHAR(10) NOT NULL,
  `channel_type` VARCHAR(100) NOT NULL,
  `channel_name` VARCHAR(100) NOT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
