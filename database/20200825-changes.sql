+--------------------------------------------+
| Catalog Diff Report                        |
+--------------------------------------------+
Table `default_schema`.`affiliate_logs` was created
  columns:
  - id of type INT
  - user_id of type INT
  - guest_id of type INT
  - referred_by_user of type INT
  - amount of type DOUBLE
  - order_id of type BIGINT
  - order_detail_id of type BIGINT
  - affiliate_type of type VARCHAR
  - status of type TINYINT
  - created_at of type TIMESTAMP
  - updated_at of type TIMESTAMP
  __
  indices:
  - PRIMARY with columns: id
  __
  attributes:
  - engine: InnoDB
  - default character set: utf8
  - default collate: utf8_unicode_ci
  __
Table `default_schema`.`affiliate_stats` was created
  columns:
  - id of type INT
  - affiliate_user_id of type INT
  - no_of_click of type INT
  - no_of_order_item of type INT
  - no_of_delivered of type INT
  - no_of_cancel of type INT
  - created_at of type TIMESTAMP
  - updated_at of type TIMESTAMP
  __
  indices:
  - PRIMARY with columns: id
  __
  attributes:
  - engine: InnoDB
  - default character set: utf8
  __
Table `default_schema`.`blogs` was created
  columns:
  - id of type BIGINT
  - category_id of type INT
  - title of type VARCHAR
  - slug of type VARCHAR
  - short_description of type TEXT
  - description of type LONGTEXT
  - banner of type INT
  - meta_title of type VARCHAR
  - meta_img of type INT
  - meta_description of type TEXT
  - meta_keywords of type TEXT
  - status of type INT
  - created_at of type TIMESTAMP
  - updated_at of type TIMESTAMP
  - deleted_at of type TIMESTAMP
  __
  indices:
  - PRIMARY with columns: id
  __
  attributes:
  - engine: InnoDB
  - default character set: utf8mb4
  - default collate: utf8mb4_unicode_ci
  __
Table `default_schema`.`blog_categories` was created
  columns:
  - id of type BIGINT
  - category_name of type VARCHAR
  - slug of type VARCHAR
  - created_at of type TIMESTAMP
  - updated_at of type TIMESTAMP
  - deleted_at of type TIMESTAMP
  __
  indices:
  - PRIMARY with columns: id
  __
  attributes:
  - engine: InnoDB
  - default character set: utf8mb4
  - default collate: utf8mb4_unicode_ci
  __
Table `default_schema`.`commission_histories` was created
  columns:
  - id of type INT
  - order_id of type INT
  - order_detail_id of type INT
  - seller_id of type INT
  - admin_commission of type DOUBLE
  - seller_earning of type DOUBLE
  - driver_earning of type DECIMAL
  - created_at of type TIMESTAMP
  - updated_at of type TIMESTAMP
  __
  indices:
  - PRIMARY with columns: id
  __
  attributes:
  - engine: InnoDB
  - default character set: utf8
  __
Table `default_schema`.`notifications` was created
  columns:
  - id of type CHAR
  - type of type VARCHAR
  - notifiable_type of type VARCHAR
  - notifiable_id of type BIGINT
  - data of type TEXT
  - read_at of type TIMESTAMP
  - created_at of type TIMESTAMP
  - updated_at of type TIMESTAMP
  __
  indices:
  - PRIMARY with columns: id
  - notifications_notifiable_type_notifiable_id_index with columns: notifiable_type, notifiable_id
  __
  attributes:
  - engine: InnoDB
  - default character set: utf8mb4
  - default collate: utf8mb4_unicode_ci
  __
Table `default_schema`.`product_taxes` was created
  columns:
  - id of type INT
  - product_id of type INT
  - tax_id of type INT
  - tax of type DOUBLE
  - tax_type of type VARCHAR
  - created_at of type TIMESTAMP
  - updated_at of type TIMESTAMP
  __
  indices:
  - PRIMARY with columns: id
  __
  attributes:
  - engine: InnoDB
  - default character set: utf8
  __
Table `default_schema`.`shipping_company_drivers` was created
  columns:
  - id of type INT
  - user_id of type INT
  - company_id of type INT
  - admin_to_pay of type DECIMAL
  - identity_document_number of type VARCHAR
  - identity_type of type VARCHAR
  - vehicle_identification_number of type VARCHAR
  - vehicle_type of type VARCHAR
  - verification_info of type LONGTEXT
  - cash_on_delivery_status of type INT
  - mobile_payment_number of type VARCHAR
  - mobile_payment_name of type VARCHAR
  - mobile_payment_status of type INT
  - bank_payment_status of type INT
  - bank_acc_number of type VARCHAR
  - bank_acc_name of type VARCHAR
  - bank_name of type VARCHAR
  - created_at of type TIMESTAMP
  - updated_at of type TIMESTAMP
  __
  indices:
  - PRIMARY with columns: id
  - id_UNIQUE with columns: id
  __
  attributes:
  - engine: InnoDB
  - default character set: utf8mb4
  __
Table `default_schema`.`shipping_driver_orders` was created
  columns:
  - id of type INT
  - order_id of type INT
  - driver_id of type INT
  - company_id of type INT
  - order_price of type DECIMAL
  - shop of type TEXT
  - qty of type INT
  - shipping_cost of type DECIMAL
  - shipping_address of type TEXT
  - payment_type of type VARCHAR
  - payment_status of type VARCHAR
  - order_code of type VARCHAR
  - shipping_type of type VARCHAR
  - status of type VARCHAR
  - status_description of type TEXT
  - signature of type LONGBLOB
  - signature_name of type VARCHAR
  - signature_identity of type VARCHAR
  - signature_contact of type VARCHAR
  - created_at of type TIMESTAMP
  - updated_at of type TIMESTAMP
  __
  indices:
  - PRIMARY with columns: id
  - id_UNIQUE with columns: id
  - driver_order_idx with columns: driver_id
  __
  foreign keys:
  - driver_order with columns: driver_id, referred table: shipping_company_drivers with columns: id
    - action on update: NO ACTION
    - action on delete: NO ACTION
  __
  attributes:
  - engine: InnoDB
  - default character set: utf8mb4
  __
Table `default_schema`.`shipping_driver_payments` was created
  columns:
  - id of type INT
  - driver_id of type INT
  - amount of type DECIMAL
  - payment_details of type LONGTEXT
  - payment_method of type VARCHAR
  - payment_reference of type VARCHAR
  - created_at of type TIMESTAMP
  - updated_at of type TIMESTAMP
  __
  indices:
  - PRIMARY with columns: id
  - driver_payment_idx with columns: driver_id
  __
  foreign keys:
  - driver_payment with columns: driver_id, referred table: shipping_company_drivers with columns: id
    - action on update: NO ACTION
    - action on delete: NO ACTION
  __
  attributes:
  - engine: InnoDB
  - default character set: utf8mb4
  __
Table `default_schema`.`taxes` was created
  columns:
  - id of type INT
  - name of type VARCHAR
  - tax_status of type TINYINT
  - created_at of type TIMESTAMP
  - updated_at of type TIMESTAMP
  __
  indices:
  - PRIMARY with columns: id
  __
  attributes:
  - engine: InnoDB
  - default character set: utf8
  __
Table `default_schema`.`transaction_payments` was created
  columns:
  - id of type INT
  - transaction_id of type INT
  - internal_reference of type VARCHAR
  - external_reference of type VARCHAR
  - payment_reciept of type VARCHAR
  - status of type VARCHAR
  - status_description of type LONGTEXT
  - amount of type DECIMAL
  - currency of type VARCHAR
  - channel_type of type VARCHAR
  - channel_name of type VARCHAR
  - created_at of type TIMESTAMP
  - updated_at of type TIMESTAMP
  __
  indices:
  - PRIMARY with columns: id
  - id_UNIQUE with columns: id
  __
  attributes:
  - engine: InnoDB
  - default character set: utf8mb4
  __
Table `default_schema`.`addresses` was modified
  columns:
  - added column longitude of type FLOAT(11)
  - added column latitude of type FLOAT(11)
  __
Table `default_schema`.`carts` was modified
  columns:
  - added column owner_id of type INT(11)
  - added column temp_user_id of type VARCHAR(255)
  - added column address_id of type INT(11)
  - added column shipping_type of type VARCHAR(30)
  - added column pickup_point of type INT(11)
  - added column discount of type DOUBLE(10,2)
  - added column product_referral_code of type VARCHAR(255)
  - added column coupon_code of type VARCHAR(255)
  - added column coupon_applied of type TINYINT(4)
  - modified column price
  - modified column tax
  - modified column shipping_cost
  __
Table `default_schema`.`categories` was modified
  columns:
  - added column order_level of type INT(11)
  __
  indices:
  - added index slug with columns: slug
  __
Table `default_schema`.`general_settings` was modified
  columns:
  - added column footer_logo of type VARCHAR(255)
  __
Table `default_schema`.`oauth_access_tokens` was modified
  columns:
  - modified column user_id
  - modified column name
  __
Table `default_schema`.`oauth_auth_codes` was modified
  columns:
  - modified column user_id
  __
Table `default_schema`.`oauth_clients` was modified
  columns:
  - modified column user_id
  - modified column name
  __
Table `default_schema`.`orders` was modified
  columns:
  - added column seller_id of type INT(11)
  - added column delivery_status of type VARCHAR(20)
  __
Table `default_schema`.`page_translations` was modified
  columns:
  - modified column content
  __
Table `default_schema`.`products` was modified
  columns:
  - added column approved of type TINYINT(1)
  - added column stock_visibility_state of type VARCHAR(10)
  - added column cash_on_delivery of type TINYINT(1)
  - added column seller_featured of type INT(11)
  - added column low_stock_quantity of type INT(11)
  - modified column subcategory_id
  - modified column tags
  __
  indices:
  - added index name with columns: name
  - added index tags with columns: tags
  __
Table `default_schema`.`product_stocks` was modified
  columns:
  - added column image of type INT(11)
  __
Table `default_schema`.`refund_requests` was modified
  columns:
  - added column reject_reason of type LONGTEXT
  __
Table `default_schema`.`sellers` was modified
  columns:
  - added column recipient_name of type VARCHAR(100)
  - added column recipient_phone of type VARCHAR(20)
  - added column mobile_payment_status of type INT(11)
  - added column registered_name of type VARCHAR(100)
  - added column registered_phone of type VARCHAR(20)
  __
  indices:
  - added index user_id with columns: user_id
  __
Table `default_schema`.`seller_withdraw_requests` was modified
  columns:
  - modified column message
  __
Table `default_schema`.`shops` was modified
  columns:
  - modified column sliders
  __
Table `default_schema`.`staff` was modified
  columns:
  - added column company_id of type INT(11)
  __
Table `default_schema`.`users` was modified
  columns:
  - added column device_token of type VARCHAR(255)
  __
----------------------------------------------
End of MySQL Workbench Report
