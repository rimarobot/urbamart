ALTER TABLE `products` ADD `discount_start_date` INT NULL DEFAULT NULL AFTER `discount_type`, ADD `discount_end_date` INT NULL DEFAULT NULL AFTER `discount_start_date`;

ALTER TABLE `products` DROP `subcategory_id`, DROP `subsubcategory_id`;
ALTER TABLE `categories` ADD INDEX(`slug`);

ALTER TABLE `products` ADD INDEX(`name`);
ALTER TABLE `products` CHANGE `tags` `tags` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

ALTER TABLE `products` ADD INDEX(`tags`);
ALTER TABLE `products` ADD `seller_featured` INT NOT NULL DEFAULT '0' AFTER `featured`;
ALTER TABLE `sellers` ADD UNIQUE(`user_id`);
