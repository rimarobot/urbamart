-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `addresses`
ADD COLUMN `longitude` FLOAT(11) NULL DEFAULT NULL AFTER `city`,
ADD COLUMN `latitude` FLOAT(11) NULL DEFAULT NULL AFTER `longitude`;

CREATE TABLE IF NOT EXISTS `affiliate_logs` (
  `id` INT(11) NOT NULL,
  `user_id` INT(11) NULL DEFAULT NULL,
  `guest_id` INT(11) NULL DEFAULT NULL,
  `referred_by_user` INT(11) NOT NULL,
  `amount` DOUBLE(20,2) NOT NULL,
  `order_id` BIGINT(20) NULL DEFAULT NULL,
  `order_detail_id` BIGINT(20) NULL DEFAULT NULL,
  `affiliate_type` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `status` TINYINT(4) NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  `updated_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `affiliate_stats` (
  `id` INT(11) NOT NULL,
  `affiliate_user_id` INT(11) NOT NULL,
  `no_of_click` INT(11) NOT NULL DEFAULT 0,
  `no_of_order_item` INT(11) NOT NULL DEFAULT 0,
  `no_of_delivered` INT(11) NOT NULL DEFAULT 0,
  `no_of_cancel` INT(11) NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  `updated_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `blogs` (
  `id` BIGINT(20) UNSIGNED NOT NULL,
  `category_id` INT(11) NOT NULL,
  `title` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `slug` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `short_description` TEXT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  `description` LONGTEXT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  `banner` INT(11) NULL DEFAULT NULL,
  `meta_title` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  `meta_img` INT(11) NULL DEFAULT NULL,
  `meta_description` TEXT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  `meta_keywords` TEXT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  `status` INT(1) NOT NULL DEFAULT 1,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NULL DEFAULT NULL,
  `deleted_at` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `blog_categories` (
  `id` BIGINT(20) UNSIGNED NOT NULL,
  `category_name` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `slug` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NULL DEFAULT NULL,
  `deleted_at` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

ALTER TABLE `carts`
ADD COLUMN `owner_id` INT(11) NULL DEFAULT NULL AFTER `id`,
ADD COLUMN `temp_user_id` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL AFTER `user_id`,
ADD COLUMN `address_id` INT(11) NOT NULL DEFAULT 0 AFTER `temp_user_id`,
ADD COLUMN `shipping_type` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT '' AFTER `shipping_cost`,
ADD COLUMN `pickup_point` INT(11) NULL DEFAULT NULL AFTER `shipping_type`,
ADD COLUMN `discount` DOUBLE(10,2) NOT NULL DEFAULT 0.00 AFTER `pickup_point`,
ADD COLUMN `product_referral_code` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL AFTER `discount`,
ADD COLUMN `coupon_code` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL AFTER `product_referral_code`,
ADD COLUMN `coupon_applied` TINYINT(4) NOT NULL DEFAULT 0 AFTER `coupon_code`,
CHANGE COLUMN `price` `price` DOUBLE(8,2) NULL DEFAULT 0.00 ,
CHANGE COLUMN `tax` `tax` DOUBLE(8,2) NULL DEFAULT 0.00 ,
CHANGE COLUMN `shipping_cost` `shipping_cost` DOUBLE(8,2) NULL DEFAULT 0.00 ;

ALTER TABLE `categories`
ADD COLUMN `order_level` INT(11) NOT NULL DEFAULT 0 AFTER `name`,
ADD INDEX `slug` (`slug` ASC);
;

CREATE TABLE IF NOT EXISTS `commission_histories` (
  `id` INT(11) NOT NULL,
  `order_id` INT(11) NOT NULL,
  `order_detail_id` INT(11) NOT NULL,
  `seller_id` INT(11) NOT NULL,
  `admin_commission` DOUBLE(25,2) NOT NULL,
  `seller_earning` DOUBLE(25,2) NOT NULL,
  `driver_earning` DECIMAL(20,2) NOT NULL DEFAULT 0.00,
  `created_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  `updated_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `general_settings`
ADD COLUMN `footer_logo` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL AFTER `logo`;

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` CHAR(36) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `type` VARCHAR(191) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `notifiable_type` VARCHAR(191) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `notifiable_id` BIGINT(20) UNSIGNED NOT NULL,
  `data` TEXT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `read_at` TIMESTAMP NULL DEFAULT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `notifications_notifiable_type_notifiable_id_index` (`notifiable_type` ASC, `notifiable_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

ALTER TABLE `oauth_access_tokens`
CHANGE COLUMN `user_id` `user_id` INT(11) NULL DEFAULT NULL ,
CHANGE COLUMN `name` `name` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL ;

ALTER TABLE `oauth_auth_codes`
CHANGE COLUMN `user_id` `user_id` INT(11) NOT NULL ;

ALTER TABLE `oauth_clients`
CHANGE COLUMN `user_id` `user_id` INT(11) NULL DEFAULT NULL ,
CHANGE COLUMN `name` `name` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ;

ALTER TABLE `orders`
ADD COLUMN `seller_id` INT(11) NULL DEFAULT NULL AFTER `guest_id`,
ADD COLUMN `delivery_status` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT 'pending' AFTER `shipping_address`;

ALTER TABLE `page_translations`
CHANGE COLUMN `content` `content` LONGTEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ;

ALTER TABLE `products`
ADD COLUMN `approved` TINYINT(1) NOT NULL DEFAULT 1 AFTER `published`,
ADD COLUMN `stock_visibility_state` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT 'quantity' AFTER `approved`,
ADD COLUMN `cash_on_delivery` TINYINT(1) NOT NULL DEFAULT 1 AFTER `stock_visibility_state`,
ADD COLUMN `seller_featured` INT(11) NOT NULL DEFAULT 0 AFTER `featured`,
ADD COLUMN `low_stock_quantity` INT(11) NULL DEFAULT NULL AFTER `min_qty`,
CHANGE COLUMN `subcategory_id` `subcategory_id` INT(11) NULL DEFAULT NULL ,
CHANGE COLUMN `tags` `tags` VARCHAR(1000) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
ADD INDEX `name` (`name` ASC),
ADD INDEX `tags` (`tags`(255) ASC);
;

ALTER TABLE `product_stocks`
ADD COLUMN `image` INT(11) NULL DEFAULT NULL AFTER `qty`;

CREATE TABLE IF NOT EXISTS `product_taxes` (
  `id` INT(11) NOT NULL,
  `product_id` INT(11) NOT NULL,
  `tax_id` INT(11) NOT NULL,
  `tax` DOUBLE(18,2) NOT NULL,
  `tax_type` VARCHAR(10) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  `updated_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `refund_requests`
ADD COLUMN `reject_reason` LONGTEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL AFTER `refund_status`;

ALTER TABLE `sellers`
ADD COLUMN `recipient_name` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL AFTER `cash_on_delivery_status`,
ADD COLUMN `recipient_phone` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL AFTER `recipient_name`,
ADD COLUMN `mobile_payment_status` INT(11) NOT NULL DEFAULT 0 AFTER `bank_payment_status`,
ADD COLUMN `registered_name` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL AFTER `mobile_payment_status`,
ADD COLUMN `registered_phone` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL AFTER `registered_name`,
ADD UNIQUE INDEX `user_id` (`user_id` ASC);
;

ALTER TABLE `seller_withdraw_requests`
CHANGE COLUMN `message` `message` LONGTEXT CHARACTER SET 'utf8' NULL DEFAULT NULL ;

CREATE TABLE IF NOT EXISTS `shipping_company_drivers` (
  `id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `company_id` INT(11) NULL DEFAULT 0,
  `admin_to_pay` DECIMAL(18,2) NOT NULL DEFAULT 0.00,
  `identity_document_number` VARCHAR(45) NOT NULL,
  `identity_type` VARCHAR(45) NOT NULL,
  `vehicle_identification_number` VARCHAR(45) NOT NULL,
  `vehicle_type` VARCHAR(45) NOT NULL,
  `verification_info` LONGTEXT NULL DEFAULT NULL,
  `cash_on_delivery_status` INT(11) NOT NULL DEFAULT 0,
  `mobile_payment_number` VARCHAR(45) NULL DEFAULT NULL,
  `mobile_payment_name` VARCHAR(100) NULL DEFAULT NULL,
  `mobile_payment_status` INT(11) NOT NULL DEFAULT 0,
  `bank_payment_status` INT(11) NOT NULL DEFAULT 0,
  `bank_acc_number` VARCHAR(50) NULL DEFAULT NULL,
  `bank_acc_name` VARCHAR(100) NULL DEFAULT NULL,
  `bank_name` VARCHAR(100) NULL DEFAULT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `shipping_driver_orders` (
  `id` INT(11) NOT NULL,
  `order_id` INT(11) NOT NULL,
  `driver_id` INT(11) NULL DEFAULT NULL,
  `company_id` INT(11) NOT NULL DEFAULT 0,
  `order_price` DECIMAL(18,2) NOT NULL,
  `shop` TEXT NULL DEFAULT NULL,
  `qty` INT(11) NULL DEFAULT NULL,
  `shipping_cost` DECIMAL(18,2) NOT NULL,
  `shipping_address` TEXT NOT NULL,
  `payment_type` VARCHAR(255) NOT NULL,
  `payment_status` VARCHAR(50) NOT NULL,
  `order_code` VARCHAR(100) NOT NULL,
  `shipping_type` VARCHAR(255) NOT NULL,
  `status` VARCHAR(50) NOT NULL,
  `status_description` TEXT NULL DEFAULT NULL,
  `cleared` INT(11) NOT NULL DEFAULT 0,
  `signature` LONGBLOB NULL DEFAULT NULL,
  `signature_name` VARCHAR(100) NULL DEFAULT NULL,
  `signature_identity` VARCHAR(100) NULL DEFAULT NULL,
  `signature_contact` VARCHAR(50) NULL DEFAULT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `driver_order_idx` (`driver_id` ASC),
  CONSTRAINT `driver_order`
    FOREIGN KEY (`driver_id`)
    REFERENCES `shipping_company_drivers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `shipping_driver_payments` (
  `id` INT(11) NOT NULL,
  `driver_id` INT(11) NOT NULL,
  `amount` DECIMAL(18,2) NOT NULL,
  `payment_details` LONGTEXT NOT NULL,
  `payment_method` VARCHAR(255) NOT NULL,
  `payment_reference` VARCHAR(255) NOT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  INDEX `driver_payment_idx` (`driver_id` ASC),
  CONSTRAINT `driver_payment`
    FOREIGN KEY (`driver_id`)
    REFERENCES `shipping_company_drivers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

ALTER TABLE `shops`
CHANGE COLUMN `sliders` `sliders` LONGTEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL AFTER `logo`;

ALTER TABLE `staff`
ADD COLUMN `company_id` INT(11) NOT NULL DEFAULT 0 AFTER `role_id`;

ALTER TABLE `orders`
DROP COLUMN `seller_id`;


CREATE TABLE IF NOT EXISTS `taxes` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `tax_status` TINYINT(1) NOT NULL DEFAULT 1,
  `created_at` TIMESTAMP NULL DEFAULT current_timestamp(),
  `updated_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `transaction_payments` (
  `id` INT(11) UNSIGNED NOT NULL,
  `transaction_id` INT(11) NOT NULL,
  `internal_reference` VARCHAR(100) NOT NULL,
  `external_reference` VARCHAR(100) NULL DEFAULT NULL,
  `payment_reciept` VARCHAR(45) NULL DEFAULT NULL,
  `status` VARCHAR(45) NULL DEFAULT NULL,
  `status_description` LONGTEXT NULL DEFAULT NULL,
  `amount` DECIMAL(18,2) NOT NULL,
  `currency` VARCHAR(10) NOT NULL,
  `channel_type` VARCHAR(100) NOT NULL,
  `channel_name` VARCHAR(100) NOT NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

ALTER TABLE `users`
ADD COLUMN `device_token` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL AFTER `remember_token`;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


INSERT INTO `business_settings`(`type`, `value`, `created_at`, `updated_at`) VALUES ('cookies_agreement_text','<p>tested</p>',current_timestamp(),current_timestamp());
INSERT INTO `business_settings`(`type`, `value`, `created_at`, `updated_at`) VALUES ('header_menu_labels','["Flash Deals","Sell on Urbamart","Urbamart Sells Force"]',current_timestamp(),current_timestamp());
INSERT INTO `business_settings`(`type`, `value`, `created_at`, `updated_at`) VALUES ('header_menu_links','["http:\/\/localhost:9090\/urbamartm\/flash-deals","http:\/\/localhost:9090\/urbamartm\/shops\/create","http:\/\/localhost:9090\/urbamartm\/affiliate"]',current_timestamp(),current_timestamp());
INSERT INTO `business_settings`(`type`, `value`, `created_at`, `updated_at`) VALUES ('product_manage_by_admin','0',current_timestamp(),current_timestamp());
INSERT INTO `business_settings`(`type`, `value`, `created_at`, `updated_at`) VALUES ('disable_image_optimization','0',current_timestamp(),current_timestamp());
INSERT INTO `business_settings`(`type`, `value`, `created_at`, `updated_at`) VALUES ('home_banner3_images','["61","55","53"]',current_timestamp(),current_timestamp());
INSERT INTO `business_settings`(`type`, `value`, `created_at`, `updated_at`) VALUES ('home_banner3_links','[null,null,null]',current_timestamp(),current_timestamp());
INSERT INTO `business_settings`(`type`, `value`, `created_at`, `updated_at`) VALUES ('topbar_banner','204',current_timestamp(),current_timestamp());
INSERT INTO `business_settings`(`type`, `value`, `created_at`, `updated_at`) VALUES ('topbar_banner_link','http://localhost:9090/urbamartm/flash-deals',current_timestamp(),current_timestamp());
INSERT INTO `business_settings`(`type`, `value`, `created_at`, `updated_at`) VALUES ('show_website_popup','on',current_timestamp(),current_timestamp());
INSERT INTO `business_settings`(`type`, `value`, `created_at`, `updated_at`) VALUES ('website_popup_content','<p><img src="http://demo.activeitzone.com/ecommerce/public/uploads/all/dwaK3um8tkVgEsgmZN1peQb844tFRAIQ1wAS8e3z.png" style="width: 100%;"></p><p style="text-align: center;"><br></p><h2 style="text-align: center;"><span style="font-weight: bolder;">Subscribe to Our Newsletter</span></h2><p style="text-align: center;">Subscribe our newsletter for coupon, offer and exciting promotional discount..</p>',current_timestamp(),current_timestamp());
INSERT INTO `business_settings`(`type`, `value`, `created_at`, `updated_at`) VALUES ('show_subscribe_form','on',current_timestamp(),current_timestamp());
INSERT INTO `business_settings`(`type`, `value`, `created_at`, `updated_at`) VALUES ('shipping_manage_by_admin','1',current_timestamp(),current_timestamp());
INSERT INTO `business_settings`(`type`, `value`, `created_at`, `updated_at`) VALUES ('driver_commission','20',current_timestamp(),current_timestamp());
INSERT INTO `business_settings`(`type`, `value`, `created_at`, `updated_at`) VALUES ('show_cookies_agreement',NULL,current_timestamp(),current_timestamp());
INSERT INTO `business_settings`(`type`, `value`, `created_at`, `updated_at`) VALUES ('refund_sticker',NULL,current_timestamp(),current_timestamp());



