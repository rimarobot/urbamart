var staticCacheName = "pwa-v2:urbamart";
var filesToCache = [
    '/urbamartm/offline',
    '/urbamartm/returnpolicy',
    '/urbamartm/help-center',
    '/urbamartm/how-to-buy',
    '/urbamartm/buyer-protection',
    '/urbamartm/payment-methods',
    '/urbamartm/shipping-delivery',
    '/urbamartm/privacypolicy',
    '/urbamartm/about-us',
    '/urbamartm/contact-us',
    '/urbamartm/urbamart-express-shipping',
    '/urbamartm/sell-on-urbamart',
    '/urbamartm/seller-guidlines',
    '/urbamartm/seller-agreement',
    '/urbamartm/product-listing-policy',
    '/urbamartm/logistics',
    '/urbamartm/terms',
    '/urbamartm/public/uploads/all/0WJGYtKUwLxmNnKTD6D2pzbgo1HAfdFvb9ADS14k.png',
    '/urbamartm/public/assets/css/vendors.css',
    '/urbamartm/public/assets/css/aiz-core.css',
    '/urbamartm/public/assets/css/custom-style.css',
    '/urbamartm/public/assets/js/vendors.js',
    '/urbamartm/public/assets/js/aiz-core.js',
    '/urbamartm/public/assets/img/placeholder.jpg',
    '/urbamartm/public/assets/img/placeholder-rect.jpg',
    '/urbamartm/public/assets/img/play.png',
    '/urbamartm/public/assets/img/app.png',
    '/urbamartm/public/assets/img/nothing.svg',
    '/urbamartm/public/assets/img/404.svg',
    '/urbamartm/public/assets/img/419.svg',
    '/urbamartm/public/assets/img/500.svg',
    '/urbamartm/public/assets/img/maintainance.svg',
    '/urbamartm/public/assets/img/disconnected.svg',
    '/urbamartm/public/assets/img/loading.svg',
    '/urbamartm/public/assets/img/verified.png',
    '/urbamartm/public/assets/img/non_verified.png',
    '/urbamartm/public/assets/img/shopping-online.png',
    '/urbamartm/public/assets/img/T_55-1-1024x718-1-500x351.png',
    '/urbamartm/public/assets/img/cards/zappay-mobile.png',
    '/urbamartm/public/assets/img/cards/zappay-card.png',
    '/urbamartm/public/assets/img/cards/cod.png',
    '/urbamartm/public/assets/img/boss.png',
    '/urbamartm/public/assets/img/connect-buyer-and-seller.png',
    '/urbamartm/public/assets/img/continuous-improvement.png',
    '/urbamartm/public/assets/img/credit-card-payment-concept.png',
    '/urbamartm/public/assets/img/delivery-concept.png',
    '/urbamartm/public/uploads/all/NbUmY5kACjsMk2gnRgPcQ14ViXWo2ABFSeCSbqlX.png',
    '/urbamartm/public/uploads/all/7Nva8eu0oLKqbzycNx40Qd21iiNiAb3Ml64IWW3k.png',
    '/urbamartm/public/uploads/all/QidWcgTze1smUVa8RyMBCHULD0eK1axa3XHBJ5S7.png',
    '/urbamartm/public/uploads/all/5kiHdepj5UrGn7O25Brq7feEAP6Gn8DxHV7HLKbU.png',
    '/urbamartm/public/uploads/all/vXJcxUJSNfyDys0TM0PWpXOkccQ1htgSrtYynvF4.png',
    '/urbamartm/public/uploads/all/CzGi0HRetYrpm4jw5QUXiexALYQeE5yOP5wC7SUf.png',
    '/urbamartm/public/assets/img/avatar-place.png',
    '/urbamartm/public/assets/fonts/la-regular-400.woff2',
    '/urbamartm/public/assets/fonts/la-solid-900.woff2',
    '/urbamartm/public/assets/fonts/la-brands-400.woff2',
    '/urbamartm/public/images/icons/icon-72x72.png',
    '/urbamartm/public/images/icons/icon-96x96.png',
    '/urbamartm/public/images/icons/icon-128x128.png',
    '/urbamartm/public/images/icons/icon-144x144.png',
    '/urbamartm/public/images/icons/icon-152x152.png',
    '/urbamartm/public/images/icons/icon-192x192.png',
    '/urbamartm/public/images/icons/icon-384x384.png',
    '/urbamartm/public/images/icons/icon-512x512.png',
];

// Cache on install
self.addEventListener("install", event => {
    console.log("install", event);
    this.skipWaiting();
    event.waitUntil(
        caches.open(staticCacheName)
        .then(cache => {
            return cache.addAll(filesToCache);
        })
    )
});

// Clear cache on activate
self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames
                .filter(cacheName => (cacheName.startsWith("pwa-")))
                .filter(cacheName => (cacheName !== staticCacheName))
                .map(cacheName => caches.delete(cacheName))
            );
        })
    );
    // return self.clients.claim();
});

// Serve from Cache
self.addEventListener("fetch", event => {
    console.log("fetch", event.request.url);
    event.respondWith(
        caches.match(event.request)
        .then(response => {
            return response || fetch(event.request)
                .then(response => {
                    const allowed = ["png", "jpeg", "gif", "jpg", "svg", "webp", "bmp", "css", "js", "woff", "woff2", "ico"];
                    if (allowed.includes(event.request.url.split('.').pop())) {
                        return caches.open(staticCacheName).then(cache => {
                            cache.put(event.request.url, response.clone());
                            return response;
                        });
                    }
                    return response;
                });
        })
        .catch((err) => {
            return caches.match('offline');
        })
    )
});