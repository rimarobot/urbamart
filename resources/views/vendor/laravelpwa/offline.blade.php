@extends('frontend.layouts.app')

@section('content')
<section class="text-center py-6">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 mx-auto">
				<img src="{{ static_asset('assets/img/nothing.svg') }}" class="mw-100 mx-auto mb-5" height="300">
			    <h1 class="fw-700">{{ translate('Connection Lost!') }}</h1>
			    <p class="fs-16 opacity-60">{{ translate('You are currently not connected to any networks.') }}</p>
			</div>
		</div>
    </div>
</section>
@endsection
