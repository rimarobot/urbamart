<div class="card border-0 shadow-sm rounded">
    <div class="card-header">
        <h3 class="fs-16 fw-600 mb-0">{{translate('Select Shipping')}}</h3>
    </div>

    <div class="card-body">
        <div class="row gutters-5">

            @foreach (compare_shipping_cost() as $courier)
                    <label class="aiz-megabox d-block d-flex bg-white mb-2 mx-2">
                        <input type="radio" name="shipping_type" value="{{ $courier['id'] }}"  @if( ($method??'') == $courier['id']) checked @endif>
                        <span class="d-flex p-2 aiz-megabox-elem">
                            <span class="aiz-rounded-check flex-shrink-0 mt-1"></span>
                            <span class="flex-grow-1 pl-1 fw-600">{{ $courier['carrier'] }}
                            <strong class="product-quantity">{{ $courier['delivery']  }}</strong></span>
                            <span class="flex-grow-1 pl-1 fw-600">{{ single_price($courier['cost']) }}</span>
                        </span>
                    </label>
            @endforeach
        </div>

        @php
            $subtotal = 0;
            $tax = 0;
            $shipping = 0;
        @endphp
        @foreach (Session::get('cart') as $key => $cartItem)
            @php
                $product = \App\Product::find($cartItem['id']);
                $subtotal += $cartItem['price']*$cartItem['quantity'];
                $tax += $cartItem['tax']*$cartItem['quantity'];
                $shipping += $cartItem['shipping'];

                $product_name_with_choice = $product->getTranslation('name');
                if ($cartItem['variant'] != null) {
                    $product_name_with_choice = $product->getTranslation('name').' - '.$cartItem['variant'];
                }
            @endphp
        @endforeach
        <table class="table">

            <tfoot>
                <tr class="cart-subtotal">
                    <th>{{translate('Order Subtotal')}}</th>
                    <td class="text-right">
                        <span class="fw-600" name="cart-subtotal" id="cart-subtotal">{{ single_price($subtotal) }}</span>
                    </td>
                </tr>

                <tr class="cart-shipping">
                    <th>{{translate('Tax')}}</th>
                    <td class="text-right">
                        <span name="cart-tax" id="cart-tax" class="font-italic">{{ single_price($tax) }}</span>
                    </td>
                </tr>

                <tr class="cart-shipping">
                    <th>{{translate('Total Shipping')}}</th>
                    <td class="text-right">
                        <span name="cart-shipping" id="cart-shipping" class="font-italic">{{ single_price($shipping) }}</span>
                    </td>
                </tr>

                @if (Session::has('coupon_discount'))
                    <tr class="cart-shipping">
                        <th>{{translate('Coupon Discount')}}</th>
                        <td class="text-right">
                            <span class="font-italic">{{ single_price(Session::get('coupon_discount')) }}</span>
                        </td>
                    </tr>
                @endif

                @php
                    $total = $subtotal+$tax+$shipping;
                    if(Session::has('coupon_discount')){
                        $total -= Session::get('coupon_discount');
                    }
                @endphp

                <tr class="cart-total">
                    <th><span class="strong-600">{{translate('Total')}}</span></th>
                    <td class="text-right">
                        <strong><span name="cart-total" id="cart-total" >{{ single_price($total) }}</span></strong>
                    </td>
                </tr>
            </tfoot>
        </table>

        @if (Auth::check() && \App\BusinessSetting::where('type', 'coupon_system')->first()->value == 1)
            @if (Session::has('coupon_discount'))
                <div class="mt-3">
                    <form class="" action="{{ route('checkout.remove_coupon_code') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="input-group">
                            <div class="form-control">{{ \App\Coupon::find(Session::get('coupon_id'))->code }}</div>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-primary">{{translate('Change Coupon')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            @else
                <div class="mt-3">
                    <form class="" action="{{ route('checkout.apply_coupon_code') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="input-group">
                            <input type="text" class="form-control" name="code" placeholder="{{translate('Have coupon code? Enter here')}}" required>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-primary">{{translate('Apply')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            @endif
        @endif

    </div>
</div>
