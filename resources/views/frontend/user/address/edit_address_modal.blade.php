<form id="address-edit-form" class="form-default" role="form" action="{{ route('addresses.update', $address_data->id) }}" method="POST">
    @csrf
    <div class="p-3">
        <div class="row">
            <div class="col-md-2">
                <label>{{ translate('Address')}}</label>
            </div>
            <div class="col-md-10">
                <textarea class="form-control mb-3" placeholder="{{ translate('Your Address')}}" rows="2" name="address" required>{{ $address_data->address }}</textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <label>{{ translate('Country')}}</label>
            </div>
            <div class="col-md-10">
                <div class="mb-3">
                    <select class="form-control aiz-selectpicker" data-live-search="true" data-placeholder="{{ translate('Select your country')}}" name="country" id="country" required>
                        <option value="">Select Country</option>
                        @foreach (\App\Country::where('status', 1)->get() as $key => $country)
                        <option value="{{ $country->name . ':' . $country->code }}" @if((explode(':', $address_data->country))[0] == $country->name) selected @endif>
                            {{ $country->name }}
                        </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <label>{{ translate('City')}}</label>
            </div>
            <div class="col-md-10">
                <select class="form-control mb-3 aiz-selectpicker" data-live-search="true" name="city" id="city" required>
                    @if(isset($address_data->city))
                        <option value="">Select City</option>
                        @foreach(\App\State::where('country_id', \App\Country::where('code', (explode(':', $address_data->country))[1])->first()->id)->get() as $key => $city)
                            <option value="{{ $city->name . ':' . $city->code }}" @if((explode(':', $address_data->city))[0] == $city->name) selected @endif>
                                {{ $city->name }}
                            </option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
   {{-- <div class="row">
            <div class="col-md-2">
                <label>{{ translate('Postal code')}}</label>
            </div>
            <div class="col-md-10">
                <input type="text" class="form-control mb-3" placeholder="{{ translate('Your Postal Code')}}" value="{{ $address_data->postal_code }}" name="postal_code" value="" required>
            </div>
        </div> --}}
        <div class="row mb-3">
            <div class="col-md-2">
                <label>{{ translate('Phone')}}</label>
            </div>
            <div class="col-md-10">
                <input type="tel" class="form-control mb-3" value="{{ $address_data->phone }}" name="phone" value=""  placeholder="eg +2567xxxxxxxx"
                pattern="(^(\+256){1}[7]{1}[7|8]{1}[0-9]{6}[0-9])|(^(\+256){1}[7]{1}[0|3|5]{1}[0-9]{6}[0-9])|(^(\+254)[7]{1}(([0129]{1}[0-9]{1})|([4]{1}[0123568])|([5]{1}[789])|([6]{1}[89]))[0-9]{6})"
                required>
            </div>
        </div>
        <div class="form-group text-right">
            <button type="submit" class="btn btn-sm btn-primary">{{translate('Save')}}</button>
        </div>
    </div>
</form>
