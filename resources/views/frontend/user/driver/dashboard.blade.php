@extends('frontend.layouts.app')

@section('content')
<section class="py-5">
    <div class="container">
        <div class="d-flex align-items-start">
            @include('frontend.inc.user_side_nav')
            <div class="aiz-user-panel">
                <div class="aiz-titlebar mt-2 mb-4">
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h1 class="h3">{{ translate('Dashboard') }}</h1>
                        </div>
                    </div>
                </div>
                <div class="row gutters-10">
                    <div class="col-md-3">
                        <div class="bg-grad-1 text-white rounded-lg mb-4 overflow-hidden">
                            <div class="px-3 pt-3 text-center">
                                <i class="las la-shipping-fast la-4x"></i>
                                <div class="opacity-50">Completed Delivery</div>
                                <div class="h3 fw-700">{{\App\DriverOrder::where('driver_id', Auth::user()->driver->id)->where('status','delivered')->where('payment_status','paid')->count()}}</div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="bg-grad-2 text-white rounded-lg mb-4 overflow-hidden">
                            <div class="px-3 pt-3 text-center">
                                <i class="las la-clock la-4x"></i>
                                <div class="opacity-50">Pending Delivery</div>
                                <div class="h3 fw-700">{{\App\DriverOrder::where('driver_id', Auth::user()->driver->id)->whereNotIn('status',['delivered', 'canceled'])->count()}}</div>

                            </div>

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="bg-grad-3 text-white rounded-lg mb-4 overflow-hidden">
                            <div class="px-3 pt-3 text-center">
                                <i class="las la-layer-group la-4x"></i>
                                <div class="opacity-50">Total Collected</div>

                                <div class="h3 fw-700">
                                   {{single_price(\App\DriverOrder::where('driver_id', Auth::user()->driver->id)->where('status','delivered')->where('payment_status','paid')->get()->sum('shipping_cost'))}}
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="bg-grad-4 text-white rounded-lg mb-4 overflow-hidden">
                            <div class="px-3 pt-3 text-center">
                                <i class="las la-dollar-sign la-4x"></i>
                                <div class="opacity-50">Earnings</div>
                                <div class="h3 fw-700">
                                    {{ single_price(\App\ShippingDriver::findOrFail(Auth::user()->driver->id)->admin_to_pay)}}
                                </div>

                            </div>

                        </div>
                    </div>

                </div>
              <div class="row gutters-10">
            		<div class="col-md-6">
            			<div class="card">
                            <div class="card-header">
                                <h6 class="mb-0">{{ translate('Default Shipping Address') }}</h6>
                            </div>
    				        <div class="card-body">
                              @if(Auth::user()->addresses != null)
                                  @php
                                      $address = Auth::user()->addresses->where('set_default', 1)->first();
                                  @endphp
                                  @if($address != null)
                                      <ul class="list-unstyled mb-0">
    					                  <li class=" py-2"><span>{{ translate('Address') }} : {{ $address->address }}</span></li>
                                          <li class=" py-2"><span>{{ translate('Country') }} : {{ $address->country }}</span></li>
                                          <li class=" py-2"><span>{{ translate('City') }} : {{ $address->city }}</span></li>
                                          <li class=" py-2"><span>{{ translate('Postal Code') }} : {{ $address->postal_code }}</span></li>
                                          <li class=" py-2"><span>{{ translate('Phone') }} : {{ $address->phone }}</span></li>
                                      </ul>
                                  @endif
                              @endif
            				</div>
            			</div>
            		</div>
                    @if (\App\BusinessSetting::where('type', 'classified_product')->first()->value)
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h6 class="mb-0">{{ translate('Purchased Package') }}</h6>
                                </div>
                                <div class="card-body text-center">
                                    @php
                                        $customer_package = \App\CustomerPackage::find(Auth::user()->customer_package_id);
                                    @endphp
                                    @if($customer_package != null)
                                        <img src="{{ uploaded_asset($customer_package->logo) }}" class="img-fluid mb-4 h-110px">
                                		<p class="mb-1 text-muted">{{ translate('Product Upload') }}: {{ $customer_package->product_upload }} {{ translate('Times')}}</p>
                                		<p class="text-muted mb-4">{{ translate('Product Upload Remaining') }}: {{ Auth::user()->remaining_uploads }} {{ translate('Times')}}</p>
                                        <h5 class="fw-600 mb-3 text-primary">{{ translate('Current Package') }}: {{ $customer_package->getTranslation('name') }}</h5>
                                    @else
                                        <h5 class="fw-600 mb-3 text-primary">{{translate('Package Not Found')}}</h5>
                                    @endif
                                    <a href="{{ route('customer_packages_list_show') }}" class="btn btn-success d-inline-block">{{ translate('Upgrade Package') }}</a>
                                </div>
                            </div>
                        </div>
                    @endif
            	</div>
            </div>
        </div>
    </div>
</section>
@endsection
