@extends('frontend.layouts.app')

@section('content')

<section class="py-5">
    <div class="container">
        <div class="d-flex align-items-start">
            @include('frontend.inc.user_side_nav')
            <div class="aiz-user-panel">
                    <div class="card">
                        <form class="" action="" method="GET">
                          <div class="card-header row gutters-5">
                            <div class="col text-center text-md-left">
                              <h5 class="mb-md-0 h6">{{ translate('Completed Deliveries') }}</h5>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group mb-0">
                                    <input type="text" class="aiz-date-range form-control" value="{{ $date }}" name="date" placeholder="{{ translate('Filter by date') }}" data-format="DD-MM-Y" data-separator=" to " data-advanced-range="true" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="input-group  mb-0">
                                    <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="{{ translate('Type Order code & hit Enter') }}">
                                    <div class="input-group-append d-block d-lg-none">
                                        <a class="btn btn-outline-primary"  onclick="Android.BarCodeScanner();">
                                            <i class="la la-barcode"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto d-none d-lg-block">
                              <div class="form-group mb-0">
                                <button type="submit" class="btn btn-primary">{{ translate('Filter') }}</button>
                              </div>
                            </div>
                          </div>
                      </form>
                      <div class="card-body">
                          <table class="table aiz-table mb-0">
                              <thead>
                                  <tr>
                                      {{-- <th>#</th> --}}
                                      <th>{{ translate('Order Code') }}</th>
                                      <th data-breakpoints="md">{{ translate('Amount') }}</th>
                                      <th>{{ translate('Shipping Fee') }}</th>
                                      <th>{{ translate('Payment Status') }}</th>
                                      <th data-breakpoints="md">{{ translate('Shipping Type') }}</th>
                                      <th data-breakpoints="md">{{ translate('Shipping Status') }}</th>
                                      <th data-breakpoints="xl">{{ translate('Contact') }}</th>
                                      <th data-breakpoints="xl">{{ translate('Address') }}</th>
                                      <th data-breakpoints="xl">{{ translate('Qty') }}</th>
                                      <th data-breakpoints="xl">{{ translate('Seller') }}</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  @foreach ($orders as $key => $order)
                                      <tr>
                                          {{-- <td>{{ ($key+1) + ($orders->currentPage() - 1)*$orders->perPage() }}</td> --}}
                                          <td>{{ $order->order_code }}</td>
                                          <td>{{ single_price($order->order_price) }}</td>
                                          <td>{{ single_price($order->shipping_cost) }}</td>
                                          <td>
                                              @if ($order->payment_status == 'paid')
                                                  <span class="badge badge-inline badge-success">{{ ucwords(str_replace('_', ' ', $order->payment_type)) }}</span>
                                              @else
                                                  <span class="badge badge-inline badge-danger">{{ ucwords(str_replace('_', ' ', $order->payment_type)) }}</span>
                                              @endif
                                          </td>
                                          <td>{{ $order->shipping_type }}</td>
                                          <td>{{ ucwords(str_replace('_', ' ', $order->status)) }}</td>
                                          <td><a href="tel:{{ json_decode($order->shipping_address)->phone }}">{{ json_decode($order->shipping_address)->phone }}</a></td>
                                          <td>{{ json_decode($order->shipping_address)->address.', '. json_decode($order->shipping_address)->country.', '.json_decode($order->shipping_address)->city }}</td>
                                          <td>{{ $order->qty }}</td>
                                          <td>{{ $order->shop }}</td>
                                      </tr>
                                  @endforeach
                              </tbody>
                          </table>
                          <div class="aiz-pagination">
                              {{ $orders->appends(request()->input())->links() }}
                          </div>
                      </div>
                  </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('modal')
    <div class="modal fade" id="order_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ translate('Received By') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <form  id="delivery-form" class="form-default" role="form" action="{{ route('driver.update.receiver') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="p-3">
                            <div class="row">
                                <label class="col-md-2 col-form-label">{{ translate('Name') }}</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control  mb-3" placeholder="{{ translate('Receiver Name') }}" rows="1" name="name" required>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-md-2 col-form-label">{{ translate('Identity No.') }}</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control  mb-3" placeholder="{{ translate('Receiver Identity No.') }}" rows="1" name="identity" required>
                                </div>
                            </div>
                            <div class="row  mb-3">
                                <div class="col-md-2">
                                    <label>{{ translate('Contact')}}</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="tel" class="form-control mb-3" placeholder="eg +2567xxxxxxxx"  name="phone"  required autocomplete="off"
                                    pattern="(^(\+256){1}[7]{1}[7|8]{1}[0-9]{6}[0-9])|(^(\+256){1}[7]{1}[0|3|5]{1}[0-9]{6}[0-9])|(^(\+254)[7]{1}(([0129]{1}[0-9]{1})|([4]{1}[0123568])|([5]{1}[789])|([6]{1}[89]))[0-9]{6})">
                                </div>
                            </div>
                            <input type="hidden" name="delivery_id" value="">
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-sm btn-primary">{{translate('Save')}}</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        function show_order_details(id)
        {
            $('input[name=delivery_id]').val(id);
            $('#order_details').modal();
            $('.c-preloader').hide();
        }

        function update_delivery_status(v,i){
            $.post('{{ route('driver.update.delivery') }}', {_token:'{{ @csrf_token() }}',id:v,state:i}, function(data){
                AIZ.plugins.notify('success', '{{ translate('Order status has been updated') }}');
                location.reload();
            });
        }

        function update_payment_status(v){
            $.post('{{ route('driver.update.payment') }}', {_token:'{{ @csrf_token() }}',id:v}, function(data){
                 AIZ.plugins.notify('success', '{{ translate('Payment status has been updated') }}');
                 location.reload();
            });
        }
    </script>

@endsection
