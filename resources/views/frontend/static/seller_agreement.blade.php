@extends('frontend.layouts.app') @section('content')
  <section class="pt-4 mb-4">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 text-center text-lg-left">
                <h1 class="fw-600 h4">{{ translate('Urbamart Seller Agreement') }}</h1>
            </div>
            <div class="col-lg-6">
                <ul class="breadcrumb bg-transparent p-0 justify-content-center justify-content-lg-end">
                    <li class="breadcrumb-item opacity-50">
                        <a class="text-reset" href="{{ route('home') }}">{{ translate('Home')}}</a>
                    </li>
                    <li class="text-dark fw-600 breadcrumb-item">
                        <a class="text-reset" href="{{ route('seller-agreement') }}">"{{ translate('Urbamart Seller Agreement') }}"</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="mb-4">
    <div class="container">
        <div class="p-4 bg-white rounded shadow-sm overflow-hidden mw-100 text-left"> 
                    <p>This Urbamart Seller Agreement (“Agreement”) is made and entered into on _____ /________ /20______, between;</p>
                    <ol>
                        <li>
                            <strong>ZByte Technology</strong>, a limited liability company incorporated under the Companies Act of Uganda, having its registered office at,&nbsp;
                            <strong>Sir Apollo Kaggwa Rd, Plot 27 Opposite DFCU Bank, next to BIFRO building</strong>&nbsp;through Mr./Mrs./Miss___________________ a duly Authorized Representative (herein referred to
                            as&nbsp;<b>“Urbamart”&nbsp;</b>which expression includes its successors and assigns), of the First Part
                        </li>
                    </ol>
                    <p><strong>And</strong></p>
                    <ol start="2">
                        <li>
                            _________________________________________ a limited liability company incorporated under the Companies Act of the a fore mentioned republic, having its registered office at _________________________, Floor _____
                            along ______________ road and of P.O. Box _________________, ___________, and of Account Number/ ___________________ through Mr./Mrs./Miss _______________________ its duly Authorized Representative (hereinafter
                            referred to as “<strong>Seller</strong>” which expression includes its successors and assigns), of the Second Part.
                        </li>
                    </ol>
                    <p><strong>WHEREAS:</strong></p>
                    <ol start="3">
                        <li><b>Urbamart&nbsp;</b>is the owner of an E‐Commerce Platform located at the following URL:<a href="{{ env('APP_URL') }}">urbamart.com</a></li>
                        <li>
                            <p>
                                Hereinafter referred to as “Urbamart” wherein various products of different nature are marketed and sold using electronic medium more particularly through the e-commerce domain and wherein there are many
                                registered users to whom Company offer various services;
                            </p>
                        </li>
                        <li>The Seller is engaged in _________________________, more details to be provided in&nbsp;<strong>Schedule</strong>&nbsp;<strong>A&nbsp;</strong>annexed hereto;</li>
                    </ol>
                    <ol start="5">
                        <li>The Seller is desirous of setting up an online store on Urbamart Marketplace and has offered to sell its products through the said online store subject to the following terms and conditions; and</li>
                        <li>The above referred Seller and Urbamart are hereinafter collectively referred to as “Parties” and individually as “Party”.</li>
                    </ol>
                    <p><strong>AND WHEREAS&nbsp;</strong>Parties have agreed to reduce their terms in writing.</p>

                    <h4>NOW THIS AGREEMENT WITNESSETH HEREWITH:</h4>
                    <h4>1. Definitions</h4>
                    <p>For the purpose of this Agreement, the following words and phrases shall have the meaning assigned to them as follows.</p>
                    <ol>
                        <li>“<b>Urbamart”&nbsp;</b>shall mean ZByte Technology<strong>.</strong></li>
                        <li>
                            “<strong>Customer”&nbsp;</strong>shall mean any individual, group of individuals, firm, company or any other<strong>&nbsp;</strong>entity placing an order for the Products of the Seller through the Online Store.
                        </li>
                        <li>“<strong>Effective Date”&nbsp;</strong>shall mean the date on which this Agreement is executed<strong>.</strong></li>
                        <li>
                            “<strong>Seller”&nbsp;</strong>shall mean any individual, group of individuals, firm, company or any other entity<strong>&nbsp;</strong>incorporated or otherwise more specifically described hereinabove, which
                            sells its products through the Online Store and more particularly described in the attached&nbsp;<strong>Schedule A</strong>.
                        </li>
                        <li>
                            “<strong>Online Store”&nbsp;</strong>shall mean a virtual electronic store created on the Urbamart shopping<strong>&nbsp;</strong>online platform for sale of the Seller’s Products either through web site of the
                            Company or
                        </li>
                        <li>
                            “<strong>Order”&nbsp;</strong>shall mean an order for purchase of products wherein customer has agreed to<strong>&nbsp;</strong>purchase the product upon the terms and conditions and at the Price indicated on the
                            online store of the Seller.
                        </li>
                        <li>“<strong>Products”&nbsp;</strong>shall mean merchandise items of the Seller put up for sale on the Online Store<strong>&nbsp;</strong>by the Seller.</li>
                        <li>
                            “<strong>Commission”&nbsp;</strong>shall mean the margin per transaction charged by the Company to the<strong>&nbsp;</strong>Seller at the rates agreed to between the parties, upon the sale of product on online
                            store.
                        </li>
                        <li>
                            “<strong>Settlement fee”&nbsp;</strong>shall mean the money owed to the seller after their product has been<strong>&nbsp;</strong>delivered and paid for by the customer according to Urbamart Settlement Policy.
                        </li>
                        <li>
                            “<strong>Seller Backend”&nbsp;</strong>shall mean all the information required to operate the Online Store,<strong>&nbsp;</strong>including information about the Seller supplied in accordance with this Agreement
                            and the provided form.
                        </li>
                        <li>“<b>Urbamart&nbsp;Seller Policy and Guidelines Channel</b>” shall mean the official channel or link that Urbamart displays its seller policy and guidelines in Urbamart official website</li>
                    </ol>
                    <h4>NOW THIS AGREEMENT WITNESSES AS UNDER</h4>
                    <ol>
                        <li>
                            <h4>Commencement, Term, Renewal</h4>
                            <p>
                                The date of execution of this agreement shall be the commencement date and the agreement shall remain valid and binding for a period of 2 years initially and can be renewed on mutually agreed terms at the time of
                                renewal for such terms as the parties agree. All renewals of the agreement shall be express and in writing. No oral agreement shall be binding on either of the party.
                            </p>
                        </li>
                        <li>
                            <h4>Marketing Tools/Support, Products, Availability of products etc.</h4>
                            <p>
                                The Seller&nbsp;<strong>will</strong>&nbsp;keep Urbamart informed at all times about the availability of the products in its inventory along with detailed specifications like size, color, texture etc. as may be
                                required in Product List in attached&nbsp;<strong>Schedule B</strong>. Order once placed on Urbamart by the customer shall be honored by the Seller at all costs.
                            </p>
                        </li>
                        <li>
                            <h4>Commissions</h4>
                            <p>
                                For all such sales that are made/generated using Urbamart Marketplace, a commission shall be paid by the Seller to Urbamart. The details of Standard Products Commissions to be paid product category wise is specified
                                in Urbamart Seller Policy and Guidelines Channel.
                            </p>
                            <p>It is expressly agreed by the parties hereto that Urbamart shall debit the amount of commission from the remittance to Seller at the time of settlement.</p>
                        </li>
                        <li>
                            <h4>Order, Handling, Delivery</h4>
                            <p>
                                Orders for the product will be reflected in Seller Backend and Seller shall follow the Urbamart Operation Guidelines – Seller Backend Operation about how to operate seller backend to review and handle the orders.
                            </p>
                            <p>For the Fulfillment by Urbamart Option where;</p>
                            <ol>
                                <li>Urbamart stores the Sellers goods in its warehouse and is also in charge of the order and dispatch process, the following will apply;</li>
                                <li>The guidelines found on the online platform will apply;</li>
                                <li>The Seller Policy and Operations found on the online platform will apply;</li>
                                <li>The Standard Products Commission;</li>
                                <li>Urbamart Local Settlement Policy</li>
                                <li>The After-Sale Policy found on the online platform will apply;</li>
                                <li>Urbamart reserves the right to return goods stored in the warehouse at its sole discretion for reasons that it may or may not give to the sellers; and</li>
                                <li>Any other Policy and Guideline updated and added in Urbamart Seller Policy and Guidelines</li>
                            </ol>
                            <p><em>Drop Shipping, referring to situations where the Sellers are not storing their goods at Urbamart’s warehouse, the following will apply.</em></p>
                            <p>The Seller Policy and Operations found on the online platform will apply;</p>
                            <ol>
                                <li>The Standard Products Commission (<strong>Schedule B)</strong>;</li>
                                <li>Urbamart Local Settlement Policy<strong>;</strong></li>
                                <li>The After-Sale Policy found on the online platform will apply; and</li>
                                <li>Any other Policy and Guideline updated and added in Urbamart Seller Policy and Guidelines Channel.</li>
                            </ol>
                            <p>
                                The Seller shall ensure that the products dispatched are of the specifications ordered and there is no variation whatsoever. The necessary guarantee/warranty shall be provided by the Seller to the customer.
                            </p>
                            <p>
                                The Seller agrees to replace the defective products supplied to the customer and/or stored at Urbamart’s warehouse at its own cost and shall not hold Urbamart responsible in any manner whatsoever. Urbamart may, at
                                its discretion arrange to lift the defective products from the customer however the Seller will still be liable to replace the defective product. Any charges incurred by Urbamart for lifting and forwarding such
                                defective goods shall be on account of the Seller. For avoidance of doubts it is clarified that defective would mean and include but is not limited to wrong product, damaged product, wrongly sized product and any
                                other shortcoming which the customer may point out.
                            </p>
                            <p>The Seller hereby authorizes Urbamart to entertain all claims of return of the Product in the mutual interest of the Seller as well as the Customer.</p>
                            <p>
                                Under circumstances where the seller delegates and/or instructs the Urbamart team to operate the seller backend on their behalf, the seller hereby authorizes the Urbamart team and gives them the full right to manage
                                the seller backend and the seller guarantees that all the required information such as stock level, campaign offer, product contents etc. will be given in due time for Urbamart team to configure in seller backend.
                                The Urbamart team will not bear any responsibility arising out of wrong information given resulting to loss or liabilities to any party.
                            </p>
                        </li>
                        <li>
                            <h4>Covenants of Seller</h4>
                            <p>The Seller hereby covenants with Urbamart as hereunder:</p>
                            <p>
                                In case the Seller is running out of supplies or is likely not to fulfill the Order received by the Urbamart or the Customers, it shall update in Seller backend one day in advance to avoid any hindrance to delivery.
                            </p>
                            <p>
                                Not to send any kind of promotion material or any such material, which is, derogatory to and/or adverse to the interests financial or otherwise of Urbamart, to the customer either along with the products supplied or
                                in any manner whatsoever.
                            </p>
                            <p>
                                Not to do any act/deal in a thing / products/goods/services which are either banned/prohibited by law or violates any of the intellectual property right of any party in respect of such product
                            </p>
                            <p>
                                The Seller declares that it has all rights and authorizations in respect of intellectual property rights of third parties and is authorized to sale/provide/license such products to the customer. The copy of such
                                authorization shall be provided on demand without failure and/or protest.
                            </p>
                            <p>
                                The Seller agrees to indemnify and keep indemnified Urbamart from all claims/losses (including advocate fee for defending/prosecuting any case) that may arise against Urbamart due to acts/omission on the part of the
                                Seller
                            </p>
                            <p>
                                To provide to the Urbamart, for the purpose of the creation/display on platform of Urbamart, the product description, images, disclaimer, delivery time lines, price and such other details for the products to be
                                displayed and offered for sale.
                            </p>
                            <p>
                                To ensure that it complies with the guidelines and rules set out by the Standards of Product Page Content and Performance Assessment in Urbamart Seller Policy and Guidelines Channel and annexed to this agreement.
                            </p>
                            <p>
                                To ensure and not to provide any description/image/text/graphic which is unlawful, illegal, intimidating, obnoxious, objectionable, obscene, vulgar, opposed to public policy, prohibited by law or morality or is in
                                violation of intellectual property rights including but not limited to Trademark and copyright of any third party or of inaccurate, false, incorrect, misleading description or is surrogate in nature. Further it will
                                forward the product description and image only for the product which is offered for sale through the platform. The Seller agrees that in case there is violation of this covenant, it shall do and cause to be done all
                                such acts as are necessary to prevent disrepute being caused to Urbamart.
                            </p>
                            <p>
                                To provide full, correct, accurate and true description of the product so as to enable the customers to make an informed decision. The Seller agrees not to provide any such description/information regarding the
                                product which amounts to misrepresentation to the customer.
                            </p>
                            <p>To be solely responsible for the quality, quantity, merchantability, guarantee, warranties in respect of the products offered for sale through Urbamart’s platform.</p>
                            <p>
                                At all times have access to the Internet and its email account to check the status of approved orders and will ensure prompt deliveries within the time frame mentioned herein before in the agreement. This provision
                                applies where the seller’s goods are not stored at Urbamart’s warehouse.
                            </p>
                            <p>
                                To be solely responsible for any dispute that may be raised by the customer relating to the goods, merchandise and services provided by the Seller. No claim of whatsoever nature will be raised towards Urbamart.
                            </p>
                            <p>
                                The Seller shall at all times be responsible for compliance of all applicable laws and regulations including but not limited to Intellectual Property Rights, Value added tax, Standards of Weights &amp; Measures
                                legislation, Sale of Goods Act, Excise and Import duties, Consumer Protections Laws, Code of Advertising Ethics and any other applicable Ugandan Law, etc.
                            </p>
                            <p>Under no Circumstances will Seller contact customers to discuss transactions and other communication out of Urbamart platform.</p>
                            <p>To provide to Urbamart copies of any document required by Urbamart for the purposes of performance of its obligations under this agreement within 48 hours of getting a written notice.</p>
                            
                        </li>
                        <li>
                             <h4>Warranties, Representations and Undertakings of the Seller</h4>
                            <p>The Seller warrants and represents that:</p>
                            <ol>
                                <li>The signatory to the present agreement is having the right and full authority to enter into this Agreement with Urbamart and the agreement so executed is binding in nature.</li>
                                <li>All obligations narrated under this Agreement are legal, valid, binding and enforceable in law against Seller.</li>
                                <li>There are no proceedings pending against the Seller, which may have a material adverse effect on its ability to perform and meet the obligations under this Agreement.</li>
                                <li>That it is an authorized business establishment and hold all the requisite permissions, authorities, approvals and sanctions to conduct its business and to enter into present agreement with Urbamart.</li>
                                <li>
                                    It shall, at all times ensure compliance with all the requirements applicable to its business and for the purposes of this agreement including but not limited to Intellectual Property Rights, Value added tax,
                                    Excise and Import duties and all other relevant laws. It further declares and confirm that it has paid and shall continue to discharge all its obligations towards statutory authorities.
                                </li>
                                <li>
                                    That it has adequate rights under relevant laws including but not limited to various Intellectual Property Legislation(s) to enter into this Agreement with the Firm and perform the obligations contained herein
                                    and that it has not violated/ infringed any intellectual property rights of any third party.
                                </li>
                                <li>It shall maintain details of all transaction and mark as complete / incomplete as the case may be and shall provide the same to the Firm upon demand.</li>
                            </ol>
                        </li>
                        <li>
                            <h4>Rights of Urbamart</h4>
                            <p>
                                Seller agrees and acknowledges that Urbamart at all times during the continuance of this Agreement, shall have the right to remove/block/delete any text, graphic, image(s) uploaded on the online store by the Seller
                                without any prior intimation to Seller in the event the said text, image, graphic is found to be in violation of law, breach of any of the terms of this Agreement, terms and conditions of its online Platform. In such
                                an event, Urbamart reserve the right to forthwith remove/close the online store of the Seller without any prior intimation or liability to the Seller.
                            </p>
                            <p>Appropriate disclaimers and terms of use on the Shopping platform shall be placed by the Urbamart.</p>
                            <p>
                                At any time if Urbamart believes that the services are being utilized by the Seller or its Customer in contravention of the terms and provisions of this Agreement, Terms and conditions of use of its Online
                            </p>
                            <p>
                                At any time if Urbamart believes that the services are being utilized by the Seller or its Customer in contravention of the terms and provisions of this Agreement, Terms and conditions of use of its Online Shopping,
                                Urbamart shall have the right either at its sole discretion or upon the receipt of a request from the legal / statutory authorities or a court order to discontinue/terminate the said service(s) to Customer or the End
                                user as the case may be, forthwith remove/block/close the online store of the Seller and furnish such details about the Seller and/or its customers upon a request received from the Legal/ Statutory Authorities or
                                under a Court order.
                            </p>
                        </li>
                        <li>
                            <h4>Termination and effects of Termination</h4>
                            <p>This Agreement may be terminated by Urbamart forthwith in the event;</p>
                            <ol>
                                <li>
                                    Seller commits a material breach of any Urbamart Policy and Guideline, representation, obligations, covenant, warranty or term of this agreement and the same is not cured within 30 days after written notice given
                                    by the Firm;
                                </li>
                                <li>If a Petition for insolvency is filed against the Seller; or</li>
                                <li>If the Seller is in infringement of the third-party rights including intellectual property rights.</li>
                            </ol>
                            <p>This agreement may be terminated without reason by either party after serving upon the other, a written notice of 30 days. The agreement shall stand terminated after expiry of such period.</p>
                            <h5>Effect of Termination:</h5>
                            <p>In the event of termination/expiry of this Agreement, Urbamart shall remove the Links and shall discontinue display of the Products on its online shopping platform with immediate effect.</p>
                            <p>Urbamart shall not be liable for any loss or damages (direct, indirect or inconsequential) incurred by the Seller by virtue of termination of this agreement.</p>
                            <p>During the period under notice both the parties shall be bound to perform its obligations incurred under this agreement and this sub-‐clause shall survive the termination of this agreement.</p>
                        </li>
                        <li>
                            <h4>Jurisdiction, governing law and ex‐parte&nbsp;Orders</h4>
                            <p>
                                This agreement is subject to exclusive jurisdiction of competent Courts of law of Uganda only. The laws of Republic of Uganda, as are in force, shall be applicable to present agreement. Urbamart is entitled to obtain
                                ex‐parte ad-interim injunction orders restraining the Seller to prevent any loss/anticipated loss either in material terms or in terms of intellectual property or causing damage/loss/harm to reputation/goodwill of
                                Urbamart by the Seller, its representatives, associates or assigns.
                            </p>
                        </li>
                        <li>
                            <h4>Notices</h4>
                            <p>
                                All notices and other communication under this Agreement shall be in writing, in English language and shall be caused to be delivered by hand or sent by telex, fax, email or courier in each case to the addresses as
                                set out at the beginning of this Agreement.
                            </p>
                        </li>
                        <li>
                            <h4>Intellectual Property Rights</h4>
                            <p>
                                The Seller expressly authorizes Urbamart to use its trade marks/copy rights/ designs /logos and other intellectual property owned and/or licensed by it for the purpose of reproduction on the platform and at such
                                other places as Urbamart may deem necessary. It is expressly agreed and clarified that, except as specified agreed in this Agreement, each Party shall retain all right, title and interest in their respective
                                trademarks and logos and that nothing contained in this Agreement, nor the use of the trademark / logos on the publicity, advertising, promotional or other material in relation to the services shall be construed as
                                giving to any Party any right, title or interest of any nature whatsoever to any of the other Party’s trademarks and / or logos.
                            </p>
                        </li>
                        <li>
                            <h4>Entire Agreement</h4>
                            <p>
                                This Agreement embodies the entire agreement and understanding of the Parties and supersedes any and all other prior and contemporaneous agreements, correspondence, arrangements and understandings (whether written or
                                oral) between the Parties with respect to its subject matter.
                            </p>
                            <p>This applies to the “shop opening agreement”. Where there is a conflict between the latter and this agreement, this agreement will prevail.</p>
                        </li>
                        <li>
                            <h4>Assignment</h4>
                            <p>
                                Neither this Agreement nor any part of it is assignable, transferable, sub licensable, sub contractible or conveyable by Seller, either by operation of law or otherwise, without the express, prior, written consent of
                                Urbamart signed by an authorized representative of such Party. Urbamart is at liberty to refuse such consent.
                            </p>
                        </li>

                        <li>
                            <h4>Confidentiality:</h4>
                            <p>
                                The contents of the agreement and any information passed on by Urbamart to the Seller is highly confidential in nature and the Seller agrees and undertakes to maintain the confidentiality of the information and
                                user/customer data disclosed, generated or made available to Seller under this Agreement. The said information shall not be used by the Seller or its agents, servants, representatives or any person acting through or
                                claiming through the Seller for any purpose other than for the performance of its obligations under this Agreement. The Seller agrees that the unauthorized disclosure or use of such information would cause
                                irreparable harm and significant injury, the degree of which may be difficult to ascertain. Accordingly, Seller agrees that Urbamart shall have the right to obtain an immediate injunction from any court of law
                                ensuing breach of this Agreement and/or disclosure of the Confidential Information. The Firm shall also have the right to pursue any other rights or remedies available at law or equity for such a breach.
                            </p>
                        </li>
                        <li>
                            <h4>Relationship of Parties</h4>
                            <p>
                                Nothing in this Agreement will be construed as creating a relationship of partnership, joint venture, agency or employment between the Parties. Urbamart shall not be responsible for the acts or omissions of the
                                Seller and Seller shall not represent Urbamart neither has, any power or authority to speak for, represent, bind or assume any obligation on behalf of Urbamart.
                            </p>
                        </li>

                        <li>
                            <h4>Two Originals</h4>
                            <p>This Agreement may be executed in two (2) counterparts, one to remain with each party and each of which shall be deemed an original and which shall together constitute one Agreement.</p>
                            </li>
                            <li>
                            <h3>Schedule A</h3>
                            <p>
                                You are allowed to sell any goods that are permitted for trade under the law. More specifically we promote the sale of;&nbsp;
                                <strong>
                                    African products, Electronics, Home products, Mobile phones, Baby products, Beauty products, Consumables, Appliances, Automotive products, School items, Sports and fitness products and equipment
                                </strong>
                                &nbsp;More items for sale highlighted under&nbsp;<strong>Schedule B</strong>&nbsp;commissions section.
                            </p>
                            <p>You are also allowed to sell goods at your own reasonable price, we can only advice on the matter but you maintain the final say.</p>
                            </li>
                            <li>
                            <h3>Schedule B</h3>
                            <p>When personally uploading your goods or handing out the task to a content uploader you will provide all necessary information about the items to be uploaded i.e.:</p>
                            <ol>
                                <li>Brand of the item where applicable</li>
                                <li>Name of the item</li>
                                <li>Size or Capacity of the item</li>
                                <li>Color of the item</li>
                                <li>Material of the item</li>
                                <li>If it comes in a set indicate the number of pieces</li>
                                <li>Weight of the item</li>
                                <li>Price of the item</li>
                                <li>Amount of stock available</li>
                                <li>A description of the item.</li>
                                <li>A few important highlights about the items in point form for a short description</li>
                            </ol>
                            </li>
                            <li>
                            <h4>Commissions.</h4>
                            <p>All Items are charged……………………………………………………………………&nbsp;<strong>{{\App\BusinessSetting::where('type', 'vendor_commission')->first()->value}}%</strong></p>
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>
                                            <p>Urbamart:</p>
                                        </td>
                                        <td>
                                            <p align="right">Authorized Rep</p>
                                        </td>
                                        <td>SELLER:</td>
                                        <td>
                                            <p align="right">Authorized Rep</p>
                                        </td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td colspan="2">Name: ………………………………</td>
                                        <td colspan="2">Name: …………………………………</td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td colspan="2">Capacity: ……………………………….</td>
                                        <td colspan="2">Capacity: ………………………………</td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td colspan="2">Signature: ………………………………</td>
                                        <td colspan="2">Signature: …………………………….</td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td colspan="2">Date: ……………………………………</td>
                                        <td colspan="2">Date: ……………………………………</td>
                                    </tr>
                                </tbody>
                            </table>
                            </li>
                    </ol>
                     
                 
           </div>
    </div>
</section>

@endsection
