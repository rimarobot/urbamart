@extends('frontend.layouts.app') @section('content')

@section('content')
<section class="pt-4 mb-4">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 text-center text-lg-left">
                <h1 class="fw-600 h4">{{ translate('About Us') }}</h1>
            </div>
            <div class="col-lg-6">
                <ul class="breadcrumb bg-transparent p-0 justify-content-center justify-content-lg-end">
                    <li class="breadcrumb-item opacity-50">
                        <a class="text-reset" href="{{ route('home') }}">{{ translate('Home')}}</a>
                    </li>
                    <li class="text-dark fw-600 breadcrumb-item">
                        <a class="text-reset" href="{{ route('about-us') }}">"{{ translate('About Us') }}"</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="mb-4">
    <div class="container">
        <div class="p-4 bg-white rounded shadow-sm overflow-hidden mw-100 text-left">
            <div class="row">
                <div class="col-md">
                    <div class="p-4 bg-white">
                        <h3 class="text-center"><strong>Urbamart aspires to be the leading e-commerce platform in East Africa</strong></h3>

                        <h6 class="text-center">
                            Launched in 2019, it is a platform tailored for the region, providing customers with an easy, secure and fast online shopping experience through strong payment and fulfillment support. We believe online shopping
                            should be accessible, easy and enjoyable. This is the vision Urbamart aspires to deliver on the platform, every single day.
                        </h6>
                        <div class="row">
                            <br />
                            <br />
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4  text-center">
                                <img class="img-fluid img-thumbnail lazyload" src="{{ static_asset('assets/img/placeholder.jpg') }}" data-src="{{ static_asset('assets/img/connect-buyer-and-seller.png') }}" />
                                <h2 class="align-top">Our Purpose</h2>
                                <p>
                                    We believe in the transformative power of technology and want to change the world for the better by providing a platform to connect buyers and sellers within one community.
                                </p>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4  text-center">
                                <img class="img-fluid img-thumbnail lazyload text-center" src="{{ static_asset('assets/img/placeholder.jpg') }}" data-src="{{ static_asset('assets/img/shopping-online.png') }}" />
                                <h2 class="align-top">Our Positioning</h2>
                                <p>
                                    To Internet users across the region, Urbamart offers a one-stop online shopping experience that provides a wide selection of products, a social community for exploration, and seamless fulfilment services.
                                </p>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4  text-center">
                                <img class="img-fluid img-thumbnail lazyload" src="{{ static_asset('assets/img/placeholder.jpg') }}" data-src="{{ static_asset('assets/img/continuous-improvement.png') }}" />
                                <h2 class="align-top">Our Personality</h2>
                                <p>
                                    To define who we are - how we talk, behave or react to any given situation - in essence, we are Simple, Happy and Together. These key attributes are visible at every step of the Urbamart journey.
                                </p>
                            </div>
                        </div>

                        <div class="row">
                            <br />
                            <br />
                        </div>
                        @if(1==0)
                        <h2 class="align-top text-center">Team Members</h2>
                        <br />
                        <div class="row">
                            <div class="col-xs-6 col-md-2 text-center">
                                <img class="img-fluid rounded-circle" src="#" />
                                <h6>
                                    <strong>Thomas Snow</strong><br />
                                    CEO/Founder
                                </h6>
                            </div>
                            <div class="col-xs-6 col-md-2 text-center">
                                <img class="img-fluid rounded-circle" src="#" />
                                <h6>
                                    <strong>Thomas Snow</strong><br />
                                    CEO/Founder
                                </h6>
                            </div>
                            <div class="col-xs-6 col-md-2 text-center">
                                <img class="img-fluid rounded-circle" src="#" />
                                <h6>
                                    <strong>Thomas Snow</strong><br />
                                    CEO/Founder
                                </h6>
                            </div>
                            <div class="col-xs-6 col-md-2 text-center">
                                <img class="img-fluid rounded-circle" src="#" />
                                <h6>
                                    <strong>Thomas Snow</strong><br />
                                    CEO/Founder
                                </h6>
                            </div>
                            <div class="col-xs-6 col-md-2 text-center">
                                <img class="img-fluid rounded-circle" src="#" />
                                <h6>
                                    <strong>Thomas Snow</strong><br />
                                    CEO/Founder
                                </h6>
                            </div>
                            <div class="col-xs-6 col-md-2 text-center">
                                <img class="img-fluid rounded-circle" src="#" />
                                <a href="javsscript:;">
                                    <h6><strong>See more</strong></h6>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <br />
                            <br />
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12 col-lg-7">
                                <h3 class="text-center pb-4"><strong>Our Values</strong></h3>
                                <div class="row">
                                    <div class="col-md-12 col-lg-6 text-left">
                                        <h4>We Serve</h4>

                                        <ul>
                                            <li>Customers are always right</li>
                                            <li>Exceed customer expectations, deliver above and beyond</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-12 col-lg-6 text-left">
                                        <h4>We Adapt</h4>
                                        <ul>
                                            <li>Anticipate changes and plan ahead</li>
                                            <li>Accept unanticipated changes and make things happen</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-lg-6 text-left">
                                        <h4>We Run</h4>
                                        <ul>
                                            <li>Self-driven to deliver, don’t need anyone to push</li>
                                            <li>Always have a sense of urgency to get things done</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-12 col-lg-6 text-left">
                                        <h4>We Commit</h4>
                                        <ul>
                                            <li>Be reliable, do what we say we will do</li>
                                            <li>Uphold high standards; don’t take shortcuts, even when no one is watching</li>
                                            <li>Act like an owner; proactively find ways to make our organization better</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-lg-6 text-left">
                                        <h4>We Stay Humble</h4>
                                        <ul>
                                            <li>Believe we are always the underdog, and seek to learn from the market and competitors</li>
                                            <li>Accept that we are not perfect, and will never be</li>
                                            <li>Work hard first, celebrate and enjoy later</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-12 col-lg-6 text-left">
                                        <h4></h4>
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-5" id="accordion">
                                <h3 class="text-center"><strong>What can we do for you ?</strong></h3>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="text-reset" data-toggle="collapse" href="#A1579772315630-49ee73a1-fd30">
                                            <h5 class="mb-0">Support 24/7</h5>
                                        </a>
                                    </div>
                                    <div id="A1579772315630-49ee73a1-fd30" class="collapse show" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>
                                                Coming Soon
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card pb-0">
                                    <div class="card-header">
                                        <a class="text-reset" data-toggle="collapse" href="#A1579772315630-49ee73a1-fd31">
                                            <h5 class="mb-0">Best Quality</h5>
                                        </a>
                                    </div>
                                    <div id="A1579772315630-49ee73a1-fd31" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>
                                                Coming Soon
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="text-reset" data-toggle="collapse" href="#A1579772315630-49ee73a1-fd32">
                                            <h5 class="mb-0">Fastest Delivery</h5>
                                        </a>
                                    </div>
                                    <div id="A1579772315630-49ee73a1-fd32" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>
                                                Coming Soon
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
