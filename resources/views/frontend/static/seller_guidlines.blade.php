@extends('frontend.layouts.app') @section('content')

 <section class="pt-4 mb-4">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 text-center text-lg-left">
                <h1 class="fw-600 h4">{{ translate('Seller Guidelines') }}</h1>
            </div>
            <div class="col-lg-6">
                <ul class="breadcrumb bg-transparent p-0 justify-content-center justify-content-lg-end">
                    <li class="breadcrumb-item opacity-50">
                        <a class="text-reset" href="{{ route('home') }}">{{ translate('Home')}}</a>
                    </li>
                    <li class="text-dark fw-600 breadcrumb-item">
                        <a class="text-reset" href="{{ route('seller-guidlines') }}">"{{ translate('Seller Guidelines') }}"</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="mb-4">
    <div class="container">
        <div class="p-4 bg-white rounded shadow-sm overflow-hidden mw-100 text-left"> 
                    <div class="row">
                        <div class="col col-sm-12">
                            <p>
                                When listing products on urbamart.com, you must comply with the following policy guidelines. Your account will be suspended if you breach Seller Terms and Conditions, and list content or products falling
                                under the Prohibited List of Products.
                            </p>
                            <p>
                                Below is a quick description of the key terms that apply to all our sellers. There are important details in the whole Seller Terms and Conditions, so you should read it.
                            </p>
                            <p>
                                <b>1</b>.&nbsp;<b>Urbamart Intellectual Property Right Policy</b>. We take any violations of intellectual property rights seriously. We are committed to removing listing and selling of products
                                or materials that infringe someone else’s Intellectual Property Right ( <b>IPR</b>).
                            </p>
                            <p>
                                <b>2</b>.&nbsp;<b>Counterfeits or Refurbished Products.</b>&nbsp;Consumer trust is very important to us. It is a criminal offence to try to financially gain by using Intellectual Property Right without the
                                owner’s permission. Urbamart will remove your listing if we suspect your products are not genuine. Urbamart vigorously pursues sellers who sell counterfeit products. We work with
                                local law enforcement officials to try to stop the sale of counterfeit products.
                            </p>
                            <p>
                                <b>3</b>.&nbsp;<b>Cancellation of Orders.&nbsp;</b>You cannot delay a confirmed purchase order or cancel a confirmed order. Any cancellation of a confirmed order will be subject to penalty by both consumer
                                affairs authorities and us.
                            </p>
                            <p>
                                <b>4</b>.&nbsp;<b>Misleading Product Information and Advertisement.</b>&nbsp;It is your duty not to place any wrong information about your products, your identity or a brand on the Site. Your advertisement
                                must not be misleading. This means that you will not engage in any unfair trade practices, which is a criminal offence that would also involve damages.
                            </p>
                            <p>
                                <b>5</b>.&nbsp;<b>Product Safety.&nbsp;</b>Product Safety is our top priority. It is also the foundation of customer trust. Ensuring the safety of your products is an important criterion for selling on
                                Urbamart. You will remain always responsible for products or any features that harm or threatens to harm individuals, and or properties.
                            </p>
                            <p>
                                <b>6</b>.&nbsp;<b>Your Content.&nbsp;</b>You are solely responsible for all text, documents or other content or information uploaded, entered or otherwise transmitted by you in connection with your use of the
                            Site. The website Terms and Conditions for use contain detailed information on content that are not prohibited on the site. So read the terms and conditions&nbsp; <a href="{{route('terms')}}"><span><b>here</b></span></a>, as they apply to
                                you as well.
                            </p>
                            <p>
                                <b>7</b>.&nbsp;<b>Your Release.</b>&nbsp;You remain always responsible for all and any dispute with an end user or a third party. You will release Urbamart and each of our affiliates (and their
                                respective employees, directors, agents and representatives) from and against any and all claims, costs, losses, damages, judgments, penalties, interest and expenses (including reasonable attorneys’ fees)
                                arising out of any claim, action, audit, investigation, inquiry or other proceedings instituted by a third party, entity or a government agency, that arises out of or relates to your conduct, the products you
                                sell, any content you provide, the advertisement, offer, sale or return of any products you sell, any actual or alleged infringement of any intellectual property or proprietary rights by any products you sell
                                or content you provide.
                            </p>
                            <p>
                                <b>8. Claims.&nbsp;</b>Whilst Seller terms contain detailed provisions for breaches or claims, we would like to highlight that we will withhold all payments to you and may recover additional amounts as
                                requires for breach of above terms and Seller Terms. Depending on the nature of a claim, we will ask you to compensate customers and do all other necessary remedial actions without prejudice to our rights and
                                remedies.
                            </p>
                            <p>
                                <b>9</b>.&nbsp;<b>Questions?&nbsp;</b>We are always happy to help with questions you
                                might have! Check out our FAQs or contact [bloginfo info=’email’] for any questions in respect to Site Terms.
                            </p>
                        </div>
                    </div>
                 
</div>
    </div>
</section>
@endsection
