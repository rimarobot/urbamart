@extends('frontend.layouts.app') @section('content')

@section('content')
<section class="pt-4 mb-4">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 text-center text-lg-left">
                <h1 class="fw-600 h4">{{ translate('Contact Us') }}</h1>
            </div>
            <div class="col-lg-6">
                <ul class="breadcrumb bg-transparent p-0 justify-content-center justify-content-lg-end">
                    <li class="breadcrumb-item opacity-50">
                        <a class="text-reset" href="{{ route('home') }}">{{ translate('Home')}}</a>
                    </li>
                    <li class="text-dark fw-600 breadcrumb-item">
                        <a class="text-reset" href="{{ route('contact-us') }}">"{{ translate('Contact Us') }}"</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="mb-4">
    <div class="container">
        <div class="p-4 bg-white rounded shadow-sm overflow-hidden mw-100 text-left">
        <div class="row">
            <div class="col">
                <div class="p-2 bg-white">
                    <div class="row">
                        <div class="col-md-12 col-lg-6 order-md-0 order-lg-1">
                            <div class="map-responsive">
                                 <iframe
                                    width="100%"
                                    height="250"
                                    src="https://maps.google.com/maps?q=Zbyte%20Technology%20Ltd&t=&z=15&ie=UTF8&iwloc=&output=embed"
                                    frameborder="0"
                                    scrolling="no"
                                    marginheight="0"
                                    marginwidth="0">
                                </iframe>
                            </div>
                            <div>
                                <h2>Our Address</h2>
                                <hr/>
                                <p>
                                    {{get_setting('contact_address')}}<br />
                                    Support {{get_setting('contact_phone')}}<br />
                                    Email: <a href="mailto:{{get_setting('contact_email')}}">{{get_setting('contact_email')}}</a>
                                </p>
                                <h3>Careers</h3>
                                <p>If you’re interested in employment opportunities at {{get_setting('company_name')}}, please email us: <a href="mailto:{{get_setting('contact_email')}}">{{get_setting('contact_email')}}</a></p>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6   oorder-md-1 order-lg-0">
                            <h2 class="contact-page-title">Leave us a Message</h2>
                            <hr/>
                            <p>
                                Message us with any questions or inquiries or call  <strong>{{get_setting('contact_phone')}}</strong>. We would be happey to answer your questions.
                            </p>
                            <div class="form">
                                <form method="post">

                                    <div class="form-group row">
                                        <div class="col-md-12 col-lg-6">
                                            <label>First name</label>
                                            <span class="required-star">*</span><br />
                                            <input type="text" name="first-name" value="" size="40" class="form-control mb-1" required/>
                                        </div>
                                        <div class="col-md-12 col-lg-6">
                                            <label>Last name</label>
                                            <span class="required-star">*</span><br />
                                            <input type="text" name="last-name" value="" size="40" class="form-control mb-1" required />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-12 col-lg-6">
                                            <label>Email</label>
                                        <span class="required-star">*</span><br />
                                        <input type="text" name="email" value="" size="40" class="form-control mb-1" required/>
                                        </div>
                                        <div class="col-md-12 col-lg-6">
                                            <label>Phone</label>
                                        <span class="required-star">*</span><br />
                                        <input type="text" name="phone" value="" size="40" class="form-control mb-1" required/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Subject</label>
                                        <span class="required-star">*</span><br />
                                        <input type="text" name="subject" value="" size="40" class="form-control mb-1" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Your Message</label><br />
                                        <textarea name="your-message" cols="40" rows="10" class="form-control mb-1" required></textarea>
                                    </div>
                                    <div class="form-group clearfix">
                                        <p><input type="submit" value="Send Message" class="btn btn-primary fw-600" /></p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div>
    </div>
</section>
@endsection

@endsection
