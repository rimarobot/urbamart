@extends('frontend.layouts.app') @section('content')

<section class="pt-4 mb-4">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 text-center text-lg-left">
                <h1 class="fw-600 h4">{{ translate('Buyer Protection') }}</h1>
            </div>
            <div class="col-lg-6">
                <ul class="breadcrumb bg-transparent p-0 justify-content-center justify-content-lg-end">
                    <li class="breadcrumb-item opacity-50">
                        <a class="text-reset" href="{{ route('home') }}">{{ translate('Home')}}</a>
                    </li>
                    <li class="text-dark fw-600 breadcrumb-item">
                        <a class="text-reset" href="{{ route('buyer-protection') }}">"{{ translate('Buyer Protection') }}"</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="mb-4">
    <div class="container">
        <div class="p-4 bg-white rounded shadow-sm overflow-hidden mw-100 text-left">
                @php
                   $refund_request_time = \App\BusinessSetting::where('type', 'refund_request_time')->first()->value;
                @endphp

                    <div class="row">
                        <div class="col">
                            <h5 class="text-center">
                                <strong>MONEY BACK GUARANTEE</strong>
                            </h5>
                            <p>
                                We promise your money back if the item you received is not as described, or if your item
                                is not delivered within the Buyer Protection period. You can get a refund {{$refund_request_time}} days after
                                the claim process finishes.
                                This guarantee is in addition to and does not limit your statutory rights as a consumer,
                                as granted by all mandatory laws and regulations applicable in your country of
                                residence.
                            </p>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <h5 class="text-center"><strong>1. Contact Seller</strong></h5>
                                    <p>Go to your order history and select the item. Discuss the issue with the seller
                                        and see possible solutions.</p>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <h5 class="text-center"><strong>2. Apply for Refund</strong></h5>
                                    <p>
                                        If you can’t come to an amicable agreement after contacting the seller, simply
                                        raise a claim by opening a dispute within {{$refund_request_time}} days following the delivery of your
                                        order or the end of the package receipt
                                        confirmation period (as listed in your order details).
                                    </p>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <h5 class="text-center"><strong>3. Get Money Back in {{$refund_request_time}} Days</strong></h5>
                                    <p>
                                        Most sellers will return your money in {{$refund_request_time}} days, however; if it’s not resolved,
                                        you can contact Urbamart from the order detail page to escalate
                                        your dispute.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-3"></div>
                        <div class="col-md-12 col-lg-9">
                            <h2>
                                <strong>Free Return</strong>
                            </h2>
                            <p>
                                <span data-spm-anchor-id="a2g0o.ams_81407.w69fe89.i2.654fFW7XFW7XA0">
                                    Sellers that offer ‘Free Return’ service will have ‘Free Return’ icon on their item
                                    details page. With ‘Free Return’ service, you get an extra {{$refund_request_time}} days (on top of the
                                    on-time delivery date) to decide if
                                    you want to keep or return the item for any reason, as long as they are unused,
                                    unwashed and in their original condition – complete with the original packaging,
                                    tags, etc. It’s easy and fast:
                                </span>
                            </p>
                            <p><strong>Easy</strong></p>
                            <ul>
                                <li>
                                    You could return items locally and easily: Get the return code or the return label,
                                    Go to the designated logistics company’s service point to complete the return and
                                    wait for the local warehouse to
                                    receive your returned parcel.
                                </li>
                                <li>Remember to select ‘Return Goods” while opening the dispute.</li>
                            </ul>
                            <p>
                                <strong><span id="s198bba">Fast</span></strong>
                            </p>
                            <ul>
                                <li>Refund will be made to you once we confirm receipt of your returned parcel which
                                    meet the conditions for reimbursement.</li>
                                <li>Items will be processed at a local warehouse, instead of being sent back to the
                                    seller’s country of origin</li>
                            </ul>
                            <p><strong>Return Shipment Fee</strong></p>
                            <ul>
                                <li><span id="e9abfba">It is free for you to return the parcel to the designated local
                                        return address.</span></li>
                                <li>
                                    <span id="e9abfba">
                                        Note: You can only enjoy one-time free local return for one order. If you
                                        initiate multiple return requests under an order, you can only enjoy the free
                                        local return service once, and other return
                                        requests can still enjoy local return service but you will have to cover the
                                        return shipping costs to the designated local return address. Therefore, please
                                        consider returning jointly the products to
                                        avoid paying that additional shipping cost.
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-3"></div>
                        <div class="col-md-12 col-lg-9">
                            <h2>
                                <strong>Local Return</strong>
                            </h2>
                            <p>
                                <span id="b6bf1b9">
                                    <span data-spm-anchor-id="a2g0o.ams_81407.m1814b8.i1.654f4liT4liT1u">
                                        Sellers that offer ‘Local Return’ service will have the above ‘Local Return’
                                        icon on their item details page. With ‘Local Return’ service, you could get an
                                        extra {{$refund_request_time}} days (on top of the on-time
                                        delivery date) to decide if you want to keep or return the item without any
                                        reason.
                                    </span>
                                </span>
                            </p>
                            <p>
                                <strong><span id="b6bf1b9">Return Shipment Fee</span></strong>
                            </p>
                            <p>
                                <span id="b6bf1b9"> <span id="p1a12b8">The return shipping costs to the designated local
                                        return address are borne by the buyer.</span><span>&nbsp;</span>goods will be
                                    paid by the seller. </span>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-3"></div>
                        <div class="col-md-12 col-lg-9">
                            <h2>
                                <strong>Statutory rights</strong>
                            </h2>
                            <p>
                                The seller must comply with other relevant legal requirements (warranty, return, etc.)
                                that may be applicable depending on your location as buyer.
                            </p>
                        </div>
                    </div>
 </div>
    </div>
</section>
@endsection
