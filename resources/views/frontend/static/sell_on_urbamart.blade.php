@extends('frontend.layouts.app') @section('content')

<section class="pt-4 mb-4">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 text-center text-lg-left">
                <h1 class="fw-600 h4">{{ translate('Selling on UrbaMart') }}</h1>
            </div>
            <div class="col-lg-6">
                <ul class="breadcrumb bg-transparent p-0 justify-content-center justify-content-lg-end">
                    <li class="breadcrumb-item opacity-50">
                        <a class="text-reset" href="{{ route('home') }}">{{ translate('Home')}}</a>
                    </li>
                    <li class="text-dark fw-600 breadcrumb-item">
                        <a class="text-reset" href="{{ route('sell-here') }}">"{{ translate('Selling on UrbaMart') }}"</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="mb-4">
    <div class="container">
        <div class="p-4 bg-white rounded shadow-sm overflow-hidden mw-100 text-left">
                    <div class="row">
                        <div class="col col-sm-12">
                            <h3 class="text-center">The Benefits of Selling on Urbamart</h3>
                            <br />
                            <div class="row">
                                <div class="col-md card m-2 text-center">
                                    <i class="las la-chart-bar la-5x p-2" style="color:var(--primary);" aria-hidden="true"></i>
                                    <h5>Power in numbers</h5>
                                    <p>
                                        99% brand awareness, 10M visits to urbamart.com per month, and 5.8MM e-newsletter subscribers.
                                    </p>
                                </div>
                                <div class="col-md card m-2 text-center">
                                    <i class="las la-laptop la-5x p-2" style="color:var(--primary);" aria-hidden="true"></i>
                                    <h5>You are in control</h5>
                                    <p >
                                        The Urbamart Marketplace platform allows you to add your products, set your price and inventory, and manage your orders through our Marketplace interface. API integration is also
                                        available for more sophisticated sellers looking to fully integrate products, pricing, inventory, and order management.
                                    </p>
                                </div>
                                <div class="col-md card m-2 text-center">
                                    <i class="lab la-connectdevelop la-5x p-2" style="color:var(--primary);" aria-hidden="true"></i>
                                    <h5>There’s more to us</h5>
                                    <p>
                                        Urbamart is rapidly expanding in new categories such as Baby, Luggage, Furniture, Jewelry, and more!
                                    </p>
                                </div>
                            </div>
                            <br>
                            <br>
                        </div>
                    </div>
                    <div class="row bg-light">
                        <div class="col col-sm-12">
                            <h3 class="text-center">How to Start Selling on our MarketPlace</h3>
                            <br />
                            <div class="row">
                                <div class="col-md card m-2">
                                    <i class="las la-user-plus la-5x p-2 text-center" style="color:var(--primary);" aria-hidden="true"></i>
                                    <h5 class="text-center">
                                        Apply to be a Seller
                                    </h5>
                                    <p>Fill the Sellers form and your account will be activated instantly.</p>
                                    <div class="text-center">
                                    <a class="btn btn-primary btn-sm fw-600 m-2"  href="{{route('shops.create')}}" role="button">
                                        Apply Now
                                    </a></div>
                                </div>
                                <div class="col-md card m-2">
                                    <i class="las la-store la-5x p-2 text-center" style="color:var(--primary);" aria-hidden="true"></i>
                                    <h5 class="text-center">
                                         Set up your store on Urbamart
                                    </h5>
                                    <ul>
                                        <li>Setup your store page</li>
                                        <li>Add your products and prices</li>
                                        <li>Manage your Orders and Shipping</li>
                                    </ul>
                                </div>
                                <div class="col-md card m-2">
                                    <i class="las la-cart-arrow-down la-5x p-2 text-center" style="color:var(--primary);" aria-hidden="true"></i>
                                    <h5  class="text-center">
                                        Start selling and get paid
                                    </h5>
                                    <p>You will receive Notification for new Orders, and Payments</p>
                                </div>
                            </div>
                            <br>
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-sm-12">
                            <h3 class="text-center">
                                Everything you need to easily expand your business and start selling your products fast. We're here for you!
                            </h3>
                            <br />
                            <div class="row">
                                <div class="col-md m-2 card text-center">
                                    <i class="las la-money-bill-wave-alt la-5x p-2" style="color:var(--primary);" aria-hidden="true"></i>
                                    <h5>Its Free</h5>
                                    <p><em>No setup or running costs</em></p>
                                </div>
                                <div class="col-md m-2 card text-center">
                                    <i class="las la-headphones la-5x p-2" style="color:var(--primary);" aria-hidden="true"></i>
                                    <h5>Support</h5>
                                    <p><em>Accept great customer support</em></p>
                                </div>
                                <div class="col-md m-2 card text-center">
                                    <i class="las la-cart-plus la-5x p-2" style="color:var(--primary);" aria-hidden="true"></i>
                                    <h5>Sell Online</h5>
                                    <p><em>Easy E-Commerce</em></p>
                                </div>
                                <div class="col-md m-2 card text-center">
                                    <i class="las la-landmark la-5x p-2" style="color:var(--primary);" aria-hidden="true"></i>
                                    <h5>Minimal Commission</h5>
                                    <p><em>We only make <strong>{{\App\BusinessSetting::where('type', 'vendor_commission')->first()->value}}%</strong> when you sell</em></p>
                                </div>
                            </div>
                            <br>
                            <br>
                            <ul>
                                <li>Comprehensive training on how to add your products</li>
                                <li>Ongoing technical support and assistance on daily operations and customer service</li>
                                <li>Secured and trusted online payment processing solution</li>
                                <li><strong>{{\App\BusinessSetting::where('type', 'vendor_commission')->first()->value}}%</strong> Payment processing fees deducted off the Customer Order</li>
                                <li>Leading fraud prevention solution</li>
                                <li>Opportunities to highlight your products: forums, blogs, product reviews, and more!</li>
                            </ul>

                            <br>
                            <br>
                            <div class="row bg-light p-2">
                                <div class="col-md-6">
                                    <h3>
                                        Your Brand, Your E-commerce Shop
                                    </h3>
                                    <p>
                                        We all work so hard to build our businesses, our brands. Why get diluted by selling on some crowded marketplace where your hard-work gets dissolved and lost.
                                    </p>
                                    <p>
                                        Urbamart allows you to keep your own brand while providing the performance and quality of a giant store. We build you, your own free e-commerce shop, that looks and feels like you do.
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <img class="img-fluid img-thumbnail lazyload" src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                    data-src="{{ static_asset('assets/img/T_55-1-1024x718-1-500x351.png') }}" />
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row p-2">
                                <div class="col-md">
                                    <img class="img-fluid img-thumbnail lazyload" src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                    data-src="{{ static_asset('assets/img/boss.png') }}" />
                                </div>
                                <div class="col-md">
                                    <h3>
                                        Be the boss
                                    </h3>
                                    <p>Manage your business centrally</p>
                                    <p>
                                        Running your online shop should be every bit as simple as selling in a physical one. Urbamart enables<br />
                                        you to run your business in the most efficient and easy ways.
                                    </p>
                                    <ul>
                                        <li>Customize your online shop</li>
                                        <li>Keep track of your sales, products and inventory</li>
                                        <li>Quick and easy order management</li>
                                        <li>Monitor growth and sales</li>
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row bg-light p-2">
                                <div class="col-md">
                                    <h3>
                                        In-Built Delivery
                                    </h3>
                                    <p>We take care of your deliveries so that you can focus on building your business.</p>
                                    <p>We pick up items that customers have bought from you, and we deliver to them to the customer at *no cost to you (merchant).</p>
                                </div>
                                <div class="col-md">
                                    <img class="img-fluid img-thumbnail lazyload" src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                    data-src="{{ static_asset('assets/img/delivery-concept.png') }}" />
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row">
                                <div class="col-md">
                                    <img class="img-fluid img-thumbnail lazyload" src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                    data-src="{{ static_asset('assets/img/credit-card-payment-concept.png') }}" />
                                </div>
                                <div class="col-md">
                                    <h3>
                                        Safe, Fast &amp; Easy Payments
                                    </h3>
                                    <p>
                                        Urbamart&nbsp; allows your customers to easily pay you online, either on mobile or card. No more con games or lost revenue.
                                    </p>
                                    <p>
                                        You also receive your money&nbsp;<strong>instantly</strong>. We don’t hold your money, and no one should.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row p-2">
                        <div class="col col-sm-12">
                            <h3>
                                Frequently Asked Questions
                            </h3>
                            <br />
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header">
                                         <a class=" text-reset" data-toggle="collapse" aria-expanded="false" href="#A1579772315630-49ee73a1-fd30">
                                          <h5 class="mb-0"> What is an online marketplace?</h5>
                                        </a>
                                    </div>
                                    <div id="A1579772315630-49ee73a1-fd30" class="collapse show" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>
                                                A marketplace is an ecommerce site that works like an online shopping mall. Sellers provide pricing, product descriptions, fulfillment, shipping & returns, and transactions are processed by
                                                the marketplace operator; in this case Urbamart. Urbamart gives sellers the opportunity to add their catalog to urbamart.com and take advantage of the marketing, advertising, and millions of
                                                visitors that come with a leading brand.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="text-reset" data-toggle="collapse" aria-expanded="false"  href="#A1579772315664-54ca9cc5-6828">
                                            <h5 class="mb-0">Why should I sell on Urbamart Marketplace?</h5>
                                        </a>
                                    </div>
                                    <div id="A1579772315664-54ca9cc5-6828" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <ul>
                                                <li>Reach the millions of customers that visit urbamart.com.</li>
                                                <li>Sell your products on a site customers know and trust.</li>
                                                <li>Expand the reach of your business beyond your own site.</li>
                                                <li>Take advantage of our constant investment in our eCommerce platform that continually improves the shopping experience for you and your customers.</li>
                                                <li>No need to invest in your own website. Just start selling on ours.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="text-reset" aria-expanded="false" data-toggle="collapse" href="#A1579772614222-c74a245f-7af3">
                                            <h5 class="mb-0">How much does it cost?</h5>
                                        </a>
                                    </div>
                                    <div id="A1579772614222-c74a245f-7af3" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <ul>
                                                <li>Competitive with other marketplaces operating in Uganda.</li>
                                                <li>No setup fees.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="text-reset" aria-expanded="false" data-toggle="collapse" href="#A1579772673858-10aa15bd-e58a">
                                            <h5 class="mb-0">Is my business big enough to take part?</h5>
                                        </a>
                                    </div>
                                    <div id="A1579772673858-10aa15bd-e58a" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>
                                                We’re looking for quality partners who want to grow their business with us. Click here to submit your information and we will contact you about how to take advantage of this opportunity.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="text-reset" aria-expanded="false" data-toggle="collapse" href="#A1579772761030-e2b18f6e-7101">
                                            <h5 class="mb-0">How do I sign-up?</h5>
                                        </a>
                                    </div>
                                    <div id="A1579772761030-e2b18f6e-7101" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>
                                                Simply provide us your contact information and all the details requested and our Business Development team will evaluate your request. Please remember to fill out as accurately as possible to
                                                increase your chances of becoming a seller on Urbamart Marketplace. Click here and fill-out the contact form
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="text-reset" aria-expanded="false" data-toggle="collapse" href="#A1579772871124-935efa88-f969">
                                           <h5 class="mb-0"> Who decides the shipping policy? Can I set my own return policy?</h5>
                                        </a>
                                    </div>
                                    <div id="A1579772871124-935efa88-f969" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>
                                                Sellers decide their own shipping policies and set their rates using the Urbamart Marketplace platform. You can charge any shipping fees you want – however, it is highly recommended to keep
                                                shipping fees as low as possible to increase sales. There is a certain amount of flexibility to be able to set your returns policy, but certain elements – such as number of days to return
                                                products and a limit on restocking fees – will be enforced.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="text-reset" aria-expanded="false" data-toggle="collapse" href="#A1579772983453-8d8aed0f-a3d2">
                                            <h5 class="mb-0">Can I decide which markets/regions I can ship to?</h5>
                                        </a>
                                    </div>
                                    <div id="A1579772983453-8d8aed0f-a3d2" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>
                                                Definitely! You can control where you’re able to ship products, but casting a bigger net usually provides the biggest return!
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="text-reset" aria-expanded="false" data-toggle="collapse" href="#A1579773128186-719d6f6b-61c0">
                                            <h5 class="mb-0">How long does it take to get paid?</h5>
                                        </a>
                                    </div>
                                    <div id="A1579773128186-719d6f6b-61c0" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>
                                                As we want to ensure you get the proceeds of your sales rapidly, all transactions are paid through Electronic Funds Transfer within 1 days of being confirmed as shipped by sellers.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="text-reset" aria-expanded="false" data-toggle="collapse" href="#A1579773238633-b367f34d-cb84">
                                            <h5 class="mb-0">Who ships items sold through the Urbamart Marketplace?</h5>
                                        </a>
                                    </div>
                                    <div id="A1579773238633-b367f34d-cb84" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>
                                                Sellers ship products directly to customers and we’re here to help manage the experience along the way with the support of our trained customer service staff.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="text-reset" aria-expanded="false" data-toggle="collapse" href="#A1579773290461-2ca5ac23-58d6">
                                            <h5 class="mb-0">Can I cancel and/or remove my products at any time?</h5>
                                        </a>
                                    </div>
                                    <div id="A1579773290461-2ca5ac23-58d6" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>
                                                Yes. We have developed a simple and easy-to-use platform for sellers that provides full control over product content, inventory, shipping fees, and more..
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="text-reset" aria-expanded="false" data-toggle="collapse" href="#A1579773405652-d71db957-4c9a">
                                            <h5 class="mb-0">Can all types of products be added to the marketplace?</h5>
                                        </a>
                                    </div>
                                    <div id="A1579773405652-d71db957-4c9a" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>
                                                At launch, assortment will be limited to existing categories but it will rapidly expand to new ones based on seller demand. Each time a new category is added, it will be communicated to all
                                                sellers to ensure they can add their products.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="text-reset" aria-expanded="false" data-toggle="collapse" href="#A1579773475577-c0dc7630-9421">
                                            <h5 class="mb-0">Can customers return products to your stores?</h5>
                                        </a>
                                    </div>
                                    <div id="A1579773475577-c0dc7630-9421" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>
                                                At this time customers are not able to return Marketplace products in-store.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="text-reset" aria-expanded="false" data-toggle="collapse" href="#A1579773503058-4d405bd4-3631">
                                            <h5 class="mb-0">Can customers have my products shipped to your stores?</h5>
                                        </a>
                                    </div>
                                    <div id="A1579773503058-4d405bd4-3631" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>
                                                No. All orders must be shipped directly to an address provided by the customer.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        <a class="text-reset" aria-expanded="false" data-toggle="collapse" href="#A1579773564362-48aae027-d492">
                                            <h5 class="mb-0">Are sellers protected from fraud?</h5>
                                        </a>
                                    </div>
                                    <div id="A1579773564362-48aae027-d492" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>
                                                Yes. All orders submitted through the urbamart.com platform go through a robust order verification process. Any suspicious orders are manually investigated to ensure most fraudulent
                                                orders are cancelled before even being released to our sellers. We’re proud to have a fraud ratio way below the market average.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
   </div>
    </div>
</section>

@endsection
