@extends('frontend.layouts.app') @section('content')

 <section class="pt-4 mb-4">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 text-center text-lg-left">
                <h1 class="fw-600 h4">{{ translate('Terms of Service Urbamart Express') }}</h1>
            </div>
            <div class="col-lg-6">
                <ul class="breadcrumb bg-transparent p-0 justify-content-center justify-content-lg-end">
                    <li class="breadcrumb-item opacity-50">
                        <a class="text-reset" href="{{ route('home') }}">{{ translate('Home')}}</a>
                    </li>
                    <li class="text-dark fw-600 breadcrumb-item">
                        <a class="text-reset" href="{{ route('payment-methods') }}">"{{ translate('Terms of Service Urbamart Express') }}"</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="mb-4">
    <div class="container">
        <div class="p-4 bg-white rounded shadow-sm overflow-hidden mw-100 text-left">
             <div class="row">
            <div class="col">
                <div class="p-4 bg-white">
                    <p>
                        Urbamart Express is an additional shipping add-on service operated and managed by {{get_setting('company_name')}} with buyers and sellers agreeing to Urbamart. Express provides a delivery service for products
                        purchased and sold on the site of {{get_setting('website_name')}} Without limitation on exclusive rights and will provide the services as stated in these Terms of Service. (“Urbamart Express Terms of
                        Service”) when a buyer purchases a product from a seller. Such merchandise will be delivered by Urbamart Express ("Urbamart Express Parcels"). Buyer and Seller agree to the terms and conditions stated in Shop's Terms
                        of Service. This Express Edition is in addition to the Urbamart Terms of Service and other policies which are Urbamart's Terms of Service. Express forms part of these
                    terms and policies - please read <a href="{{route('terms')}}"> Other Urbamart's Terms of Service</a> and <a href="{{ route('sellerpolicy') }}">Policies </a>

                        displayed on the Site. As the terms and policies contain important information about your rights and obligations, any wording used in the Urbamart Terms of Service which is not defined in this
                        document, has the same meaning as the wording in the applicable Urbamart Terms of Service and / or Policies. All terms and conditions may be changed at the discretion of Urbamart or Urbamart Express in accordance with the Terms of Service without the need for your prior consent.
                    </p>
                        <br />
                    <p class="MsoNormal">
                        <strong>
                            1. Products sent and delivered by Urbamart Express
                        </strong>
                    </p>
                    <p>
                        1.1 When a buyer purchases goods from a seller The above products will be shipped using Urbamart Express, the buyer must specify the delivery time. Payment methods and shipping addresses for Urbamart packages. Such
                        express ("Shipping order") by Urbamart Express will deliver the parcel. The said express is given to the buyer in accordance with the delivery order and will charge the seller a shipping fee. Which is calculated
                        according to shopping terms and conditions.

                        <br />
                        1.2 Urbamart's parcel delivery address. Express must be within delivery zone. According to Urbamart Express is designated and posted on the website from time to time, and Urbamart Express may modify the delivery zone
                        at any time. According to Urbamart discretion

                        <br />
                        1.3 Urbamart Express will deliver the parcel to the buyer whose name and address appear in the direct delivery order. In the event that the buyer is not at the delivery address at the time of delivery,
                        Urbamart Express may deliver the parcel to (a) receptionist or Front desk staff Or internal security personnel If the delivery address is a commercial building or (b) a close family member or housewife In the
                        event that the delivery address is a residential building (“recipient”), upon delivery of the product, Urbamart Express will require the buyer or receiver to sign the electronic form of the consignment note. Name and
                        status of the buyer or recipient (“Consignment”) and a signed delivery slip are considered final evidence that Urbamart's parcel has been delivered.
                    </p>
                    <p>
                        1.4 Failed delivery
                    </p>
                    <p>
                        1.4.1 Failed deliveries can occur for a variety of reasons, such as:
                    </p>
                    <p>
                        1.4.1.1 The recipient refuses to accept the delivery of the product. Not signed on the delivery slip Or do not pay for the goods collected at destination (COD)

                        <br />
                        1.4.1.2 The recipient's identity or location can not be identified for reasonable reasons
                        <br />
                        1.4.1.3 There is no suitable person at the address of the recipient to receive the goods
                        <br />

                        1.4. 1.4 Goods shipped pending from Seller. (For example Call back to confirm delivery The delivery is postponed or shipping details change)
                    </p>
                    <p>
                        1.4.2 In the event that the product cannot be delivered for the first time due to the inability to contact the recipient, Urbamart Express will attempt to deliver the product 3 more times without additional charges.
                        If the delivery is still not possible, the merchandise will be returned to the Seller.

                        <br />

                        1.4.3 If the buyer information of the delivery order is incorrect or incomplete, Urbamart Express will suspend delivery of the product until the seller. There will be additional instructions, with Urbamart Express
                        will not be liable for any damages or expenses. In connection with the delay or cancellation of such delivery.

                        <br />
                        1.5 In the event that both Buyer and Consignee are not at the delivery address at the time of delivery, Urbamart Express may notify Buyer of the failed delivery and take Urbamart's parcel to the store at the
                        shop. Express requires the buyer to collect the product himself. Which is at Urbamart's sole discretion of Express and subject to service availability If no one comes to pick up the parcel Express within thirty (30)
                        days from the date of notification. To be considered that the parcel of Urbamart has been delivered Express, then

                        <br />

                        1.6 parcel of Urbamart Express will not be shipped to an address other than the address stated in the delivery order. Urbamart Express reserves the right to withhold delivery. In the case of Urbamart Express believes
                        that the shipping address is incorrect or incomplete.

                        <br />

                        1.7 Not withstanding any requirements stated or otherwise construed, Urbamart Express reserves the right to refuse, refuse, return or postpone delivery of the following products. ("Unacceptable goods")

                        <br />

                        (a) shopping goods that are considered or classified as hazardous materials. Dangerous goods Prohibited or restricted items by law National regulations or rules or international rules Including but not
                        limited to International Air Transport Association (IATA) International Civil Aviation Organization (ICAO)

                        <br />

                        (b) Urbamart Parcel that is likely to cause damage to the cargo, transport equipment, or other persons Or delay

                        <br />

                        (c) any item specified in Urbamart and / or Urbamart Express's restricted and prohibited items;&nbsp;

                        <br />
                        (D) money or convertible instruments with cash equivalent, such as stocks and bonds;
                        <br />
                        (e) credit cards, debit cards or cash cards;<br />

                        (f) personal documents issued by government agencies such as ID cards, passports, driving licenses, etc. or

                        <br />
                        (G) Documents containing a large amount of confidential information
                        <br />
                        (h) Cargoes that do not have suitable or adequate packaging
                        <br />
                        (i) Freight packaged in styrofoam boxes<br />
                        (j) Freight that may cause the Delayed or damaged other transport, equipment or person
                        <br />

                        (k) Goods transported with a combined length, width and height of more than 180 centimeters or weighing more than 20 kilograms

                        <br />

                        (k). Goods transported for which the Company must have a license. Or requesting special permission to transport, import or export&nbsp;

                        <br />

                        (M) In the case of Urbamart receiving packages that may endanger Urbamart Express' services to other customers and businesses in accordance with Urbamart. Express solely determines

                        <br />

                        1.8.Regardless of the above terms in this Terms, Urbamart Express shall not be liable for any loss or damage. Of the items listed in item 1.7 above

                        <br />

                        1.9 shopping parcel may be examined by means, which may include x-ray, explosive detection. And other security inspection methods and Urbamart Express reserves the right to open and inspect Urbamart's
                        parcels. Express without prior notice to buyer or seller
                    </p>
                    <p>
                        <br />
                    </p>
                    <p>
                        <br />
                    </p>
                    <p>
                        <strong>
                            2. Calculation of the weight and shipping costs of Urbamart Express
                        </strong>
                    </p>
                    <p>
                        2.1 Shipping rates on Urbamart Express is determined by Urbamart Express and posted on the from time to time. Urbamart Express may change the shipping rates of Urbamart Express at any time Without having to notice. Such changes are subject to Urbamart's discretion.
                    </p>
                    <p>
                        2.2 For the purpose of calculating the shopping cost of Urbamart Express, the weight of the Urbamart parcel Express will be based on actual weight or volumetric weight. (Rounded to the nearest kilogram), whichever is
                        greater
                    </p>
                    <p>
                        2.3 Volumetric weight of Urbamart parcel Express calculates using the formula Length (cm.) X Height (cm.) X Width (cm.) / 5,000.
                    </p>
                    <p>
                        2.4 The buyer will be informed of the shipping cost. Express and the above shipping charges are to be paid at the time of purchase of Urbamart Express packages.
                    </p>
                    <p>
                        <br />
                    </p>
                    <p>
                        <strong>
                            3. Cash on Delivery (COD) and Surcharge for Cash on Delivery
                        </strong>
                    </p>
                    <p>
                        3.1 If the buyer chooses to use the cash on delivery service The seller will be charged for cash on delivery at the rate specified by Urbamart Express.
                    </p>
                    <p>
                        3.2 Seller assumes all risks associated with any claim. This includes the absence of payments, insufficient funds and forging of documents.
                    </p>
                    <p>
                        <br />
                    </p>
                    <p>
                        <strong>
                            4. Parcel restrictions
                        </strong>
                    </p>
                    <p>
                        4.1 Package size restrictions&nbsp;
                    </p>
                    <p>
                        &nbsp; &nbsp; &nbsp;The total length, width, and height of the outer side per box is no more than 180 centimeters. In addition, the length, width or height of the box cannot exceed 100 centimeters. size And in
                        imperfect condition
                    </p>
                    <p>
                        4.2 Package weight restrictions&nbsp;
                    </p>
                    <p>
                        &nbsp; &nbsp; &nbsp; 20 kg per box. Urbamart Express reserves the right to refuse packages that exceed the weight restrictions.
                    </p>
                    <p>
                        4.3 Restrictions for Branches&nbsp;
                    </p>
                    <p>
                        &nbsp; &nbsp; &nbsp;Restriction: Weight less than 20 kg per box. For parcel weight restrictions Size limitation: the total length of the long side. Width and height per piece 150 cm. In addition, the length, width or
                        height of the box must not exceed 180 cm. The branch-based courier project is intended to provide services to merchants or shippers who deliver packages no more than 20. Each box per pick-up at each branch, the
                        cut-off time at the Urbamart Express courier is 4.00 PM. Urbamart Express reserves the right to refuse to accept packages that are over the size or weight restrictions.
                    </p>
                    <p>
                        <br />
                    </p>
                    <p>
                        <strong>
                            5. Taxes and duties
                        </strong>
                    </p>
                    <p>
                        5.1 Buyer or Seller (As the case may be) must pay any taxes and / or duties Applicable to Urbamart Express delivery
                    </p>
                    <p>
                        5.2 In the event that the Buyer or Seller fails to perform the tax duties in connection with Urbamart Express Parcel Buyers or Sellers (As the case may be) is obliged to pay the said tax in accordance with the
                        applicable laws and regulations. And shall indemnify damages or expenses resulting from the buyer or seller non-performing of the tax duty.
                    </p>
                    <p>
                        <br />
                    </p>
                    <p>
                        <strong>
                            6. Loss or damage during delivery.
                        </strong>
                    </p>
                    <p>
                        6.1 In the case of shopping goods Express damaged during delivery Or did not have to ship the above. Buyers must notify Urbamart Express note in writing within the warranty period of Urbamart. If the buyer does not
                        notify or Urbamart Express will not be liable for anything. Per buyer or seller
                    </p>
                    <p>
                        6.2 The buyer must keep the box. All original packaging and contents are kept in order for Urbamart to Express does an audit Such items must be retained until a conclusion of the damage inspection or the case when
                        the product was not shipped.
                    </p>
                    <p>
                        <br />
                    </p>
                    <p>
                        <strong>
                            7.Exclusions and limitations of liability
                        </strong>
                    </p>
                    <p>
                        7.1 Subject to Article 6 and 7, Urbamart's liability Express to buyer and seller is limited to direct damage only. And all liability of Urbamart Express to buyer and seller will not exceed 2,000 baht for Urbamart
                        Express package.
                    </p>
                    <p>
                        7.2 Under no circumstances will Urbamart Express be liable for indirect damages. Damages due to breach of contractSpecial damagesPunitive damagesOr continuous damages Including the loss of opportunities for use Loss
                        of profitOr interruption of businessNo matter what happensOr according to any theory of liability
                    </p>
                    <p>
                        7.3 Buyer and Seller agree that Urbamart Express is not liable or liable in the following cases.
                    </p>
                    <p>
                        &nbsp; &nbsp; &nbsp;1. In case of Urbamart Express is unable to ship shopping packages. Can expressIf the buyer's address is incomplete or incorrect
                    </p>
                    <p>
                        &nbsp; &nbsp; &nbsp;2. In the case of Urbamart Express does not comply with any laws and regulations. Including export control laws, penalties, restriction measures, and orders to prohibit shipping or
                    </p>
                    <p>
                        &nbsp; &nbsp; &nbsp;3. For shopping goods Express that was confiscated or destroyed by a government agency
                    </p>
                    <p>
                        <br />
                    </p>
                    <p>
                        <strong>
                            8 independent contractors
                        </strong>
                    </p>
                    <p>
                        &nbsp; &nbsp;Any provision in the Urbamart Terms of Service This Express cannot be interpreted as forming a representative, partnership or joint venture relationship between Urbamart. Express with buyers or sellers
                        and buyers, sellers and Urbamart Express does not represent or pretend to have the power to perform in any manner as an agent, partner, employee or representative of another party.
                    </p>
                    <p>
                        <br />
                    </p>
                    <p>
                        <strong>
                            9. Products at UrbamartExpress is the delivery manager.
                        </strong>
                    </p>
                    <p>
                        <strong>&nbsp; &nbsp;</strong>

                        The seller acknowledges and agrees that Urbamart Express has the right to outsource or procure other couriers to provide delivery services. (Whether providing services in whole or in part) in Uganda or elsewhere (if
                        any) and in such event the Seller agrees to be bound by the relevant terms of service of such service provider
                    </p>
                    <p>
                        <br />
                    </p>
                    <p>
                        <strong>
                            10. Force majeure
                        </strong>
                    </p>
                    <p>
                        10.1 Urbamart Express is not liable for performing any duties. Under the Urbamart Express Terms of Service due to circumstances or events beyond the control of Urbamart Express, including but not limited to:
                    </p>
                    <p>
                        (A) Natural events, including earthquakes, cyclones, storms, floods, fire, disease, fog, snow or frost

                        <br />

                        (b) war, accidents, acts of public enemies Strike The embargo air raid dispute locally or turmoil in the country

                        <br />

                        (c) disruptions of the transport network by air or by land in the national or local technical problem of how to transport or machinery

                        <br />
                        (d) Crimes of third parties such as theft and arson or<br />
                        (e) accidents.
                    </p>
                    <p>
                        <br />
                    </p>
                    <p>
                        <strong>
                            11.Governing law
                        </strong>
                    </p>
                    <p>
                        This provision of service is governed by the laws of the Republic of Uganda and both parties irrevocably agree to the courts of the Republic of Uganda as the sole court of jurisdiction.
                    </p>
                    <br />
                </div>
            </div>
        </div>
        </div>
    </div>
</section>
@endsection
