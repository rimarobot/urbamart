 @extends('frontend.layouts.app') 
 
 @section('content')

<section class="pt-4 mb-4">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 text-center text-lg-left">
                <h1 class="fw-600 h4">{{ translate('Product Listing Policy') }}</h1>
            </div>
            <div class="col-lg-6">
                <ul class="breadcrumb bg-transparent p-0 justify-content-center justify-content-lg-end">
                    <li class="breadcrumb-item opacity-50">
                        <a class="text-reset" href="{{ route('home') }}">{{ translate('Home')}}</a>
                    </li>
                    <li class="text-dark fw-600 breadcrumb-item">
                        <a class="text-reset" href="{{ route('product-listing-policy') }}">"{{ translate('Product Listing Policy') }}"</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="mb-4">
    <div class="container">
        <div class="p-4 bg-white rounded shadow-sm overflow-hidden mw-100 text-left">
                    <div class="row">
                        <div class="col col-sm-10"> 
                            <p>
                                You may not post or sell any item that is restricted or prohibited by a federal, state or local law in any country or jurisdiction. Please be aware that the urbamart.com website (hereinafter the “UrbaMart
                                Website”, (hereinafter “Wholesale Marketplace”)) function as a global marketplace; thus the selling or posting of items may be prohibited because of laws outside of the jurisdiction where you reside. Below,
                                we have listed some categories of prohibited or restricted items.
                            </p>
                            <p>
                                HOWEVER, THIS LIST IS NOT INTENDED TO BE EXHAUSTIVE; YOU, AS THE SELLER, ARE RESPONSIBLE FOR ENSURING THAT YOU ARE NOT POSTING AN ITEM THAT IS PROHIBITED BY LAW IN ANY JURISDICTION.
                            </p>
                            <p>
                                Unless otherwise indicated and annotated accordingly below, the list of prohibited or restricted items as listed shall be applicable to the urbamart.com Website.
                            </p>
                            <p>
                                For the purposes of this Product Listing Policy, transactions which are supported by the urbamart.com Term of Service Agreement shall be “Relevant Online Transactions”.
                            </p>
                        </div>
                    </div>
                    <a></a>1.&nbsp;&nbsp;&nbsp;&nbsp; GENERAL PROHIBITIONS

                    <div class="row">
                        <div class="col col-sm-12">
                            <ul>
                                <li>
                                    urbamart.com has chosen to also prohibit the posting of items which may not be restricted or prohibited by law but are nonetheless controversial including:
                                </li>
                            </ul>
                            <ul>
                                <li>Items that encourage illegal activities (e.g. lock pick tools, synthetic urine for cheating drug tests);</li>
                                <li>Items that are racially, religiously or ethnically derogatory, or that promote hatred, violence, racial or religious intolerance;</li>
                                <li>Giveaways, lotteries, raffles, or contests;</li>
                                <li>Stocks, bonds, investment interests, and other securities;</li>
                                <li>Pornographic materials or items that are sexual in nature;</li>
                                <li>
                                    Items that do not offer a physical product or service for sale, such as digital currencies and advertisements solely for the purpose of collecting user information.
                                    <ul>
                                        <li>urbamart.com, in its sole and exclusive discretion, reserves the right to impose additional restrictions and prohibitions.</li>
                                    </ul>
                                </li>
                            </ul>
                            <p>&nbsp;</p>
                            <ul>
                                <li>
                                    In the event of inconsistency, ambiguity or conflict of the contents of this policy with any other terms of the urbamart.com platform, or between the English and other language versions of this policy,
                                    the English version and the decision of urbamart.com exercised in its absolute discretion shall always prevail.
                                </li>
                            </ul>
                            <h4>
                                <a></a>
                                2.&nbsp;&nbsp;&nbsp;&nbsp; PROHIBITED AND CONTROLLED ITEMS
                            </h4>
                            <ul>
                                <li>
                                    ILLICIT DRUGS, PRECURSORS AND DRUG PARAPHERNALIA
                                    <ul>
                                        <li>
                                            urbamart.com expressly forbids any and all listing or sale of narcotics, tranquilizers, psychotropic drugs, natural drugs, synthetic drugs, steroids and other controlled substances (including all
                                            drugs listed in Schedules I, II, III, IV or V of the Uniform Controlled Substances Act, 21 U.S.C. 801 et seq.). Such activity can result in your account being delisted.
                                        </li>
                                        <li>The listing or sale of all drug precursor chemicals (such as those listed in the Convention on Psychotropic Substances of 1971) is strictly prohibited.</li>
                                        <li>
                                            Drug paraphernalia, including all items that are primarily intended or designed for use in manufacturing, concealing, or using a controlled substance, are strictly forbidden on the Site. Such
                                            items include, but are not limited to those items used for the ingestion of illicit substances, including pipes such as water pipes, carburetor pipes, chamber pipes, ice pipes, bongs etc.
                                        </li>
                                        <li>
                                            The listing or sale of packaging materials which may be utilized to contain controlled substances, materials conducive to smuggling, storing, trafficking, transporting and manufacturing illicit
                                            drugs (e.g. marijuana grow lights), publications and other media providing information related to the production of illicit drugs.
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul>
                                <li>
                                    FLAMMABLE, EXPLOSIVE AND HAZARDOUS CHEMICALS
                                    <ul>
                                        <li>The posting of explosives and related ignition and detonation equipment is strictly prohibited. Such activity can result in your account being delisted.</li>
                                        <li>Radioactive substances, toxic and poisonous chemicals are forbidden on the urbamart.com Website</li>
                                        <li>
                                            The posting, offering for sale, or offering for purchase of hazardous or dangerous materials (such as the categories of dangerous goods as defined under the International Maritime Dangerous Goods
                                            Code) are forbidden on the urbamart.com Website.
                                        </li>
                                        <li>Ozone depleting substances are not permitted to be listed.</li>
                                        <li>
                                            The posting, offering for sale, or offering for purchase of any products containing harmful substances (e.g. items containing asbestos) are forbidden on the urbamart.com Website.
                                        </li>
                                        <li>
                                            Listing of fireworks, firecrackers and associated products are forbidden on both the Wholesaler Marketplace by UrbaMart platforms for Relevant Online Transactions. Where the seller is a properly
                                            licensed seller of these products in Kenya and commonwealth countries, and exception can be made where the sale will not amount to a Relevant Online Transaction.
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    FIREARMS AND AMMUNITIONS
                                    <ul>
                                        <li>
                                            Any service, instruction, process, or aid for producing any biological, chemical, or nuclear weapons, or other Weapons of Mass Destruction (WMD) or known associated agents is strictly prohibited
                                            by international law and accordingly prohibited on the Site. Any violation of this policy will result in the notification of government authorities by urbamart.com and your account being delisted.
                                        </li>
                                        <li>
                                            The posting of, offering for sale, or offering for purchase of any arms, munitions, military ordnance, weapons (including explosive weapons), and/or any related parts and components (whether
                                            integral or otherwise) is strictly prohibited. Such activity can result in your account being delisted.
                                        </li>
                                        <li>
                                            urbamart.com does not permit the posting, offering for sale, or offering of purchase of replica, “look-alike” or imitation firearms, and/or any related parts and components (whether integral or
                                            otherwise). This prohibition covers such products as air guns, BB guns, paintball guns, harpoons, spear guns and other weapons that may discharge a projectile containing any gas, chemical, or
                                            explosive substance.
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <p>&nbsp;</p>
                            <h4>
                                <a></a>
                                3.&nbsp;&nbsp;&nbsp;&nbsp; WEAPONS
                            </h4>
                            <ul>
                                <li>
                                    urbamart.com does not permit the posting, offering for sale, or offering of purchase of weapons that can incapacitate or cause serious physical harm to others (e.g. stun guns, batons, crossbows)
                                </li>
                                <li>
                                    While listing of most knives and other cutting instruments is permitted, switchblade knives, gravity knifes, knuckledusters (bladed or not), bladed handheld devices, and disguised knives are prohibited.
                                </li>
                                <li>urbamart.com maintains discretion over what items are appropriate and may cause removal of a listing that it deems as a weapon.</li>
                            </ul>
                            <p>&nbsp;</p>
                            <h4>
                                <a></a>
                                4.&nbsp;&nbsp;&nbsp;&nbsp; GOVERNMENT, LAW ENFORCEMENT AND MILITARY ISSUED ITEMS
                            </h4>
                            <ul>
                                <li>The following items are not permitted to be listed:</li>
                            </ul>
                            <ul>
                                <li>Articles of clothing or identification that claim to be, or appear similar to, official government uniforms.</li>
                                <li>Law enforcement badges or official law enforcement equipment from any public authority, including badges issued by the government of any country.</li>
                                <li>
                                    Military decorations, medals and awards, in addition to items with substantially similar designs.
                                    <ul>
                                        <li>
                                            Police uniforms, police insignia and police vehicles may not be posted unless they are obsolete and in no way resemble current issue police uniforms, police insignia and police vehicles. This fact
                                            must be clearly stated within the posting description.
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul>
                                <li>There are some police items that may be listed on the urbamart.com Website, provided they observe the following guidelines:</li>
                                <li>
                                    Authorized general souvenir items, such as hats, mugs, pins, pens, buttons, cuff links, T-shirts, money clips that do not resemble badges, and paperweights that do not contain badges.
                                </li>
                                <li>Badges that are clearly not genuine or official (e.g. toy badges).</li>
                                <li>
                                    Historical badges that do not resemble modern law enforcement badges, provided that the item description clearly states that the badge is a historical piece at least 75 years old or issued by an
                                    organization which no longer exists.
                                </li>
                                <li>
                                    The following mass-transit related items are not permitted to be listed:
                                    <ul>
                                        <li>
                                            Any article of clothing or identification related to transportation industries, including but not limited to, commercial airline pilots, flight attendants, airport service personnel, railway
                                            personnel, mass-transit security personnel. Vintage clothing related to commercial airlines or other mass-transit may be listed provided that the description clearly states that the item is at
                                            least 10 years old, is no longer in use and does not resemble any current uniform.
                                        </li>
                                        <li>
                                            Manuals or other materials related to commercial transportation, including safety manuals published by commercial airlines and entities operating subways, trains or buses. Such items may only be
                                            listed if the description clearly states that the material is obsolete and no longer in use.
                                        </li>
                                        <li>Any official, internal, classified or non-public documents.</li>
                                    </ul>
                                </li>
                                <li>
                                    Listing of police equipment and associated products are forbidden on both the Wholesaler Marketplace by UrbaMart platforms for Relevant Online Transactions. Where the seller is a properly licensed seller
                                    of these products in Kenya and commonwealth countries, and exception can be made where the sale will not amount to a Relevant Online Transaction.
                                </li>
                            </ul>
                            <h4>
                                <a></a>
                                5.&nbsp;&nbsp;&nbsp;&nbsp; MEDICAL DRUGS
                            </h4>
                            <ul>
                                <li>The posting of prescription drugs, psychotropic drugs and narcotics is strictly prohibited.</li>
                                <li>The listing or sale of orally administered or ingested sexual enhancement foods and supplements is prohibited.</li>
                                <li>Prescription veterinary drugs may not be listed.</li>
                                <li>
                                    Members may post OTC (over-the-counter) drugs on the urbamart.com Website after provision of appropriate production and sales permits to the Website, while transactions of these products are strictly
                                    prohibited to be entered into as a Relevant Online Transaction.
                                </li>
                            </ul>
                            <p>&nbsp;</p>
                            <h4>
                                <a></a>
                                6.&nbsp;&nbsp;&nbsp;&nbsp; MEDICAL DEVICES
                            </h4>
                            <ul>
                                <li>
                                    urbamart.com does not permit the posting of unauthorized medical devices. Members may only post authorized medical devices after provision of appropriate production and sales permits to the Website, while
                                    transactions of these products are strictly prohibited to be entered into as a Relevant Online Transaction.
                                </li>
                            </ul>
                            <p>&nbsp;</p>
                            <h4>
                                <a></a>
                                7.&nbsp;&nbsp;&nbsp;&nbsp; ADULT AND OBSCENE MATERIALS
                            </h4>
                            <ul>
                                <li>
                                    The posting or sale of pornographic materials is strictly prohibited, as it violates laws in many countries. While pornography is difficult to define, and standards vary from nation to nation,
                                    urbamart.com will generally follow guidelines accepted in Kenya and commonwealth states.
                                </li>
                                <li>
                                    Items depicting or suggestive of bestiality, rape sex, incest or sex with graphic violence or degradation, and any items depicting or suggestive of sex involving minors, are strictly prohibited.
                                </li>
                                <li>
                                    In determining whether listings or information should be removed from the urbamart.com Website and the Basket by UrbaMart Website, we consider the overall content of the posting, including images,
                                    pictorials, and text.
                                </li>
                                <li>
                                    While sex toys and related products are permitted to be listed, product descriptions may not include nude or otherwise sexually explicit images.
                                </li>
                            </ul>
                            <p>&nbsp;</p>
                            <h4>
                                <a></a>
                                8.&nbsp;&nbsp;&nbsp;&nbsp; CIRCUMVENTION DEVICES AND OTHER EQUIPMENT USED FOR ILLICIT PURPOSES
                            </h4>
                            <ul>
                                <li>
                                    Descramblers and other items that can be used to gain unauthorized access to television programming (such as satellite and cable TV), internet access, telephone, data or other protected, restricted, or
                                    premium services are prohibited. Stating the item is for educational or test purposes will not legitimize a product that is otherwise inappropriate. Some examples of items which are not permitted include
                                    smart cards and card programmers, descramblers, DSS emulators and hacking software.
                                </li>
                                <li>
                                    Similarly, information on “how to” descramble or gain access to cable or satellite television programming or other services without authorization or payment is prohibited. urbamart.com‘s policy is to
                                    prohibit any encouragement of this type of activity.
                                </li>
                                <li>
                                    Devices designed to intentionally block, jam or interfere with authorized radio communications, such as cellular and personal communication services, police radar, global positioning systems (GPS) and
                                    wireless networking services (Wi-Fi) are prohibited.
                                </li>
                                <li>The listing or sale of spy equipment and devices used for interception of wire, oral and electronic communications is not permitted on the Site.</li>
                                <li>Hidden photographic devices are permitted on the urbamart.com Website, unless used for sexual or illicit purposes.</li>
                                <li>Bank card readers and “skimmers” are prohibited from being listed.</li>
                                <li>Any and all unauthorized circumvention devices not included in the above are also strictly prohibited.</li>
                            </ul>
                            <p>&nbsp;</p>
                            <h4>
                                <a></a>
                                9.&nbsp;&nbsp;&nbsp;&nbsp; ILLEGAL SERVICES
                            </h4>
                            <ul>
                                <li>Listings claiming to provide government services and related products are strictly prohibited. Examples include:</li>
                                <li>Official government-issued identification documents, such as birth certificates, driving licenses, passports and visas;</li>
                                <li>
                                    Completed applications for the above-mentioned documents
                                    <ol>
                                        <li>
                                            Any materials, equipment or processes designed for use in the production of government-issued identification documents (e.g. driving license holograms, passport booklets).
                                        </li>
                                        <li>The offering for sale or purchase of textile quota is prohibited on the urbamart.com Website and the Basket by Mzizzi Website.</li>
                                    </ol>
                                </li>
                                <li>The listing or sale of any form of invoices or receipts (including blank, pre-filled, or value-added invoices or receipts), is strictly prohibited on the Site.</li>
                                <li>
                                    urbamart.com prohibits listings that offer financial services, including money transfers, issuing bank guarantees and letters of credit, loans, fundraising and funding for person investment purposes, etc.
                                </li>
                                <li>urbamart.com prohibits listings for the sole purpose of collecting user information or raising money.</li>
                                <li>
                                    Listings that offer medical or healthcare services, including services for medical treatment, rehabilitation, vaccination, health checks, psychological counselling, dietetics, plastic surgery and massage
                                    are prohibited.
                                </li>
                                <li>
                                    The posting or sale of bulk email or mailing lists that contain personally identifiable information including names, addresses, phone numbers, fax numbers and email addresses, is strictly prohibited. Also
                                    prohibited are software or other tools which are designed or used to send unsolicited commercial email (i.e. “spam”).
                                </li>
                                <li>Job postings from which a factory/company/institute may directly recruit employees are prohibited on the Site.</li>
                                <li>urbamart.com is an online business to business information platform; personal and non-business information is prohibited.</li>
                                <li>
                                    Non-transferable items may not be posted or sold through the urbamart.com Website and the Basket by UrbaMart Website. Many items including lottery tickets, airline tickets and some event tickets may not
                                    be resold or transferred.
                                </li>
                            </ul>
                            <p>&nbsp;</p>
                            <h4><a></a>10.&nbsp; COLLECTIONS, ARTIFACTS, PRECIOUS METALS AND GAME TROPHIES</h4>
                            <ul>
                                <li>
                                    urbamart.com strictly forbids the sale and purchase of currency, coins, banknotes, stocks, bonds, money orders, credit and debit cards, investment interest, currency in digital or any intangible form
                                    (e.g. crypto-currency), as well as the equipment and materials used to produce such items.
                                </li>
                                <li>Counterfeits of the identified articles in 10.1, legal tender and stamps are strictly prohibited.</li>
                                <li>
                                    Reproductions or replicas of coins as collectible items must be clearly marked with the word “COPY”, “REPRODUCTION” or “REPLICA” and comply with all relevant local laws.
                                </li>
                                <li>Listings that offer the sale or buying of gold, silver and other precious metals (not including jewellery) are prohibited.</li>
                                <li>Rough diamonds and “conflict minerals” originating from non-compliant countries may not be listed.</li>
                                <li>
                                    Artefacts, cultural relics, historical grave markers, and related items are protected under the laws of Kenya, the United States, and other jurisdictions; and may not be posted or sold through the
                                    UrbaMart Website.
                                </li>
                                <li>
                                    com and Basket by UrbaMart strictly forbids the sale and purchase of game trophies of any kind or the sale of game meat, or items directly obtained from game.
                                </li>
                            </ul>
                            <p>&nbsp;</p>
                            <h4><a></a>11.&nbsp; HUMAN PARTS, HUMAN REMAINS AND PROTECTED FLORA AND FAUNA</h4>
                            <ul>
                                <li>
                                    prohibits the listing of human body parts and remains. Examples of such prohibited items include, but are not limited to: organs, bones, blood, sperm, and eggs. Items made of human hair, such as wigs for
                                    commercial uses, are permitted.
                                </li>
                                <li>
                                    The listing or sale of any species (including any animal parts such as pelts, skins, internal organs, teeth, claws, shells, bones, tusks, ivory，logs, roots and other parts) protected by the Convention on
                                    International Trade in Endangered Species of Wild Fauna and Flora (CITES) or any other local law or regulation is strictly forbidden on the urbamart.com Website and the Basket by UrbaMart Website.
                                </li>
                                <li>
                                    The listing or sale of products made with any part of and/or containing any ingredient derived from sharks or marine mammals is prohibited on the urbamart.com Website and the Basket by UrbaMart Website.
                                </li>
                                <li>
                                    The listing or sale of products made from cats, dogs and bears, as well as any processing equipment, is prohibited on the urbamart.com Website and the Basket by UrbaMart Website.
                                </li>
                                <li>
                                    The listing or sale of poultry, livestock and pets for commercial purposes is permitted on the urbamart.com Website. For the avoidance of doubt, live animals are prohibited from being listed on the
                                    Wholesaler Marketplace and Basket by UrbaMart Website platforms.
                                </li>
                            </ul>
                            <p>&nbsp;</p>
                            <h4><a></a>12.&nbsp; OFFENSIVE MATERIAL AND INFORMATION DETRIMENTAL TO NATIONAL SECURITY</h4>
                            <ul>
                                <li>
                                    Any and all publications and other media containing state secrets or information detrimental to national security or public order are prohibited. Such activity can result in your account being delisted.
                                </li>
                                <li>
                                    Any information supporting or advocating infringement of national sovereignty, terrorist organizations or discrimination on grounds of race, sex, or religion is strictly prohibited on the urbamart.com
                                    Website and the Basket by UrbaMart Website. Such activity can result in your account being delisted.
                                </li>
                                <li>
                                    Postings that are ethnically or racially offensive are prohibited on the Site. Sellers and purchasers must ensure that any wording used portrays appropriate sensitivity to those who might read it in their
                                    postings, and when they are offering or purchasing potentially offensive items or services.
                                </li>
                                <li>
                                    Occasionally, if materials are of historical value or integral to the item (such as a book title), members may use offensive words and phrases such as “NIGGER” in the subject and description of a posting.
                                    urbamart.com reserves the sole discretion to decide the removal of such items and encourages all members to treat others as they themselves would like to be treated.
                                </li>
                                <li>Materials advocating, promoting or otherwise supporting fascism, Nazism and other extreme ideologies are strictly prohibited.</li>
                            </ul>
                            <p>&nbsp;</p>
                            <h4><a></a>13.&nbsp; TOBACCO PRODUCTS</h4>
                            <ul>
                                <li>
                                    The posting of tobacco products, including but not limited to cigars, cigarettes, cigarette tobacco, pipe tobacco, hookah tobacco, chewing tobacco and tobacco leaf is prohibited.
                                </li>
                                <li>The posting of electronic cigarettes and accessories is permitted, however nicotine and other liquids (e-liquids) for use in electronic cigarettes is forbidden.</li>
                                <li>
                                    Members located in Uganda and other commonwealth countries may only list equipment used for tobacco processing and production of tobacco products after provision of appropriate production and sales
                                    permits to the Website.
                                </li>
                            </ul>
                            <p>&nbsp;</p>
                            <h4><a></a>14.&nbsp; GAMBLING EQUIPMENT</h4>
                            <p>
                                The listing or sale of equipment specifically used for gambling is prohibited. Products which have other legitimate uses (such as dice and playing cards) will generally be permitted.
                            </p>
                            <p>&nbsp;</p>
                            <h4><a></a>15.&nbsp; SANCTIONED AND PROHIBITED ITEMS</h4>
                            <ul>
                                <li>
                                    Products prohibited by laws, regulations, sanctions and trade restrictions in any relevant country or jurisdiction worldwide are strictly forbidden on urbamart.com.
                                </li>
                                <li>The listing or sale of petroleum, petroleum products and petrochemical products originating in the Islamic Republic of Iran is strictly forbidden.</li>
                                <li>
                                    The listing or sale of coal, iron, iron ore, gold, titanium ore, vanadium ore and rare earth minerals originating in the Democratic People’s Republic of Korea is strictly forbidden.
                                </li>
                                <li>The listing or sale of crude oil by sellers and buyers located in Kenya is prohibited.</li>
                            </ul>
                            <p>&nbsp;</p>
                            <h4><a></a>16.&nbsp; OTHER LISTING PROHIBITIONS</h4>
                            <ul>
                                <li>The posting of any products containing harmful substances (e.g. toys containing lead paint) is forbidden on the urbamart.com Website.</li>
                                <li>Automotive airbags are expressly forbidden on the urbamart.com Website due to containing explosive materials.</li>
                                <li>
                                    Refurbished products: The sale and purchase of refurbished mobile phones, laptops and computers is controlled on the urbamart.com and Basket by UrbaMart Website. Incidences of items with a high failure
                                    rate as may solely be determined by urbamart.com will lead to termination of your account immediately with no option recourse or intervention.
                                </li>
                                <li>
                                    Used products
                                    <ul>
                                        <li>
                                            Used undergarments may not be listed or sold on the Site. Other used clothing may be listed, so long as the clothing has been thoroughly cleaned. Postings that contain inappropriate or extraneous
                                            descriptions will be removed.
                                        </li>
                                        <li>The listing or sale of used cosmetics is prohibited on the urbamart.com Website.</li>
                                    </ul>
                                </li>
                                <li>Contracts and tickets</li>
                                <li>
                                    You are responsible for ensuring that your transaction is lawful and not in violation of any contractual obligation. Before posting an item on the Site, you should carefully read any contracts that you
                                    have entered into that might limit your right to sell your item on the Site. Some items, such as airline tickets, have terms printed on the item that may limit your ability to sell that item. In other
                                    cases, such as when you are distributing a company’s products, you may have signed a separate contract restricting your ability to market the product.
                                </li>
                                <li>
                                    urbamart.com does not search for items that may raise these types of issues, nor can it review copies of private contracts, or adjudicate or take sides in private contract disputes. However, we want you
                                    to be aware that posting items in violation of your contractual obligations could put you at risk with third parties. urbamart.com therefore urges that you not list any item until you have reviewed any
                                    relevant contracts or agreements and are confident you can legally sell it on the Site.
                                </li>
                                <li>
                                    If you have any questions regarding your rights under a contract or agreement, we strongly recommend that you contact the company with whom you entered into the contract and/or consult with an attorney.
                                </li>
                            </ul>
                            <p>&nbsp;</p>
                            <ul>
                                <li>
                                    Event ticket resale policy
                                    <ul>
                                        <li>
                                            com allows the listing of tickets to performance, sporting and entertainment events to the extent permitted by law. However, as a ticket seller, you are responsible for ensuring that your
                                            particular transaction does not violate any applicable law or the terms on the ticket itself.
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul>
                                <li>
                                    Real estate
                                    <ul>
                                        <li>
                                            A real estate posting allows buyers to contact the seller to get more information and express interest about the property listed. Before you post a listing relating to sale or purchase of real
                                            estate, you must ensure that you have complied with all applicable laws and regulations
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul>
                                <li>
                                    Stolen property
                                    <ul>
                                        <li>
                                            The posting or sale of stolen property is strictly forbidden on the Site and violates international law. Stolen property includes items taken from private individuals, as well as property taken
                                            without authorization from companies or governments.
                                        </li>
                                        <li>
                                            com supports and cooperates with law enforcement efforts involving the recovery of stolen property and the prosecution of responsible individuals. If you are concerned that the images and/or text
                                            in your item description have been used by another Site user without your authorization, or that your intellectual property rights have been violated by such user, please contact our service team
                                            at service (@) urbamart.com
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <p>&nbsp;</p>
                            <h4><a></a>17.&nbsp; INTELLECTUAL PROPERTY RIGHTS (IPR) PROTECTION POLICY</h4>
                            <ul>
                                <li>
                                    FACES, NAMES AND SIGNATURES Items containing the likeness, image, name, or signature of another person are prohibited, unless the products were made or authorized by the person whose likeness, image, name
                                    or signature has been used.
                                </li>
                            </ul>
                            <p>&nbsp;</p>
                            <ul>
                                <li>REPLICA AND COUNTERFEIT ITEMS</li>
                            </ul>
                            <ol>
                                <li>
                                    Listing of counterfeits, non-licensed replicas, or unauthorized items, such as counterfeit designer garments, watches, handbags, sunglasses, or other accessories, is strictly prohibited on the Site.
                                </li>
                            </ol>
                            <p>&nbsp;</p>
                            <ol>
                                <li>
                                    If the products sold bear the name or logo of a company but did not originate from or were not endorsed by that company, such products are prohibited from the Site.
                                </li>
                            </ol>
                            <p>&nbsp;</p>
                            <ul>
                                <li>Postings of branded products are permitted if a certificate of authorization has been issued by the brand owner.</li>
                            </ul>
                            <p>&nbsp;</p>
                            <ol>
                                <li>
                                    Postings offering to sell, or purchase replicas, counterfeits or other unauthorized items shall be subject to removal by urbamart.com. Repeated postings of counterfeit or unauthorized items shall result
                                    in the immediate suspension of your membership.
                                </li>
                            </ol>
                            <p>&nbsp;</p>
                            <ul>
                                <li>SOFTWARE</li>
                                <li>Academic Software</li>
                                <li>Academic software is software sold at discounted prices to students, teachers, and employees of accredited learning institutions.</li>
                                <li>
                                    On the Site, please do not list any academic software unless you are so authorized. Postings violating urbamart.com’s academic software policy may be deleted prior to publication.
                                </li>
                                <li>
                                    For postings of academic software on behalf of an authorized educational reseller or an educational institution, such licensure must be stated conspicuously in the listings. A certificate of authorization
                                    issued by the authorized educational reseller (or the educational institution) must also be provided to urbamart.com.
                                </li>
                                <li>OEM Software</li>
                                <li>
                                    Do not list “OEM” or “bundled” copies of software on the urbamart.com Website unless you are selling it with computer hardware. Original Equipment Manufacturer (OEM), or bundled software, is software that
                                    is obtained as part of the purchase of a new computer. OEM software licenses usually prohibit the purchaser from reselling the software without the computer or, in some cases, without any computer
                                    hardware.
                                </li>
                                <li>
                                    UNAUTHORIZED COPIES OF INTELLECTUAL PROPERTY The listing or sale of unauthorized (pirated, duplicated, backup, bootleg, etc.) copies of software programs, video games, music albums, movies, television
                                    programs, photographs or other protected works is forbidden on the Site.
                                </li>
                            </ul>
                        </div>
                    </div>
                 </div>
    </div>
</section>

@endsection
