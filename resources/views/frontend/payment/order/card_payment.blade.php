@extends('frontend.layouts.app')

@section('content') 
<section class="gry-bg py-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="card">
                        <div class="align-items-center card-header d-flex justify-content-center text-center">
                            <h3 class="d-inline-block heading-4 mb-0 mr-3 strong-600">{{ translate('Bank Payment Details') }}
                            </h3>
                        </div>
                        <div class="card-body">
                            
                            <form id='pay' style="display: none;">
                                
                                <script src="{{env('XPRESSPAY_CHECKOUT')}}"
                                        data-error="errorCallback" 
                                        data-cancel="cancelCallback" 
                                        data-complete="completeCallback" 
                                        data-beforeRedirect="beforeRedirect" 
                                        data-afterRedirect="afterRedirect" 
                                        >
                                </script>
                                <input type="hidden" name="_token" value="{!!csrf_token()!!}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> 
@endsection

@section('script')
    <script type="text/javascript">
        var vmerchantId = "{{env('XPRESSPAY_MERCHANT')}}"; 
        var vsessionId =  "{{$json_response->session->id}}";
        var vsuccessIndicator =  "{{$json_response->successIndicator}}";
        
        var type =  "{{Session::get('payment_type')}}";

        @if (Session::get('payment_type') == 'cart_payment')
            var vorderId = "{{$order->code}}" ;
            var vamount  = {{$order->grand_total}}; 
            var Id = "{{$order->id}}";
            
        @elseif (Session::get('payment_type') == 'wallet_payment' || Session::get('payment_type') == 'seller_package_payment')
            var vorderId = "{{$payment_data['code']}}" ;
            var Id = "{{$payment_data['code']}}";
            var vamount  = {{ $payment_data['amount']}};         
        @endif

        

        var vresultIndicator = null;
        var vcurrency = "UGX";

        function beforeRedirect() {
            return {
                successIndicator: vsuccessIndicator,
                orderId: vorderId,
                sessionId: vsessionId,
                sessionVersion: vsessionVersion,
                merchantId: vmerchantId,
                currency:vcurrency,
                amount:vamount
            };
        }
            
        function afterRedirect(data) { 
            if (resultIndicator) {
                var result = (resultIndicator === data.successIndicator) ? 'SUCCESS' : 'ERROR';
                $.ajax({
                    type:"POST",
                    url: '{{ route('zappay.card') }}',
                    data: { "_token": "{{ csrf_token() }}", "id": Id, "type": type },
                    success: function(data){
                            $('#addToCart').modal('hide'); 
                        window.location.replace(data.next);
                    } 
                });  
            }
            else {
                vsuccessIndicator = data.successIndicator;
                vorderId = data.orderId;
                vsessionId = data.sessionId;
                vsessionVersion = data.sessionVersion;
                vmerchantId = data.merchantId;
                vcurrency = data.currency;
                vamount = data.amount;
                } 
        }
        
        function errorCallback(error) {
            console.log(JSON.stringify(error));
             $.ajax({
               type:"POST",
               url: '{{ route('zappay.card') }}',
               data: { "_token": "{{ csrf_token() }}", "id": Id, "type": type },
               success: function(data){
                    $('#addToCart').modal('hide'); 
                   window.location.replace(data.next);
               }
           }); 
        }

        function cancelCallback() {            
                 $.ajax({
                    type:"POST",
                    url: '{{ route('zappay.card') }}',
                    data:  { "_token": "{{ csrf_token() }}", "id": Id, "type": type },
                    success: function(data){
                         $('#addToCart').modal('hide'); 
                        window.location.replace(data.next);
                    }
                });
            
        }
 

        function completeCallback(_resultIndicator, sessionVersion) { 
            vresultIndicator = _resultIndicator; 
            $.ajax({
               type:"POST",
               url: '{{ route('zappay.card') }}',
               data:  { "_token": "{{ csrf_token() }}", "id": Id, "type": type },
               success: function(data){
                    $('#addToCart').modal('hide'); 
                   window.location.replace(data.next);
               }
           });

                
        }
        document.addEventListener("DOMContentLoaded", function(event) { 
            $('#modal-size').removeClass('modal-lg');
            $('#addToCart').modal({backdrop: 'static', keyboard: false});
            $('.c-preloader').show();  
            
            
                Checkout.configure({ 
                        merchant: "{{ env('XPRESSPAY_MERCHANT') }}",
                        session:{id: "{{ $json_response->session->id }}" },
                        order:{ 
                            amount: vamount, 
                            currency: "UGX", 
                            description:"{{env('APP_NAME')}} " +  vorderId, 
                            id: vorderId
                        },
                        interaction:{
                            merchant:{	
                                name: "{{env('APP_NAME')}}" , 
                                address:{ line1:"{{env('APP_ADDRESS')}}" }, 
                                email:"{{env('MAIL_USERNAME')}}" , 
                                phone:'256751345334' 
                            },
                        displayControl:{ billingAddress: 'HIDE', orderSummary: 'HIDE' } }
                    });
                    Checkout.showLightbox();
        });
    </script>
@endsection
