@extends('frontend.layouts.app')

@section('content')
    <section class="gry-bg py-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="card">
                        <div class="align-items-center card-header d-flex justify-content-center text-center">
                            <h3 class="d-inline-block heading-4 mb-0 mr-3 strong-600">{{ translate('Mobile Money Payment Details') }}
                            </h3>
                        </div>
                        <div class="card-body">
                            <form id="payment-form" method="post">
                                @csrf
                                        <h4 class="d-flex align-items-center justify-content-center" style="pointer-events: none;cursor: default;">
                                                    <label class="aiz-megabox p-2" data-toggle="tooltip" data-title="MTN Mobile Money" height="30">
                                                        <input class="online_payment" type="radio" id="" name="mobilemoney_option" value="mtn">
                                                        <span class="d-block p-3 aiz-megabox-elem">
                                                            <img loading="lazy" src="{{ static_asset('assets/img/cards/mtn-momo.png') }}" class="img-fluid">
                                                            <span class="d-block text-center">
                                                                <span class="d-block fw-600 fs-14">MTN</span>
                                                            </span>
                                                        </span>
                                                    </label>

                                                    <label class="aiz-megabox p-2" data-toggle="tooltip"
                                                        data-title="AirTel Money" height="30">
                                                        <input class="online_payment" type="radio" id="" name="mobilemoney_option" value="airtel">
                                                        <span class="d-block p-3 aiz-megabox-elem">
                                                            <img loading="lazy"
                                                                src="{{ static_asset('assets/img/cards/airtelmoney.png') }}"
                                                                class="img-fluid">
                                                                <span class="d-block text-center">
                                                                    <span class="d-block fw-600 fs-14">AIrTel</span>
                                                                </span>
                                                        </span>
                                                    </label>

                                                    <label class="aiz-megabox p-2" data-toggle="tooltip"
                                                        data-title="M-Pesa" height="30">
                                                        <input class="online_payment" type="radio" id="" name="mobilemoney_option" value="mpesa">
                                                        <span class="d-block p-3 aiz-megabox-elem">
                                                            <img loading="lazy"
                                                                src="{{ static_asset('assets/img/cards/xmpesa.png') }}"
                                                                class="img-fluid">
                                                                <span class="d-block text-center">
                                                                    <span class="d-block fw-600 fs-14">MPasa</span>
                                                                </span>
                                                        </span>
                                                    </label>
                                        </h4>
                                        <p>Pay using your Mobile Money account. Enter Phone number and Click Pay Now to make payment</p>

                                        <div class="form-group phone-form-group">
                                            <div class="input-group input-group--style-1 px-1">
                                                <input type="tel" id="phone-code" class="form-control" width="80%" value="{{ old('phone') }}" name="phone"  placeholder="eg +2567xxxxxxxx or +2547xxxxxxxx"
                                                pattern="(^(\+256){1}[7]{1}[7|8]{1}[0-9]{6}[0-9])|(^(\+256){1}[7]{1}[0|3|5]{1}[0-9]{6}[0-9])|(^(\+254)[7]{1}(([0129]{1}[0-9]{1})|([4]{1}[0123568])|([5]{1}[789])|([6]{1}[89]))[0-9]{6})"
                                                required>
                                            </div>
                                            <div class='form-row'>
                                                <div class='col-12 error form-group d-none'>
                                                    <div class='alert-danger alert'> {{ translate('Please correct the errors and try again.') }}</div>
                                                </div>
                                            </div>
                                        </div>
                                         @if (Session::get('payment_type') == 'cart_payment')
                                             <input type="hidden" name="order_id" value="{{Session::get('order_id')}}">
                                         @elseif(Session::get('payment_type') == 'wallet_payment')
                                            <input type="hidden" name="order_id" value="{{Session::get('payment_data')['code']}}">
                                         @elseif(Session::get('payment_type') == 'seller_package_payment')
                                            <input type="hidden" name="order_id" value="{{Session::get('payment_data')['code']}}">
                                         @endif


                               <div class="row">
                                    <div class="col-12">
                                        @if (Session::get('payment_type') == 'cart_payment')
                                            <button class="btn btn-primary fw-600 btn-block" type="submit" id="submit" name="submit">{{ translate('Pay Now') }} ({{ single_price(\App\Order::findOrFail(Session::get('order_id'))->grand_total) }}) </button>
                                        @elseif(Session::get('payment_type') == 'wallet_payment')
                                            <button class="btn btn-primary fw-600 btn-block" type="submit" id="submit" name="submit">{{ translate('Pay Now') }} ({{ single_price(Session::get('payment_data')['amount']) }})</button>
                                        @elseif(Session::get('payment_type') == 'seller_package_payment')
                                            <button class="btn btn-primary fw-600 btn-block" type="submit" id="submit" name="submit">{{ translate('Pay Now') }} ({{ single_price(Session::get('payment_data')['amount']) }})</button>
                                        @endif
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script type="text/javascript">
        var type =  "{{Session::get('payment_type')}}";

        function setSelected(index) {
            var radios = document.getElementsByName("mobilemoney_option");
            for (var i = 0; i < radios.length; i++) {
                if (i == index)
                    radios[i].checked = true;
                else
                    radios[i].checked = false;
            }
        }

        $(document).on('input',"input[name='phone']", function() {
            console.log("input[name='phone']");
            var phone_input = document.querySelector("input[name='phone']");
            var phone = phone_input.value;
            console.log(phone);
            setSelected(3);
            if (phone.length != 13) {
                $('div.error').removeClass('d-none').find('.alert').text("Please provide a valid Phone Number!");
            } else {
                $('div.error').addClass('d-none');

                if (/^(\+256){1}[7]{1}[7|8]{1}[0-9]{6}[0-9]/g.test(phone))  {
                    setSelected(0);
                } else if (/(\+256){1}[7]{1}[0|3|5]{1}[0-9]{6}[0-9]/g.test(phone)) {
                    setSelected(1);
                }  else if (/^(\+254)[7]{1}(([0129]{1}[0-9]{1})|([4]{1}[0123568])|([5]{1}[789])|([6]{1}[89]))[0-9]{6}/g.test(phone)) {
                    setSelected(2);
                }
                else {
                    $('div.error').removeClass('d-none').find('.alert').text("Phone number Not supported");
                }
            }
        });



        function validate() {
            var phone = $('input[name=phone]').val();
            if(!(/(^(\+256){1}[7]{1}[7|8]{1}[0-9]{6}[0-9])|(^(\+256){1}[7]{1}[0|3|5]{1}[0-9]{6}[0-9])|(^(\+254)[7]{1}(([0129]{1}[0-9]{1})|([4]{1}[0123568])|([5]{1}[789])|([6]{1}[89]))[0-9]{6})/g.test(phone))){

                 AIZ.plugins.notify('danger', "Please provide a valid Phone Number!");
                 return false;
            }
            else
                return true;
            }

        $('#submit').click(function(e){
            e.preventDefault();
            if(validate()) {
            $('#modal-size').removeClass('modal-lg');
            $('#addToCart').modal({backdrop: 'static', keyboard: false});
            $('.c-preloader').show();

            var frm = $('#payment-form').serializeArray();
            //console.log(frm);
            var frmObject = {};
            $.each(frm, function(i, v) { frmObject[v.name] = v.value; });
            //console.log(frmObject);

            $.ajax({
               type:"POST",
               url: '{{ route('mobile.session') }}',
               contentType: "application/json",
               data: JSON.stringify(frmObject),
               success: function(xdata){
                   //console.log('xdata:');
                   //console.log(xdata);
                   if(xdata.status){
                         var delay = 1000;
                        var timer = setTimeout(function checkstatus(){
                            $.ajax({
                                    type:"POST",
                                    url: xdata.next,
                                    data:  { "_token": "{{ csrf_token() }}", "id": xdata.id, 'op': xdata.op, 'type': type },
                                    success: function(data){
                                            console.log('data:');
                                            console.log(data);
                                            if(data.status =='success' || data.status =='failed' ){
                                                $('#addToCart').modal('hide');
                                                window.location.href = data.next;
                                            }
                                            else{
                                                delay *= 2;
                                                setTimeout(checkstatus,delay);
                                            }
                                        }
                                    });
                            },  delay);

                   }
                   else{
                        $('#addToCart').modal('hide');
                        window.location.href = xdata.next;
                   }


               }
           });
        }


        })


    </script>
@endsection
