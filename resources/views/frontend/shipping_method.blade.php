
<form id="user_shipping_method" role="form"  method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" id="type" name="type" value="{{$type}}">
    <input type="hidden" id="shop" name="shop" value="{{$location}}">  
    <div class="form-group row">
        <label class="col-sm-2" for="location">{{ translate('Country:')}}</label>
        <div class="col-sm-4">
            <select class="form-control mb-3 aiz-selectpicker"  data-live-search="true" name="location" id="location"  required>
                @foreach (\App\Country::where('status', 1)->get() as $key => $country)
                    <option value="{{ $country->code }}" @if($country->code == $location) selected @endif>{{ $country->name }}</option>
                @endforeach
            </select>
        </div>
        <label class="col-sm-2" for="location">{{ translate('City:')}}</label>
        <div class="col-sm-4">
            <select class="form-control mb-3 aiz-selectpicker"  data-live-search="true" name="city" id="city">
                @foreach (\App\State::where('country_id', \App\Country::where('code',  $location)->first()->id)->get() as $key => $state)
                    <option value="{{ $state->code }}" @if($state->name == $city) selected @endif>{{ $state->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row">
        <label class="pl-3"><strong>{{ translate('Shipping Methods')}}</strong></label>
        <div class="col-12">
            <table class="table table-borderless table-hover" id="shipping_method_table">
                <thead>
                <tr>  
                    <th>Estimated Delivery</th>
                    <th>Cost</th> 
                    <th>Carrier</th>
                </tr>
                </thead>
                <tbody id='table_data'>
                @foreach ($shipping as $method)
                    <tr>  
                        <td>  
                            <input type="radio" class="radio" id="method_{{$method['id']}}"  name="method"> 
                            <input type="hidden" value="{{$method['id']}}" name="shipping_man">
                            {{$method['delivery']}}
                            <input type="hidden" value="{{$method['delivery']}}" name="delivery"> 
                        </td>
                        <td>
                            {{$method['cost']}}
                            <input type="hidden" value="{{$method['cost']}}" name="cost">
                        </td> 
                            <td>{{$method['carrier']}}
                            <input type="hidden" value="{{$method['carrier']}}" name="carrier">
                        </td>
                    </tr>
                @endforeach
                
                </tbody>
            </table>
        </div>
    </div>  
</form>
<div class="d-flex align-items-center justify-content-center">
    <button id="user_shipping_method_btn"  class="btn btn-primary buy-now fw-600" onclick="ApplyShippingMethod();">{{ translate('Apply')}}</button>
</div>  
 


