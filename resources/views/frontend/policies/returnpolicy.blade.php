@extends('frontend.layouts.app')

@section('content')
<section class="pt-4 mb-4">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 text-center text-lg-left">
                <h1 class="fw-600 h4">{{ translate('Refund and Return Policy') }}</h1>
            </div>
            <div class="col-lg-6">
                <ul class="breadcrumb bg-transparent p-0 justify-content-center justify-content-lg-end">
                    <li class="breadcrumb-item opacity-50">
                        <a class="text-reset" href="{{ route('home') }}">{{ translate('Home')}}</a>
                    </li>
                    <li class="text-dark fw-600 breadcrumb-item">
                        <a class="text-reset" href="{{ route('returnpolicy') }}">"{{ translate('Return Policy') }}"</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="mb-4">
    <div class="container">
        <div class="p-4 bg-white rounded shadow-sm overflow-hidden mw-100 text-left">
             <div class="row">
            <div class="col">
                <div class="p-0 bg-white">
                    <ol class="list">
                        <li>
                            <h5>Application for return / refund</h5>
                            <p>
                                Buyers may submit a claim for the return of the purchased item ("Product") and / or a refund prior to the Urbamart Guarantee period expiry as set out in the Terms of Service. In accordance with the terms and
                                conditions of service
                            </p>
                            <p>
                                Urbamart Guarantee is a service provided by Urbamart at the request of the user. To assist users in dealing with certain conflicts that may arise during transactions. Users can communicate with each other in
                                private to resolve their differences. Or contact the relevant local authorities to assist in resolving disputes before, during or after using Urbamart Guarantee.
                            </p>
                        </li>
                        <li>
                            <h5>Filing a request to return the product</h5>
                            <p>
                                Buyers can only request a refund and / or return of the product in the following situations:
                            </p>
                            <ul>
                                <li>
                                    The buyer did not receive the product
                                </li>
                                <li>
                                    Defective product Not working and / or damaged during delivery
                                </li>
                                <li>
                                    The seller sends the product does not meet the agreed specifications (e.g. wrong size or color, etc.) to the buyer.
                                </li>
                                <li>
                                    The Goods delivered to the Buyer are materially different from the description provided by the Seller on the Product Listing, or
                                </li>
                                <li>
                                    By entering into a private agreement with Seller and Seller must send his or her confirmation to Urbamart to confirm the Agreement.
                                </li>
                                <li>
                                    The law requires a refund.
                                </li>
                            </ul>
                            <p>
                                Buyers are required to submit their applications through the Urbamart mobile app.
                            </p>
                            <p>
                                Urbamart will review the requests of each buyer on a case-by-case basis and decide whether buyer's requests should be processed or not. In his sole discretion
                            </p>
                            <p>
                                In the event that the buyer makes a mistake in payment (for example, wrong amount entered or wrong phone number to top up Pay in the wrong way Or the wrong channel of payment, etc.) or make a double payment
                                The Customer will not be able to correct or change any erroneous payment or cancel a duplicate payment and Urbamart will not provide a refund to the Customer, including the payment transaction fee for such
                                payment transaction. Customers are required to contact merchants or service providers directly to request amendments to faulty payments or to request a refund for duplicate payments.
                            </p>
                            <p>
                                In the event that the buyer has prosecuted the seller Buyers may send an official notification from the relevant authority to Urbamart asking Urbamart to continue to keep the purchases made until a formal
                                decision is made, Urbamart in its sole discretion. Decide if it is necessary to keep the purchase.
                            </p>
                        </li>
                        <li>
                            <h5>Prohibiting changing hearts</h5>
                            <p>
                                Buyer may not apply for return and / or refund due to change of mind. Unless otherwise stated in this refund and return policy.
                            </p>
                        </li>
                        <li>
                            <h5>Seller's rights</h5>
                            <p>
                                Once Urbamart receives a request for return and / or refund from Buyer, Urbamart will notify Seller in writing. The seller can accept the buyer's request through the steps set by Urbamart in the notification The
                                seller must respond within the deadline stated in the notice. ("Period") If Urbamart has not received contact from Seller within the specified period, Urbamart will be deemed that Seller has not accepted Buyer's
                                request. And will proceed to assess the request of the buyer without further notice to the seller
                            </p>
                            <p>
                                Urbamart will examine the responses of each Seller on a case-by-case basis and use its sole discretion to decide if the Buyer's request is successful, based on the circumstances provided by Seller.
                            </p>
                        </li>
                        <li>
                            <h5>Condition of the returned goods</h5>
                            <p>
                                To avoid the hassle of returning products. The buyer should make sure that the product Including free items such as accessories that come with that product The seller must be returned in the condition that
                                the buyer received while shipping. We recommend that the buyer take a picture of the product while receiving the item.
                            </p>
                        </li>
                        <li>
                            <h5>Cost of Returning Products</h5>
                            <p>
                                The Buyer and Seller shall discuss and mutually agree on who will be responsible for the costs of returning the item.
                            </p>
                        </li>
                        <li>
                            <h5>Refund</h5>
                            <p>
                                The buyer will receive a refund only after Urbamart has confirmed from Seller that Seller has received the returned item, in the event that Urbamart has not been contacted by Seller within the specified time,
                                Urbamart has the right to refund. The relevant amount to the buyer without further notice to the seller. For more information on seller acceptance deadline, please click on this link.Refunds will be credited to
                                buyer's credit / debit card. Or designated Mobile Money account Whichever is appropriate
                            </p>
                        </li>
                        <li>
                            <h5>Communication between buyers and sellers</h5>
                            <p>
                                Urbamart encourages users to communicate with each other in the event of a transaction problem as Urbamart is a platform for users to trade. Buyers should contact the seller directly if there is a problem with
                                the purchased item.
                            </p>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        </div>
    </div>
</section>
@endsection
