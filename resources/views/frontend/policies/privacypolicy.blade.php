@extends('frontend.layouts.app')

@section('content')
@php
    $privacy_policy =  \App\Page::where('type', 'privacy_policy_page')->first();
@endphp
<section class="pt-4 mb-4">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 text-center text-lg-left">
                <h1 class="fw-600 h4">{{ translate('Privacy Policy') }}</h1>
            </div>
            <div class="col-lg-6">
                <ul class="breadcrumb bg-transparent p-0 justify-content-center justify-content-lg-end">
                    <li class="breadcrumb-item opacity-50">
                        <a class="text-reset" href="{{ route('home') }}">{{ translate('Home')}}</a>
                    </li>
                    <li class="text-dark fw-600 breadcrumb-item">
                        <a class="text-reset" href="{{ route('privacypolicy') }}">"{{ translate('Privacy Policy') }}"</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="mb-4">
    <div class="container">
        <div class="p-4 bg-white rounded shadow-sm overflow-hidden mw-100 text-left">
             <div class="row">
            <div class="col">
                <div class="p-0 bg-white">
                    <h3>Our Privacy Commitment</h3>
                    <div>
                        At Urbamart we know how important it is to protect your personal information. Whether you are shopping with us online or in person we want to make every customer experience safe and secure. In keeping with that goal,
                        we have developed this Privacy Policy to explain our practices for the collection, use, and disclosure of your personal information.
                    </div>
                    <div>
                        This Policy applies to our Ugandan retail stores, our Web Sites, mobile apps, call centres and all other channels in which Urbamart collects and uses your personal information. All references to “Web Sites” in this
                        Privacy Policy mean urbamart.com,&nbsp; or other Urbamart provided Web sites or mobile apps where this Privacy Policy is posted. Urbamart, urbamart.com,&nbsp; are divisions of zByte Technology Ltd.
                    </div>
                    <h3>
                        <br />
                    </h3>
                    <div>
                        <h3>What we collect</h3>
                        <p>The types of information we collect depend on the nature of your interaction with us.</p>
                        <h3>Personal information</h3>
                        <p>
                            Personal information is information that can identify an individual or information about an identifiable individual. We may collect personal information such as:
                        </p>
                        <ul>
                            <li>your contact information – e.g., name, postal address, telephone number, and email address;</li>
                            <li>your personal preferences – e.g., product wish lists, language preferences, and marketing consent</li>
                            <li>your transaction information – e.g., products purchased, method of payment, amount paid, and mobile money numbers (We do not store credit/debit card information).</li>
                        </ul>
                        <p>In addition, we may collect the following types of information:</p>
                        <ul>
                            <li>we may ask you for your postal code, for example, to assist us with market research, planning our flyer distribution, and other marketing communications;</li>
                            <li>we may collect certain data when you visit our Web Sites, such as your IP address, the type of browser you are using or the referring URL; or</li>
                            <li>
                                we may collect and summarize customer information in an aggregate format for internal statistical and research purposes. We may, for example, summarize our data to determine that a certain percentage of
                                contest entrants are male, aged 18 to 25.
                            </li>
                        </ul>
                        <h3>Supplementing Information</h3>
                        <p>
                            From time to time we may supplement information you give us with information from other sources, such as information validating your address or other available information you have provided us. This is to help us
                            maintain the accuracy of the information we collect and to help us provide better service.
                        </p>
                        <h3>How we collect and use information Collected from you</h3>
                        <p>Listed here are some further examples of the ways that we collect personal information directly from you and how we use it.</p>
                        <h3>In-Store and Online Purchases</h3>
                        <p>
                            When you purchase a product or service from Urbamart or a Web Site, you may need to provide us with contact and payment information (such as credit card information) so that we can process your request or
                            complete your transaction and communicate with you regarding your purchase. We may also need to collect contact information to provide certain services, including delivery services, reserve and pick-up, product
                            servicing, in-home consultations or installations, and extended warranty coverage. If you provide us your email address, we will also collect your email address for the purpose of sending you an e-receipt. We
                            also may use your contact information to send you promotional information. You can opt-out by following the instructions under “Unsubscribe” below.
                        </p>
                        <h3>Returns and Exchanges</h3>
                        <p>
                            Our goal is to ensure that you are completely satisfied with your purchase and to this end, Urbamart offers a Return Policy. For Urbamart to continue to offer our Return Policy, we ask for personal information
                            such as your name, address, and telephone number for the purposes of detecting and preventing fraud. You may also be asked to produce photo ID for verification purposes only.
                        </p>

                        <h3>Purchase Follow-Up, Surveys, and Customer Research</h3>
                        <p>
                            Following a purchase, we may send you a follow-up letter to thank you for your business, or we may contact you to ensure that you are completely satisfied with the delivery, setup, or installation of your new
                            product.
                            <br />
                            We may use contact information collected in-store, online, or via our call centres to conduct occasional surveys and other customer research. These surveys are entirely voluntary and you may easily decline to
                            participate. The information obtained through our surveys and customer research is used to help us understand our customers, to enhance our product and service offerings, promotions and events, and/or to assist
                            in the selection of store locations.
                        </p>
                        <h3>Online Accounts</h3>
                        <p>
                            To engage in certain special features or functions of our Web Sites, or to order products and services from our Web Sites, you may be asked to provide certain personal information, such as your name, email
                            address, postal address, telephone number, or payment card information. We use this information to create your account, process your payment, contact and correspond with you about your order, respond to your
                            inquiries, and monitor functions of our Web Sites that you choose to use, such as Customer Reviews. If you purchase an e-gift card, we collect the recipient’s name, email address and any message you provide in
                            order to send the gift card on your behalf. We rely on you to ensure that you have obtained all necessary consents to provide this information to us.<br style="box-sizing: border-box; margin-bottom: 0px;" />
                            If you choose to ship an order to a third party, you represent and warrant that you have obtained all necessary consents from the third party to permit Urbamart to collect, use, and disclose the third party’s
                            personal information for the purposes of fraud prevention and processing and shipping the order.
                        </p>
                        <h3>Customer Reviews</h3>
                        <p>
                            The Customer Review functionality is provided as a rating system that offers you the chance to share your thoughts on a product. These reviews are accessible to all Web Site visitors so please use discretion when
                            posting information to them and do not post personal information.
                        </p>
                        <h3>Newsletters or Updates</h3>
                        <p>
                            Urbamart offers a variety of newsletters and promotional communications. If you sign-up or are subscribed for our newsletter or other promotional communications, we will send you communications that may be
                            tailored based on your interests, preferences and behaviour. You may choose to stop receiving these communications at any time. Each marketing or promotional message we send to you will include a link or other
                            method to quickly and easily unsubscribe and decline further similar messages. You may also use the unsubscribe link included below under the heading “Unsubscribe.”. Please note that even if you unsubscribe from
                            receiving our promotional communications, you may continue to receive certain transactional or account-related communications from us. Our subscriber list is not sold or rented to other parties.
                        </p>
                        <h3>Participate in a Contest or Promotion</h3>
                        <p>
                            From time to time, we may run contests or promotions. If you participate, you may be asked for contact information as well as additional optional survey information (for example, product preferences). Personal
                            information from contest entries will be used to administer the contest and contact you if you win. For internal research and analysis purposes, we may also summarize contest survey information in an aggregated
                            manner. All contests and promotions are subject to this Privacy Policy and the rules that will be available with each particular campaign. Some contests are conducted by third parties and will be subject to the
                            third parties’ policies.
                        </p>
                        <h3>Email a Friend</h3>
                        <p>
                            While visiting our Web Sites you may choose to use the “Email a Friend” tool which allows you to inform a friend about products of interest to you. The information you provide will be used to send a one-time
                            email to your friend, inviting them to our Web Sites. We will not add your friend to a mailing list and will not send them further email unless they choose to receive such communications from us or where
                            otherwise legally permitted. If you provide us with personal information concerning another individual (e.g., email address), you represent and warrant that you have a personal or family relationship with the
                            recipient and obtained all necessary consents from that individual to enable us to use that personal information for the identified purpose.
                        </p>
                        <h3>Subscription Services and Activations</h3>
                        <p>
                            Urbamart may offer activation of certain products and services like cellular plans, satellite services, Internet services or smart-home services. These activations may require that certain contact and financial
                            information be collected and provided by Urbamart to the respective service providers as part of the activation process. For example, a cellular carrier may need to perform a credit check prior to activation so
                            certain identification such as a government-issued ID number and your credit card number may be provided to the carrier for that purpose.
                        </p>
                        <h3>Call Centres</h3>
                        <p>
                            Urbamart operates customer support call centres to accept and address your questions, concerns, or complaints. When you contact our call centres, we may ask for personal information to help us respond to your
                            inquiry or to verify your identity. For example if you have a question about the status of a recent online order, we will ask for personal information to verify that you are the account holder. We may also
                            monitor or record your telephone discussions with our customer support representatives for training purposes and to ensure service quality. If you do not want your call to be monitored and recorded, please let us
                            know and we will provide you with an alternative means of communicating with us.
                        </p>
                        <h3>In-store Video Surveillance</h3>
                        <p>
                            Urbamart retail stores are under video surveillance for safety and loss prevention purposes. The recorded images of our customers are viewed only when required. There may also be occasion where video footage is
                            used for general demographic or traffic flow analysis.
                        </p>
                        <h3>Collected automatically</h3>
                        <p>In some cases, we automatically collect certain information.</p>
                        <h3>Our Web Sites</h3>
                        <p>
                            Our Web Sites can be browsed without you providing us with any personal information. However, when using our Web Sites, we may collect the Internet Protocol (IP) address of your computer, the IP address of your
                            Internet Service Provider, the date and time you access our Web Sites, the Internet address of the Web site from which you linked directly to our Web Site, the operating system you are using, the sections of the
                            Web Site you visit, the Web Site pages read and images viewed, and the content you download from the Web Site. This information is used for Web Site and system administration purposes, to understand Web Site
                            activity and to improve the Web Sites.
                        </p>
                        <h3>Cookies, Tracer Tags and Web Beacons</h3>
                        <p>
                            The Web Sites use “cookies”, a tiny element of data that our Web Sites can send to your browser, which may then be stored on a Web Site user’s computer to permit the Web Site to recognize future visits using that
                            browser. Cookies enhance the convenience and use of the Web Site. For example, the information provided through cookies is used to recognize you as a previous user of the Web Site (so you do not have to enter
                            your language preference every time), to offer personalized Web page content and information for your use, to track your activity at the Web Site, to respond to your needs, and to otherwise facilitate your Web
                            Site experience. You may choose to decline cookies if your browser permits, but doing so may affect your use of the Web Site and your ability to access certain features of the Web Site or engage in transactions
                            through the Web Site. Our Web Sites or email communications may also use a technology such as “tracer tags” or “Web Beacons”. This technology allows us to understand which areas of our emails you click on and our
                            behaviour on our Web Sites, such as which pages you visit. These tracer tags are used to help us optimize and tailor our Web Site for you and other future visitors to our Web Site, and to help provide you with
                            email content and ads tailored to your interests.
                        </p>
                        <h3>Web Site Tracking Information</h3>
                        <p>
                            We may use web beacons or pixel tags to compile tracking information reports regarding Web Site user demographics, Web Site traffic patterns, and Web Site purchases. We may then provide these reports to
                            advertisers and others. For our own research purposes we may link tracking information with personal information voluntarily provided to us. Once such a link is made, all of the linked information is treated as
                            personal information and will be used and disclosed only in accordance with this Privacy Policy.
                        </p>
                        <h3>Interest-Based Advertising</h3>
                        <p>
                            We or third parties such as ad networks and other advertising companies may serve advertisements on our Web Sites and on other websites and social networking sites. We or those third parties may use cookies,
                            tracer tags or web beacons to report certain information about your visits to our Web Sites and other websites, such as web pages you visit and your response to ads, in order to measure the effectiveness of our
                            marketing campaigns, better understand our users and to deliver ads that are more relevant to you, both on and off our Web Sites.&nbsp; Please note that if you choose to opt-out you must opt-out of each web
                            browser on each computer, mobile device and tablet you use. If you opt-out of receiving interest-based ads, you will continue to receive advertising, but it will not be based on online behavioural information
                            about you. We may also use services provided by third-party platforms, including social networking platforms, to serve you tailored ads. We do this by providing a hashed version of your email address or other
                            information to the platform provider. To opt-out of this type of tailored advertising, please contact us as set out below under the heading “Unsubscribe”.
                        </p>
                        <h3>Social Media</h3>
                        <p>
                            We may offer you the opportunity to engage with our content on or through third-party social networking websites, plug-ins and applications. When you engage with our content on or through third-party social
                            networking websites, plug-ins and applications, you may allow us to have access to certain information associated with your social media account (e.g., name, username, email address, profile picture, gender) to
                            deliver the content or as part of the operation of the website, plug-in or application. When you provide information from your social media account, we may use this information to personalize your experience on
                            the Web Site and on the third-party social networking websites, plug-ins and applications, and to provide you with other products or services you may request.
                        </p>
                        <h3>Links to Other Web Sites</h3>
                        <p>
                            Our Web Site may contain links to other Web sites or Internet resources which are provided solely for your convenience and information. When you click on one of those links you are contacting another Web site or
                            Internet resource. Urbamart has no responsibility or liability for, or control over, those other Web sites or Internet resources or their collection, use and disclosure of your personal information. We encourage
                            you to read the privacy policies of those other Web sites to learn how they collect and use your personal information.
                        </p>
                        <h3>How else we use the information we collect</h3>
                        <p>In addition to the uses described above, Urbamart may use your personal information for a number of different business purposes, for example to:</p>
                        <ul>
                            <li>fulfill orders and requests for products, services or information;</li>
                            <li>process returns, exchanges and layaway requests;</li>
                            <li>detect and protect against fraud and error;</li>
                            <li>establish and manage your accounts with us;</li>
                            <li>track and confirm online orders;</li>
                            <li>deliver or install products;</li>
                            <li>track and analyze your purchases and preferences to better understand your product and service needs and eligibility;</li>
                            <li>market and advertise products and services;</li>
                            <li>communicate things like special events, promotions and surveys;</li>
                            <li>tailor our online content or in-store offerings to you based on your interactions with us and your preferences;</li>
                            <li>conduct research and analysis;</li>
                            <li>operate, evaluate and improve our business; and</li>
                            <li>for other purposes as described in this Privacy Policy.</li>
                        </ul>
                        <h3>When is Your Information Removed?</h3>
                        <p>
                            We keep your information only as long as we need it for legitimate business purposes and to meet any legal requirements. We have retention standards that meet these parameters. We destroy or erase your
                            information when it is no longer needed, or we remove your personally identifiable information to render it anonymous.
                        </p>
                        <h3>How we share your information</h3>
                        <p>
                            Urbamart does not sell or rent our customers’ personal information to any other party. However, in the normal course of business we may share some of your personal information within our corporate family and with
                            third parties acting on our behalf or as permitted or required by applicable law.
                        </p>
                        <p>
                            Our affiliates and third party service providers may be located outside of Uganda and this may result in personal information collected by Urbamart being stored or processed
                            outside of Uganda and, as a result, your personal information may be accessible to law enforcement, courts and regulatory authorities in accordance with the law of these foreign jurisdictions.
                        </p>
                        <h3>Service Providers</h3>
                        <p>
                            Urbamart may use third parties to manage one of more aspects of our business operations, including the processing or handing of personal information. When we do use an outside company, we use contractual or other
                            appropriate means to ensure that your personal information is used in a manner that is consistent with this Privacy Policy.
                        </p>
                        <p>
                            For example, we may share personal information with third parties to perform services on our behalf such as: fulfilling online orders, processing non-cash payments, sending marketing communications, servicing
                            products, conducting research surveys, verifying and validating information that you have provided to us, delivering products, and providing customer support services.
                        </p>
                        <p>
                            Urbamart has taken precautions to prevent the fraudulent use of your information on our Web Sites. Urbamart has adopted industry standard authentication procedures to ensure your protection while shopping with us
                            online. This includes, but is not limited to, contacting financial institutions or credit reporting agencies to ensure the authenticity of your credit card and to validate your credit card billing information.
                        </p>
                        <h3>
                            Information Shared Amongst Urbamart, its Corporate Parents, Affiliates, Subsidiaries and Divisions
                        </h3>
                        <p>
                            Urbamart may share personal information gathered on our Web Sites with Urbamart retail stores (and vice versa) and amongst our corporate parents, subsidiaries, affiliates or divisions for internal business
                            purposes in accordance with this Privacy Policy. If Urbamart shares your information within its corporate family, we will ensure that your information continues to be used only in accordance with this Privacy
                            Policy.
                        </p>
                        <h3>Product Safety Recalls</h3>
                        <p>
                            In the event we receive notice from a manufacturer of a product safety recall, we may provide your contact information, limited to name, address, and telephone number, to the manufacturer so that they may notify
                            you of the recall and supply details of any replacement or repair programs. This transfer or your contact information will occur only for recalls that may impact your personal safety or impair the functionality
                            of the product you purchased from us.
                        </p>
                        <h3>Sale or Transfer of all or part of Urbamart</h3>
                        <p>
                            Any information we have about you may be transferred or disclosed to a purchaser or prospective purchaser in the event of a sale, assignment, or other transfer of all or a portion of our business or assets.
                            Should such a transfer occur, we will use reasonable efforts to try to ensure that the transferee uses your information in a manner that is consistent with this Privacy Policy unless otherwise permitted or
                            required by applicable law.
                        </p>
                        <h3>Legal Disclosure</h3>
                        <p>
                            Urbamart and its Ugandan and other service providers or affiliates may disclose your information as permitted or required by applicable law. For example, we may be compelled to release information by a court of
                            law or other person or entity with jurisdiction to compel production of such information. If we have reasonable grounds to believe information could be useful in the investigation of unlawful activity, we may
                            disclose information to law enforcement agencies or other appropriate investigative bodies. We may also disclose personal information to other organizations in the case of investigating a breach of an agreement
                            or contravention of law or detecting, suppressing or preventing fraud.
                        </p>
                        <h3>How we protect your information</h3>
                        <p>
                            The security of your personal information is a high priority for Urbamart. We maintain appropriate physical, technical and administrative safeguards and industry-standard security standards in an effort to
                            protect your personal information in our custody and control, whether recorded on paper or captured electronically, against unauthorized access, disclosure, or misuse. For example, electronic records are stored
                            in secure, limited-access servers; we employ technological tools like firewalls and passwords; and we ensure our employees are trained on the importance of maintaining the security and confidentiality of personal
                            information.
                        </p>
                        <h3>Encryption Technology</h3>
                        <p>
                            Our Web Sites use encryption technology to protect your personal information during data transport. Such technology encrypts ordering information such as your name, address, and credit card number.
                        </p>
                        <h3>Choosing a Password</h3>
                        <p>
                            When you create an online account on our Web Sites, you need to select a personal password. To maximize your level of protection, you should choose at least 6 characters including a combination of both letters,
                            numbers and special characters and a mix of upper and lower case letters. You are solely responsible for maintaining the secrecy of your password and any account information. Urbamart will never send an
                            unsolicited communication asking you for your password.
                        </p>
                        <h3>How you can verify your information</h3>
                        <p>
                            You may check your information to verify, update, or correct it. If you created an account on one of our Web Sites, you can access and change your online account profile yourself. You can also ask to access or
                            review any of the information in our records, how we have used it, and to whom we have disclosed it at any time by contacting us as indicated below under the heading “Contacting Urbamart”. Subject to certain
                            exceptions prescribed by law, and provided we can authenticate your identity, you will be given reasonable access to your personal information, and will be entitled to challenge the accuracy and completeness of
                            the information and to have it amended as appropriate.
                        </p>
                        <h3>Data on Returned Product</h3>
                        <p>
                            When returning products which may contain stored or recorded personal information, like computers, tablets, mobile devices, smart devices, digital cameras, etc., you are responsible for deleting or removing all
                            personal information and media from your product before you return it. Urbamart is not responsible for any use by a third party of personal data or media left behind on a returned item.
                        </p>
                        <h3>Unsubscribe</h3>
                        <p>
                            If you no longer wish to be contacted regarding promotions, new product and entertainment releases and how to make the most of the latest technology&nbsp;
                            <a>
                                click here.
                            </a>
                            &nbsp;Please note that you may continue to receive certain transactional or account-related email messages.
                        </p>
                        <h3>How to contact us:</h3>
                        <p>
                            Urbamart is responsible for all personal information under its control. Our Privacy Manager is accountable for Urbamart’s compliance with the principles described here. If you have any questions, concerns or
                            complaints about the privacy practices of our organization or would like information on our use of service providers located outside of Uganda please contact us at the following:
                        </p>
                        <p>
                            <strong>Email:</strong> {{get_setting('contact_email')}} (please read the Important Reminder below)<br />
                            <strong>Telephone:</strong> {{get_setting('contact_phone')}}<br />
                            <strong>Company:</strong> {{get_setting('company_name')}}<br />
                            <strong>Attention:</strong> Privacy Manager<br />
                            <strong>Address:</strong>{{get_setting('contact_address')}}
                        </p>
                        <p>&nbsp;</p>
                        <p>We will respond to your request or investigate your concern as quickly as we can, but no later than 2 days from the date of receipt of your query.</p>
                        <h3>Important Reminder about Privacy and Security:</h3>
                        <p>
                            Please remember that email sent over the public Internet is not secure. If you send an email directly to us from your own email account the contents will not be encrypted. We strongly recommend that you do not
                            send sensitive information (like a credit card number) to us via unencrypted email. Urbamart is not responsible for any transmission by you of any personal information over the public Internet.
                        </p>
                        <p>
                            We cannot promise that your use of our Web Sites or mobile applications will be completely safe. We encourage you to use caution when using the internet, to protect your personal information and to know your
                            rights regarding privacy.
                        </p>
                        <p>To learn more about protecting your privacy online, we encourage you to visit the website of the Office of the Privacy Commissioner of Canada:</p>
                        <p>
                            <a href="https://www.priv.gc.ca/en/privacy-topics/technology-and-privacy/online-privacy/tips_pw/">
                                Tips for Creating and Managing Your Passwords
                            </a>
                        </p>
                        <p>
                            <a href="https://www.priv.gc.ca/en/privacy-topics/technology-and-privacy/online-privacy/protecting-your-privacy-online/">
                                Protecting your Privacy Online
                            </a>
                        </p>
                        <p>
                            <a href="https://www.priv.gc.ca/en/privacy-topics/technology-and-privacy/online-privacy/phishing/">
                                Recognizing Threats to Personal Data Online
                            </a>
                        </p>
                        <p>&nbsp;</p>
                        <p><strong>Last update:&nbsp;</strong><strong>December 2020</strong></p>
                        <p>
                            To accommodate changes in our services, personal information practices, changes in technology, and legal developments, this Privacy Policy may change over time without notice to you. We may add, change, or remove
                            portions of the Privacy Policy when we feel it is appropriate to do so. We encourage you to review our Privacy Policy periodically. Each time you submit personal information or use our services you agree to be
                            bound by the then current terms of the Privacy Policy. Whenever we update the Privacy Policy we will change the date to indicate when the changes were made.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</section>
@endsection
