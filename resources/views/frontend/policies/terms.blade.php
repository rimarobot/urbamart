@extends('frontend.layouts.app')

@section('content')
<section class="pt-4 mb-4">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 text-center text-lg-left">
                <h1 class="fw-600 h4">{{ translate('Terms & Conditions') }}</h1>
            </div>
            <div class="col-lg-6">
                <ul class="breadcrumb bg-transparent p-0 justify-content-center justify-content-lg-end">
                    <li class="breadcrumb-item opacity-50">
                        <a class="text-reset" href="{{ route('home') }}">{{ translate('Home')}}</a>
                    </li>
                    <li class="text-dark fw-600 breadcrumb-item">
                        <a class="text-reset" href="{{ route('terms') }}">"{{ translate('Terms & conditions') }}"</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="mb-4">
    <div class="container">
        <div class="p-4 bg-white rounded shadow-sm overflow-hidden mw-100 text-left">
             <div class="row">
                <div class="col">
                    <div class="p-0 bg-white">
                        <h3><strong>1.&nbsp;Introduction</strong></h3>
                        <p style="padding-left: 30px;">1.1&nbsp;These terms and conditions shall govern your use of Urbamart website.</p>
                        <p style="padding-left: 30px;">
                            1.2&nbsp;By using our website, you accept these terms and conditions in full; accordingly, if you disagree with these terms and conditions or any part of these terms and conditions, you must not use our website.
                        </p>
                        <p style="padding-left: 30px;">1.3&nbsp;If you register with our website, submit any material to our website or use any of our website services, we will ask you to expressly agree to these terms and conditions.</p>
                        <p style="padding-left: 30px;">
                            1.4&nbsp;You must be at least [18] years of age to use our website; by using our website or agreeing to these terms and conditions, you warrant and represent to us that you are at least [18] years of age.
                        </p>
                        <p style="padding-left: 30px;">
                            1.5&nbsp;urbamart.com uses cookies; by using our website or agreeing to these terms and conditions, you consent to our use of cookies in accordance with the terms of our privacy and cookies policy.
                        </p>
                        <h3><strong>2.&nbsp;Copyright notice</strong></h3>
                        <p style="padding-left: 30px;">2.1&nbsp;Copyright (c)&nbsp;<em>[year(s) of first publication]</em> <em>[full name]</em>.</p>
                        <p style="padding-left: 30px;">2.2&nbsp;Subject to the express provisions of these terms and conditions:</p>
                        <p style="padding-left: 30px;">(a)&nbsp;we, together with our licensors, own and control all the copyright and other intellectual property rights in our website and the material on our website; and</p>
                        <p style="padding-left: 30px;">(b)&nbsp;all the copyright and other intellectual property rights in our website and the material on our website are reserved.</p>
                        <h3>3. License&nbsp;to use website</h3>
                        <p style="padding-left: 30px;">3.1&nbsp;You may:</p>
                        <p style="padding-left: 60px;">(a)&nbsp;view pages from our website in a web browser;</p>
                        <p style="padding-left: 60px;">(b)&nbsp;download pages from our website for caching in a web browser;</p>
                        <p style="padding-left: 60px;">(c)&nbsp;print pages from our website;</p>
                        <p style="padding-left: 60px;">(d)&nbsp;[stream audio and video files from our website]; and</p>
                        <p style="padding-left: 60px;">(e)&nbsp;[use [our website services] by means of a web browser],</p>
                        <p style="padding-left: 60px;">subject to the other provisions of these terms and conditions.</p>
                        <p style="padding-left: 30px;">
                            3.2&nbsp;Except as expressly permitted by Section 3.1 or the other provisions of these terms and conditions, you must not download any material from our website or save any such material to your computer.
                        </p>
                        <p style="padding-left: 30px;">3.3&nbsp;You may only use our website for [your own personal and business purposes], and you must not use our website for any other purposes.</p>
                        <p style="padding-left: 30px;">3.4&nbsp;Except as expressly permitted by these terms and conditions, you must not edit or otherwise modify any material on our website.</p>
                        <p style="padding-left: 30px;">3.5&nbsp;Unless you own or control the relevant rights in the material, you must not:</p>
                        <p style="padding-left: 60px;">(a)&nbsp;republish material from our website (including republication on another website);</p>
                        <p style="padding-left: 60px;">(b)&nbsp;sell, rent or sub-license material from our website;</p>
                        <p style="padding-left: 60px;">(c)&nbsp;show any material from our website in public;</p>
                        <p style="padding-left: 60px;">(d)&nbsp;exploit material from our website for a commercial purpose; or</p>
                        <p style="padding-left: 60px;">(e)&nbsp;redistribute material from our website.</p>
                        <p style="padding-left: 30px;">3.6&nbsp;Notwithstanding Section 3.5, you may redistribute our newsletter in print and electronic form to any person.</p>
                        <p style="padding-left: 30px;">
                            3.7&nbsp;We reserve the right to restrict access to areas of our website, or indeed our whole website, at our discretion; you must not circumvent or bypass, or attempt to circumvent or bypass, any access restriction
                            measures on our website.
                        </p>
                        <h3><strong>4.&nbsp;Acceptable use</strong></h3>
                        <p style="padding-left: 30px;">4.1&nbsp;You must not:</p>
                        <p style="padding-left: 60px;">(a)&nbsp;use our website in any way or take any action that causes, or may cause, damage to the website or impairment of the performance, availability or accessibility of the website;</p>
                        <p style="padding-left: 60px;">(b)&nbsp;use our website in any way that is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity;</p>
                        <p style="padding-left: 60px;">
                            (c)&nbsp;use our website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or
                            other malicious computer software;
                        </p>
                        <p style="padding-left: 60px;">
                            (d)&nbsp;conduct any systematic or automated data collection activities including without limitation scraping, data mining, data extraction and data harvesting on or in relation to our website without our express
                            written consent;
                        </p>
                        <p style="padding-left: 60px;">(e)&nbsp;access or otherwise interact with our website using any robot, spider or other automated means, except for the purpose of search engine indexing;</p>
                        <p style="padding-left: 60px;">(f)&nbsp;violate the directives set out in the robots.txt file for our website; or</p>
                        <p style="padding-left: 60px;">(g)&nbsp;use data collected from our website for any direct marketing activity including without limitation email marketing, SMS marketing, telemarketing and direct mailing.</p>
                        <p style="padding-left: 30px;">4.2&nbsp;You must ensure that all the information you supply to us through our website, or in relation to our website, is true, accurate, current, complete and non-misleading.</p>
                        <h3><strong>5.&nbsp;Use on behalf of&nbsp;</strong>organization</h3>
                        <p style="padding-left: 30px;">5.1&nbsp;If you use our website or expressly agree to these terms and conditions in the course of a business or other organizational project, then by so doing you bind both:</p>
                        <p style="padding-left: 60px;">(a)&nbsp;yourself; and</p>
                        <p style="padding-left: 60px;">(b)&nbsp;the person, company or other legal entity that operates that business or organizational project,</p>
                        <p style="padding-left: 30px;">
                            to these terms and conditions, and in these circumstances references to “you” in these terms and conditions are to both the individual user and the relevant person, company or legal entity, unless the context
                            requires otherwise OR except that&nbsp;<em>specify exceptions</em>.
                        </p>
                        <h3><strong>6.&nbsp;Buyer registration and accounts</strong></h3>
                        <p style="padding-left: 30px;">6.1&nbsp;This Section 6 applies to buyers and prospective buyers.</p>
                        <p style="padding-left: 30px;">6.2&nbsp;To be eligible for a buyer account on our website under this Section 6, you must [be at least 18 years of age and resident in Uganda.</p>
                        <p style="padding-left: 30px;">
                            6.3&nbsp;You may register for a buyer account with our website by completing and submitting the account registration form on our website, and clicking on the verification link in the email that the website will send
                            to you.
                        </p>
                        <h3><strong>7.&nbsp;Seller registration and accounts</strong></h3>
                        <p style="padding-left: 30px;">7.1&nbsp;This Section 7 applies to sellers and prospective sellers.</p>
                        <p style="padding-left: 30px;">7.2&nbsp;To be eligible for [a business account] on our website under this Section 7, you must be operating a business and:</p>
                        <p style="padding-left: 60px;">(a)&nbsp;if you are a sole trader, you must be at least 18 years of age and resident in Uganda;</p>
                        <p style="padding-left: 60px;">(b)&nbsp;if you are a partnership, you must [be established under the laws of Uganda; and</p>
                        <p style="padding-left: 60px;">(c)&nbsp;if you are a limited company or other limited liability entity, you must be incorporated in Uganda.</p>
                        <p style="padding-left: 30px;">7.3&nbsp;To be eligible for an individual account on our website under this Section 7, you must be at least 18 years of age and resident in Uganda.</p>
                        <p style="padding-left: 30px;">
                            7.4&nbsp;You may register for a seller account with our website by completing and submitting the account registration form on our website, and clicking on the verification link in the email that the website will send
                            to you.
                        </p>
                        <h3><strong>8.&nbsp;User login details</strong></h3>
                        <p style="padding-left: 30px;">8.1&nbsp;If you register for an account with our website, we will provide you with OR you will be asked to choose a user ID and password.</p>
                        <p style="padding-left: 30px;">
                            8.2&nbsp;Your user ID must not be liable to mislead and must comply with the content rules set out in Section 20; you must not use your account or user ID for or in connection with the impersonation of any
                            person.&nbsp;
                        </p>
                        <p style="padding-left: 30px;">8.3&nbsp;You must keep your password confidential.</p>
                        <p style="padding-left: 30px;">8.4&nbsp;You must notify us in writing immediately if you become aware of any disclosure of your password.</p>
                        <p style="padding-left: 30px;">
                            8.5&nbsp;You are responsible for any activity on our website arising out of any failure to keep your password confidential, and may be held liable for any losses arising out of such a failure.
                        </p>
                        <h3><strong>9.&nbsp;Cancellation and suspension of account</strong></h3>
                        <p style="padding-left: 30px;">9.1&nbsp;We may:</p>
                        <p style="padding-left: 60px;">(a)&nbsp;suspend your account;</p>
                        <p style="padding-left: 60px;">(b)&nbsp;cancel your account; and/or</p>
                        <p style="padding-left: 60px;">(c)&nbsp;edit your account details,</p>
                        <p style="padding-left: 30px;">
                            at any time in our sole discretion without notice or explanation, providing that if we cancel any services you have paid for and you have not breached these terms and conditions, we will refund to you a pro rata
                            amount of your payment, such amount to be calculated by us using any reasonable methodology.
                        </p>
                        <p style="padding-left: 30px;">
                            9.2&nbsp;You may cancel your account on Urbamart website using your account control panel on the website. You will not be entitled to any refund if you cancel your account in accordance with this Section 9.2.
                        </p>
                        <h3><strong>10.&nbsp;Seller stores</strong></h3>
                        <p style="padding-left: 30px;">10.1&nbsp;If you register with our website as a seller, you will be able to create your own store OR stores on the website.</p>
                        <p style="padding-left: 30px;">10.2&nbsp;To create a store on our website, you should take the following steps:&nbsp;<em>[detail steps]</em>.</p>
                        <p style="padding-left: 30px;">10.3&nbsp;Seller stores that are submitted will be automatically processed OR individually reviewed within 24 hours following submission.</p>
                        <p style="padding-left: 30px;">
                            10.4&nbsp;Without prejudice to our other rights under these terms and conditions, we reserve the right to reject, unpublish and/or delete any seller store that breaches these terms and conditions or that does not
                            meet any additional guidelines for seller stores published on our website.
                        </p>
                        <p style="padding-left: 30px;">
                            10.5&nbsp;If we permit the publication of a seller store, it will remain published on our website for the relevant period set out on our website from time to time, subject to these terms and conditions.
                        </p>
                        <h3><strong>11.&nbsp;Seller listings</strong></h3>
                        <p style="padding-left: 30px;">11.1&nbsp;If you register with our website as a seller and create a store on the website, you will be able to submit listings to the website.</p>
                        <p style="padding-left: 30px;">11.2&nbsp;To create a listing on our website, you should take the following steps:&nbsp;<em>[detail steps]</em>.</p>
                        <p style="padding-left: 30px;">11.3&nbsp;Listings that are submitted will be individually reviewed within 24 hours following submission.</p>
                        <p style="padding-left: 30px;">
                            11.4&nbsp;Without prejudice to our other rights under these terms and conditions, we reserve the right to reject, unpublish and/or delete any listings that breach these terms and conditions or that do not meet any
                            additional guidelines for listings published on our website; and if we reject, unpublish or delete a listing for any of these reasons, we will not refund any fees you may have paid to us in respect of the listing.
                        </p>
                        <p style="padding-left: 30px;">
                            11.5&nbsp;If we permit the publication of a listing, it will remain published on our website for the relevant period set out on our website from time to time, subject to these terms and conditions.
                        </p>
                        <p style="padding-left: 30px;">11.6&nbsp;Listings submitted to our website must be true, fair, complete and accurate in all respects.</p>
                        <p style="padding-left: 30px;">11.7&nbsp;Listings submitted to our website must constitute bona fide listings relating to the <em>subject matter</em>.</p>
                        <p style="padding-left: 30px;">11.8&nbsp;Listings must be allocated to the appropriate category or categories. If you cannot identify an appropriate category for a listing, please contact us.</p>
                        <p style="padding-left: 30px;">11.9&nbsp;You must keep your listings up to date using our website interface; in particular, you must remove any listings in respect of products that have ceased to be available.</p>
                        <p style="padding-left: 30px;">11.10&nbsp;You must ensure that all prices specified in or in relation to a listing are in Ugandan Shilling only.</p>
                        <h3><strong>12.&nbsp;Product rules</strong></h3>
                        <p style="padding-left: 30px;">12.1&nbsp;The only products that may be the subject of a listing on our website are products falling within the following categories as specified on our website.</p>
                        <p style="padding-left: 30px;">12.2&nbsp;You must not advertise, buy, sell or supply through our website any product that:</p>
                        <p style="padding-left: 60px;">
                            (a)&nbsp;breaches any law, regulations or code, or infringes any person’s intellectual property rights or other rights, or gives rise to a cause of action against any person, in each case in any jurisdiction and
                            under any applicable law;
                        </p>
                        <p style="padding-left: 60px;">(b)&nbsp;consists of or contains material that would, if published on our website by you, contravene the provisions of Section 4 or Section 20; or</p>
                        <p style="padding-left: 60px;">
                            (c)&nbsp;is or relates to: drugs, narcotics, steroids or controlled substances; pornography; obscene, indecent or sexually explicit materials; knives, swords, firearms or other weapons; ammunition for any weapon;
                            items that encourage or facilitate criminal acts or civil wrongs; or items that encourage or facilitate the infringement of any intellectual property right.
                        </p>
                        <h3><strong>13.&nbsp;The buying and selling process</strong></h3>
                        <p style="padding-left: 30px;">
                            13.1&nbsp;You agree that a contract for the sale and purchase of a product or products will come into force between you and another registered website user, and accordingly that you commit to buying or selling the
                            relevant product or products, in the following circumstances: [<em>[describe contracting process]</em>] OR [
                        </p>
                        <p style="padding-left: 60px;">(a)&nbsp;a buyer must add the products he or she wishes to purchase to the shopping cart, and then proceed to the checkout;</p>
                        <p style="padding-left: 60px;">(b)&nbsp;if the buyer is a new user, he or she must create a buyer account with the website and log in; otherwise, the buyer must enter his or her login details;</p>
                        <p style="padding-left: 60px;">
                            (c)&nbsp;once the buyer is logged in, he or she must select the preferred method of delivery and confirm the order and his or her consent to these terms and conditions and the applicable terms and conditions of sale;
                        </p>
                        <p style="padding-left: 60px;">
                            (d)&nbsp;the buyer will be transferred to the website of the appointed payment service provider, and the payment service provider will handle the buyer’s payment to the seller; buyers may submit payment in full via
                            credit or debit card. Buyers may also pay cash on delivery- COD (or use other payment methods, which Urbamart may make available on the Site from time to time).
                        </p>
                        <p style="padding-left: 60px;">(e)&nbsp;the website will then send to the buyer an automatically generated acknowledgement of payment;</p>
                        <p style="padding-left: 60px;">
                            (f)&nbsp;once the seller has checked whether it is able to meet the buyer’s order, the seller will send to the buyer an order confirmation (at which point the order will become a binding contract between the seller
                            and the buyer) or the seller will confirm by email that it is unable to meet the buyer’s order.
                        </p>
                        <h3><strong>14.&nbsp;Terms and conditions of sale</strong></h3>
                        <p style="padding-left: 30px;">14.1&nbsp;Sellers must use the website interface to create legal notices applying to their relationships with customers.</p>
                        <p style="padding-left: 30px;">14.2&nbsp;A seller must ensure that:&nbsp;</p>
                        <p style="padding-left: 30px;">(a)&nbsp;the seller’s legal notices are sufficient to meet the seller’s legal disclosure obligations and other legal obligations; and</p>
                        <p style="padding-left: 30px;">
                            (b)&nbsp;the seller complies with all laws applicable to their product listings and sales, including where applicable the Consumer Rights Act 2015, the Consumer Contracts (Information, Cancellation and Additional
                            Charges) Regulations 2013 and the Electronic Commerce (EC Directive) Regulations 2002.
                        </p>
                        <p style="padding-left: 30px;">
                            14.3&nbsp;Except to the extent that a buyer and seller expressly agree otherwise (but subject to the mandatory requirements of applicable law), the following provisions will be incorporated into the contract of sale
                            and purchase between the buyer and the seller:&nbsp;
                        </p>
                        <p style="padding-left: 60px;">(a)&nbsp;the price for a product will be as stated [in the relevant product listing];</p>
                        <p style="padding-left: 60px;">
                            (b)&nbsp;delivery charges, packaging charges, handling charges, administrative charges, insurance costs, other ancillary costs and charges, and VAT and other taxes will only be payable by the buyer if this is
                            expressly and clearly stated in the product listing;
                        </p>
                        <p style="padding-left: 60px;">(c)&nbsp;deliveries of products must be made within 30 days following the date the contract of sale comes into force or such shorter period as the buyer and seller may agree;</p>
                        <p style="padding-left: 60px;">(d)&nbsp;appropriate means of delivery of products must be used by the seller; and</p>
                        <p style="padding-left: 60px;">
                            (e)&nbsp;products must be of satisfactory quality, and must be fit for any purpose specified in, and conform in all material respects to, the product listing and any other description of the products supplied or made
                            available by the seller to the buyer.
                        </p>
                        <p style="padding-left: 30px;">14.4&nbsp;If the seller is a trader and the buyer is a consumer, the provisions of Section 15 shall be incorporated into the contract of sale and purchase between a buyer and a seller.</p>
                        <p style="padding-left: 30px;">14.5&nbsp;Both buyers and sellers undertake to comply with the agreed terms and conditions of sale and purchase.</p>
                        <h3><strong>15.&nbsp;Distance contracts: cancellation right</strong></h3>
                        <p style="padding-left: 30px;">
                            15.1&nbsp;This Section 15 applies if and only if the seller is a trader (a person acting for purposes relating to that person’s trade, business, craft or profession, whether acting personally or through another
                            person acting in the trader’s name or on the trader’s behalf) and the buyer is a consumer (an individual acting wholly or mainly outside his or her trade, business, craft or profession).
                        </p>
                        <p style="padding-left: 30px;">
                            15.2&nbsp;The buyer may withdraw an offer to enter into a contract with a seller through our website or cancel a contract entered into with a seller through our website (without giving any reason for the withdrawal
                            or cancellation) at any time within the period:
                        </p>
                        <p style="padding-left: 60px;">(a)&nbsp;beginning upon the submission of the buyer’s offer; and</p>
                        <p style="padding-left: 60px;">
                            (b)&nbsp;ending at the end of 14 days after the day on which the goods come into the buyer’s physical possession or the physical possession of a person identified by the buyer to take possession of them (or, if the
                            contract is for delivery of multiple goods, lots or pieces of something, 14 days after the day on which the last of those goods, lots or pieces comes into the buyer’s physical possession or the physical possession of
                            a period identified by the buyer to take possession of them).
                        </p>
                        <p style="padding-left: 30px;">
                            15.3&nbsp;In order to withdraw an offer to contract or cancel a contract on the basis described in this Section 15, the buyer must inform the seller of the buyer’s decision to withdraw or cancel (as the case may be).
                            The buyer may inform the seller by means of any clear statement setting out the decision. In the case of cancellation, the buyer may inform the seller using the cancellation form that we or the seller will make
                            available to the buyer. To meet the cancellation deadline, it is sufficient for the buyer to send the communication concerning the exercise of the right to cancel before the cancellation period has expired.
                        </p>
                        <p style="padding-left: 30px;">
                            15.4&nbsp;If the buyer cancels a contract on the basis described in this Section 15, the buyer must send the products back to the seller (to the address specified by the seller on our website) or hand them over to
                            the seller or a person authorised by the seller to receive them. The buyer must comply with this obligation without undue delay and in any event not later than 14 days after the day on which the buyer informs the
                            seller of the decision to cancel the contract. The buyer must pay the direct cost of returning the products.
                        </p>
                        <p style="padding-left: 30px;">
                            15.5&nbsp;If the buyer cancels an order in accordance with this Section 15, the buyer will receive from the seller a full refund of the amount you paid to the seller in respect of the order including the costs of
                            delivery to the buyer, except:
                        </p>
                        <p style="padding-left: 60px;">
                            (a)&nbsp;if the buyer chose a kind of delivery costing more than the least expensive kind of delivery that the seller offers, the seller reserves the right to retain the difference in cost between the kind of
                            delivery the buyer chose and the least expensive kind of delivery that the seller offer; and
                        </p>
                        <p style="padding-left: 60px;">(b)&nbsp;as otherwise provided in this Section 15.</p>
                        <p style="padding-left: 30px;">
                            15.6&nbsp;If the value of the products returned by the buyer is diminished by any amount as a result of the handling of those products by the buyer beyond what is necessary to establish the nature, characteristics
                            and functioning of the products, the seller may recover that amount from the buyer up to the contract price. The seller may recover that amount by deducting it from any refund due to the buyer or require the buyer to
                            pay that amount direct to the seller. Handling which goes beyond the sort of handling that might reasonably be allowed in a shop will be “beyond what is necessary to establish the nature, characteristics and
                            functioning of the products” for these purposes.
                        </p>
                        <p style="padding-left: 30px;">
                            15.7&nbsp;The seller will refund money using the same method used to make the payment, unless the buyer has expressly agreed otherwise. In any case, the buyer will not incur any fees as a result of the refund.
                        </p>
                        <p style="padding-left: 30px;">
                            15.8&nbsp;Unless the seller has offered to collect the products, the seller will process a refund due to the buyer as a result of a cancellation on the basis described in this Section 15 within the period of 14 days
                            after the day on which the seller receive the returned products or (if earlier) after the day on which the buyer supplies to the seller evidence of having sent the products back. If the seller has not sent the
                            products at the time of withdrawal or cancellation or has offered to collect the products, the seller will process a refund due to the buyer without undue delay and, in any case, within the period of 14 days after
                            the day on which the seller is informed of the withdrawal or cancellation.
                        </p>
                        <p style="padding-left: 30px;">15.9&nbsp;The buyer will not have any right to cancel a contract as described in this Section 15 insofar as the contract relates to:</p>
                        <p style="padding-left: 60px;">(a)&nbsp;the supply of any sealed audio recordings, sealed video recordings or sealed computer software which have been unsealed by the buyer;</p>
                        <p style="padding-left: 60px;">
                            (b)&nbsp;the supply of products the price of which is dependent upon fluctuations in financial markets which the seller cannot control and which may occur during the cancellation period;
                        </p>
                        <p style="padding-left: 60px;">(c)&nbsp;the supply of newspapers, periodicals or magazines, with the exception of subscription contracts for the supply of such publications;</p>
                        <p style="padding-left: 60px;">(d)&nbsp;the supply of goods which are liable to deteriorate or expire rapidly;</p>
                        <p style="padding-left: 60px;">(e)&nbsp;the supply of non-prefabricated goods that are made on the basis of an individual choice of or decision by the buyer, or goods that are clearly personalized;</p>
                        <p style="padding-left: 60px;">(f)&nbsp;the supply of sealed goods which are not suitable for return due to health protection or hygiene reasons, where such goods have been unsealed by the buyer; and</p>
                        <p style="padding-left: 60px;">(g)&nbsp;the supply of goods which are, according to their nature, inseparably mixed with other items after delivery.</p>
                        <h3><strong>16.&nbsp;Marketplace fees</strong></h3>
                        <p style="padding-left: 30px;">16.1&nbsp;Marketplace sellers must pay to us the following amounts:&nbsp;</p>
                        <p style="padding-left: 60px;">(a)&nbsp;commission in respect of each sale made through our website</p>
                        <p style="padding-left: 60px;"><a href="{{route('seller-agreement')}}">seller agreement</a></p>
                        <p style="padding-left: 60px;"><em>[additional list items – refer to the link above]</em></p>
                        <p style="padding-left: 30px;">16.2&nbsp;In respect of fees payable to us by sellers:&nbsp;</p>
                        <p style="padding-left: 60px;">(a)&nbsp;the fees will be as specified on our website from time to time; and</p>
                        <p style="padding-left: 60px;">(b)&nbsp;the scope and duration of the services in respect of which the fees are payable will be as specified on our website at the time those fees are paid.</p>
                        <p style="padding-left: 30px;">16.3&nbsp;In respect of commission payable to us by sellers:&nbsp;</p>
                        <p style="padding-left: 60px;">(a)&nbsp;commission shall be payable at the rate or rates specified on our website from time to time;</p>
                        <p style="padding-left: 60px;">(b)&nbsp;we shall deduct commission due from amounts held or processed by us on behalf of the seller; and</p>
                        <p style="padding-left: 60px;">
                            (c)&nbsp;commission payments are non-refundable, irrespective of whether a buyer subsequently cancels the underlying order and irrespective of whether a buyer is entitled to, or receives, a refund in respect of such
                            an order.
                        </p>
                        <p style="padding-left: 30px;">16.4&nbsp;All amounts stated in these terms and conditions or on our website are stated [inclusive of VAT] OR [exclusive of VAT].</p>
                        <p style="padding-left: 30px;">16.5&nbsp;We may vary fees from time to time by posting new fees on our website, but this will not affect fees for services that have been previously paid.</p>
                        <p style="padding-left: 30px;">
                            16.6&nbsp;We may vary commission rates from time to time by posting new rates on our website, but this will not affect any liability to pay commission that accrues before the new rates are posted.
                        </p>
                        <h3><strong>17.&nbsp;Payments</strong></h3>
                        <p style="padding-left: 30px;">17.1&nbsp;You must pay to us the fees in respect of our website services in advance, in cleared funds, in accordance with any instructions on our website.</p>
                        <p style="padding-left: 30px;">17.2&nbsp;If you dispute any payment made to us, you must contact us immediately and provide full details of your claim.</p>
                        <p style="padding-left: 30px;">17.3&nbsp;If you make an unjustified credit card, debit card or other charge-back then you will be liable to pay us, within [7 days] following the date of our written request:&nbsp;</p>
                        <p style="padding-left: 60px;">(a)&nbsp;an amount equal to the amount of the charge-back;</p>
                        <p style="padding-left: 60px;">(b)&nbsp;all third party expenses incurred by us in relation to the charge-back (including charges made by our or your bank or payment processor or card issuer);</p>
                        <p style="padding-left: 60px;">(c)&nbsp;an administration fee of [GBP 25.00 including VAT]; and</p>
                        <p style="padding-left: 60px;">
                            (d)&nbsp;all our reasonable costs, losses and expenses incurred in recovering the amounts referred to in this Section 17.3 (including without limitation legal fees and debt collection fees),
                        </p>
                        <p style="padding-left: 30px;">
                            and for the avoidance of doubt, if you fail to recognise or fail to remember the source of an entry on your card statement or other financial statement, and make a charge-back as a result, this will constitute an
                            unjustified charge-back for the purposes of this Section 17.3.
                        </p>
                        <p style="padding-left: 30px;">17.4&nbsp;If you owe us any amount under or relating to these terms and conditions, we may suspend or withdraw the provision of services to you.</p>
                        <p style="padding-left: 30px;">17.5&nbsp;We may at any time set off any amount that you owe to us against any amount that we owe to you, by sending you written notice of the set-off.</p>
                        <h3><strong>18.&nbsp;Our role</strong></h3>
                        <p style="padding-left: 30px;">18.1&nbsp;You acknowledge that:</p>
                        <p style="padding-left: 60px;">(a)&nbsp;[we do not confirm the identity of website users, check their credit worthiness or bona fides, or otherwise vet them];</p>
                        <p style="padding-left: 60px;">(b)&nbsp;[we do not check, audit or monitor the information contained in listings];</p>
                        <p style="padding-left: 60px;">(c)&nbsp;we are not party to any contract for the sale or purchase of products advertised on the website;</p>
                        <p style="padding-left: 60px;">
                            (d)&nbsp;we are not involved in any transaction between a buyer and a seller in any way, save that we facilitate a marketplace for buyers and sellers[ and process payments on behalf of sellers];&nbsp;
                        </p>
                        <p style="padding-left: 60px;">(e)&nbsp;we are not the agents for any buyer or seller,</p>
                        <p style="padding-left: 30px;">
                            and accordingly we will not be liable to any person in relation to the offer for sale or sale or purchase of any products advertised on our website; furthermore we are not responsible for the enforcement of any
                            contractual obligations arising out of a contract for the sale or purchase of any products and we will have no obligation to mediate between the parties to any such contract.
                        </p>
                        <p style="padding-left: 30px;">18.2&nbsp;The provisions of this Section 18 are subject to Section 23.1.</p>
                        <h3><strong>19.&nbsp;Your content: licence</strong></h3>
                        <p style="padding-left: 30px;">
                            19.1&nbsp;In these terms and conditions, “your content” means [all works and materials (including without limitation text, graphics, images, audio material, video material, audio-visual material, scripts, software
                            and files) that you submit to us or our website for storage or publication on, processing by, or transmission via, our website].
                        </p>
                        <p style="padding-left: 30px;">
                            19.2&nbsp;You grant to us a [worldwide, irrevocable, non-exclusive, royalty-free licence] to [use, reproduce, store, adapt, publish, translate and distribute your content in any existing or future media] OR
                            [reproduce, store and publish your content on and in relation to this website and any successor website] OR [reproduce, store and, with your specific consent, publish your content on and in relation to this website].
                        </p>
                        <p style="padding-left: 30px;">19.3&nbsp;You grant to us the right to sub-license the rights licensed under Section 19.2.</p>
                        <p style="padding-left: 30px;">19.4&nbsp;You grant to us the right to bring an action for infringement of the rights licensed under Section 19.2.</p>
                        <p style="padding-left: 30px;">
                            19.5&nbsp;You hereby waive all your moral rights in your content to the maximum extent permitted by applicable law; and you warrant and represent that all other moral rights in your content have been waived to the
                            maximum extent permitted by applicable law.
                        </p>
                        <p style="padding-left: 30px;">19.6&nbsp;You may edit your content to the extent permitted using the editing functionality made available on our website.</p>
                        <p style="padding-left: 30px;">
                            19.7&nbsp;Without prejudice to our other rights under these terms and conditions, if you breach any provision of these terms and conditions in any way, or if we reasonably suspect that you have breached these terms
                            and conditions in any way, we may delete, unpublish or edit any or all of your content.
                        </p>
                        <h3><strong>20.&nbsp;Your content: rules</strong></h3>
                        <p style="padding-left: 30px;">20.1&nbsp;You warrant and represent that your content will comply with these terms and conditions.</p>
                        <p style="padding-left: 30px;">
                            20.2&nbsp;Your content must not be illegal or unlawful, must not infringe any person’s legal rights, and must not be capable of giving rise to legal action against any person (in each case in any jurisdiction and
                            under any applicable law).
                        </p>
                        <p style="padding-left: 30px;">20.3&nbsp;Your content, and the use of your content by us in accordance with these terms and conditions, must not:</p>
                        <p style="padding-left: 60px;">(a)&nbsp;be libelous or maliciously false;</p>
                        <p style="padding-left: 60px;">(b)&nbsp;be obscene or indecent;</p>
                        <p style="padding-left: 60px;">(c)&nbsp;infringe any copyright, moral right, database right, trade mark right, design right, right in passing off, or other intellectual property right;</p>
                        <p style="padding-left: 60px;">(d)&nbsp;infringe any right of confidence, right of privacy or right under data protection legislation;</p>
                        <p style="padding-left: 60px;">(e)&nbsp;constitute negligent advice or contain any negligent statement;</p>
                        <p style="padding-left: 60px;">(f)&nbsp;constitute an incitement to commit a crime[, instructions for the commission of a crime or the promotion of criminal activity];</p>
                        <p style="padding-left: 60px;">(g)&nbsp;be in contempt of any court, or in breach of any court order;</p>
                        <p style="padding-left: 60px;">(h)&nbsp;be in breach of racial or religious hatred or discrimination legislation;</p>
                        <p style="padding-left: 60px;">(i)&nbsp;be blasphemous;</p>
                        <p style="padding-left: 60px;">(j)&nbsp;be in breach of official secrets legislation;</p>
                        <p style="padding-left: 60px;">(k)&nbsp;be in breach of any contractual obligation owed to any person;</p>
                        <p style="padding-left: 60px;">(l)&nbsp;[depict violence[ in an explicit, graphic or gratuitous manner]];</p>
                        <p style="padding-left: 60px;">(m)&nbsp;[be pornographic[, lewd, suggestive or sexually explicit]];</p>
                        <p style="padding-left: 60px;">(n)&nbsp;[be untrue, false, inaccurate or misleading];</p>
                        <p style="padding-left: 60px;">
                            (o)&nbsp;[consist of or contain any instructions, advice or other information which may be acted upon and could, if acted upon, cause illness, injury or death, or any other loss or damage];
                        </p>
                        <p style="padding-left: 60px;">(p)&nbsp;[constitute spam];</p>
                        <p style="padding-left: 60px;">(q)&nbsp;[be offensive, deceptive, fraudulent, threatening, abusive, harassing, anti-social, menacing, hateful, discriminatory or inflammatory]; or</p>
                        <p style="padding-left: 60px;">(r)&nbsp;[cause annoyance, inconvenience or needless anxiety to any person].</p>
                        <p style="padding-left: 30px;">20.4&nbsp;Your content must be appropriate, civil and tasteful, and accord with generally accepted standards of etiquette and behavior on the internet.</p>
                        <p style="padding-left: 30px;">
                            20.5&nbsp;You must not use our website to link to any website or web page consisting of or containing material that would, were it posted on our website, breach the provisions of these terms and conditions.
                        </p>
                        <p style="padding-left: 30px;">20.6&nbsp;You must not submit to our website any material that is or has ever been the subject of any threatened or actual legal proceedings or other similar complaint.</p>
                        <h3><strong>21.&nbsp;Report abuse</strong></h3>
                        <p style="padding-left: 30px;">21.1&nbsp;If you learn of any unlawful material or activity on our website, or any material or activity that breaches these terms and conditions, please let us know.</p>
                        <p style="padding-left: 30px;">21.2&nbsp;You can let us know about any such material or activity [by email or using our abuse reporting form].</p>
                        <h3><strong>22.&nbsp;Limited warranties</strong></h3>
                        <p style="padding-left: 30px;">22.1&nbsp;We do not warrant or represent:</p>
                        <p style="padding-left: 60px;">(a)&nbsp;the completeness or accuracy of the information published on our website;</p>
                        <p style="padding-left: 60px;">(b)&nbsp;that the material on the website is up to date; or</p>
                        <p style="padding-left: 60px;">(c)&nbsp;that the website or any service on the website will remain available.</p>
                        <p style="padding-left: 30px;">
                            22.2&nbsp;We reserve the right to discontinue or alter any or all of our website services, and to stop publishing our website, at any time in our sole discretion without notice or explanation; and save to the extent
                            expressly provided otherwise in these terms and conditions, you will not be entitled to any compensation or other payment upon the discontinuance or alteration of any website services, or if we stop publishing the
                            website.
                        </p>
                        <p style="padding-left: 30px;">
                            22.3&nbsp;To the maximum extent permitted by applicable law and subject to Section 23.1, we exclude all representations and warranties relating to the subject matter of these terms and conditions, our website and the
                            use of our website.
                        </p>
                        <h3><strong>23.&nbsp;Limitations and exclusions of liability</strong></h3>
                        <p style="padding-left: 30px;">23.1&nbsp;Nothing in these terms and conditions will:</p>
                        <p style="padding-left: 60px;">(a)&nbsp;limit or exclude any liability for death or personal injury resulting from negligence;</p>
                        <p style="padding-left: 60px;">(b)&nbsp;limit or exclude any liability for fraud or fraudulent misrepresentation;</p>
                        <p style="padding-left: 60px;">(c)&nbsp;limit any liabilities in any way that is not permitted under applicable law; or</p>
                        <p style="padding-left: 60px;">(d)&nbsp;exclude any liabilities that may not be excluded under applicable law,</p>
                        <p style="padding-left: 30px;">and, if you are a consumer, your statutory rights will not be excluded or limited by these terms and conditions, except to the extent permitted by law.</p>
                        <p style="padding-left: 30px;">23.2&nbsp;The limitations and exclusions of liability set out in this Section 23 and elsewhere in these terms and conditions:</p>
                        <p style="padding-left: 60px;">(a)&nbsp;are subject to Section 23.1; and</p>
                        <p style="padding-left: 60px;">
                            (b)&nbsp;govern all liabilities arising under these terms and conditions or relating to the subject matter of these terms and conditions, including liabilities arising in contract, in tort (including negligence) and
                            for breach of statutory duty, except to the extent expressly provided otherwise in these terms and conditions.
                        </p>
                        <p style="padding-left: 30px;">23.3&nbsp;To the extent that our website and the information and services on our website are provided free of charge, we will not be liable for any loss or damage of any nature.</p>
                        <p style="padding-left: 30px;">23.4&nbsp;We will not be liable to you in respect of any losses arising out of any event or events beyond our reasonable control.</p>
                        <p style="padding-left: 30px;">
                            23.5&nbsp;We will not be liable to you in respect of any business losses, including (without limitation) loss of or damage to profits, income, revenue, use, production, anticipated savings, business, contracts,
                            commercial opportunities or goodwill.
                        </p>
                        <p style="padding-left: 30px;">
                            23.6&nbsp;We will not be liable to you in respect of any loss or corruption of any data, database or software[, providing that if you contract with us under these terms and conditions as a consumer, this Section 23.6
                            shall not apply].
                        </p>
                        <p style="padding-left: 30px;">
                            23.7&nbsp;We will not be liable to you in respect of any special, indirect or consequential loss or damage[, providing that if you contract with us under these terms and conditions as a consumer, this Section 23.7
                            shall not apply].
                        </p>
                        <p style="padding-left: 30px;">
                            23.8&nbsp;You accept that we have an interest in limiting the personal liability of our officers and employees and, having regard to that interest, you acknowledge that we are a limited liability entity; you agree
                            that you will not bring any claim personally against our officers or employees in respect of any losses you suffer in connection with the website or these terms and conditions (this will not, of course, limit or
                            exclude the liability of the limited liability entity itself for the acts and omissions of our officers and employees).
                        </p>
                        <p style="padding-left: 30px;">23.9&nbsp;Our aggregate liability to you in respect of any contract to provide services to you under these terms and conditions shall not exceed the greater of:</p>
                        <p style="padding-left: 60px;">(a)&nbsp;<em>[amount]</em>; and</p>
                        <p style="padding-left: 60px;">(b)&nbsp;[the total amount paid and payable to us under the contract].</p>
                        <h3><strong>24.&nbsp;Indemnity</strong></h3>
                        <p style="padding-left: 30px;">
                            24.1&nbsp;You hereby indemnify us, and undertake to keep us indemnified, against any and all losses, damages, costs, liabilities and expenses (including without limitation legal expenses and any amounts paid by us to
                            a third party in settlement of a claim or dispute) incurred or suffered by us and arising directly or indirectly out of [your use of our website or any breach by you of any provision of these terms and conditions].
                        </p>
                        <h3><strong>25.&nbsp;Breaches of these terms and conditions</strong></h3>
                        <p style="padding-left: 30px;">
                            25.1&nbsp;Without prejudice to our other rights under these terms and conditions, if you breach these terms and conditions in any way, or if we reasonably suspect that you have breached these terms and conditions in
                            any way, we may:&nbsp;
                        </p>
                        <p style="padding-left: 60px;">(a)&nbsp;send you one or more formal warnings;</p>
                        <p style="padding-left: 60px;">(b)&nbsp;temporarily suspend your access to our website;</p>
                        <p style="padding-left: 60px;">(c)&nbsp;permanently prohibit you from accessing our website;</p>
                        <p style="padding-left: 60px;">(d)&nbsp;block computers using your IP address from accessing our website;</p>
                        <p style="padding-left: 60px;">(e)&nbsp;contact any or all of your internet service providers and request that they block your access to our website;</p>
                        <p style="padding-left: 60px;">(f)&nbsp;commence legal action against you, whether for breach of contract or otherwise; and/or</p>
                        <p style="padding-left: 60px;">(g)&nbsp;suspend or delete your account on our website.</p>
                        <p style="padding-left: 30px;">
                            25.2&nbsp;Where we suspend or prohibit or block your access to our website or a part of our website, you must not take any action to circumvent such suspension or prohibition or blocking including without limitation
                            creating and/or using a different account.
                        </p>
                        <h3><strong>26.&nbsp;Third party websites</strong></h3>
                        <p style="padding-left: 30px;">26.1&nbsp;Our website includes hyperlinks to other websites owned and operated by third parties; such hyperlinks are not recommendations.</p>
                        <p style="padding-left: 30px;">
                            26.2&nbsp;We have no control over third party websites and their contents, and subject to Section 23.1 we accept no responsibility for them or for any loss or damage that may arise from your use of them.
                        </p>
                        <h3><strong>27.&nbsp;Trade marks</strong></h3>
                        <p style="padding-left: 30px;">
                            27.1&nbsp;<em>urbamart.com</em>, our logos and our other registered and unregistered trademarks are trademarks belonging to us; we give no permission for the use of these trademarks, and such use may constitute an
                            infringement of our rights.
                        </p>
                        <p style="padding-left: 30px;">
                            27.2&nbsp;The third party registered and unregistered trade marks or service marks on our website are the property of their respective owners and, unless stated otherwise in these terms and conditions, we do not
                            endorse and are not affiliated with any of the holders of any such rights and as such we cannot grant any license to exercise such rights.
                        </p>
                        <h3><strong>28.&nbsp;Variation</strong></h3>
                        <p style="padding-left: 30px;">28.1&nbsp;We may revise these terms and conditions from time to time.</p>
                        <p style="padding-left: 30px;">
                            28.2&nbsp;The revised terms and conditions shall apply to the use of our website from the date of publication of the revised terms and conditions on the website, and you hereby waive any right you may otherwise have
                            to be notified of, or to consent to, revisions of these terms and conditions.
                        </p>
                        <p style="padding-left: 30px;">
                            28.3&nbsp;If you have given your express agreement to these terms and conditions, we will ask for your express agreement to any revision of these terms and conditions; and if you do not give your express agreement to
                            the revised terms and conditions within such period as we may specify, we will disable or delete your account on the website, and you must stop using the website.
                        </p>
                        <h3><strong>29.&nbsp;Assignment</strong></h3>
                        <p style="padding-left: 30px;">
                            29.1&nbsp;You hereby agree that we may assign, transfer, sub-contract or otherwise deal with our rights and/or obligations under these terms and conditions- providing, if you are a consumer, that such action does not
                            serve to reduce the guarantees benefiting you under these terms and conditions.
                        </p>
                        <p style="padding-left: 30px;">
                            29.2&nbsp;You may not without our prior written consent assign, transfer, sub-contract or otherwise deal with any of your rights and/or obligations under these terms and conditions.&nbsp;
                        </p>
                        <h3><strong>30.&nbsp;Severability</strong></h3>
                        <p style="padding-left: 30px;">
                            30.1&nbsp;If a provision of these terms and conditions is determined by any court or other competent authority to be unlawful and/or unenforceable, the other provisions will continue in effect.
                        </p>
                        <p style="padding-left: 30px;">
                            30.2&nbsp;If any unlawful and/or unenforceable provision of these terms and conditions would be lawful or enforceable if part of it were deleted, that part will be deemed to be deleted, and the rest of the provision
                            will continue in effect.&nbsp;
                        </p>
                        <h3><strong>31.&nbsp;Third party rights</strong></h3>
                        <p style="padding-left: 30px;">31.1&nbsp;A contract under these terms and conditions is for our benefit and your benefit, and is not intended to benefit or be enforceable by any third party.</p>
                        <p style="padding-left: 30px;">31.2&nbsp;The exercise of the parties’ rights under a contract under these terms and conditions is not subject to the consent of any third party.</p>
                        <h3><strong>32.&nbsp;Entire agreement</strong></h3>
                        <p style="padding-left: 30px;">
                            32.1&nbsp;Subject to Section 23.1, these terms and conditions, together with our privacy and cookies policy, shall constitute the entire agreement between you and us in relation to your use of our website and shall
                            supersede all previous agreements between you and us in relation to your use of our website.
                        </p>
                        <h3><strong>33.&nbsp;Law and jurisdiction</strong></h3>
                        <p style="padding-left: 30px;">33.1&nbsp;These terms and conditions shall be governed by and construed in accordance with Ugandan law.</p>
                        <p style="padding-left: 30px;">33.2&nbsp;Any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the courts of Uganda.</p>
                        <h3><strong>34.&nbsp;Statutory and regulatory disclosures</strong></h3>
                            <p style="padding-left: 30px;">
                                34.1&nbsp;We will specify on the website or elsewhere in these terms and conditions the different technical steps you must follow to conclude a contract under these terms and conditions, and also the technical
                                means for identifying and correcting input errors prior to the placing of your order.
                            </p>
                            <p style="padding-left: 30px;">
                                34.2&nbsp;We will not file a copy of these terms and conditions specifically in relation to each user or customer and, if we update these terms and conditions, the version to which you originally agreed will no
                                longer be available on our website. We recommend that you consider saving a copy of these terms and conditions for future reference.
                            </p>
                            <p style="padding-left: 30px;">34.3&nbsp;These terms and conditions are available in the English language only.</p>
                            <p style="padding-left: 30px;">
                                34.4&nbsp;We are registered in&nbsp;<em>[trade register]</em>; you can find the online version of the register at <em>[URL]</em>, and our registration number is&nbsp;<em>[number]</em>.
                            </p>
                            <p style="padding-left: 30px;">34.5&nbsp;We are subject to&nbsp;<em>[authorization&nbsp;scheme]</em>, which is supervised by <em>[supervisory authority]</em>.</p>
                            <p style="padding-left: 30px;">
                                34.6&nbsp;We are registered as&nbsp;<em>[title]</em>&nbsp;with <em>[professional body]</em>&nbsp;in&nbsp;<em>[the United Kingdom]</em>&nbsp;and are subject to&nbsp;<em>[rules]</em>, which can be found
                                at&nbsp;<em>[URL]</em>.
                            </p>
                            <p style="padding-left: 30px;">34.7&nbsp;We subscribe to&nbsp;<em>[code(s) of conduct]</em>, which can be consulted electronically at <em>[URL(s)]</em>.</p>
                            <p style="padding-left: 30px;">34.8&nbsp;Our VAT number is&nbsp;<em>[number]</em>.</p>
                            <p style="padding-left: 30px;">
                                34.9&nbsp;The website of the European Union’s online dispute resolution platform is available at https://webgate.ec.europa.eu/odr/main.[ The online dispute resolution platform may be used for resolving disputes.]
                            </p>
                            <p style="padding-left: 30px;">34.10&nbsp;The name of the [alternative dispute resolution entity] that we use dispute resolution is&nbsp;<em>[name]</em>&nbsp;and its website address is&nbsp;<em>[URL]</em>.</p>

                        <h3><strong>35.&nbsp;Our details</strong></h3>
                        <p style="padding-left: 30px;">35.1&nbsp;This website is owned and operated by zByte Technology Ltd.</p>
                        <p style="padding-left: 30px;">35.2&nbsp;We are registered in [England and Wales] under registration number&nbsp;<em>[number]</em>, and our registered office is at&nbsp;<em>[address]</em>.</p>
                        <p style="padding-left: 30px;">35.3&nbsp;Our principal place of business is at&nbsp;<em>[address]</em>.</p>
                        <p style="padding-left: 30px;">35.4&nbsp;You can contact us:</p>
                        <p style="padding-left: 60px;">(a)&nbsp;[by post, using the postal address [given above]];</p>
                        <p style="padding-left: 60px;">(b)&nbsp;[using our website contact form];</p>
                        <p style="padding-left: 60px;">(c)&nbsp;[by telephone, on [the contact number published on our website from time to time]]; or</p>
                        <p style="padding-left: 60px;">(d)&nbsp;[by email, using [the email address published on our website from time to time]].</p>
                        <p style="padding-left: 30px;"><em>[additional list items]</em></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
