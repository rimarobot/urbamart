@extends('backend.layouts.app')

@section('content')
<div class="card">
      <form class="" action="" method="GET">
        <div class="card-header row gutters-5">
          <div class="col text-center text-md-left">
            <h5 class="mb-md-0 h6">{{ translate('All Orders') }}</h5>
          </div>
          <div class="col-md-3 ml-auto">
            <select class="form-control aiz-selectpicker" data-placeholder="{{ translate('Delivery Status')}}" name="deliveryt_status" onchange="sort_orders()">
                <option value="">{{ translate('Select')}}</option>
                <option value="paid">{{ translate('Paid')}}</option>
                <option value="unpaid">{{ translate('Un-Paid')}}</option>
            </select>
        </div>
          <div class="col-lg-2">
              <div class="form-group mb-0">
                  <input type="text" class="aiz-date-range form-control" value="{{ $date }}" name="date" placeholder="{{ translate('Filter by date') }}" data-format="DD-MM-Y" data-separator=" to " data-advanced-range="true" autocomplete="off">
              </div>
          </div>
          <div class="col-lg-2">
            <div class="form-group mb-0">
              <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="{{ translate('Type Order code & hit Enter') }}">
            </div>
          </div>
          <div class="col-auto">
            <div class="form-group mb-0">
              <button type="submit" class="btn btn-primary">{{ translate('Filter') }}</button>
            </div>
          </div>
        </div>
    </form>
    <div class="card-body">
        <table class="table aiz-table mb-0">
            <thead>
                <tr>
                    {{-- <th>#</th> --}}
                    <th>{{ translate('Order Code') }}</th>
                    <th>{{ translate('Driver') }}</th>
                    <th data-breakpoints="md">{{ translate('VIN') }}</th>
                    <th>{{ translate('Amount') }}</th>
                    <th data-breakpoints="md">{{ translate('Shipping Fee') }}</th>
                    <th data-breakpoints="md">{{ translate('Payment Status') }}</th>
                    <th data-breakpoints="md">{{ translate('Shipping Type') }}</th>
                    <th data-breakpoints="md">{{ translate('Shipping Status') }}</th>
                    <th data-breakpoints="md" class="text-right" width="15%">{{translate('options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($orders as $key => $order)
                    <tr>
                        {{-- <td>{{ ($key+1) + ($orders->currentPage() - 1)*$orders->perPage() }}</td> --}}
                        <td>{{ $order->order_code }}</td>
                        <td>{{ $order->driver->user->name }}</td>
                        <td>{{ $order->driver->vehicle_identification_number }}</td>
                        <td>{{ single_price($order->order_price) }}</td>
                        <td>{{ single_price($order->shipping_cost) }}</td>
                        <td>
                            @if ($order->payment_status == 'paid')
                                <span class="badge badge-inline badge-success">{{ ucwords(str_replace('_', ' ', $order->payment_type)) }}</span>
                            @else
                                <span class="badge badge-inline badge-danger">{{ ucwords(str_replace('_', ' ', $order->payment_type)) }}</span>
                            @endif
                        </td>
                        <td>{{ $order->shipping_type }}</td>
                        <td>{{ ucwords(str_replace('_', ' ', $order->status)) }}</td>
                        <td class="text-right">
                            <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{route('all_orders.show', encrypt($order->id))}}" title="{{ translate('View') }}">
                                <i class="las la-eye"></i>
                            </a>
                            <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{ route('customer.invoice.download', $order->id) }}" title="{{ translate('Download Invoice') }}">
                                <i class="las la-download"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="aiz-pagination">
            {{ $orders->appends(request()->input())->links() }}
        </div>
    </div>
</div>

@endsection

@section('modal')
    @include('modals.delete_modal')
@endsection

@section('script')
    <script type="text/javascript">

    </script>
@endsection
