@extends('backend.layouts.app')

@section('content')
<div class="card">
      <form class="" action="{{ route('deliveries.store') }}" method="POST">
        @csrf
        <div class="card-header row gutters-5">
          <div class="col text-center text-md-left">
            <h5 class="mb-md-0 h6">{{ translate('All Pending Orders') }}</h5>
          </div>
          <div class="col-lg-4">
            <div class="form-group mb-0">
                <input type="hidden" value="" name="selected_orders" required/>
                <select class="form-control aiz-selectpicker" data-placeholder="{{ translate('Select Driver')}}" name="driver_id" required>
                    <option value="">{{ translate('Select Driver')}}</option>
                    @foreach ($drivers as $key => $driver)
                        <option value="{{ $driver->driver_id }}">{{ translate( $driver->name .' ('. $driver->vehicle_type .')' )}}</option>
                    @endforeach
                </select>
            </div>
          </div>
          <div class="col-auto">
            <div class="form-group mb-0">
              <button type="submit" class="btn btn-primary">{{ translate('Asign Driver') }}</button>
            </div>
          </div>
        </div>
    </form>
    <div class="card-body">
        <table class="table aiz-table mb-0">
            <thead>
                <tr>
                    <th>{{ translate('Order Code') }}</th>
                    <th data-breakpoints="md">{{ translate('Amount') }}</th>
                    <th>{{ translate('Shipping Fee') }}</th>
                    <th data-breakpoints="md">{{ translate('Payment Status') }}</th>
                    <th data-breakpoints="md">{{ translate('Shipping Type') }}</th>
                    <th data-breakpoints="md">{{ translate('Qty') }}</th>
                    <th data-breakpoints="md">{{ translate('Seller') }}</th>
                    <th data-breakpoints="xl">{{ translate('Buyer') }}</th>
                    <th class="text-right"><input type="checkbox" name='selectAll'></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($orders as $key => $order)
                    <tr>
                        <td>{{ $order->code }}</td>
                        <td>{{ single_price($order->price) }}</td>
                        <td>{{ single_price($order->shipping_cost )}}</td>
                        <td>
                            @if ($order->payment_status == 'paid')
                                <span class="badge badge-inline badge-success">{{ ucwords(str_replace('_', ' ', $order->payment_type)) }}</span>
                            @else
                                <span class="badge badge-inline badge-danger">{{ ucwords(str_replace('_', ' ', $order->payment_type)) }}</span>
                            @endif
                        </td>
                        <td>{{ $order->shipping_type }}</td>
                        <td>{{ $order->qty }}</td>
                        <td>{{$order->shop}}</td>
                        <td>{{ json_decode($order->shipping_address)->address.', '. json_decode($order->shipping_address)->country.', '.json_decode($order->shipping_address)->city.', '.json_decode($order->shipping_address)->phone }}</td>
                        <td class="text-right">
                            <input type="checkbox" name='orderid' value="'{{$order->code}}'">
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{-- <div class="aiz-pagination">
            {{ $orders->appends(request()->input())->links() }}
        </div> --}}
    </div>
</div>

@endsection

@section('modal')
    @include('modals.delete_modal')
@endsection

@section('script')
    <script type="text/javascript">
          $(document).on('click','input[name="selectAll"]', function() {
                $('input[name=orderid]').prop('checked',this.checked);
                $('input[name="selected_orders"]').val($('input:checked[name="orderid"]').map(function () { return $(this).val(); }).get());
                console.log($('input[name="selected_orders"]').val());
          })
          $(document).on('click','input[name="orderid"]', function() {
               $('input[name="selected_orders"]').val($('input:checked[name="orderid"]').map(function () { return $(this).val(); }).get());
               console.log($('input[name="selected_orders"]').val());
          })
    </script>
@endsection
