@extends('frontend.layouts.app')

@section('content')

<section class="py-8 bg-soft-primary">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 mx-auto text-center">
                <h1 class="mb-0 fw-700">{{translate('Premium Packages for Sellers')}}</h1>
            </div>
        </div>
    </div>
</section>

<section class="py-4 py-lg-5">
    <div class="container"> 
        <div class="row row-cols-xxl-4 row-cols-lg-3 row-cols-md-2 row-cols-1 gutters-10 justify-content-center">
            @foreach ($seller_packages as $key => $seller_package)
                <div class="col">
                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <div class="text-center mb-4 mt-3">
                                <img class="mw-100 mx-auto mb-4 img-fluid lazyload" 
                                    src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                    data-src="{{ uploaded_asset($seller_package->logo) }}"
                                    onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                                    height="100">
                                <h5 class="mb-3 h5 fw-600">{{ $seller_package->name }}</h5>
                            </div>
                            <ul class="list-group list-group-raw fs-15 mb-5">
                                <li class="list-group-item py-2">
                                    <i class="las la-check text-success mr-2"></i>
                                    {{ $seller_package->product_upload }} {{__(' Product Upload')}}
                                </li>
                                <li class="list-group-item py-2">
                                    <i class="las la-check text-success mr-2"></i>
                                    {{ $seller_package->digital_product_upload }} {{__(' Digital Product Upload')}}
                                </li>
                            </ul>
                            <div class="mb-5 d-flex align-items-center justify-content-center">
                                <span class="display-4 fw-600 lh-1 mb-0">{{ single_price($seller_package->amount) }}</span>
                                <span class="text-secondary border-left ml-2 pl-2">
                                    {{ $seller_package->duration }}
                                    <br>
                                    {{__('Days')}}
                                </span>
                            </div>
                            <div class="text-center">
                                @if ($seller_package->amount == 0)
                                    <button class="btn btn-primary fw-600" onclick="get_free_package({{ $seller_package->id}})">{{__('Free Package')}}</button>
                                @else
                                    <button class="btn btn-primary fw-600" onclick="show_price_modal({{ $seller_package->id}})">{{__('Purchase Package')}}</button>
                                @endif 
                            </div>

                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section> 

<div class="modal fade" id="price_modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
        <div class="modal-content position-relative">
            <div class="modal-header">
                <h5 class="modal-title strong-600 heading-5">{{__('Purchase Your Package')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="" id="package_payment_form" action="{{ route('seller_packages.purchase') }}" method="post">
                @csrf
                <input type="hidden" name="seller_package_id" value="">
                <div class="modal-body gry-bg px-3 pt-3">
                    <div class="row">
                        <div class="col-md-2">
                            <label>{{__('Payment Method')}}</label>
                        </div>
                        <div class="col-md-10">
                            <div class="mb-3">
                                <select class="form-control selectpicker" data-minimum-results-for-search="Infinity" name="payment_option">
                                    @if (\App\BusinessSetting::where('type', 'card_payment')->first()->value == 1)
                                            <option value="bank_payment">{{ translate('Bank Payment')}}</option>
                                    @endif
                                    @if (\App\BusinessSetting::where('type', 'mobile_payment')->first()->value == 1)
                                            <option value="mobile_payment">{{ translate('Mobile Payment')}}</option>
                                    @endif 
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('cancel')}}</button>
                    <button type="submit" class="btn btn-base-1">{{__('Confirm')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('script')
    <script type="text/javascript">
        function show_price_modal(id){
            $('input[name=seller_package_id]').val(id);
            $('#price_modal').modal('show');
        }

        function get_free_package(id){
            $('input[name=seller_package_id]').val(id);
            $('#package_payment_form').submit();
        }
    </script>
@endsection
