@extends('backend.layouts.app')

@section('content')



<div class="px-15px px-lg-25px">
    <div class="aiz-titlebar mt-2 mb-3">
        <div class="row align-items-center">
            <div class="col-md-6">
                <h1 class="h3">All Seller Packages</h1>
            </div>
            <div class="col-md-6 text-md-right">
                <a href="{{ route('seller_packages.create')}}" class="btn btn-circle btn-info">
                    <span>Add New Package</span>
                </a>
            </div>
        </div>
    </div>


    <div class="row">
        @foreach ($seller_packages as $key => $seller_package)
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="card">
                        <div class="card-body text-center">
                            <img alt="Package Logo" src="{{ uploaded_asset($seller_package->logo) }}" class="mw-100 mx-auto mb-4" height="150px">
                            <p class="mb-3 h6 fw-600">{{$seller_package->name}}</p>
                            <p class="h4">{{single_price($seller_package->amount)}}</p>
                            <p class="fs-15">Product Upload:
                                <b class="text-bold">{{$seller_package->product_upload}}</b>
                            </p>
                            <p class="fs-15">Digital Product Upload:
                                <b class="text-bold">{{$seller_package->digital_product_upload}}</b>
                            </p>
                            <p class="fs-15">Package Duration:
                                <b class="text-bold">{{$seller_package->duration}} Days</b>
                            </p>
                            <div class="mar-top">
                                <a href="{{route('seller_packages.edit', encrypt($seller_package->id))}}" class="btn btn-sm btn-info">Edit</a>
                                <a href="#" data-href="{{route('seller_packages.destroy', $seller_package->id)}}" class="btn btn-sm btn-danger confirm-delete">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>            
        @endforeach    
    </div>
</div>
 
@endsection
