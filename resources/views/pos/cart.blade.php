
<div class="aiz-pos-cart-list mb-4 mt-3 c-scrollbar-light">
    <ul class="list-group list-group-flush">
        @php
            $subtotal = 0;
            $tax = 0;
            $shipping = 0;
        @endphp
        @if (Session::has('posCart'))
            @forelse (Session::get('posCart') as $key => $cartItem)
                @php
                $product = \App\Product::find($cartItem['id']);
                $product_name_with_choice = $product->name;
                $subtotal += $cartItem['price']*$cartItem['quantity'];
                $tax += $cartItem['tax']*$cartItem['quantity'];
                $shipping += $cartItem['shipping']*$cartItem['quantity'];
                if(Session::get('shipping', 0) == 0){
                    $shipping = 0;
                }
                if ($cartItem['variant'] != null) {
                    $product_name_with_choice = $product->name.' - '.$cartItem['variant'];
                }
                if($cartItem['variant'] != null && $product->variant_product){
                    $product_stock = $product->stocks->where('variant', $cartItem['variant'])->first();
                    $quantity = $product_stock->qty;
                }
                else{
                    $quantity = $product->current_stock;
                }
                @endphp
            <li class="list-group-item py-0 pl-2">
                <div class="row gutters-5 align-items-center">
                    <div class="col-auto w-60px">
                        <div class="row no-gutters align-items-center flex-column aiz-plus-minus">
                            <button class="btn col-auto btn-icon btn-sm fs-15" type="button" data-type="plus" data-field="qty-{{ $key }}">
                                <i class="las la-plus"></i>
                            </button>
                            <input type="text" name="qty-{{ $key }}" id="qty-{{ $key }}" class="col border-0 text-center flex-grow-1 fs-16 input-number" placeholder="1" value="{{ $cartItem['quantity'] }}" min="1" max="{{ $quantity }}" onchange="updateQuantity({{ $key }})" />
                            <button class="btn col-auto btn-icon btn-sm fs-15" type="button" data-type="minus" data-field="qty-{{ $key }}">
                                <i class="las la-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="col">
                        <div class="text-truncate-2">{{ $product->name }} ({{ $cartItem['variant'] }})</div>
                        <span class="span badge badge-inline fs-12 badge-soft-secondary"></span>
                    </div>
                    <div class="col-auto">
                        <div class="fs-12 opacity-60">{{ single_price($cartItem['price']) }} x {{ $cartItem['quantity'] }}</div>
                        <div class="fs-15 fw-600">{{  single_price($cartItem['price']*$cartItem['quantity'])}}</div>
                    </div>
                    <div class="col-auto">
                        <button type="button" class="btn btn-circle btn-icon btn-sm btn-soft-danger ml-2 mr-0" onclick="removeFromCart({{ $key }})">
                            <i class="las la-trash-alt"></i>
                        </button>
                    </div>
                </div>
            </li>
            @empty
                <div class="text-center">
                    <i class="las la-frown la-3x opacity-50"></i>
                    <p>{{ translate('No Product Added') }}</p>
                </div>
            @endforelse
            @endif
    </ul>
</div>
<div>
    <div class="d-flex justify-content-between fw-600 mb-2 opacity-70">
        <span>{{ translate('Sub Total')}}</span>
        <span>{{ single_price($subtotal) }}</span>
    </div>
    <div class="d-flex justify-content-between fw-600 mb-2 opacity-70">
        <span>{{ translate('Tax')}}</span>
        <span>{{ single_price($tax) }}</span>
    </div>
    <div class="d-flex justify-content-between fw-600 mb-2 opacity-70">
        <span>{{ translate('Shipping')}}</span>
        <span>{{ single_price($shipping) }}</span>
    </div>
    <div class="d-flex justify-content-between fw-600 mb-2 opacity-70">
        <span>{{ translate('Discount')}}</span>
        <span>{{ single_price(Session::get('pos_discount', 0)) }}</span>
    </div>
    <div class="d-flex justify-content-between fw-600 fs-18 border-top pt-2">
        <span>{{ translate('TOTAL')}}</span>
        <span>{{ single_price($subtotal+$tax+$shipping - Session::get('pos_discount', 0)) }}</span>
    </div>
</div>

<script type="text/javascript">
AIZ.extra.plusMinus();
</script>
