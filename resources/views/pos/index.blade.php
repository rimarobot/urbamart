@extends('backend.layouts.app')

@section('content')

<section class="gry-bg py-4 profile">
    <div class="container-fluid">
        <form class="" action="" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row gutters-10">
                <div class="col-md">
                        <div class="row gutters-5 mb-3">

                            <div class="col-md-6 mb-2 mb-md-0">
                                <div class="form-group mb-0">
                                    <input class="form-control form-control-sm" type="text" name="keyword" placeholder="Search by Product Name/Barcode" onkeyup="filterProducts()">
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div class="">
                                    <div class="form-group">
                                        <select name="poscategory" class="form-control form-control-sm aiz-selectpicker" data-live-search="true" onchange="filterProducts()">
                                            <option value="">{{ translate('All Categories') }}</option>
                                            @foreach ( json_decode(\App\Category::all()) as $key => $category)
                                                <option value="category-{{ $category->id }}">{{ $category->name }}</option>
                                                @foreach (\App\Utility\CategoryUtility::get_immediate_children($category->id) as $key => $subcategory)
                                                    <option value="subcategory-{{ $subcategory->id }}">- {{ $subcategory->name }}</option>
                                                    @foreach (\App\Utility\CategoryUtility::get_immediate_children($subcategory->id) as $key => $subsubcategory)
                                                        <option value="subsubcategory-{{ $subsubcategory->id }}">- - {{ $subsubcategory->name }}</option>
                                                    @endforeach
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div class="">
                                    <div class="form-group">
                                        <select name="brand" class="form-control form-control-sm aiz-selectpicker" data-live-search="true" onchange="filterProducts()">
                                            <option value="">{{ translate('All Brands') }}</option>
                                            @foreach (\App\Brand::all() as $key => $brand)
                                                <option value="{{ $brand->id }}">{{ $brand->getTranslation('name') }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="aiz-pos-product-list c-scrollbar-light">
                            <div class="d-flex flex-wrap justify-content-center" id="product-list">

                            </div>
                            <div id="load-more">
                                <p class="text-center fs-14 fw-600 p-2 bg-soft-primary c-pointer" onclick="loadMoreProduct()">{{ translate('Load More') }}</p>
                            </div>
                        </div>
                </div>
                <div class="col-md-auto w-md-350px w-lg-400px w-xl-500px">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="d-flex border-bottom pb-3">
                                <div class="flex-grow-1">
                                    <select name="user_id" class="form-control form-control-sm pos-customer aiz-selectpicker" data-live-search="true" onchange="getShippingAddress()">
                                        <option value="">{{translate('Walk In Customer')}}</option>
                                        @foreach (\App\Customer::all() as $key => $customer)
                                            @if ($customer->user)
                                                <option value="{{ $customer->user->id }}" data-contact="{{ $customer->user->email }}">{{ $customer->user->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <button type="button" class="btn btn-icon btn-soft-dark ml-3" data-target="#new-customer" data-toggle="modal">
									<i class="las la-truck"></i>
								</button>
                            </div>
                            <div class="" id="cart-details">
                                <div class="aiz-pos-cart-list mb-4 mt-3 c-scrollbar-light">
                                    <ul class="list-group list-group-flush">
                                        @php
                                            $subtotal = 0;
                                            $tax = 0;
                                            $shipping = 0;
                                        @endphp
                                        @if (Session::has('posCart'))
                                            @forelse (Session::get('posCart') as $key => $cartItem)
                                            @php
                                            $product = \App\Product::find($cartItem['id']);
                                            $product_name_with_choice = $product->name;
                                            $subtotal += $cartItem['price']*$cartItem['quantity'];
                                            $tax += $cartItem['tax']*$cartItem['quantity'];
                                            $shipping += $cartItem['shipping']*$cartItem['quantity'];
                                                if(Session::get('shipping', 0) == 0){
                                                    $shipping = 0;
                                                }
                                                if ($cartItem['variant'] != null) {
                                                    $product_name_with_choice = $product->name.' - '.$cartItem['variant'];
                                                }
                                                if($cartItem['variant'] != null && $product->variant_product){
                                                    $product_stock = $product->stocks->where('variant', $cartItem['variant'])->first();
                                                    $quantity = $product_stock->qty;
                                                }
                                                else{
                                                    $quantity = $product->current_stock;
                                                }
                                            @endphp
                                            <li class="list-group-item py-0 pl-2">
                                                <div class="row gutters-5 align-items-center">
                                                    <div class="col-auto w-60px">
                                                        <div class="row no-gutters align-items-center flex-column aiz-plus-minus">
                                                            <button class="btn col-auto btn-icon btn-sm fs-15" type="button" data-type="plus" data-field="qty-{{ $key }}">
                                                                <i class="las la-plus"></i>
                                                            </button>
                                                            <input type="text" name="qty-{{ $key }}" id="qty-{{ $key }}" class="col border-0 text-center flex-grow-1 fs-16 input-number" placeholder="1" value="{{ $cartItem['quantity'] }}" min="1" max="{{ $quantity }}" onchange="updateQuantity({{ $key }})" />
                                                            <button class="btn col-auto btn-icon btn-sm fs-15" type="button" data-type="minus" data-field="qty-{{ $key }}">
                                                                <i class="las la-minus"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="text-truncate-2">{{ $product->name }} ({{ $cartItem['variant'] }})</div>
                                                        <span class="span badge badge-inline fs-12 badge-soft-secondary"></span>
                                                    </div>
                                                    <div class="col-auto">
                                                        <div class="fs-12 opacity-60">{{ single_price($cartItem['price']) }} x {{ $cartItem['quantity'] }}</div>
                                                        <div class="fs-15 fw-600">{{  single_price($cartItem['price']*$cartItem['quantity'])}}</div>
                                                    </div>
                                                    <div class="col-auto">
                                                        <button type="button" class="btn btn-circle btn-icon btn-sm btn-soft-danger ml-2 mr-0" onclick="removeFromCart({{ $key }})">
                                                            <i class="las la-trash-alt"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </li>
                                            @empty

                                            <div class="text-center">
                                                <i class="las la-frown la-3x opacity-50"></i>
                                                <p>{{ translate('No Product Added') }}</p>
                                            </div>
                                            @endforelse
                                        @endif
                                    </ul>
                                </div>
                                <div>
                                    <div class="d-flex justify-content-between fw-600 mb-2 opacity-70">
                                        <span>{{ translate('Sub Total')}}</span>
                                        <span>{{ single_price($subtotal) }}</span>
                                    </div>
                                    <div class="d-flex justify-content-between fw-600 mb-2 opacity-70">
                                        <span>{{ translate('Tax')}}</span>
                                        <span>{{ single_price($tax) }}</span>
                                    </div>
                                    <div class="d-flex justify-content-between fw-600 mb-2 opacity-70">
                                        <span>{{ translate('Shipping')}}</span>
                                        <span>{{ single_price($shipping) }}</span>
                                    </div>
                                    <div class="d-flex justify-content-between fw-600 mb-2 opacity-70">
                                        <span>{{ translate('Discount')}}</span>
                                        <span>{{ single_price(Session::get('pos_discount', 0)) }}</span>
                                    </div>
                                    <div class="d-flex justify-content-between fw-600 fs-18 border-top pt-2">
                                        <span>{{ translate('TOTAL')}}</span>
                                        <span>{{ single_price($subtotal+$tax+$shipping - Session::get('pos_discount', 0)) }}</span>
                                    </div>
                                </div>
                        </div>
                        </div>
                    </div>
                    <div class="pos-footer mar-btm">
                        <div class="d-flex flex-column flex-md-row justify-content-between">
                            <div class="d-flex">
                                <div class="dropdown mr-3 ml-0 dropup">
                                    <button class="btn btn-outline-dark btn-styled dropdown-toggle" type="button" data-toggle="dropdown">
                                        {{translate('Shipping')}}
                                    </button>
                                    <div class="dropdown-menu p-3 dropdown-menu-lg">
                                        <div class="radio radio-inline">
                                            <input type="radio" name="shipping" id="radioExample_2a" value="0" checked onchange="setShipping()">
                                            <label for="radioExample_2a">{{translate('Without Shipping Charge')}}</label>
                                        </div>

                                        <div class="radio radio-inline">
                                            <input type="radio" name="shipping" id="radioExample_2b" value="1" onchange="setShipping()">
                                            <label for="radioExample_2b">{{translate('With Shipping Charge')}}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="dropdown dropup">
                                    <button class="btn btn-outline-dark btn-styled dropdown-toggle" type="button" data-toggle="dropdown">
                                        {{translate('Discount')}}
                                    </button>
                                    <div class="dropdown-menu p-3 dropdown-menu-lg">
                                        <div class="input-group">
                                            <input type="number" min="0" placeholder="Amount" name="discount" class="form-control" value="0" required="" onchange="setDiscount()">
                                            <div class="input-group-append">
                                                <span class="input-group-text">Flat</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="my-2 my-md-0">
                                <button type="button" class="btn btn-primary btn-block" onclick="orderConfirmation()">Place Order</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>
</section>

@endsection

@section('modal')
    <!-- Address Modal -->
    <div id="new-customer" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom" role="document">
            <div class="modal-content">
                <div class="modal-header bord-btm">
                    <h4 class="modal-title h6">{{translate('Shipping Address')}}</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                </div>
                <form id="shipping_form">
                    <div class="modal-body" id="shipping_address">


                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-styled btn-base-3" data-dismiss="modal" id="close-button">{{translate('Close')}}</button>
                    <button type="button" class="btn btn-primary btn-styled btn-base-1" data-dismiss="modal" id="confirm-address">{{translate('Confirm')}}</button>
                </div>
            </div>
        </div>
    </div>

    <!-- new address modal -->
    <div id="new-address-modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom" role="document">
            <div class="modal-content">
                <div class="modal-header bord-btm">
                    <h4 class="modal-title h6">{{translate('Shipping Address')}}</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                </div>
                <form class="form-horizontal" action="{{ route('addresses.store') }}" method="POST" enctype="multipart/form-data">
                	@csrf
                    <div class="modal-body">
                        <input type="hidden" name="customer_id" id="set_customer_id" value="">
                        <div class="form-group">
                            <div class=" row">
                                <label class="col-sm-2 control-label" for="address">{{translate('Address')}}</label>
                                <div class="col-sm-10">
                                    <textarea placeholder="{{translate('Address')}}" id="address" name="address" class="form-control" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class=" row">
                                <label class="col-sm-2 control-label" for="email">{{translate('Country')}}</label>
                                <div class="col-sm-10">
                                    <select name="country" id="country" class="form-control aiz-selectpicker" required data-placeholder="{{translate('Select country')}}">
                                        @foreach (\App\Country::where('status',1)->get() as $key => $country)
                                            <option value="{{ $country->name }}">{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class=" row">
                                <label class="col-sm-2 control-label" for="city">{{translate('City')}}</label>
                                <div class="col-sm-10">
                                    <input type="text" placeholder="{{translate('City')}}" id="city" name="city" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class=" row">
                                <label class="col-sm-2 control-label" for="postal_code">{{translate('Postal code')}}</label>
                                <div class="col-sm-10">
                                    <input type="number" min="0" placeholder="{{translate('Postal code')}}" id="postal_code" name="postal_code" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class=" row">
                                <label class="col-sm-2 control-label" for="phone">{{translate('Phone')}}</label>
                                <div class="col-sm-10">
                                    <input type="number" min="0" placeholder="{{translate('Phone')}}" id="phone" name="phone" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-styled btn-base-3" data-dismiss="modal">{{translate('Close')}}</button>
                        <button type="submit" class="btn btn-primary btn-styled btn-base-1">{{translate('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="product-variation" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom modal-lg">
            <div class="modal-content" id="variants">

            </div>
        </div>
    </div>

    <div id="order-confirm" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom modal-xl">
            <div class="modal-content" id="variants">
                <div class="modal-header bord-btm">
                    <h4 class="modal-title h6">Order Summary</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body" id="order-confirmation">
                    <div class="p-4 text-center">
                        <i class="las la-spinner la-spin la-3x"></i>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-styled btn-base-3" data-dismiss="modal">Close</button>
                    <button type="button" onclick="submitOrder('cash')" class="btn btn-styled btn-base-1 btn-primary">Comfirm Order</button>
                </div>
            </div>
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection


@section('script')
    <script type="text/javascript">

        var products = null;

        $(document).ready(function(){
            $('#container').removeClass('mainnav-lg').addClass('mainnav-sm');
            $('#product-list').on('click','.product-card',function(){
                var id = $(this).data('id');
                $.get('{{ route('variants') }}', {id:id}, function(data){
                    if (data == 0) {
                        addToCart(id, null, 1);
                    }
                    else {
                        $('#variants').html(data);
                        $('#product-variation').modal('show');
                    }
                });
            });
            filterProducts();
            getShippingAddress();
        });

        $("#confirm-address").click(function (){
            var data = new FormData($('#shipping_form')[0]);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': AIZ.data.csrf },
                method: "POST",
                url: '{{ route('set.shipping.address') }}',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data, textStatus, jqXHR) {
                }
            })
        });

        function filterProducts(){
            var keyword = $('input[name=keyword]').val();
            var category = $('select[name=poscategory]').val();
            var brand = $('select[name=brand]').val();
            $.get('{{ route('pos.search_product') }}',{keyword:keyword, category:category, brand:brand}, function(data){
                products = data;
                $('#product-list').html(null);
                setProductList(data);
            });
        }

        function loadMoreProduct(){
            if(products != null && products.links.next != null){
                $.get(products.links.next,{}, function(data){
                    products = data;
                    setProductList(data);
                });
            }
        }

        function setProductList(data){
            for (var i = 0; i < data.data.length; i++) {
                $('#product-list').append('<div class="w-140px w-xl-180px w-xxl-210px mx-2">' +
                    '<div class="card bg-white c-pointer product-card hov-container" data-id="'+data.data[i].id+'" >'+
                        '<span class="absolute-top-left badge badge-inline badge-success fs-13 m-2">'+data.data[i].price +'</span>'+
                        '<img src="'+ data.data[i].thumbnail_image +'" class="card-img-top img-fit h-120px h-xl-180px h-xxl-210px mw-100 mx-auto">'+
                        '<div class="card-body p-2">'+
                            '<div class="text-truncate-2 small">'+ data.data[i].name +'</div>'+
                        '</div>'+
                    '</div>'+
                '</div>');
            }
            if (data.links.next != null) {
                $('#load-more').find('.text-center').html('Load More');
            }
            else {
                $('#load-more').find('.text-center').html('Nothing more found');
            }
            $('[data-toggle="tooltip"]').tooltip();
        }

        function removeFromCart(key){
            $.post('{{ route('pos.removeFromCart') }}', {_token:AIZ.data.csrf, key:key}, function(data){
                $('#cart-details').html(data);
                $('#product-variation').modal('hide');
            });
        }

        function addToCart(product_id, variant, quantity){
            $.post('{{ route('pos.addToCart') }}',{_token:AIZ.data.csrf, product_id:product_id, variant:variant, quantity, quantity}, function(data){
                $('#cart-details').html(data);
                $('#product-variation').modal('hide');
            });
        }

        function addVariantProductToCart(id){
            var variant = $('input[name=variant]:checked').val();
            addToCart(id, variant, 1);
        }

        function updateQuantity(key){
            $.post('{{ route('pos.updateQuantity') }}',{_token:AIZ.data.csrf, key:key, quantity: $('#qty-'+key).val()}, function(data){
                $('#cart-details').html(data);
                $('#product-variation').modal('hide');
            });
        }

        function setDiscount(){
            var discount = $('input[name=discount]').val();
            $.post('{{ route('pos.setDiscount') }}',{_token:AIZ.data.csrf, discount:discount}, function(data){
                $('#cart-details').html(data);
                $('#product-variation').modal('hide');
            });
        }

        function setShipping(){
            var shipping = $('input[name=shipping]:checked').val();
            $.post('{{ route('pos.setShipping') }}',{_token:AIZ.data.csrf, shipping:shipping}, function(data){
                $('#cart-details').html(data);
                $('#product-variation').modal('hide');
            });
        }

        function getShippingAddress(){

            $.post('{{ route('pos.getShippingAddress') }}',{_token: AIZ.data.csrf, id:$('select[name=user_id]').val()}, function(data){
                $('#shipping_address').html(data);
            });
        }

        function add_new_address(){
             var customer_id = $('#customer_id').val();
            $('#set_customer_id').val(customer_id);
            $('#new-address-modal').modal('show');
            $("#close-button").click();
        }

        function orderConfirmation(){
            $('#order-confirmation').html(`<div class="p-4 text-center"><i class="las la-spinner la-spin la-3x"></i></div>`);
            $('#order-confirm').modal('show');
            $.post('{{ route('pos.order_summary') }}',{_token:AIZ.data.csrf}, function(data){
                $('#order-confirmation').html(data);
            });
        }

        function submitOrder(payment_type){
            var user_id = $('select[name=user_id]').val();
            var name = $('input[name=name]').val();
            var email = $('input[name=email]').val();
            var address = $('textarea[name=address]').val();
            var country = $('select[name=country]').val();
            var city = $('input[name=city]').val();
            var postal_code = $('input[name=postal_code]').val();
            var phone = $('input[name=phone]').val();
            var shipping = $('input[name=shipping]:checked').val();
            var discount = $('input[name=discount]').val();
            var address = $('input[name=address_id]:checked').val();

            $.post('{{ route('pos.order_place') }}',
            {
                _token:AIZ.data.csrf,
                user_id:user_id,
                name:name,
                email:email,
                address:address,
                country:country,
                city:city,
                phone:phone,
                shipping_address:address,
                payment_type:payment_type,
                shipping:shipping,
                discount:discount
            }, function(data){
                if(data == 1){
                    AIZ.plugins.notify('success', '{{ translate('Order Completed Successfully.') }}');
                    location.reload();
                }
                else{
                    AIZ.plugins.notify('danger', '{{ translate('Something went wrong') }}');
                }
            });
        }
    </script>
@endsection
