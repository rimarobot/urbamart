<div class="row">
    <div class="col-xl-6">
        <ul class="list-group list-group-flush">

            @php
                $subtotal = 0;
                $tax = 0;
                $shipping = 0;
            @endphp
            @if (Session::has('posCart'))
                @forelse (Session::get('posCart') as $key => $cartItem)
                    @php
                    $product = \App\Product::find($cartItem['id']);
                    $product_name_with_choice = $product->name;
                    $subtotal += $cartItem['price']*$cartItem['quantity'];
                    $tax += $cartItem['tax']*$cartItem['quantity'];
                    $shipping += $cartItem['shipping']*$cartItem['quantity'];
                    if(Session::get('shipping', 0) == 0){
                        $shipping = 0;
                    }
                    @endphp
                    <li class="list-group-item px-0">
                        <div class="row gutters-10 align-items-center">
                            <div class="col">
                                <div class="d-flex">
                                    <img src="{{ uploaded_asset($product->thumbnail_img) }}" class="img-fit size-60px" />
                                    <span class="flex-grow-1 ml-3 mr-0">
                                        <div class="text-truncate-2"> {{$product->name}}</div>
                                        <span class="span badge badge-inline fs-12 badge-soft-secondary">{{ $cartItem['variant'] }}</span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-xl-3">
                                <div class="fs-14 fw-600 text-right">{{ single_price($cartItem['price']) }}</div>
                                <div class="fs-14 text-right">QTY: {{  $cartItem['quantity'] }}</div>
                            </div>
                        </div>
                    </li>
                @empty
                    <div class="text-center">
                        <i class="las la-frown la-3x opacity-50"></i>
                        <p>{{ translate('No Product Added') }}</p>
                    </div>
                @endforelse
                @endif
        </ul>
    </div>
    <div class="col-xl-6">
        <div class="pl-xl-4">
            <div class="card mb-4">
                <div class="card-header"><span class="fs-16">Customer Info</span></div>
                <div class="card-body">
                    @if (Session::has('pos_shipping_info'))
                            @php
                                $shipping_address  = \App\Address::findOrFail(Session::get('pos_shipping_info'))
                            @endphp
                            <div class="d-flex justify-content-between mb-2">
                                <span class="">Name:</span>
                                <span class="fw-600">{{$shipping_address->name}}</span>
                            </div>
                            <div class="d-flex justify-content-between mb-2">
                                <span class="">Email:</span>
                                <span class="fw-600">{{$shipping_address->email}}</span>
                            </div>
                            <div class="d-flex justify-content-between mb-2">
                                <span class="">Phone:</span>
                                <span class="fw-600">{{ $shipping_address->phone }}</span>
                            </div>
                            <div class="d-flex justify-content-between mb-2">
                                <span class="">Address:</span>
                                <span class="fw-600">{{ $shipping_address->address }}</span>
                            </div>
                            <div class="d-flex justify-content-between mb-2">
                                <span class="">Country:</span>
                                <span class="fw-600">{{ $shipping_address->country }}</span>
                            </div>
                            <div class="d-flex justify-content-between mb-2">
                                <span class="">City:</span>
                                <span class="fw-600">{{ $shipping_address->city }}</span>
                            </div>
                    @else
                            <div class="text-center p-4">
                                {{translate('No customer information selected.')}}
                            </div>
                    @endif
                </div>
            </div>

            <div class="d-flex justify-content-between fw-600 mb-2 opacity-70">
                <span>TOTAL</span>
                <span>{{ single_price($subtotal) }}</span>
            </div>
            <div class="d-flex justify-content-between fw-600 mb-2 opacity-70">
                <span>Tax</span>
                <span>{{ single_price($tax) }}</span>
            </div>
            <div class="d-flex justify-content-between fw-600 mb-2 opacity-70">
                <span>Shipping</span>
                <span>{{ single_price($shipping) }}</span>
            </div>
            <div class="d-flex justify-content-between fw-600 mb-2 opacity-70">
                <span>Discount</span>
                <span>{{ single_price(Session::get('pos_discount', 0)) }}</span>
            </div>
            <div class="d-flex justify-content-between fw-600 fs-18 border-top pt-2">
                <span>TOTAL</span>
                <span>{{ single_price($subtotal+$tax+$shipping - Session::get('pos_discount', 0)) }}</span>
            </div>
        </div>
    </div>
</div>
