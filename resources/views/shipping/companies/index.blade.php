@extends('backend.layouts.app')

@section('content')


<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="row align-items-center">
		<div class="col-md-6">
			<h1 class="h3">{{translate('Shipping Company')}}</h1>
		</div>
		<div class="col-md-6 text-md-right">
			<a href="{{ route('companies.create')}}" class="btn btn-circle btn-primary">
				<span>{{translate('Add Company')}}</span>
			</a>
		</div>
	</div>
</div>

<div class="card">
    <div class="card-header row gutters-5">
        <div class="col text-center text-md-left">
            <h5 class="mb-md-0 h6">{{ translate('Company') }}</h5>
        </div>
        <div class="col-md-4">
            <form class="" id="sort_currencies" action="" method="GET">
                <div class="input-group input-group-sm">
                    <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="{{ translate('Type name & Enter') }}">
                </div>
            </form>
        </div>
    </div>
    <div class="card-body">
        <table class="table aiz-table mb-0">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{ translate('Name')}}</th>
                    <th>{{ translate('Country')}}</th>
                    <th data-breakpoints="md">{{ translate('Address')}}</th>
                    <th data-breakpoints="md">{{ translate('Phone')}}</th>
                    <th data-breakpoints="md">{{ translate('Status')}}</th>
                    <th class="text-right">{{translate('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($companies as $key => $company)
                    <tr>
                        <td>{{ ($key+1) + ($companies->currentPage() - 1)*$companies->perPage() }}</td>
                        <td>{{$company->company_name}}</td>
                        <td>{{\App\Country::findOrFail($company->country_id)->name}}</td>
                        <td>{{$company->company_address}}</td>
                        <td>{{$company->company_phone}}</td> 
                        <td>
                            <label class="aiz-switch aiz-switch-success mb-0">
                                <input onchange="update_company_status(this)" value="{{ $company->id }}" type="checkbox" <?php if($company->status == 1) echo "checked";?> >
                                <span class="slider round"></span>
                            </label>
                        </td>                        
                        <td class="text-right">
                            <a href="{{route('companies.edit', encrypt($company->id))}}" class="btn btn-soft-primary btn-icon btn-circle btn-sm" title="{{ translate('Edit') }}">
                                   <i class="las la-edit"></i>
                            </a>
                            <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" data-href="{{route('companies.destroy', $company->id)}}" title="{{ translate('Delete') }}">
                                <i class="las la-trash"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="aiz-pagination">
            {{ $companies->appends(request()->input())->links() }}
        </div>
    </div>
</div> 
@endsection

@section('modal')
    @include('modals.delete_modal')
@endsection 

@section('script')
    <script type="text/javascript"> 

        function update_company_status(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }

            $.post('{{ route('companies.update_status') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    AIZ.plugins.notify('success', '{{ translate('Campany Status updated successfully') }}');
                }
                else{
                    AIZ.plugins.notify('danger', '{{ translate('Something went wrong') }}');
                }
            });
        } 
    </script>
@endsection