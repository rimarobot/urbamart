@extends('backend.layouts.app')

@section('content')
<div class="px-15px px-lg-25px">
        <div class="col-lg-10 mx-auto">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0 h6">Shipping Company Information</h5>
                </div>
                <form class="form-horizontal" action="{{ route('companies.update', $company->id) }}" method="POST" enctype="multipart/form-data">
                    <input name="_method" type="hidden" value="PATCH">
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-2 col-from-label" for="name">{{__('Company Name')}}</label>
                            <div class="col-sm-10">
                                <input type="text" placeholder="{{__('Company Name')}}" value="{{$company->company_name}}" id="name" name="name" class="form-control" required="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-from-label" for="country">{{__('Country')}}</label>
                            <div class="col-sm-10">
                                <select class="form-control aiz-selectpicker" data-live-search="true" name="country"  id="country"   required>
                                   <option value="">Select your country</option>
                                    @foreach (\App\Country::where('status', 1)->get() as $key => $country)
                                        <option value="{{ $country->id }}"  @if($country->id == $company->country_id) selected @endif>{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-from-label" for="address">{{__('Address')}}</label>
                            <div class="col-sm-10">
                                <input type="text"   placeholder="{{__('Address')}}" value="{{$company->company_address}}"  id="address" name="address" class="form-control" required="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-from-label" for="email">{{__('Email')}}</label>
                            <div class="col-sm-10">
                                <input type="text"  placeholder="{{__('Email')}}" value="{{$company->company_email}}" id="email" name="email" class="form-control" required="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-from-label" for="phone">{{__('Phone')}}</label>
                            <div class="col-sm-10">
                                <input type="text"   placeholder="{{__('Phone')}}" value="{{$company->company_phone}}" id="phone" name="phone" class="form-control" required="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-from-label" for="url">{{__('Status url')}}</label>
                            <div class="col-sm-10">
                                <input type="text"  placeholder="{{__('Status url')}}" value="{{$company->company_url}}" id="url" name="url" class="form-control" required="" />
                            </div>
                        </div>
                         
                        <div class="form-group mb-0 text-right">
                            <button type="submit" class="btn btn-sm btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div> 
 
@endsection
 
