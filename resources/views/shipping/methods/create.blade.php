<form id="new_shipping_method_form" role="form" action="{{ route('shipping_methods.store') }}" method="POST">
    @csrf
    <input type="hidden" id="zone_id" name="zone_id" value="{{$zone_id}}" />
    <div class="modal-header">
        <h5 class="modal-title h6">{{translate('Add New Shipping Method')}}</h5>
        <button type="button" class="close" data-dismiss="modal"></button>
    </div>
    <div class="modal-body">
        <div class="row gutters-5 row-cols-xl-2 row-cols-1">
            <div class="col">
                <div class="form-group row">
                    <label class="col-sm-4 control-label" for="method_id">{{ translate('Method')}}</label>
                    <div class="col-sm-8">
                        <select class="form-control aiz-selectpicker" data-live-search="true" name="method_id" id="method_id" required>
                            @foreach (\App\ShippingMethod::all() as $key => $method)
                            <option value="{{ $method->slug }}"
                                @if($method->slug == 'local-pickup') 
                                selected 
                                @endif>
                                {{ $method->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="form-group row">
                    <label class="col-sm-4 control-label" for="company_id">{{ translate('Shipping Company')}}</label>
                    <div class="col-sm-8">
                        <select class="form-control aiz-selectpicker" data-live-search="true" name="company_id" id="company_id" required>
                            @foreach (\App\ShippingCompany::where('country_id', \App\Country::where('code', \App\ShippingZone::findOrFail($zone_id)->location_code)->first()->id )->get() as $key => $company)
                            <option value="{{ $company->id }}">{{ $company->company_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="form-group row">
                    <label class="col-sm-4 control-label" for="name">{{ translate('Shipping Name')}}</label>
                    <div class="col-sm-8">
                        <input type="text" placeholder="{{ translate('Shipping Name')}}" id="name" name="name" class="form-control" required />
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="form-group row">
                    <label class="col-sm-4 control-label" for="description">{{ translate('Description')}}</label>
                    <div class="col-sm-8">
                        <input type="text" placeholder="{{ translate('Description')}}" id="description" name="description" class="form-control" required />
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="free-shipping form-group row" style="display: none;">
                    <label class="col-sm-4 control-label" for="url">{{ translate('Free Shipping')}}</label>
                    <div class="col-sm-8">
                        <select class="form-control  aiz-selectpicker" data-live-search="true" name="free_shipping_requires" id="free_shipping_requires" required>
                            <option value="no">N/A</option>
                            <option value="fee">A minimum order amount</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="free-shipping form-group row" style="display: none;">
                    <label class="col-sm-4 control-label" for="min_order_amount">{{ translate('Min Order Amount')}}</label>
                    <div class="col-sm-8">
                        <input type="text" placeholder="{{ translate('Min Order Amount')}}" id="min_order_amount" name="min_order_amount" class="form-control" required />
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="form-group row">
                    <label class="col-sm-4 control-label" for="delivery_time">{{ translate('Delivery Time (Days)')}}</label>
                    <div class="col-sm-4">
                        <input type="number" placeholder="{{ translate('Min')}}" min="1" id="delivery_time_min" name="delivery_time_min" class="form-control" value="" required />
                    </div>
                    <div class="col-sm-4">
                        <input type="number" placeholder="{{ translate('Max')}}" max="40" id="delivery_time_max" name="delivery_time_max" class="form-control" value="" required />
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="form-group row">
                    <label class="col-sm-4 control-label" for="cost">{{ translate('Cost')}}</label>
                    <div class="col-sm-8">
                        <input type="text" placeholder="{{ translate('Cost')}}" id="cost" name="cost" class="form-control" required />
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="form-group row">
                    <label class="col-sm-4 control-label" for="order">{{ translate('Priority')}}</label>
                    <div class="col-sm-8">
                        <input type="text" placeholder="{{ translate('Priority')}}" id="order" name="order" class="form-control" required />
                    </div>
                </div>
            </div>
            @foreach (\App\ShippingClass::all() as $key => $shipping_class )
            <div class="col">
                <div class="flat-rate form-group row" style="display: none;">
                    <label class="col-sm-4 control-label" for="{{$shipping_class->slug}}"><strong>{{ translate( $shipping_class->name) }}</strong> shipping cost</label>
                    <div class="col-sm-8">
                        <input type="text" placeholder="{{ translate($shipping_class->name)}}" value="N/A" id="{{$shipping_class->slug}}" name="{{$shipping_class->slug}}" class="form-control" required />
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="modal-footer">
        <button id="new_shipping_method_btn" class="btn btn-sm btn-primary" onclick="createMethod();">{{ translate('Save')}}</button>
        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">{{translate('Cancel')}}</button>
    </div>
</form>
