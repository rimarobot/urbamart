@extends('backend.layouts.app')

@section('content')

<div class="col-lg-12 mx-auto">
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0 h6">{{translate('Driver Information')}}</h5>
        </div>

        <form action="{{ route('drivers.update', $driver->id) }}" method="POST">
            <input name="_method" type="hidden" value="PATCH">
        	@csrf
            <div class="card-body">
                <div class="row gutters-5 row-cols-xl-2 row-cols-1">
                    <div class="col">
                        <div class="form-group row">
                            <label class="col-sm-3 col-from-label" for="name">{{translate('Name')}}</label>
                            <div class="col-sm-9">
                                <input type="text" placeholder="{{translate('Name')}}" id="name" name="name" value="{{ $driver->user->name }}"  class="form-control" required autocomplete>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label class="col-sm-3 col-from-label" for="email">{{translate('Email')}}</label>
                            <div class="col-sm-9">
                                <input type="text" placeholder="{{translate('Email')}}" value="{{ $driver->user->email }}" id="email" name="email" class="form-control" autocomplete>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label class="col-sm-3 col-from-label" for="mobile">{{translate('Phone')}}</label>
                            <div class="col-sm-9">
                                <input type="text"  value="{{ $driver->user->phone }}" id="mobile" name="mobile" class="form-control"  placeholder="eg +2567xxxxxxxx"
                                pattern="(^(\+256){1}[7]{1}[7|8]{1}[0-9]{6}[0-9])|(^(\+256){1}[7]{1}[0|3|5]{1}[0-9]{6}[0-9])|(^(\+254)[7]{1}(([0129]{1}[0-9]{1})|([4]{1}[0123568])|([5]{1}[789])|([6]{1}[89]))[0-9]{6})"
                                required autocomplete>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label class="col-sm-3 col-from-label" for="password">{{translate('Password')}}</label>
                            <div class="col-sm-9">
                                <input type="password" placeholder="{{translate('Password')}}" id="password" name="password" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label class="col-sm-3 col-from-label" for="id_type">{{translate('ID Type')}}</label>
                            <div class="col-sm-9">
                                <select name="id_type" required class="form-control aiz-selectpicker">
                                    <option value="National ID" @php if($driver->identity_type == 'National ID') echo "selected"; @endphp>National ID</option>
                                    <option value="Passport" @php if($driver->identity_type == 'Passport') echo "selected"; @endphp>Passport</option>
                                    <option value="Driving Permit" @php if($driver->identity_type == 'Driving Permit') echo "selected"; @endphp>Driving Permit</option>
                                 </select>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label class="col-sm-3 col-from-label" for="id_number">{{translate('ID Number')}}</label>
                            <div class="col-sm-9">
                                <input type="text" value="{{ $driver->identity_document_number }}" placeholder="{{translate('ID Number')}}" id="id_number" name="id_number" class="form-control" required autocomplete>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label class="col-sm-3 col-from-label" for="vehicle_type">{{translate('Vehicle Type')}}</label>
                            <div class="col-sm-9">
                                <select name="vehicle_type" required class="form-control aiz-selectpicker">
                                   <option value="Car" @php if($driver->vehicle_type == 'Car') echo "selected"; @endphp>Car</option>
                                   <option value="MotorCycle" @php if($driver->vehicle_type == 'MotorCycle') echo "selected"; @endphp>MotorCycle</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label class="col-sm-3 col-from-label" for="vin">{{translate('Vehicle Number')}}</label>
                            <div class="col-sm-9">
                                <input type="text" value="{{ $driver->vehicle_identification_number }}" placeholder="{{translate('Vehicle Number')}}" id="vin" name="vin" class="form-control" required autocomplete>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label class="col-sm-3 col-from-label" for="vin">{{translate('Bank Payment')}}</label>
                            <div class="col-sm-9">
                                <label class="aiz-switch aiz-switch-success mb-0">
                                    <input type="checkbox" name="bank_payment_status" class="form-control demo-sw" @php if($driver->bank_payment_status == 1) echo "checked"; @endphp >
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label class="col-sm-3 col-from-label" for="bank_name">{{translate('Bank Name')}}</label>
                            <div class="col-sm-9">
                                <input type="text" value="{{ $driver->bank_name }}" placeholder="{{translate('Bank Name')}}" id="bank_name" name="bank_name" class="form-control" required autocomplete>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label class="col-sm-3 col-from-label" for="bank_acc_name">{{translate('Account Name')}}</label>
                            <div class="col-sm-9">
                                <input type="text" value="{{ $driver->bank_acc_name }}" placeholder="{{translate('Account Name')}}" id="bank_acc_name" name="bank_acc_name" class="form-control" required autocomplete>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label class="col-sm-3 col-from-label" for="bank_acc_number">{{translate('Account Number')}}</label>
                            <div class="col-sm-9">
                                <input type="text" value="{{ $driver->bank_acc_number }}" placeholder="{{translate('Account Number')}}" id="bank_acc_number" name="bank_acc_number" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label class="col-sm-3 col-from-label" for="mobile_payment_status">{{translate('Mobile Money')}}</label>
                            <div class="col-sm-9">
                                <label class="aiz-switch aiz-switch-success mb-0">
                                    <input type="checkbox" name="mobile_payment_status" class="form-control demo-sw" @php if($driver->mobile_payment_status == 1) echo "checked"; @endphp>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label class="col-sm-3 col-from-label" for="mobile_payment_number">{{translate('Mobile Money Number')}}</label>
                            <div class="col-sm-9">
                                <input type="text" value="{{ $driver->mobile_payment_number }}" placeholder="{{translate('Mobile Money Number')}}" id="mobile_payment_number" name="mobile_payment_number" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label class="col-sm-3 col-from-label" for="mobile_payment_name">{{translate('Mobile Money Name')}}</label>
                            <div class="col-sm-9">
                                <input type="text" value="{{ $driver->mobile_payment_name }}" placeholder="{{translate('Mobile Money Name')}}" id="mobile_payment_name" name="mobile_payment_name" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group row">
                            <label class="col-sm-3 col-from-label" for="cod_status">{{translate('Cash On Delivery')}}</label>
                            <div class="col-sm-9">
                                <label class="aiz-switch aiz-switch-success mb-0">
                                    <input type="checkbox" name="cod_status" class="form-control demo-sw" @php if($driver->cash_on_delivery_status == 1) echo "checked"; @endphp>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-0 text-right">
                    <button type="submit" class="btn btn-sm btn-primary">{{translate('Save')}}</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
