@extends('backend.layouts.app')

@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="row align-items-center">
		<div class="col-md-6">
			<h1 class="h3">{{translate('All Drivers')}}</h1>
		</div>
		<div class="col-md-6 text-md-right">
			<a href="{{ route('drivers.create') }}" class="btn btn-circle btn-info">
				<span>{{translate('Add New Driver')}}</span>
			</a>
		</div>
	</div>
</div>

<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">{{translate('Drivers')}}</h5>
    </div>
    <div class="card-body">
        <table class="table aiz-table mb-0">
            <thead>
                <tr>
                    {{-- <th width="10%">#</th> --}}
                    @if (Auth::user()->user_type == 'admin')
                        <th>{{translate('Company')}}</th>
                    @endif
                    <th>{{translate('Name')}}</th>
                    <th data-breakpoints="md">{{translate('Email')}}</th>
                    <th data-breakpoints="md">{{translate('Phone')}}</th>
                    <th data-breakpoints="md">{{translate('ID No.')}}</th>
                    <th data-breakpoints="md">{{translate('VIN No.')}}</th>
                    <th data-breakpoints="md">{{translate('Vehicle Type')}}</th>
                    <th>{{translate('Admin to pay')}}</th>
                    @if (Auth::user()->user_type != 'admin')
                        <th data-breakpoints="md" width="10%">{{translate('Options')}}</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach($drivers as $key => $driver)
                    @if($driver->user != null)
                        <tr>
                            {{-- <td>{{ ($key+1) + ($drivers->currentPage() - 1)*$drivers->perPage() }}</td> --}}
                            @if (Auth::user()->user_type == 'admin')
                                <td>{{$driver->company->company_name}}</td>
                            @endif
                            <td>{{$driver->user->name}}</td>
                            <td>{{$driver->user->email}}</td>
                            <td>{{$driver->user->phone}}</td>

                            <td>{{$driver->identity_document_number}}</td>
                            <td>{{$driver->vehicle_identification_number}}</td>
                            <td>{{$driver->vehicle_type}}</td>
                            <td>{{single_price($driver->admin_to_pay)}}</td>
                            @if (Auth::user()->user_type != 'admin')
                                <td class="text-right">
                                    <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{route('drivers.edit', encrypt($driver->id))}}" title="{{ translate('Edit') }}">
                                        <i class="las la-edit"></i>
                                    </a>
                                    <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" data-href="{{route('drivers.destroy', $driver->id)}}" title="{{ translate('Delete') }}">
                                        <i class="las la-trash"></i>
                                    </a>
                                </td>
                            @endif
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
        <div class="aiz-pagination">
            {{ $drivers->appends(request()->input())->links() }}
        </div>
    </div>
</div>

@endsection

@section('modal')
    @include('modals.delete_modal')
@endsection
