@extends('backend.layouts.app') @section('content')

<div class="px-2px">
    <div class="col-lg-11 mx-auto">
        <form class="form-horizontal" role="form" action="{{ route('zones.update', $zone->id) }}" method="POST" enctype="multipart/form-data">
              <div class="card">
                <div class="card-header"> 
                    <div class="col-md-6 text-center text-lg-left">
                        <h5 class="h6">{{ translate('Shipping Zone Information') }}</h5>
                    </div>
                    <div class="col-md-6 text-center text-lg-right"> 
                        <button type="submit" class="btn btn-sm btn-primary">Save</button>
                        </a>
                    </div> 
                </div>
                <div class="card-body">
                    <input name="_method" type="hidden" value="PATCH" />
                    @csrf
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <div class="row">
                                <label class="col-sm-4 col-from-label" for="name">{{ translate('Zone Name')}}</label>
                                <div class="col-sm-8">
                                    <input type="text" placeholder="{{ translate('Zone Name')}}" id="name" name="name" class="form-control" value="{{$zone->name}}" required />
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <div class="row">
                                <label class="col-sm-4 control-label" for="country">{{ translate('Origin')}}</label>
                                <div class="col-sm-8">
                                    <select class="form-control aiz-selectpicker" data-live-search="true" name="country" id="country" required>
                                        @foreach (\App\Country::where('status', 1)->get() as $key => $country)
                                            <option value="{{ $country->code }}" @if($country->code == $zone->location_code) selected @endif>{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <div class="row">
                                <label class="col-sm-4 control-label" for="regions">{{ translate('Destination')}}</label>
                                <div class="col-sm-8">
                                    <select class="form-control aiz-selectpicker" data-live-search="true" data-selected-text-format="count > 3" name="regions[]" id="regions" multiple>
                                  		@foreach (\App\Country::where('status', 1)->get() as $key => $country)
                                                    <option value="{{'{"code":"'.$country->code .'","type":"country","id":'. $country->id.'}'}}" 
                                                        @if(\App\ShippingZoneLocation::where('location_code','{"code":"'.$country->code .'","type":"country","id":'. $country->id.'}' )->where('zone_id', $zone->id)->exists()) 
                                                        selected  @endif> {{ $country->name }}</option>                                        
                                                    @foreach ($country->states as $state )
                                                        <option value="{{'{"code":"'.$country->code .':'. $state->code .'","type":"state","id":'. $state->id.'}'}}"
                                                            @if(\App\ShippingZoneLocation::where('location_code', '{"code":"'.$country->code .':'. $state->code .'","type":"state","id":'. $state->id.'}' )->where('zone_id', $zone->id)->exists()) 
                                                            selected 
                                                            @endif
                                                            >{{ $country->name.' '. $state->name }}</option>
                                                    @endforeach
                                                @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <div class="row">
                                <label class="col-sm-4 control-label" for="order">{{ translate('Zone Order')}}</label>
                                <div class="col-sm-8">
                                    <input type="number" placeholder="{{ translate('Zone Order')}}" id="order" name="order" value="{{$zone->order}}" class="form-control" required />
                                </div>
                            </div>
                        </div>
                    </div> 
            
                </div>
              </div>
        </form>             

        <div class="card">
            <div class="card-header row gutters-5">
                <div class="col-md-6 text-center text-lg-left">
                    <h5 class="h6">{{ translate('Shipping Method(s)') }}</h5>
                </div>
                <div class="col-md-6 text-center text-lg-right"> 
                      <a onclick="show_add_shopping_method('{{$zone->id}}');" href="#" class="btn btn-circle btn-primary">
                        <span>{{translate('Add Shipping Method')}}</span>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <table class="table aiz-table mb-0">
                        <thead>
                            <tr>
                                <th>{{ translate('#')}}</th>
                                <th>{{ translate('Name')}}</th>                                
                                <th>{{ translate('Company')}}</th>
                                <th data-breakpoints="md">{{ translate('Description')}}</th>
                                <th>{{ translate('Enabled')}}</th>
                                <th data-breakpoints="md" width="10%">{{ translate('Options')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($zone->methods as $method)
                            <tr>
                                <td>{{$method->order}}</td>
                                <td>{{$method->name}}</td> 
                                <td>{{ \App\ShippingCompany::findOrFail($method->company_id)->company_name}}</td>
                                <td>{{ $method->description }}</td>
                                <td>
                                    <label class="aiz-switch aiz-switch-success mb-0">
                                        <input onchange="update_zone_status(this)" value="{{ $method->id }}" type="checkbox" <?php if($method->status == 1) echo "checked";?> >
                                        <span class="slider round"></span>
                                    </label>
                                </td>                        
                                <td class="text-right">
                                    <a onclick="show_edit_shopping_method('{{$method->id}}');" class="btn btn-soft-primary btn-icon btn-circle btn-sm" title="{{ translate('Edit') }}">
                                        <i class="las la-edit"></i>
                                    </a>
                                    <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" data-href="{{route('shipping_methods.destroy', $method->id)}}" title="{{ translate('Delete') }}">
                                        <i class="las la-trash"></i>
                                    </a>
                                </td> 
                            </tr>
                            @endforeach
                        </tbody>
                </table>
            </div>
        </div>

        <div class="modal fade" id="shipping_method" >
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document" id="shipping_method" tabindex="-1" role="dialog">
                <div class="modal-content" id="modal-content">

                </div>
            </div>
        </div> 

        
    </div>
</div> 
@endsection 

@section('modal')
    @include('modals.delete_modal')
@endsection 

@section('script')
<script type="text/javascript">

    function show_add_shopping_method(id){
        $.post('{{ route('shipping_methods.create') }}', { _token: '{{ @csrf_token() }}', id:id},function(data){
            $('#shipping_method #modal-content').html(null);
            $('#shipping_method #modal-content').html(data);
            $('#shipping_method').modal('show', {backdrop: 'static'});
        });
    }

    function show_edit_shopping_method(id){

        $.post('{{ route('shipping_methods.edit') }}', { _token: '{{ @csrf_token() }}', id:id},function(data){
            $('#shipping_method #modal-content').html(null);
            $('#shipping_method #modal-content').html(data);
            $('#shipping_method').modal('show', {backdrop: 'static'});
        });
    }
       function update_approved(el){
        if(el.checked){
            var status = 1;
        }
        else{
            var status = 0;
        }

    }

    function update_zone_status(el){
        if(el.checked){
            var status = 1;
        }
        else{
            var status = 0;
        }

        $.post('{{ route('zones.update_status') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
            if(data == 1){
                AIZ.plugins.notify('success', '{{ translate('Zone Status updated successfully') }}');
            }
            else{
                AIZ.plugins.notify('danger', '{{ translate('Something went wrong') }}');
            }
        });
    }

    $(document).on('change','#method_id', function() {
        var option = $(this).find("option:selected");
            console.log(option.val());
            if(option.val() == 'flat-rate'){
                $('.flat-rate').show();
                $('.flat-rate :input').prop('required',true);
                $('.free-shipping').hide();
                $('.free-shipping :input').prop('required',false);
            }
            else if(option.val() == 'free-shipping'){
                $('.flat-rate').hide();
                $('.flat-rate :input').prop('required',false);
                $('.free-shipping').show();
                $('.free-shipping :input').prop('required',true);
            }
            else{
                $('.flat-rate').hide();
                $('.flat-rate :input').prop('required',false);
                $('.free-shipping').hide();
                $('.free-shipping :input').prop('required',false);
            }
    })

    function createMethod() {

        var frm = $('#new_shipping_method_form').serializeArray();
        console.log(frm);
        var frmObject = {};
        $.each(frm, function(i, v) { frmObject[v.name] = v.value; });
        console.log(frmObject);

        $.ajax({
           type:"POST",
           url: '{{ route('shipping_methods.store') }}',
           contentType: "application/json",
           data: JSON.stringify(frmObject),
           success: function(data){
            window.location.href = data.referer;
           }
       });

    }

    function updateMethod(id) {
        var frm = $('#update_shipping_method_form').serializeArray();
        console.log(frm);
        var frmObject = {};
        $.each(frm, function(i, v) { frmObject[v.name] = v.value; });
        console.log(frmObject);

        $.ajax({
           type:"POST",
           url: '{{ route('shipping_methods.update','') }}/'+ id ,
           contentType: "application/json",
           data: JSON.stringify(frmObject),
           success: function(data){
            window.location.href = data.referer;
           }
       });

    }
</script>
@endsection
