@extends('backend.layouts.app')

@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="row align-items-center">
		<div class="col-md-6">
			<h1 class="h3">{{translate('Shipping Zone')}}</h1>
		</div>
		<div class="col-md-6 text-md-right">
			<a href="{{ route('zones.create')}}" class="btn btn-circle btn-primary">
				<span>{{translate('Add Zone')}}</span>
			</a>
		</div>
	</div>
</div> 

<div class="card">
    <div class="card-header row gutters-5">
        <div class="col text-center text-md-left">
            <h5 class="mb-md-0 h6">{{ translate('Company') }}</h5>
        </div>
        <div class="col-md-4">
            <form class="" id="sort_currencies" action="" method="GET">
                <div class="input-group input-group-sm">
                    <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="{{ translate('Type name & Enter') }}">
                </div>
            </form>
        </div>
    </div>
    <div class="card-body">
        <table class="table aiz-table mb-0">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{ translate('Name')}}</th>
                    <th>{{ translate('Country')}}</th>
                    <th data-breakpoints="md" width="40%">{{ translate('Region(s)')}}</th>
                    <th data-breakpoints="md">{{ translate('Shipping Method(s)')}}</th>
                    <th data-breakpoints="md">{{ translate('Status')}}</th>
                    <th>{{ translate('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($zones as $key => $zone)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$zone->name}}</td>
                        <td>{{\App\Country::where('code', $zone->location_code )->first()->name}}</td>
                        <td>
                            @foreach($zone->locations as $key => $location)
                               <strong> 
                                    @if($location->type=='state')                                    
                                      {{(($key > 0)?', ':'').\App\State::findOrFail((json_decode($location->location_code, true))['id'])->name }}                                    
                                    @else
                                      {{(($key > 0)?', ':'').\App\Country::findOrFail((json_decode($location->location_code, true))['id'])->name }}
                                    @endif
                               </strong> 
                            @endforeach
                        </td>
                        <td>
                            @foreach($zone->methods as $key => $method) 
                                   {{  (($key > 0)?', ':'').$method->name }}
                            @endforeach
                        </td>
                         <td>
                            <label class="aiz-switch aiz-switch-success mb-0">
                                <input onchange="update_zone_status(this)" value="{{ $zone->id }}" type="checkbox" <?php if($zone->status == 1) echo "checked";?> >
                                <span class="slider round"></span>
                            </label>
                        </td>                        
                        <td class="text-right">
                            <a href="{{route('zones.edit', encrypt($zone->id))}}" class="btn btn-soft-primary btn-icon btn-circle btn-sm" title="{{ translate('Edit') }}">
                                   <i class="las la-edit"></i>
                            </a>
                            <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" data-href="{{route('zones.destroy', $zone->id)}}" title="{{ translate('Delete') }}">
                                <i class="las la-trash"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="aiz-pagination">
            {{ $zones->appends(request()->input())->links() }}
        </div>
    </div>
</div>

@endsection

@section('modal')
    @include('modals.delete_modal')
@endsection 

@section('script')
    <script type="text/javascript"> 

        function update_zone_status(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }

            $.post('{{ route('zones.update_status') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    AIZ.plugins.notify('success', '{{ translate('Zone Status updated successfully') }}');
                }
                else{
                    AIZ.plugins.notify('danger', '{{ translate('Something went wrong') }}');
                }
            });
        } 
    </script>
@endsection