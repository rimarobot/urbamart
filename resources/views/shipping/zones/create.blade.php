@extends('backend.layouts.app')

@section('content')


<div class="px-15px px-lg-25px">
        <div class="col-lg-10 mx-auto">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0 h6">Shipping Zone Information</h5>
                </div>
            <div class="card-body">
                <form class="form-horizontal" role="form" action="{{ route('zones.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="panel-body">
                        <div class="form-group row">
                            <label class="col-sm-4 control-label" for="name">{{ translate('Zone Name')}}</label>
                            <div class="col-sm-8">
                                <input type="text" placeholder="{{ translate('Zone Name')}}" id="name" name="name" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 control-label" for="country">{{ translate('Origin')}}</label>
                            <div class="col-sm-8">
                                <select class="form-control aiz-selectpicker" data-live-search="true" name="country" id="country"  required>
                                    <option value="">Select Country of Origin</option>
                                    @foreach (\App\Country::where('status', 1)->get() as $key => $country)
                                        <option value="{{ $country->code }}">{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> 
                        <div class="form-group row">
                            <label class="col-sm-4 control-label" for="regions">{{ translate('Destination')}}</label>
                            <div class="col-sm-8">
                                <select class="form-control aiz-selectpicker" data-live-search="true"  data-selected-text-format="count > 3" name="regions[]" id="regions" multiple>
                                    @foreach (\App\Country::where('status', 1)->get() as $key => $country)
                                        <option value="{{'{"code":"'.$country->code .'","type":"country","id":'. $country->id.'}'}}">{{ $country->name }}</option>                                        
                                        @foreach ($country->states as $state )
                                            <option value="{{'{"code":"'.$country->code .':'. $state->code .'","type":"state","id":'. $state->id.'}'}}">{{ $country->name.' '. $state->name }}</option>
                                        @endforeach
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 control-label" for="order">{{ translate('Zone Order')}}</label>
                            <div class="col-sm-8">
                                <input type="number" placeholder="{{ translate('Zone Order')}}" id="order" name="order" class="form-control" required>
                            </div>
                        </div>                 
                    </div>  
                    <div class="form-group mb-0 text-right">
                            <button type="submit" class="btn btn-sm btn-primary">Save</button>
                        </div>
                </form> 
            </div> 
            </div>
        </div>
</div> 
 
@endsection
 